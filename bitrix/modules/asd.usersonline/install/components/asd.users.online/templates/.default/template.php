<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}
?>

<b><?= GetMessage('ASD_ONLINE_ALL')?>:</b>
<?= GetMessage('ASD_ONLINE_REG')?> &mdash;

<?foreach ($arResult['USERS'] as $arItem):?>
	<?if (strlen($arItem['PATH'])):?>
		<a class="feed-com-name" id="anchor_<?=$arItem['ID']?>" href="<?= $arItem['PATH']?>"><?=$arItem['NAME']?></a>,
		<?= $arItem['POPUP'];?>
	<?else:?>
		<?= $arItem['NAME']?>,
	<?endif;?>
<?endforeach;?>
<?if (empty($arResult['USERS'])) echo GetMessage('ASD_NOONE') . ', '?>
<?= GetMessage('ASD_ONLINE_GUESTS')?> &mdash; <?= $arResult['GUESTS']>0 ? $arResult['GUESTS'] : GetMessage('ASD_NOONE')?>.

<?if ($arParams['COUNTRY']=='Y' && !empty($arResult['COUNTRIES'])):?>
<br clear="all"/><br/>
<b><?= GetMessage('ASD_COUNTRY')?>:</b>
<ul class="country-people">
	<?
	$cc == 0;
	foreach ($arResult['COUNTRIES'] as $cntr => $count):?>
		<?
		if (
				($arParams['COUNTRY_MAX'] > 0 && $cc >= $arParams['COUNTRY_MAX']) ||
				$count < $arParams['COUNTRY_MIN_COUNT'] ||
				!file_exists($_SERVER['DOCUMENT_ROOT'].'/bitrix/images/statistic/flags/'.strtolower($cntr).'.gif')
		) {
			continue;
		}
		$cc++;
		?>
		<li><img src="/bitrix/images/statistic/flags/<?= strtolower($cntr)?>.gif" title="<?= $cntr?>" width="18" height="12" />(<?= $count?>)</li>
	<?endforeach;?>
</ul>
<?= GetMessage('ASD_AND_OTHER')?>
<?endif;?>
