<?php
$MESS ['ASD_CMP_PARAM_USER_PATH'] = 'Путь до пользователя';
$MESS ['ASD_CMP_PARAM_POPUP'] = 'Подключить всплывающую информационную карточку';
$MESS ['ASD_CMP_PARAM_COUNTRY'] = 'Выводить страны';
$MESS ['ASD_CMP_PARAM_COUNTRY_MAX'] = 'Максимальное количество стран';
$MESS ['ASD_CMP_PARAM_COUNTRY_MIN_COUNT'] = 'Минимально допустимый счетчик для страны';