<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();


$arComponentParameters = array(
	'GROUPS' => array(
		'DATA_SORT' => array(
			'NAME' => GetMessage('ASD_CMP_PARAM_SORT_BLOCK'),
		),
	),
	'PARAMETERS' => array(
		'USER_PATH' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('ASD_CMP_PARAM_USER_PATH'),
			'TYPE' => 'STRING',
			'DEFAULT' => '/users/#ID#/'
		),
		'POPUP' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('ASD_CMP_PARAM_POPUP'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N'
		),
		'COUNTRY' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('ASD_CMP_PARAM_COUNTRY'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
			'REFRESH' => 'Y'
		),
		'COUNTRY_MAX' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('ASD_CMP_PARAM_COUNTRY_MAX'),
			'TYPE' => 'STRING',
			'DEFAULT' => '10'
		),
		'COUNTRY_MIN_COUNT' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('ASD_CMP_PARAM_COUNTRY_MIN_COUNT'),
			'TYPE' => 'STRING',
			'DEFAULT' => ''
		),
		'CACHE_TIME' => array('DEFAULT' => 300),
	),
);

if ($arCurrentValues['COUNTRY'] != 'Y') {
	unset($arComponentParameters['PARAMETERS']['COUNTRY_MAX']);
	unset($arComponentParameters['PARAMETERS']['COUNTRY_MIN_COUNT']);
}