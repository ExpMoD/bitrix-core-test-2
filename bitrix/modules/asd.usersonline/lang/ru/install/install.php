<?
$MESS ['usersonline_module_name'] = "Пользователи онлайн";
$MESS ['usersonline_module_description'] = "Список пользователей, находящихся онлайн на сайте.";
$MESS ['usersonline_install_title'] = "Установка модуля \"Пользователи онлайн\"";
$MESS ['usersonline_uninstall_title'] = "Деинсталляция модуля \"Пользователи онлайн\"";
$MESS ['usersonline_partner_name'] = "Долганин Антон";
$MESS ['need_module_statistic'] = "Для работы компонента необходим установленный модуль Веб-аналитики";
?>