<?
$MESS["T_NEWS_NEWS_NA"] = "Раздел не найден.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль Информационных блоков не установлен";
$MESS["PRICE_TITLE"] = "Прайс-лист от ";
$MESS["NAME"] = "Название";
$MESS["PREVIEW_TEXT"] = "Описание";
$MESS["DETAIL_TEXT"] = "Детальное описание";
$MESS["PRICE"] = "Цена";
$MESS["CURRENCY"] = "Валюта";
$MESS["SECTION_ID"] = "ID Раздела";
$MESS["SECTION_NAME"] = "Название раздела";
$MESS["PREVIEW_PICTURE"] = "Картинка для анонса";
$MESS["DETAIL_PICTURE"] = "Детальная картинка";
$MESS["EXCEPTION_ERROR"] = "Произошла ошибка. Проверьте параметры компонента.";

$MESS["PREVIEW_PICTURE"] = "Картинка для анонса";
$MESS["DETAIL_PICTURE"] = "Детальная картинка";
?>