<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="generation">
	<? if ($arResult['RESULT'] == 'Y') { ?>
		<span class="success">
			<?=GetMessage('GENERATE_SUCCESS')?>
			<br/>
			<a href="<?=$arResult['PATH'].$arResult['FILE_NAME'].'.'.$arResult['FILE_EXPANSION']?>">
				<?=GetMessage('GENERATE_DOWNLOAD_LINK')?>
			</a>
		</span>
	<? } ?>
	<form method='POST' action=''>
		<input type="hidden" name="generate" value="Y"/>
		<button type="submit" class="generate_btn"><?=GetMessage('GENERATE_BUTTON')?></button>
	</form>
</div>