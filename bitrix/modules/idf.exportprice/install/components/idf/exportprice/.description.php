<? 
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
		"NAME" => GetMessage("NAME"),
		"DESCRIPTION" => GetMessage("DESC"),
		"ICON" => "images/price.gif",
		"CACHE_PATH" => "Y",
		"SORT" => 10,
		"PATH" => array(
				"ID" => "idf",
				"NAME" => "IDF Company",
				"CHILD" => array(
						"ID" => "idf-catalog",
						"NAME" => GetMessage("NAME_CHILD"),
						"SORT" => 10,
				)
		),
);
?>