<?php
$module_id = "tatonimus.reviews";
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($MODULE_RIGHT >= "R") {
    $modulStatus = CModule::IncludeModuleEx($module_id);
    CModule::IncludeModule('sale');

    $arSaleStatus = array();
    $rsStatus = CSaleStatus::GetList(array('SORT' => 'ASC'), array('LID' => LANGUAGE_ID));
    while ($arStatus = $rsStatus->Fetch()) {
        $arSaleStatus[$arStatus['ID']] = $arStatus['NAME'];
    }

    $arAllOptions = array(
        array(
            'status_id',
            GetMessage('TRVWS_STATUS_ID'),
            COption::GetOptionString($module_id, 'status_id'),
            array('selectbox', $arSaleStatus)
        ),
        array(
            'time_reviews',
            GetMessage('TRVWS_TIME_'),
            COption::GetOptionString($module_id, 'time_reviews_payed'),
            array('text', 3)
        ),
        array(
            'fields',
            GetMessage('TFOLI_FIELDS_DESCRIPTION'),
            COption::GetOptionString($module_id, 'fields'),
            array('textarea', 3, 30)
        ),
    );


    $aTabs = array(
        array("DIV" => "edit1", "TAB" => GetMessage("TRVWS_TAB_PARAM"), "ICON" => "replica_settings", "TITLE" => GetMessage("TRVWS_TAB_TITLE_PARAM")),
        array("DIV" => "edit2", "TAB" => GetMessage("TRVWS_TAB_STAT"), "ICON" => "replica_settings", "TITLE" => GetMessage("TRVWS_TAB_TITLE_STAT")),
    );

    $tabControl = new CAdminTabControl("tabControl", $aTabs);

    if ($_SERVER['REQUEST_METHOD'] == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && $MODULE_RIGHT >= "W" && check_bitrix_sessid()) {
        if (strlen($RestoreDefaults) > 0) {
            COption::RemoveOption($module_id);
        } else {
            COption::RemoveOption($module_id, "sid");

            foreach ($arAllOptions as $arOption) {
                $name = $arOption[0];
                $val = $_REQUEST[$name];
                if ($arOption[2][0] == "checkbox" && $val != "Y")
                    $val = "N";
                COption::SetOptionString($module_id, $name, $val, $arOption[1]);
            }
        }
    }


?>
<form method="post" action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<?= LANGUAGE_ID ?>">
<?
    if ($modulStatus == MODULE_DEMO_EXPIRED) {
        CAdminMessage::ShowMessage(GetMessage('TRVWS_DEMO_EXPIRED'));
    }
    if ($modulStatus == MODULE_DEMO) {
        CAdminMessage::ShowMessage(GetMessage('TRVWS_DEMO'));
    }

    $tabControl->Begin();
    $tabControl->BeginNextTab();


    $agentName = 'CTatonimusReviews::AgentRefresh();';
    $rsAgent = CAgent::GetList(array(), array('MODULE_ID' => $module_id, 'NAME' => $agentName));
    if ($rsAgent->SelectedRowsCount() > 0) {
        while ($arAgent = $rsAgent->Fetch()) {
?>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l"><?=GetMessage('TRVWS_AGENT')?></td>
        <td width="50%" class="adm-detail-content-cell-r"><a href="agent_edit.php?ID=<?=$arAgent['ID']?>&lang=<?=LANG?>"<?=$arAgent['ACTIVE'] != 'Y' ? ' style="color:red;"' : ''?>><?=$arAgent['ID']?></a> (<?=$arAgent['ACTIVE'] == 'Y' ? GetMessage('TRVWS_ACTIVE') : GetMessage('TRVWS_INACTIVE') ?>)</td>
    </tr>
<?
        }
    } else {
?>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l"><?=GetMessage('TRVWS_AGENT')?></td>
        <td width="50%" class="adm-detail-content-cell-r"><a href="/bitrix/admin/agent_edit.php?lang=<?=LANG?>" style="color:red;"><?=GetMessage('TRVWS_AGENT_EMPTY')?> &laquo;<?=$agentName?>&raquo;</a></td>
    </tr>
<?
    }


    __AdmSettingsDrawRow($module_id, $arAllOptions[0]);
    __AdmSettingsDrawRow($module_id, $arAllOptions[1]);

    $type = 'TATONIMUS_REVIEWS';
    $rsEventType = CEventType::GetList(array('TYPE_ID' => $type));
    $rsEventMessage = CEventMessage::GetList($by = 'id', $order = 'asc', array('TYPE' => $type));
    if ($rsEventType->SelectedRowsCount() == 0) {
        $status = GetMessage('TRVWS_NON_EVENT') . ' (' . $type . ')';
    } elseif ($rsEventMessage->SelectedRowsCount() == 0) {
        $status = GetMessage('TRVWS_EVENT_NON_MESSAGE');
    } else {
        $status = GetMessage('TRVWS_EVENT_MESSAGE') . ' (' . $rsEventMessage->SelectedRowsCount() . ')';
    }

?>
    <tr>
        <td width="50%" class="adm-detail-content-cell-l"><?=GetMessage('TRVWS_EVENT_SID')?></td>
        <td width="50%" class="adm-detail-content-cell-r"><a href="type_edit.php?EVENT_NAME=<?=$type?>"><?=$status?></td>
    </tr>
<?


?>
    <tr class="heading">
		<td colspan="2"><b><?=GetMessage('TRVWS_FIELDS')?></b></td>
	</tr>
<?
    __AdmSettingsDrawRow($module_id, $arAllOptions[2]);


    //b_even result
    global $DB;
    $rsEvent = $DB->Query('SELECT `ID`, `EVENT_NAME`, `DATE_INSERT`, `DATE_EXEC`, `SUCCESS_EXEC` '
        . 'FROM b_event '
        . 'WHERE `EVENT_NAME` = "TATONIMUS_REVIEWS"'
    );
    $arDateEvents = array();
    while ($arEvent = $rsEvent->Fetch()) {
        $date = date('Y-m-d', strtotime($arEvent['DATE_INSERT']));
        $arDateEvents[$date]['COUNT']++;
        if (!empty($arEvent['DATE_EXEC']) && $arEvent['SUCCESS_EXEC'] != 'N') {
            $arDateEvents[$date]['SUCCESS']++;
        }
    }
    krsort($arDateEvents);

    $tabControl->BeginNextTab();
?>
<tr><td colspan="2">
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="internal">
	<tr class="heading">
		<td valign="top"><?=GetMessage("TRVWS_TAB_STAT_COLS_DATE")?></td>
		<td valign="top"><?=GetMessage("TRVWS_TAB_STAT_COLS_SUCCESS") ?></td>
		<td valign="top"><?=GetMessage("TRVWS_TAB_STAT_COLS_COUNT")?></td>
	</tr>
<?
    foreach ($arDateEvents as $date => $arEvent) {
?>
    <tr>
        <td><?=date('d.m.Y', strtotime($date))?></td>
        <td><?=$arEvent['SUCCESS']?></td>
        <td><?=$arEvent['COUNT']?></td>
    </tr>
<?
    }
?>
</table>
</td></tr>
<?


    $tabControl->Buttons();
?>
    <input <?= $MODULE_RIGHT < "W" ? "disabled" : "" ?> type="submit" name="Update" value="<?= GetMessage("MAIN_SAVE") ?>" title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>">
    <input <?= $MODULE_RIGHT < "W" ? "disabled" : "" ?> type="submit" name="Apply" value="<?= GetMessage("MAIN_OPT_APPLY") ?>" title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>">
<?
    if (strlen($_REQUEST["back_url_settings"]) > 0) {
?>
    <input type="button" name="Cancel" value="<?= GetMessage("MAIN_OPT_CANCEL") ?>" title="<?= GetMessage("MAIN_OPT_CANCEL_TITLE") ?>" onclick="window.location='<?= htmlspecialchars(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
    <input type="hidden" name="back_url_settings" value="<?= htmlspecialchars($_REQUEST["back_url_settings"]) ?>">
<?
    }
?>
    <input <?= $MODULE_RIGHT < "W" ? "disabled" : "" ?> type="submit" name="RestoreDefaults" title="<?= GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>" OnClick="confirm('<?= AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')" value="<?= GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>
<? } else { ?>
    <?= CAdminMessage::ShowMessage(GetMessage('NO_RIGHTS_FOR_VIEWING')); ?>
<? } ?>