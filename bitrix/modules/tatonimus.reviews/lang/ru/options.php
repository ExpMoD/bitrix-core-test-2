<?
$MESS ["TRVWS_DEMO_EXPIRED"] = "Срок работы демо-режима модуля истек";
$MESS ["TRVWS_DEMO"] = "Модуль работает в демо-режиме";

$MESS ["TRVWS_TAB_PARAM"] = "Настройки";
$MESS ["TRVWS_TAB_TITLE_PARAM"] = "Основные параметры";
$MESS ["TRVWS_TAB_STAT"] = "Статистика";
$MESS ["TRVWS_TAB_TITLE_STAT"] = "Статистика отправки";
$MESS ["TRVWS_TAB_RIGHTS"] = "Доступ";
$MESS ["TRVWS_TAB_TITLE_RIGHTS"] = "Настройка прав доступа";
$MESS ["TRVWS_TAB_STAT_COLS_DATE"] = "Дата";
$MESS ["TRVWS_TAB_STAT_COLS_EVENT"] = "Событие";
$MESS ["TRVWS_TAB_STAT_COLS_SUCCESS"] = "Успешно отправлено";
$MESS ["TRVWS_TAB_STAT_COLS_COUNT"] = "Добавлено в очередь";
$MESS ["TRVWS_TAB_STAT_PAYED"] = 'Неоплаченный заказ';

$MESS ['TRVWS_AGENT'] = "ИД агента";
$MESS ['TRVWS_AGENT_EMPTY'] = "создайте агента с функцией";
$MESS ['TRVWS_ACTIVE'] = "Активен";
$MESS ['TRVWS_INACTIVE'] = "Неактивен";


$MESS ['TRVWS_STATUS_ID'] = "Статус заказа";
$MESS ['TRVWS_TIME_'] = "Через сколько часов отправлять <br> (минимум 1 час)";
$MESS ['TRVWS_EVENT_SID'] = "Почтовое событие";
$MESS ['TFOLI_FIELDS_DESCRIPTION'] = "Укажите поля пользователя через запятую";

$MESS ['TRVWS_FIELDS'] = "Дополнительные поля пользователя, передаваемые в письмо";
$MESS ['TRVWS_FIELDS_DESCRIPTION'] = "Укажите поля пользователя через запятую";

$MESS ['TRVWS_NON_EVENT'] = "Нет почтового события";
$MESS ['TRVWS_EVENT_NON_MESSAGE'] = "Нет почтовых шаблонов";
$MESS ['TRVWS_EVENT_MESSAGE'] = "Есть почтовое событие и шаблоны";



?>