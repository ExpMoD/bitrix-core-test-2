<?
$MESS ['TRVWS_COMPANY_NAME'] = "tatonimus";
$MESS ['TRVWS_PARTNER_URI'] = "http://www.tatonimus.ru";
$MESS ['TRVWS_INSTALL_NAME'] = "Увеличитель отзывов";
$MESS ['TRVWS_INSTALL_DESCRIPTION'] = "Модуль отправляет напоминания пользователям о возможности оставить отзыв на купленный товар";
$MESS ['TRVWS_INST_INSTALL_TITLE'] = "Установка модуля &laquo;Увеличитель отзывов&raquo;";
$MESS ['TRVWS_INST_UNINSTALL_TITLE'] = "Удаление модуля &laquo;Увеличитель отзывов&raquo;";

//step1.php
$MESS ["TRVWS_STEP1_INSTALL_MESS_1"] = "Вы уверены что хотите установить данный модуль?";
$MESS ["TRVWS_STEP1_INSTALL_MESS_2"] = "Установить";

//step2.php
$MESS ["TRVWS_SUCC_INST"] = "Модуль успешно установлен <br> Пройдите в настройки модуля для начала его работы";
$MESS ["TRVWS_ERR_INST"] = "Ошибки, возникшие при установке";
$MESS ['TRVWS_BACK_TO_LIST'] = "Вернуться к списку модулей";


$MESS ['TRVWS_EVENT_STATUS_NAME'] = 'Не хотите оставить отзыв';
$MESS ['TRVWS_EVENT_DESCRIPTION'] = '#ID# - Код заказа.
#LID# - Код сайта, на котором сделан заказ.

#USER_ID# - Код пользователя заказчика.
#USER_LOGIN# - Логин пользователя заказчика.
#USER_NAME# - Имя пользователя заказчика.
#USER_LAST_NAME# - Фамилия пользователя заказчика.
#USER_EMAIL# - Электронная почта пользователя заказчика.

#DATE_INSERT# - Дата добавления заказа.
#USER_DESCRIPTION# - Описание заказа заказчиком.
#ADDITIONAL_INFO# - Дополнительная информация по заказу.
#COMMENTS# - Произвольные комментарии.

#PRICE# - Общая стоимость заказа.
#PRICE_FORMAT# - Общая стоимость заказа (в формате установленной валюты).
#CURRENCY# - Валюта стоимости заказа.

#PRODUCT_ID# - ИД товара.
#PRODUCT_NAME# - Название товара.
#PRODUCT_DETAIL_PAGE_URL# - Ссылка на страницу товара.
#PRODUCT_QUANTITY# - Количество купленного товара.
#PRODUCT_PRICE# - Стоимость товара.
#PRODUCT_PRICE_FORMAT# - Стоимость товара (в формате установленной валюты).
';

$MESS ['TRVWS_MESSAGE_SUBJECT'] = '#SITE_NAME#: Вы недавно купили "#PRODUCT_NAME#"';
$MESS ['TRVWS_MESSAGE_BODY'] = 'Интернет-магазин "#SITE_NAME#"

Вы совершили заказ №#ID# на сумму #PRICE_FORMAT#.
В вашем заказе присутствует товар "#PRODUCT_NAME#" на сумму #PRODUCT_PRICE_FORMAT#.
Нам очень хочется услышать ваш отзыв об этом товаре.

Для написания отзыва перейдите на страницу #SERVER_NAME##PRODUCT_DETAIL_PAGE_URL#

Для работы с заказом перейдите в интернет-магазин #SERVER_NAME#';


//unstep1.php
$MESS ['TRVWS_CAUTION_MESS'] = 'Внимание! Модуль будет удален из системы';
$MESS ['TRVWS_UNINST_MESS_1'] = "Вы можете сохранить некоторые компоненты модуля:";
$MESS ['TRVWS_UNINST_MESS_2'] = "Сохранить данные БД";
$MESS ['TRVWS_UNINST_MOD'] = "Удалить модуль";

//unstep2.php
$MESS ['TRVWS_UNINSTALL_SUCCESS'] = "Модуль &laquo;Увеличитель отзывов&raquo; успешно удален";
$MESS ['TRVWS_UNINSTALL_ERROR'] = "Ошибка деинсталляции модуля";
$MESS ['TRVWS_BACK_TO_MOD_LIST'] = "Вернуться к списку модулей";

?>