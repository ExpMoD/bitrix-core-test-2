<?if(!check_bitrix_sessid()) return;?>

<?if(is_array($errors) && count($errors)>0):?>
	<?foreach($errors as $val):?>
		<?$alErrors .= $val."<br>";?>
	<?endforeach;?>
	<?=CAdminMessage::ShowMessage(Array("TYPE"=>"ERROR", "MESSAGE" =>GetMessage('TRVWS_ERR_INST'), "DETAILS"=>$alErrors, "HTML"=>true));?>
<?else:?>
	<?=CAdminMessage::ShowNote(GetMessage('TRVWS_SUCC_INST'));?>
<?endif;?>

<form action="<?=$APPLICATION->GetCurPage()?>">
	<input type="hidden" name="lang" value="<?=LANG?>">
	<input type="submit" name="" value="<?=GetMessage('TRVWS_BACK_TO_LIST')?>">
</form>