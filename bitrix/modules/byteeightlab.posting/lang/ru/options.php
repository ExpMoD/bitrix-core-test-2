<?
$MESS['MAIN_TAB_SETING_VK'] = "ВКонтакте";
$MESS['MAIN_TAB_TITLE_SETING_VK'] = "Настройки публикации в ВКонтакте";

$MESS['MAIN_TAB_SETING_FB'] = "Facebook";
$MESS['MAIN_TAB_TITLE_SETING_FB'] = "Настройки публикации в Facebook";

$MESS['BEL_POSTING_IBLOCK'] = "Инфоблок";
$MESS['BEL_POSTING_PUBLICATION'] = "Событие публикации";
$MESS['BEL_POSTING_PUBLICATION_1'] = "Добавление нового элемента";
$MESS['BEL_POSTING_PUBLICATION_2'] = "Изменение элемента";
$MESS['BEL_POSTING_PUBLICATION_3'] = "Добавление и изменение элемента";
$MESS['BEL_POSTING_FIELDS'] = "Поля";
$MESS['BEL_POSTING_ACCESS'] = "Доступ";
$MESS['BEL_POSTING_URL'] = "Поле \"Ссылка\"";
$MESS['BEL_POSTING_PUBLICATION_CODE'] = "Поле \"Опубликовать\"";
$MESS['BEL_POSTING_PUBLICATION_CODE_INFO'] = "Данное поле работает следующим образом: если оно заполнено, запись публикуется.<br> Если поле не заполнено, публикуется все.";
$MESS['BEL_POSTING_MESSAGE'] = "Сообщение";
$MESS['BEL_POSTING_ATTACHMENTS'] = "Изображение";
$MESS['BEL_POSTING_TOKEN'] = "Токен";
$MESS['BEL_POSTING_STOK'] = "Публиковать в <a target='_blank' href='http://vk.com/public88378270'>\"Паблик сток\"</a>";

//VK
$MESS['BEL_VK_POSTING_TOKEN_TITLE'] = "Как получить токен?";
$MESS['BEL_VK_POSTING_TOKEN_TEXT'] = "Cоздйте <b>Standalone</b> приложение в ВКонтакте <a target='_blank' href='http://vk.com/editapp?act=create'>тут</a> и внесите в это поле <b>ID своего приложения</b>:";
$MESS['BEL_VK_POSTING_URL_GET'] = "Получить ссылку";
$MESS['BEL_VK_POSTING_URL_GET_TEXT'] = "После авторизации в открывшемся окне, скопируйте адресную строку в это поле:";
$MESS['BEL_VK_POSTING_TOKEN_GET'] = " > Получить токен > ";
$MESS['BEL_VK_POSTING_OWNER_ID'] = "Идентификатор \"ID\"";
$MESS['BEL_VK_POSTING_OWNER_ID_INFO'] = "Как узнать id группы или профиля читайте <a href='http://vk.com/pages?oid=-4489985&p=%D0%9A%D0%B0%D0%BA_%D1%83%D0%B7%D0%BD%D0%B0%D1%82%D1%8C_id_%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D1%8B_%D0%B8%D0%BB%D0%B8_%D0%BF%D1%80%D0%BE%D1%84%D0%B8%D0%BB%D1%8F%3F' target='_blank'>тут</a>";
$MESS['BEL_VK_POSTING_FROM_GROUP'] = "Запись будет опубликована";
$MESS['BEL_VK_POSTING_FROM_GROUP_1'] = "от имени группы";
$MESS['BEL_VK_POSTING_FROM_GROUP_2'] = "от имени пользователя";
$MESS['BEL_VK_POSTING_FROM_GROUP_3'] = "от имени группы с подписью";
$MESS['BEL_VK_POSTING_PUBLISH_DATE'] = "Дата публикации";
$MESS['BEL_VK_POSTING_OWNER_ID_GROUP'] = "Сообщество";
$MESS['BEL_VK_POSTING_OWNER_ID_USER'] = "Пользователь";

//FB
$MESS['BEL_FB_POSTING_CAPTION'] = "Подпись";
$MESS['BEL_FB_POSTING_DESCRIPTION'] = "Описание"; 
$MESS['BEL_FB_POSTING_APP_ID'] = "Идентификатор Приложения \"App ID\"";
$MESS['BEL_FB_POSTING_APP_SECRET'] = "Секретный ключ \"App Secret\""; 
$MESS['BEL_FB_POSTING_PAGE'] = "ID страницы";
$MESS['BEL_FB_POSTING_NAME'] = "Заголовок";

$MESS['FIELD_TIMESTAMP_X'] = "Время последней модификации";
$MESS['FIELD_DATE_CREATE'] = "Время создания элемента";
$MESS['FIELD_ACTIVE_FROM'] = "Начало активности";
$MESS['FIELD_ACTIVE_TO'] = "Окончание активности";
$MESS['FIELD_NAME'] = "Имя элемента";
$MESS['FIELD_PREVIEW_PICTURE'] = "Изображения для анонса";
$MESS['FIELD_PREVIEW_TEXT'] = "Описание для анонса";
$MESS['FIELD_DETAIL_PICTURE'] = "Детальное изображения";
$MESS['FIELD_DETAIL_TEXT'] = "Детальное описание";
$MESS['FIELD_CODE'] = "Символьный код элемента";
$MESS['FIELD_TAGS'] = "Теги";
$MESS['FIELD_DETAIL_PAGE_URL'] = "Путь к элементу";

$MESS['BEL_VK_POSTING_ATTACHMENTS_INFO'] = "Обратите внимание, если указать множественное поле, загружены будут максимум 10 изображение, 9 если была передана ссылка.";

$MESS['BUT_ADD'] = "Добавить";
$MESS['BUT_SAVE'] = "Сохранить";
$MESS['BUT_DEFAULTS'] = "Сбросить все";
$MESS['BUT_DEFAULTS_WARNING'] = "Все настройки будут сброшены и возвращены к изначальным. Вы уверенны?";
$MESS['BUT_STATISTIC'] = "Перейти к статистике";
$MESS['BUT_STATISTIC_WARNING'] = "Прежде чем перейти к статистике, не забудьте сохранить все изменения. Перейти?";

$MESS['EDIT_DOC_TITLE'] = "Документация";
$MESS['EDIT_DOC'] = "Документация и описание модуля доступно <a href='http://byteeightlab.ru/article/publikatsiya_v_sots_seti/' target='_blanc'>здесь</a>";
?>