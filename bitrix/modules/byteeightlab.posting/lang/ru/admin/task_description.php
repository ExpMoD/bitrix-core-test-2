<?
$MESS["TASK_NAME_BYTEEIGHTLAB_POSTING_DENIED"] = "Доступ закрыт";
$MESS["TASK_NAME_BYTEEIGHTLAB_POSTING_VIEW_STATS"] = "Просмотр статистики";
$MESS["TASK_NAME_BYTEEIGHTLAB_POSTING_VIEW_SETTINGS"] = "Просмотр настроек";
$MESS["TASK_NAME_BYTEEIGHTLAB_POSTING_EDIT_SETTINGS"] = "Полный доступ";
$MESS["TASK_DESC_BYTEEIGHTLAB_POSTING_DENIED"] = "Доступ к модулю запрещен";
$MESS["TASK_DESC_BYTEEIGHTLAB_POSTING_VIEW_STATS"] = "Разрешен просмотр всей статистики";
$MESS["TASK_DESC_BYTEEIGHTLAB_POSTING_VIEW_SETTINGS"] = "Разрешен просмотр всей статистики и настроек";
$MESS["TASK_DESC_BYTEEIGHTLAB_POSTING_EDIT_SETTINGS"] = "Разрешен просмотр всей статистики, а также изменение настроек модуля";
?>