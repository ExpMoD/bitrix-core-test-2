<?
$MESS['MAIN_TAB_SETING_VK'] = "ВКонтакте";
$MESS['MAIN_TAB_TITLE_SETING_VK'] = "Статистика публикаций в ВКонтакте";
$MESS['MAIN_TAB_SETING_FB'] = "Facebook";
$MESS['MAIN_TAB_TITLE_SETING_FB'] = "Статистика публикаций в Facebook";

$MESS['BEL_POSTING_IB'] = "Инфоблок";
$MESS['BEL_POSTING_OK'] = "Ошибок зафиксировано не было.";
$MESS['BEL_POSTING_NONE'] = "Инфоблоки не настроены.";

$MESS['BEL_POSTING_ERROR_TITLE'] = "Если возникли ошибки";
$MESS['BEL_VK_POSTING_ERROR_TEXT'] = "Ошибки публикации в ВКонтакте Вы можете посмотреть <a target=_blank'' href='http://vk.com/dev/errors'>по этой ссылке</a>, по коду написанному в начале.";
$MESS['BEL_FB_POSTING_ERROR_TEXT'] = "Ошибки публикации в Facebook Вы можете посмотреть <a target=_blank'' href='https://developers.facebook.com/docs/marketing-api/error-reference'>по этой ссылке</a> по коду написанному в начале.";

$MESS['BEL_POSTING_UPDATE'] = "Обновление данных";
$MESS['BEL_POSTING_UPDATE_TEXT'] = "Обновление данных произойдет при следующей попытки публикации.";

$MESS['BUT_SETING'] = "Настройки";
?>