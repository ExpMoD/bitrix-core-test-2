<?
$MESS['BEL_POSTING_MODULE_NAME'] = "Публикация в соц. сети";
$MESS['BEL_POSTING_MODULE_DESCRIPTION'] = "Автоматическая публикация в соц. сети";
$MESS['BEL_POSTING_PARTNER_NAME'] = "Byte Eight Lab";
$MESS['BEL_POSTING_PARTNER_URI'] = "http://byteeightlab.ru/";

$MESS['BEL_POSTING_INSTALL_TITLE'] = "Установка модуля 'Публикация в соц. сети'";
$MESS['BEL_POSTING_INST_OK'] = "Установка модуля 'Публикация в соц. сети' прошла успешно.\n\nНе забудьте настроить модуль перед использованием.";
$MESS['BEL_POSTING_INST_BUTTON_SETTING'] = "Настройки";
$MESS['BEL_POSTING_INST_BUTTON_SETTING_TITLE'] = "Настройки модуля 'Публикация в соц. сети'";

$MESS['BEL_POSTING_UNINSTALL_TITLE'] = "Удаление модуля 'Публикация в соц. сети'";
$MESS['BEL_POSTING_UNINST_OK'] = "Удаление прошло успешно.\n\nСпасибо за использование нашего модуля. Если в его работе Вам что-то не понравилось или что-то работало не так, пожалуйста сообщите нам info@byeeightlab.ru";

$MESS['BEL_POSTING_BACK'] = "Вернуться в список";

$MESS['BEL_POSTING_DENIED'] = "Доступ закрыт";
$MESS['BEL_POSTING_VIEW_STATS'] = "Просмотр статистики";
$MESS['BEL_POSTING_VIEW_SETTINGS'] = "Просмотр статистики и настроек";
$MESS['BEL_POSTING_EDIT_SETTINGS'] = "Изменение настроек";
?>