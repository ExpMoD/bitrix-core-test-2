<?
$MESS['BEL_POSTING_MODULE_NAME'] = "Publication in social networks";
$MESS['BEL_POSTING_MODULE_DESCRIPTION'] = "Automatically publish to social networks";
$MESS['BEL_POSTING_PARTNER_NAME'] = "Byte Eight Lab";
$MESS['BEL_POSTING_PARTNER_URI'] = "http://byteeightlab.ru/";

$MESS['BEL_POSTING_INSTALL_TITLE'] = "installation the module 'Publication in social networks'";
$MESS['BEL_POSTING_INST_OK'] = "Installing the module 'Publication in social networks' was successful.\n\nDo not forget to configure the module before use it.";
$MESS['BEL_POSTING_INST_BUTTON_SETTING'] = "settings";
$MESS['BEL_POSTING_INST_BUTTON_SETTING_TITLE'] = "Settings the module 'Publication in social networks'";

$MESS['BEL_POSTING_UNINSTALL_TITLE'] = "Remove the module 'Publication in social networks'";
$MESS['BEL_POSTING_UNINST_OK'] = "Removal was successful.\n\nThank you for using our module, if you do not like something in his work or something does not work properly, please let us know info@byeeightlab.ru";

$MESS['BEL_POSTING_BACK'] = "Back to List";

$MESS['BEL_POSTING_DENIED'] = "Access denied";
$MESS['BEL_POSTING_VIEW_STATS'] = "View Statistics";
$MESS['BEL_POSTING_VIEW_SETTINGS'] = "Viewing statistics and settings";
$MESS['BEL_POSTING_EDIT_SETTINGS'] = "Changing the settings";
?>