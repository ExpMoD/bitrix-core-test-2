<?
$MESS['MAIN_TAB_SETING_VK'] = "VK";
$MESS['MAIN_TAB_TITLE_SETING_VK'] = "Statistics published in VK";
$MESS['MAIN_TAB_SETING_FB'] = "Facebook";
$MESS['MAIN_TAB_TITLE_SETING_FB'] = "Statistics published in Facebook";

$MESS['BEL_POSTING_IB'] = "Information block";
$MESS['BEL_POSTING_OK'] = "Errors were not detected.";
$MESS['BEL_POSTING_NONE'] = "Information blocks are not configured.";

$MESS['BEL_POSTING_ERROR_TITLE'] = "If the error occurred";
$MESS['BEL_VK_POSTING_ERROR_TEXT'] = "Publication bias in VK you can see <a target=_blank'' href='http://vk.com/dev/errors'>on this page</a>, the code is written in the beginning.";
$MESS['BEL_FB_POSTING_ERROR_TEXT'] = "Publication bias in Facebook you can see <a target=_blank'' href='https://developers.facebook.com/docs/marketing-api/error-reference'>on this page</a> the code is written in the beginning.";

$MESS['BEL_POSTING_UPDATE'] = "Data updating";
$MESS['BEL_POSTING_UPDATE_TEXT'] = "Update information will occur at the next attempt to publication.";

$MESS['BUT_SETING'] = "Seting";
?>