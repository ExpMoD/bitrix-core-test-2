<?
$MESS["TASK_NAME_BYTEEIGHTLAB_POSTING_DENIED"] = "Access is denied";
$MESS["TASK_NAME_BYTEEIGHTLAB_POSTING_VIEW_STATS"] = "View Statistics";
$MESS["TASK_NAME_BYTEEIGHTLAB_POSTING_VIEW_SETTINGS"] = "Viewing statistics and settings";
$MESS["TASK_NAME_BYTEEIGHTLAB_POSTING_EDIT_SETTINGS"] = "Changing the settings";
$MESS["TASK_DESC_BYTEEIGHTLAB_POSTING_DENIED"] = "Access is denied";
$MESS["TASK_DESC_BYTEEIGHTLAB_POSTING_VIEW_STATS"] = "View Statistics";
$MESS["TASK_DESC_BYTEEIGHTLAB_POSTING_VIEW_SETTINGS"] = "Allowed to view all the settings and statistics";
$MESS["TASK_DESC_BYTEEIGHTLAB_POSTING_EDIT_SETTINGS"] = "Allowed to view all the statistics and change the settings module";
?>