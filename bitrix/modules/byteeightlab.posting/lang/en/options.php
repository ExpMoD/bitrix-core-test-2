<?
$MESS['MAIN_TAB_SETING_VK'] = "VK";
$MESS['MAIN_TAB_TITLE_SETING_VK'] = "Publish settings in VK";

$MESS['MAIN_TAB_SETING_FB'] = "Facebook";
$MESS['MAIN_TAB_TITLE_SETING_FB'] = "Publish settings in Facebook";

$MESS['BEL_POSTING_IBLOCK'] = "Information block";
$MESS['BEL_POSTING_PUBLICATION'] = "Events of publication";
$MESS['BEL_POSTING_PUBLICATION_1'] = "Adding a new element";
$MESS['BEL_POSTING_PUBLICATION_2'] = "Change element";
$MESS['BEL_POSTING_PUBLICATION_3'] = "Adding and modifying element";
$MESS['BEL_POSTING_FIELDS'] = "Fields";
$MESS['BEL_POSTING_ACCESS'] = "Access";
$MESS['BEL_POSTING_URL'] = "Field\"URL\"";
$MESS['BEL_POSTING_PUBLICATION_CODE'] = "Field \"Publish\"";
$MESS['BEL_POSTING_PUBLICATION_CODE_INFO'] = "This field operates as follows: if it is filled , the record is published.<br> If the field is blank, all published.";
$MESS['BEL_POSTING_MESSAGE'] = "Message";
$MESS['BEL_POSTING_ATTACHMENTS'] = "Image";
$MESS['BEL_POSTING_TOKEN'] = "token";
$MESS['BEL_POSTING_STOK'] = "Publish in <a target='_blank' href='http://vk.com/public88378270'>\"Public Stock\"</a>";

//VK
$MESS['BEL_VK_POSTING_TOKEN_TITLE'] = "How to get a token?";
$MESS['BEL_VK_POSTING_TOKEN_TEXT'] = "Create <b>Standalone</b> an application in VK <a target='_blank' href='http://vk.com/editapp?act=create'>here</a> and fill in this field <b>ID of your application</b>:";
$MESS['BEL_VK_POSTING_URL_GET'] = "Get the link";
$MESS['BEL_VK_POSTING_URL_GET_TEXT'] = "In the open window after login, copy the address bar in this field:";
$MESS['BEL_VK_POSTING_TOKEN_GET'] = " > Get the token > ";
$MESS['BEL_VK_POSTING_OWNER_ID'] = "Identifier \"ID\"";
$MESS['BEL_VK_POSTING_OWNER_ID_INFO'] = "How you can find group id or profile id read <a href='http://vk.com/pages?oid=-4489985&p=%D0%9A%D0%B0%D0%BA_%D1%83%D0%B7%D0%BD%D0%B0%D1%82%D1%8C_id_%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D1%8B_%D0%B8%D0%BB%D0%B8_%D0%BF%D1%80%D0%BE%D1%84%D0%B8%D0%BB%D1%8F%3F' target='_blank'>here</a>";
$MESS['BEL_VK_POSTING_FROM_GROUP'] = "The record will be published";
$MESS['BEL_VK_POSTING_FROM_GROUP_1'] = "on behalf of the group";
$MESS['BEL_VK_POSTING_FROM_GROUP_2'] = "on behalf of the user";
$MESS['BEL_VK_POSTING_FROM_GROUP_3'] = "on behalf of the group with a signature";
$MESS['BEL_VK_POSTING_PUBLISH_DATE'] = "Date of publication";
$MESS['BEL_VK_POSTING_OWNER_ID_GROUP'] = "Group";
$MESS['BEL_VK_POSTING_OWNER_ID_USER'] = "User";

//FB
$MESS['BEL_FB_POSTING_CAPTION'] = "Caption";
$MESS['BEL_FB_POSTING_DESCRIPTION'] = "Description"; 
$MESS['BEL_FB_POSTING_APP_ID'] = "The application ID \"App ID\"";
$MESS['BEL_FB_POSTING_APP_SECRET'] = "Secret key \"App Secret\""; 
$MESS['BEL_FB_POSTING_PAGE'] = "Page ID";
$MESS['BEL_FB_POSTING_NAME'] = "Title";

$MESS['FIELD_TIMESTAMP_X'] = "Date of last modification";
$MESS['FIELD_DATE_CREATE'] = "Date of create";
$MESS['FIELD_ACTIVE_FROM'] = "Active from";
$MESS['FIELD_ACTIVE_TO'] = "Active to";
$MESS['FIELD_NAME'] = "Name";
$MESS['FIELD_PREVIEW_PICTURE'] = "Preview picture";
$MESS['FIELD_PREVIEW_TEXT'] = "Preview text";
$MESS['FIELD_DETAIL_PICTURE'] = "Detail picture";
$MESS['FIELD_DETAIL_TEXT'] = "Detail text";
$MESS['FIELD_CODE'] = "Code";
$MESS['FIELD_TAGS'] = "Tags";
$MESS['FIELD_DETAIL_PAGE_URL'] = "URL If it is required";

$MESS['BEL_VK_POSTING_ATTACHMENTS_INFO'] = "Pay attention, if you specify multiple field, max.10 images will be uploaded, 9 if the link has been transferred.";

$MESS['BUT_ADD'] = "Add";
$MESS['BUT_SAVE'] = "Save";
$MESS['BUT_DEFAULTS'] = "Reset all";
$MESS['BUT_DEFAULTS_WARNING'] = "All settings will be reset and returned to the original. Are you sure?";
$MESS['BUT_STATISTIC'] = "Go to statistics";
$MESS['BUT_STATISTIC_WARNING'] = "Before you move on the statistics, do not forget to save all changes . Do it?";

$MESS['EDIT_DOC_TITLE'] = "Data Sheet";
$MESS['EDIT_DOC'] = "Documentation and description of the module is available <a href='http://byteeightlab.ru/article/publikatsiya_v_sots_seti/' target='_blanc'>here</a>";
?>