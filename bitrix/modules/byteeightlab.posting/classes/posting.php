<?php
class BEL_posting {
    static $MODULE_ID="byteeightlab.posting";

    static function BEL_OnAfterIBlockElementAdd($arFields){
	
		if($arFields['RESULT_MESSAGE']!='') return true;
		
		$VKPOST = array();
		$VKPOST_NUM = COption::GetOptionString(self::$MODULE_ID,'VKPOST_NUM',0);
		for($i=0;$i<$VKPOST_NUM;$i++){ $VKPOST['n'.$i] = json_decode(COption::GetOptionString(self::$MODULE_ID,'VKPOST_n'.$i,'[]')); }
		foreach($VKPOST as $value){
			if(intval($value->IBLOCK) == intval($arFields['IBLOCK_ID'])&&($value->PUBLICATION==1||$value->PUBLICATION==3)){
				self::PostingVK($arFields,$value);
			}
		}

		$FBPOST = array();
		$FBPOST_NUM = COption::GetOptionString(self::$MODULE_ID,'FBPOST_NUM',0);
		if($FBPOST_NUM>0) for($i=0;$i<$FBPOST_NUM;$i++){ $FBPOST['n'.$i] = json_decode(COption::GetOptionString(self::$MODULE_ID,'FBPOST_n'.$i,'[]')); }			
		foreach($FBPOST as $value){					
			if(intval($value->IBLOCK) == intval($arFields['IBLOCK_ID'])&&($value->PUBLICATION==1||$value->PUBLICATION==3)){				
				self::PostingFB($arFields,$value);
			}
		}
		
        return true;
    }
	
    static function BEL_OnAfterIBlockElementUpdate($arFields){	
	
		if($arFields['RESULT_MESSAGE']!='') return true;
		
		$VKPOST = array();
		$VKPOST_NUM = COption::GetOptionString(self::$MODULE_ID,'VKPOST_NUM',0);
		for($i=0;$i<$VKPOST_NUM;$i++){ $VKPOST['n'.$i] = json_decode(COption::GetOptionString(self::$MODULE_ID,'VKPOST_n'.$i,'[]')); }
		foreach($VKPOST as $value){			
			if(intval($value->IBLOCK) == intval($arFields['IBLOCK_ID'])&&($value->PUBLICATION==2||$value->PUBLICATION==3)){
				self::PostingVK($arFields,$value);
			}
		}
		
		$FBPOST = array();
		$FBPOST_NUM = COption::GetOptionString(self::$MODULE_ID,'FBPOST_NUM',0);
		if($FBPOST_NUM>0) for($i=0;$i<$FBPOST_NUM;$i++){ $FBPOST['n'.$i] = json_decode(COption::GetOptionString(self::$MODULE_ID,'FBPOST_n'.$i,'[]')); }			
		foreach($FBPOST as $value){					
			if(intval($value->IBLOCK) == intval($arFields['IBLOCK_ID'])&&($value->PUBLICATION==2||$value->PUBLICATION==3)){				
				self::PostingFB($arFields,$value);
			}
		}	
		
		return true;
    }
	
	static function PostingVK($arFields,$value){	
		global $MESS;
		IncludeModuleLangFile(__FILE__);		
		
		$VK_ERRORS = array();
		
		if($value->OWNER_ID[0]=='-'){
			$value->OWNER_ID = substr($value->OWNER_ID,1);
			$group = true;
		}else $group = false;
		
		if(!extension_loaded('openssl')||!ini_get('allow_url_fopen')){
			if(!extension_loaded('openssl')) $VK_ERRORS[] = "0 - openssl not exist";
			if(!ini_get('allow_url_fopen')) $VK_ERRORS[] = "0 - set value allow_url_fopen=on in php.ini";
		}else{		
			CModule::IncludeModule(self::$MODULE_ID);
			CModule::IncludeModule('iblock');
			
			$arSelect = Array("ID","IBLOCK_ID");
			if($value->PUBLICATION_CODE!='') $arSelect[] = "PROPERTY_".$value->PUBLICATION_CODE;
			if($value->MESSAGE!='') $arSelect[] = $value->MESSAGE;
			if($value->PUBLISH_DATE!='') $arSelect[] = $value->PUBLISH_DATE;
			if($value->URL!='') $arSelect[] = $value->URL;
			if($value->ATTACHMENTS!=''&&strripos($value->ATTACHMENTS,"PROPERTY_")===false) $arSelect[] = $value->ATTACHMENTS;
			$arFilter = Array("IBLOCK_ID"=>IntVal($value->IBLOCK),"ID"=>$arFields['ID']);
			$res = CIBlockElement::GetList(Array(),$arFilter,false,Array("nPageSize"=>1),$arSelect);
			if($ob = $res->GetNextElement()){
				$arFields_in = $ob->GetFields();
				if($value->PUBLICATION_CODE!='') 
					if(!isset($arFields_in["PROPERTY_".$value->PUBLICATION_CODE.'_VALUE']))
						$arFields_in["PROPERTY_".$value->PUBLICATION_CODE.'_VALUE'] = '';
			}
			
			if(strripos($value->MESSAGE,"PROPERTY_")!==false) $value->MESSAGE .= "_VALUE";
			if(strripos($value->PUBLISH_DATE,"PROPERTY_")!==false) $value->PUBLISH_DATE .= "_VALUE";
			if(strripos($value->URL,"PROPERTY_")!==false) $value->URL .= "_VALUE";
			
			if($value->URL=="DETAIL_PAGE_URL") $arFields_in[$value->URL] = 'http://'.$_SERVER['SERVER_NAME'].$arFields_in[$value->URL];	
			
			if($arFields_in["PROPERTY_".$value->PUBLICATION_CODE.'_VALUE']!=''||$value->PUBLICATION_CODE==''){			
			
				if($value->ATTACHMENTS!=''&&strripos($value->ATTACHMENTS,"PROPERTY_")!==false){
					$PROPERTY = str_replace("PROPERTY_","",$value->ATTACHMENTS);					
					$arFields_in[$value->ATTACHMENTS] = array();
					$db_props = CIBlockElement::GetProperty(IntVal($value->IBLOCK),$arFields_in['ID'],array(),Array("CODE"=>$PROPERTY));
					$i=0; while($ar_props = $db_props->Fetch()){ if($ar_props["VALUE"]!=''&&$i<10){ $arFields_in[$value->ATTACHMENTS][] = $ar_props["VALUE"]; $i++; } }
				}else{
					if(!isset($arFields_in[$value->ATTACHMENTS])) $arFields_in[$value->ATTACHMENTS] = array();
					else $arFields_in[$value->ATTACHMENTS] = array($arFields_in[$value->ATTACHMENTS]);
				}

				if($value->PUBLICATION_CODE!='') CIBlockElement::SetPropertyValuesEx($arFields_in['ID'],$arFields_in['IBLOCK_ID'],array($value->PUBLICATION_CODE=>''));

				$VK = new BEL_vk((string) $value->TOKEN);
				

				$attachments = false; $attachments_str = '';
				
				if($arFields_in[$value->URL]!=''){
					if($attachments_str!='') $attachments_str .= ',';
					$attachments_str .= $arFields_in[$value->URL];				
				}				

				if(count($arFields_in[$value->ATTACHMENTS])>0){					
					if($group) $params = array("gid"=>$value->OWNER_ID);
					else $params = array("uid"=>$value->OWNER_ID);
					$post = $VK->method("photos.getWallUploadServer", $params); unset($params);						
					if(isset($post->error)){				
						$VK_ERRORS[] = $post->error->error_code." ".$post->error->error_msg;
					}else{
						COption::RemoveOption(self::$MODULE_ID,'VKPOST_'.$value->IBLOCK.'_GETWALLUPLOADSERVER_ERROR');	
						$files = array();
						$i=0; $par=0; foreach($arFields_in[$value->ATTACHMENTS] as $id=>$attachment){
							if($i==0) $files['par'.$par] = array();
							$img_src = $_SERVER["DOCUMENT_ROOT"].CFile::GetPath($attachment);
							$files['par'.$par]['file'.$i] = (class_exists('CURLFile',false))?new CURLFile(realpath($img_src)):''.realpath($img_src);
							$i++; if($i>=1){ $i=0; $par++; }
						}			
						foreach($files as $par){								
							$MULTIPART_BOUNDARY = '--------------------------'.microtime(true);
							$header = 'Content-Type: multipart/form-data; boundary='.$MULTIPART_BOUNDARY;
							$content = "--".$MULTIPART_BOUNDARY."\r\n";
							foreach($par as $FieldName=>$file){
								if(is_object($file)){//cast 1
									if(isset($file->name)) $file = $file->name;
								}
								$file_contents = file_get_contents($file); 
								$content .= "Content-Disposition: form-data; name=\"".$FieldName."\"; filename=\"".basename($file)."\"\r\n";
								$content .= "Content-Type: application/zip\r\n\r\n";
								$content .= $file_contents."\r\n";
								$content .= "--".$MULTIPART_BOUNDARY."--\r\n";								
							}	
							$context = stream_context_create(array('http'=>array('ignore_errors'=>true,'method'=>'POST','header'=>$header,'content'=>$content)));							
							$upload_data = (array) json_decode(file_get_contents($post->response->upload_url,false,$context));		
							$params = array();
							if($group) $params["group_id"] = $value->OWNER_ID;
							else $params["user_id"] = $value->OWNER_ID;						
							$params["photo"] = $upload_data['photo'];
							$params["server"] = $upload_data['server'];
							$params["hash"] = $upload_data['hash'];									
							$attachments = $VK->method("photos.saveWallPhoto", $params); unset($params);				
							if(isset($attachments->error)){
								$VK_ERRORS[] = $attachments->error->error_code." ".$attachments->error->error_msg;
							}else{
								COption::RemoveOption(self::$MODULE_ID,'VKPOST_'.$value->IBLOCK.'_SAVEWALLPHOTO_ERROR');							
								if(count($attachments->response)>0){
									foreach($attachments->response as $item){
										if($attachments_str!='') $attachments_str .= ',';
										$attachments_str .= $item->id;
									}
								}
							}							
						}	
					}
				}							
					
				$params = array();						
			
				$message = false;
				if(isset($arFields_in[$value->MESSAGE])){
					$message = true;
					if(is_array($arFields_in[$value->MESSAGE])) $arFields_in[$value->MESSAGE] = $arFields_in[$value->MESSAGE]['TEXT'];	
					$params['message'] = trim(strip_tags($arFields_in[$value->MESSAGE]));
				}
				
				if($attachments_str!='') $params['attachments'] = $attachments_str;
			
				if($group){
					$params["owner_id"] = '-'.$value->OWNER_ID;
					if($value->FROM_GROUP<3){
						$params["from_group"] = $value->FROM_GROUP;
					}else{
						$params["from_group"] = 2;
						$params["signed"] = 1;
					}
				}else $params["owner_id"] = $value->OWNER_ID;				
				
				if(isset($arFields_in[$value->PUBLISH_DATE])){
					if($arFields_in[$value->PUBLISH_DATE]!=''){
						$params["publish_date"] = (round(round(MakeTimeStamp($arFields_in[$value->PUBLISH_DATE])/60)/5)*5)*60;
						if($params["publish_date"]<time()) unset($params["publish_date"]);
					}
				}	
			
				if($attachments!==false||$message){
					$post = $VK->method("wall.post", $params);
				
					if(isset($value->STOK)){
						$params['owner_id'] = "-88378270";
						$params['from_group'] = "0";
						$post = $VK->method("wall.post", $params);unset($params);
					}
					
					if(isset($post->error)){
						$VK_ERRORS[] = $post->error->error_code." ".$post->error->error_msg;
					}else{
						COption::RemoveOption(self::$MODULE_ID,'VKPOST_'.$value->IBLOCK.'_POST_ERROR');     
					}
				}
			}
		}
		
		if(count($VK_ERRORS)>0){
			COption::SetOptionString(self::$MODULE_ID,'POSTVK_'.$value->IBLOCK.'_'.$value->OWNER_ID.'_POST_ERRORS',json_encode($VK_ERRORS));
			$ar = Array(
				"MESSAGE" => GetMessage('NOTIFY_POSTVK')." ".GetMessage('NOTIFY_ERRORS')." ".GetMessage('NOTIFY_INFO'),
				"TAG" => "POSTVK",
				"MODULE_ID" => self::$MODULE_ID,
				"ENABLE_CLOSE" => "Y"
			);
			$ID = CAdminNotify::Add($ar);				
		}else COption::RemoveOption(self::$MODULE_ID,'POSTVK_'.$value->IBLOCK.'_'.$value->OWNER_ID.'_POST_ERRORS');  		
	}
	
	static function PostingFB($arFields,$value){
		global $MESS;
		IncludeModuleLangFile(__FILE__);		
		
		$FB_ERRORS = array();
		
		if(!extension_loaded('openssl')||!ini_get('allow_url_fopen')){
			if(!extension_loaded('openssl')) $FB_ERRORS[] = "0 - openssl not exist";
			if(!ini_get('allow_url_fopen')) $FB_ERRORS[] = "0 - set value allow_url_fopen=on in php.ini";
		}else{
			CModule::IncludeModule(self::$MODULE_ID);
			CModule::IncludeModule('iblock');
			
			$arSelect = Array("ID","IBLOCK_ID");
			if($value->PUBLICATION_CODE!='') $arSelect[] = "PROPERTY_".$value->PUBLICATION_CODE;
			if($value->MESSAGE!='') $arSelect[] = $value->MESSAGE;
			if($value->PICTURE!='') $arSelect[] = $value->PICTURE;
			if($value->CAPTION!='') $arSelect[] = $value->CAPTION;
			if($value->NAME!='') $arSelect[] = $value->NAME;
			if($value->DESCRIPTION!='') $arSelect[] = $value->DESCRIPTION;
			if($value->LINK!='') $arSelect[] = $value->LINK;
			$arFilter = Array("IBLOCK_ID"=>IntVal($value->IBLOCK),"ID"=>$arFields['ID']);
			$res = CIBlockElement::GetList(Array(),$arFilter,false,Array("nPageSize"=>1),$arSelect);
			if($ob = $res->GetNextElement()){
				$arFields_in = $ob->GetFields();
				if($value->PUBLICATION_CODE!='') 
					if(!isset($arFields_in["PROPERTY_".$value->PUBLICATION_CODE.'_VALUE']))
						$arFields_in["PROPERTY_".$value->PUBLICATION_CODE.'_VALUE'] = '';
			}
			
			if(strripos($value->MESSAGE,"PROPERTY_")!==false) $value->MESSAGE = strtoupper($value->MESSAGE."_VALUE");
			if(strripos($value->PICTURE,"PROPERTY_")!==false) $value->PICTURE = strtoupper($value->PICTURE. "_VALUE");
			if(strripos($value->CAPTION,"PROPERTY_")!==false) $value->CAPTION = strtoupper($value->CAPTION. "_VALUE");
			if(strripos($value->NAME,"PROPERTY_")!==false) $value->NAME = strtoupper($value->NAME. "_VALUE");
			if(strripos($value->DESCRIPTION,"PROPERTY_")!==false) $value->DESCRIPTION = strtoupper($value->DESCRIPTION."_VALUE");
			if(strripos($value->LINK,"PROPERTY_")!==false) $value->LINK = strtoupper($value->LINK."_VALUE");
			if($value->LINK=="DETAIL_PAGE_URL") $arFields_in[$value->LINK] = 'http://'.$_SERVER['SERVER_NAME'].$arFields_in[$value->LINK];	
			if($arFields_in[$value->PICTURE]!='') $arFields_in[$value->PICTURE] = 'http://'.$_SERVER['SERVER_NAME'].CFile::GetPath($arFields_in[$value->PICTURE]);		
			
			if($arFields_in["PROPERTY_".$value->PUBLICATION_CODE.'_VALUE']!=''||$value->PUBLICATION_CODE==''){			

				if($value->PUBLICATION_CODE!='') CIBlockElement::SetPropertyValuesEx($arFields_in['ID'],$arFields_in['IBLOCK_ID'],array($value->PUBLICATION_CODE=>''));
			
				$URL = 	"https://graph.facebook.com/oauth/access_token".
						"?client_id=".$value->APP_ID.
						"&client_secret=".$value->APP_SECRET.
						"&grant_type=fb_exchange_token".
						"&fb_exchange_token=".$value->EXISTING_ACCESS_TOKEN;
						
				$context = stream_context_create(array(
					'http' => array(
						'ignore_errors'=>true,
						'method'=>'GET'
					)
				));
				
				$access_token = explode("&",file_get_contents($URL,false,$context));
				$access_token_err = json_decode($access_token[0]);
				
				if(isset($access_token_err->error)){ 
					$FB_ERRORS[] = $access_token_err->error->code." - ".$access_token_err->error->message;
				}else{			
					foreach($access_token as $k=>$v){
						$access_token[$k] = explode('=',$v);
						$access_token[$access_token[$k][0]] = $access_token[$k][1];
						unset($access_token[$k]);
					}
					
					$URL = "https://graph.facebook.com/".$value->PAGE;
					$context = stream_context_create(array(
						'http' => array(
							'ignore_errors'=>true,
							'method'=>'GET',
						)
					));
					$site = json_decode(file_get_contents($URL,false,$context));			
					
					if(isset($site->error)){ 
						$FB_ERRORS[] = $site->error->code." - ".$site->error->message;
					}else{		
						$URL = "https://graph.facebook.com/v2.2/".$site->id."/feed";

						$POST_str = array();
						$POST_str['access_token'] = $access_token['access_token'];
						$POST_str['message'] = iconv(LANG_CHARSET,'UTF-8',trim(strip_tags($arFields_in[$value->MESSAGE])));
						if($arFields_in[$value->PICTURE]!=""){
							$POST_str['picture'] = $arFields_in[$value->PICTURE];
							if($arFields_in[$value->NAME]!="") $POST_str['name'] = iconv(LANG_CHARSET,'UTF-8',trim(strip_tags($arFields_in[$value->NAME])));
							if($arFields_in[$value->DESCRIPTION]!="") $POST_str['description'] = iconv(LANG_CHARSET,'UTF-8',trim(strip_tags($arFields_in[$value->DESCRIPTION])));
							if($arFields_in[$value->CAPTION]!="") $POST_str['caption'] = iconv(LANG_CHARSET,'UTF-8',trim(strip_tags($arFields_in[$value->CAPTION]."123123")));
							if($arFields_in[$value->LINK]!="") $POST_str['link'] = $arFields_in[$value->LINK];
						}	
						$POST_str['actions'] = '{"name":"Byte Eight Lab","link":"http:\/\/byteeightlab.ru\/"}';		
						$POST_str = http_build_query($POST_str);
						$context = stream_context_create(array(
							'http' => array(
								'ignore_errors'=>true,
								'method'=>'POST',
								'header'=>"Connection: close\r\nContent-Length: ".strlen($POST_str)."\r\n",
								'content'=>$POST_str
							)
						));

						$response = json_decode(file_get_contents($URL,false,$context));
						if(isset($response->error)){ $FB_ERRORS[] = $response->error->code." - ".$response->error->message; }
					}
				}
			}
		}
		
		if(count($FB_ERRORS)>0){
			COption::SetOptionString(self::$MODULE_ID,'FBPOST_'.$value->IBLOCK.'_'.$value->PAGE.'_POST_ERRORS',json_encode($FB_ERRORS));
			$ar = Array(
				"MESSAGE" => GetMessage('NOTIFY_POSTFB')." ".GetMessage('NOTIFY_ERRORS')." ".GetMessage('NOTIFY_INFO'),
				"TAG" => "POSTFB",
				"MODULE_ID" => self::$MODULE_ID,
				"ENABLE_CLOSE" => "Y"
			);
			$ID = CAdminNotify::Add($ar);				
		}else COption::RemoveOption(self::$MODULE_ID,'FBPOST_'.$value->IBLOCK.'_'.$value->PAGE.'_POST_ERRORS');   
	}
}
?>