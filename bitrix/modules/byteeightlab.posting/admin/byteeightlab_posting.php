<?
$module_id = "byteeightlab.posting";
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

IncludeModuleLangFile(__FILE__);

$APPLICATION->SetTitle(GetMessage("PAGE_TITLE"));

$VKPOST = array();
$VKPOST_NUM = COption::GetOptionString($module_id,'VKPOST_NUM',0);
if($VKPOST_NUM>0) for($i=0;$i<$VKPOST_NUM;$i++){ $VKPOST['n'.$i] = (array) json_decode(COption::GetOptionString($module_id,'VKPOST_n'.$i,'[]')); }
$VKPOST_OLD = (array) json_decode(COption::GetOptionString($module_id,'VKPOST','[]'));
foreach($VKPOST_OLD as $key=>$value){ $VKPOST['n'.$i] = (array) $value; $i++; }

$FBPOST = array();
$FBPOST_NUM = COption::GetOptionString($module_id,'FBPOST_NUM',0);
if($FBPOST_NUM>0) for($i=0;$i<$FBPOST_NUM;$i++){ $FBPOST['n'.$i] = (array) json_decode(COption::GetOptionString($module_id,'FBPOST_n'.$i,'[]')); }

$aTabs = array(
	array("DIV" => "edit_vk", "TAB" => GetMessage("MAIN_TAB_SETING_VK"), "ICON" => "", "TITLE" => GetMessage("MAIN_TAB_TITLE_SETING_VK")),
	array("DIV" => "edit_fb", "TAB" => GetMessage("MAIN_TAB_SETING_FB"), "ICON" => "", "TITLE" => GetMessage("MAIN_TAB_TITLE_SETING_FB")),
);

$tabControl = new CAdmintabControl("tabControl", $aTabs);

CModule::IncludeModule("iblock");
$IblockArray = array();
$res = CIBlock::GetList(Array(),Array('ACTIVE'=>'Y'));
while($ar_res = $res->Fetch()){ 
	$IblockArray[$ar_res['ID']] = array("NAME"=>$ar_res['NAME'],'FIELDS'=>$FIELD_DEF);
	$properties = CIBlockProperty::GetList(Array(),Array("ACTIVE"=>"Y","IBLOCK_ID"=>$ar_res['ID']));
	while ($prop_fields = $properties->GetNext()){
		if($prop_fields["CODE"]!='') $IblockArray[$ar_res['ID']]['FIELDS'][] = array("CODE"=>$prop_fields["CODE"],"NAME"=>$prop_fields["NAME"]);
	}	
}

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$tabControl->Begin();?>

<?$tabControl->BeginNextTab();?>
<tr><td>
	<table border="0" cellspacing="0" cellpadding="0" class="internal" style="width:690px; margin: 0 auto;"><tbody>
<?if(count($VKPOST)>0){ foreach($VKPOST as $value){
	if($value['OWNER_ID'][0]=='-'){
		$value['OWNER_ID'] = substr($value['OWNER_ID'],1);
	}
?>
	<tr class="heading"><td width="125"><?=GetMessage("BEL_POSTING_IB")?> - <?=$IblockArray[$value['IBLOCK']]['NAME']?></h3></td></tr>
	<?$FBPOST_ERRORS = json_decode(COption::GetOptionString($module_id,'POSTVK_'.$value['IBLOCK'].'_'.$value['OWNER_ID'].'_POST_ERRORS','[]'));
	if(count($FBPOST_ERRORS)>0){
		foreach($FBPOST_ERRORS as $ERROR){?>
			<tr><td><b style='color: #ED2121;'><?=$ERROR?></b></td></tr>
		<?}
	}else{?>
	<tr><td><b style="color: #86AD00;"><?=GetMessage("BEL_POSTING_OK")?></b></td></tr>
	<?}?>
<?}}else{?>
	<tr class="heading"><td><?=GetMessage("BEL_POSTING_NONE")?></td></tr>
<?}?>
	</tbody></table>
</td></tr>
<tr>
	<td align="center">
		<div class="adm-info-message">
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_POSTING_ERROR_TITLE")?></h3>
			<?=GetMessage("BEL_VK_POSTING_ERROR_TEXT")?><br><br>
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_POSTING_UPDATE")?></h3>
			<?=GetMessage("BEL_POSTING_UPDATE_TEXT")?><br><br>			
		</div>
	</td>
</tr>
<?$tabControl->BeginNextTab();?>
<tr><td>
	<table border="0" cellspacing="0" cellpadding="0" class="internal" style="width:690px; margin: 0 auto;"><tbody>
<?if(count($FBPOST)>0){ foreach($FBPOST as $value){?>
	<tr class="heading"><td width="125"><?=GetMessage("BEL_POSTING_IB")?> - <?=$IblockArray[$value['IBLOCK']]['NAME']?></h3></td></tr>
	<?$FBPOST_ERRORS = json_decode(COption::GetOptionString($module_id,'FBPOST_'.$value['IBLOCK'].'_'.$value['PAGE'].'_POST_ERRORS','[]'));
	if(count($FBPOST_ERRORS)>0){
		foreach($FBPOST_ERRORS as $ERROR){?>
			<tr><td><b style='color: #ED2121;'><?=$ERROR?></b></td></tr>
		<?}
	}else{?>
	<tr><td><b style="color: #86AD00;"><?=GetMessage("BEL_POSTING_OK")?></b></td></tr>
	<?}?>
<?}}else{?>
	<tr class="heading"><td><?=GetMessage("BEL_POSTING_NONE")?></td></tr>
<?}?>
	</tbody></table>
</td></tr>
<tr>
	<td align="center">
		<div class="adm-info-message">
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_POSTING_ERROR_TITLE")?></h3>
			<?=GetMessage("BEL_FB_POSTING_ERROR_TEXT")?><br><br>
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_POSTING_UPDATE")?></h3>
			<?=GetMessage("BEL_POSTING_UPDATE_TEXT")?><br><br>			
		</div>
	</td>
</tr>
<?$tabControl->Buttons();?>

<script>
	function GoSeting(){
		window.location = "/bitrix/admin/settings.php?lang=<?=LANG?>&mid=<?=$module_id?>&mid_menu=1";
	}	
</script>
<input type="button" OnClick="GoSeting();" value="<?=GetMessage('BUT_SETING')?>" class="adm-btn-save">


<?$tabControl->End();?>

<?require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>