<?
IncludeModuleLangFile(__FILE__);
if(
	CModule::IncludeModule('byteeightlab.posting')
	&& $APPLICATION->GetGroupRight("byteeightlab.posting") != "D"
)
{
	$aMenu = Array(
		array(
			"parent_menu" => "global_menu_content",
			"sort" => 110,
			"text" => GetMessage("BEL_POSTING_MENU_NAME"),
			"title"=>GetMessage("BEL_POSTING_MENU_NAME"),
			"url" => "byteeightlab_posting.php?lang=".LANGUAGE_ID,
			"icon" => "byteeightlab_posting_menu_icon",
			"page_icon" => "byteeightlab_posting_menu_icon",
			"items_id" => "byteeightlab_posting",
		),
	);
	return $aMenu;
}
return false;
?>
