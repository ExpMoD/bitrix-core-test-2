<?$module_id = "byteeightlab.posting";
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/admin/task_description.php");

if(!$USER->CanDoOperation('byteeightlab_posting_view_all_settings')) $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
if($REQUEST_METHOD=="GET" && $USER->CanDoOperation('byteeightlab_posting_edit_all_settings') && strlen($RestoreDefaults)>0 && check_bitrix_sessid()){
	COption::RemoveOption($module_id);
}

global $MESS;
IncludeModuleLangFile(__FILE__);

$aTabs = array(
	array("DIV" => "edit_vk", "TAB" => GetMessage("MAIN_TAB_SETING_VK"), "ICON" => "", "TITLE" => GetMessage("MAIN_TAB_TITLE_SETING_VK")),
	array("DIV" => "edit_fb", "TAB" => GetMessage("MAIN_TAB_SETING_FB"), "ICON" => "", "TITLE" => GetMessage("MAIN_TAB_TITLE_SETING_FB")),
	array("DIV" => "edit_access", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "ICON" => "", "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
);

$tabControl = new CAdmintabControl("tabControl", $aTabs);


if($REQUEST_METHOD == "POST" && strlen($Update)>0 && $USER->CanDoOperation('byteeightlab_sitemap_edit_all_settings') && check_bitrix_sessid()){
	COption::RemoveOption($module_id,'VKPOST');
	
	foreach($_POST['VKPOST'] as $key=>$value){
		if($value['IBLOCK']==''||$value['TOKEN']==''||$value['OWNER_ID']==''){
			unset($_POST['VKPOST'][$key]); COption::RemoveOption($module_id,'VKPOST_'.$key);
		}else{			
			$value['OWNER_ID'] = preg_replace('~\D+~','',$value['OWNER_ID']);
			if($value['OWNER_ID_GROUP']=='1') $value['OWNER_ID'] = "-".$value['OWNER_ID'];
			
			$value['FROM_GROUP'] = intval($value['FROM_GROUP']);
			if($value['FROM_GROUP']>3||$value['FROM_GROUP']<1) $value['FROM_GROUP'] = 1;
			
			$value['MESSAGE'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['MESSAGE']));
			if($value['MESSAGE']=='') $value['MESSAGE'] = "PREVIEW_TEXT";			

			$value['ATTACHMENTS'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['ATTACHMENTS']));
			if($value['ATTACHMENTS']=='') $value['ATTACHMENTS'] = "PREVIEW_PICTURE";	

			$value['PUBLISH_DATE'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['PUBLISH_DATE']));
			if($value['PUBLISH_DATE']=='') $value['PUBLISH_DATE'] = "";	

			$value['PUBLICATION'] = intval($value['PUBLICATION']);
			if($value['PUBLICATION']>3||$value['PUBLICATION']<1) $value['PUBLICATION'] = 1;			
			
			$value['PUBLICATION_CODE'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['PUBLICATION_CODE']));
			if($value['PUBLICATION_CODE']=='') $value['PUBLICATION_CODE'] = "";				
		
			$value['URL'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['URL']));
			if($value['URL']=='') $value['URL'] = "";	
		
			$_POST['VKPOST'][$key] = $value;
		}
	}
	$vkpost_num = 0;
	if(count($_POST['VKPOST'])>0){
		foreach($_POST['VKPOST'] as $k=>$VKPOST){ 
			COption::SetOptionString($module_id,'VKPOST_n'.$vkpost_num,json_encode($VKPOST)); $vkpost_num++; 
		}
	}
	COption::SetOptionString($module_id,'VKPOST_NUM',$vkpost_num);
	
	foreach($_POST['FBPOST'] as $key=>$value){
		if($value['IBLOCK']==''||$value['APP_ID']==''||$value['APP_SECRET']==''||$value['EXISTING_ACCESS_TOKEN']==''||$value['PAGE']==''){
			unset($_POST['FBPOST'][$key]); COption::RemoveOption($module_id,'FBPOST_'.$key);
		}else{			
			$value['MESSAGE'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['MESSAGE']));
			if($value['MESSAGE']=='') $value['MESSAGE'] = "PREVIEW_TEXT";			
			
			$value['PICTURE'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['PICTURE']));
			if($value['PICTURE']=='') $value['PICTURE'] = "PREVIEW_PICTURE";				
			
			$value['CAPTION'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['CAPTION']));
			if($value['CAPTION']=='') $value['CAPTION'] = "";

			$value['NAME'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['NAME']));
			if($value['NAME']=='') $value['NAME'] = "";
			
			$value['DESCRIPTION'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['DESCRIPTION']));
			if($value['DESCRIPTION']=='') $value['DESCRIPTION'] = "";			
			
			$value['PUBLICATION'] = intval($value['PUBLICATION']);
			if($value['PUBLICATION']>3||$value['PUBLICATION']<1) $value['PUBLICATION'] = 1;	
			
			$value['PUBLICATION_CODE'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['PUBLICATION_CODE']));
			if($value['PUBLICATION_CODE']=='') $value['PUBLICATION_CODE'] = "";	
			
			$value['PUBLISH_DATE'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['PUBLISH_DATE']));
			if($value['PUBLISH_DATE']=='') $value['PUBLISH_DATE'] = "";	

			$value['LINK'] = trim(preg_replace("/[^a-zA-Z0-9_]/","",$value['LINK']));
			if($value['LINK']=='') $value['LINK'] = "";			
			
			$_POST['FBPOST'][$key] = $value;
		}
	}
	$fbpost_num = 0;
	if(count($_POST['FBPOST'])>0){
		foreach($_POST['FBPOST'] as $k=>$FBPOST){ 
			COption::SetOptionString($module_id,'FBPOST_n'.$fbpost_num,json_encode($FBPOST)); $fbpost_num++; 
		}
	}
	COption::SetOptionString($module_id,'FBPOST_NUM',$fbpost_num);
	
	LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANG."&mid=".urlencode($mid)."&".bitrix_sessid_get()."&mid_menu=1&tabControl_active_tab=".$_REQUEST['tabControl_active_tab']);
}



CModule::IncludeModule($module_id);

$VKPOST = array();
$VKPOST_NUM = COption::GetOptionString($module_id,'VKPOST_NUM',0);
if($VKPOST_NUM>0) for($i=0;$i<$VKPOST_NUM;$i++){ $VKPOST['n'.$i] = (array) json_decode(COption::GetOptionString($module_id,'VKPOST_n'.$i,'[]')); }
$VKPOST_OLD = (array) json_decode(COption::GetOptionString($module_id,'VKPOST','[]'));
foreach($VKPOST_OLD as $key=>$value){ $VKPOST['n'.$i] = (array) $value; $i++; }

$FBPOST = array();
$FBPOST_NUM = COption::GetOptionString($module_id,'FBPOST_NUM',0);
if($FBPOST_NUM>0) for($i=0;$i<$FBPOST_NUM;$i++){ $FBPOST['n'.$i] = (array) json_decode(COption::GetOptionString($module_id,'FBPOST_n'.$i,'[]')); }

$FIELD_DEF = array(
	array("CODE"=>"NAME","NAME"=>"[NAME] - ".GetMessage("FIELD_NAME"),"PROPERTY"=>""),
	array("CODE"=>"CODE","NAME"=>"[CODE] - ".GetMessage("FIELD_CODE"),"PROPERTY"=>""),
	array("CODE"=>"ACTIVE_FROM","NAME"=>"[ACTIVE_FROM] - ".GetMessage("FIELD_ACTIVE_FROM"),"PROPERTY"=>""),
	array("CODE"=>"ACTIVE_TO","NAME"=>"[ACTIVE_TO] - ".GetMessage("FIELD_ACTIVE_TO"),"PROPERTY"=>""),
	array("CODE"=>"DATE_CREATE","NAME"=>"[DATE_CREATE] - ".GetMessage("FIELD_DATE_CREATE"),"PROPERTY"=>""),
	array("CODE"=>"TIMESTAMP_X","NAME"=>"[TIMESTAMP_X] - ".GetMessage("FIELD_TIMESTAMP_X"),"PROPERTY"=>""),
	array("CODE"=>"PREVIEW_TEXT","NAME"=>"[PREVIEW_TEXT] - ".GetMessage("FIELD_PREVIEW_TEXT"),"PROPERTY"=>""),
	array("CODE"=>"PREVIEW_PICTURE","NAME"=>"[PREVIEW_PICTURE] - ".GetMessage("FIELD_PREVIEW_PICTURE"),"PROPERTY"=>""),
	array("CODE"=>"DETAIL_TEXT","NAME"=>"[DETAIL_TEXT] - ".GetMessage("FIELD_DETAIL_TEXT"),"PROPERTY"=>""),
	array("CODE"=>"DETAIL_PICTURE","NAME"=>"[DETAIL_PICTURE] - ".GetMessage("FIELD_DETAIL_PICTURE"),"PROPERTY"=>""),
	array("CODE"=>"TAGS","NAME"=>"[TAGS] - ".GetMessage("FIELD_TAGS"),"PROPERTY"=>""),
	array("CODE"=>"DETAIL_PAGE_URL","NAME"=>"[DETAIL_PAGE_URL] - ".GetMessage("FIELD_DETAIL_PAGE_URL"),"PROPERTY"=>""), 
);

CModule::IncludeModule("iblock");
$IblockArray = array();
$res = CIBlock::GetList(Array(),Array('ACTIVE'=>'Y'));
while($ar_res = $res->Fetch()){ 
	$IblockArray[$ar_res['ID']] = array("NAME"=>$ar_res['NAME'],'FIELDS'=>$FIELD_DEF);
	$properties = CIBlockProperty::GetList(Array(),Array("ACTIVE"=>"Y","IBLOCK_ID"=>$ar_res['ID']));
	while ($prop_fields = $properties->GetNext()){
		if($prop_fields["CODE"]!='') $IblockArray[$ar_res['ID']]['FIELDS'][] = array("CODE"=>$prop_fields["CODE"],"NAME"=>"[".$prop_fields["CODE"]."] - ".$prop_fields["NAME"],"PROPERTY"=>"P");
	}	
}

$tabControl->Begin();

?>

<style>
	.tablestyleinit select, .tablestyleinit input{ width: 250px; }
</style>

<form method="POST" enctype="multipart/form-data" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&lang=<?echo LANG?>&mid_menu=1">
<?=bitrix_sessid_post()?>
<?if(strlen($_REQUEST["back_url_settings"])>0):?>
	<input type="hidden" name="back_url_settings" value="<?=htmlspecialchars($_REQUEST["back_url_settings"])?>">
<?endif?>
<?$tabControl->BeginNextTab();?>
<tr>
	<td align="center">
		<table border="0" cellspacing="0" cellpadding="0" class="internal tablestyleinit" id="VKPOSTTABLE">
			<tr class="heading">
				<td><?=GetMessage("BEL_POSTING_IBLOCK")?></td>
				<td colspan="2"><?=GetMessage("BEL_POSTING_FIELDS")?></td>
				<td><?=GetMessage("BEL_POSTING_PUBLICATION")?></td>
			</tr>
			<?$key = -1; foreach($VKPOST as $value){ $key++;?>
			<tr id="n<?=$key?>">
				<td align="center">
					<?=GetMessage("BEL_POSTING_IBLOCK")?><b style='color: #ED2121;'>*</b><br>
					<select name="VKPOST[n<?=$key?>][IBLOCK]" data="n<?=$key?>" class="iblock_selected"> 
						<option></option>
						<?foreach($IblockArray as $IblockId => $Iblock){?>
							<option value="<?=$IblockId?>"<?=($IblockId==$value['IBLOCK'])?" selected='selected'":"";?>>[<?=$IblockId?>] <?=$Iblock['NAME']?></option>		
						<?}?>
					</select><br><br>
					<input type="checkbox" name="VKPOST[n<?=$key?>][STOK]" value="Y" id="VKPOST[n<?=$key?>][STOK]"<?if(isset($value['STOK'])){?> checked="checked"<?}?> class="adm-designed-checkbox">
					<label class="adm-designed-checkbox-label" for="VKPOST[n<?=$key?>][STOK]" title=""></label> <?=GetMessage("BEL_POSTING_STOK")?>
				</td>			
				<td align="center">
					<?=GetMessage("BEL_POSTING_TOKEN")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="VKPOST[n<?=$key?>][TOKEN]" value="<?=$value['TOKEN']?>">
					<br><br>
					<?=GetMessage("BEL_VK_POSTING_OWNER_ID")?><b style='color: #ED2121;'>*</b><br>
					<?if($value['OWNER_ID'][0]=='-'){$group = true; $value['OWNER_ID'] = substr($value['OWNER_ID'],1); }else{$group = false;}?>
					<input class="adm-input adm-input-calendar" style="width: 90px;" type="text" name="VKPOST[n<?=$key?>][OWNER_ID]" value="<?=$value['OWNER_ID']?>">
					<select style="width: 150px;" name="VKPOST[n<?=$key?>][OWNER_ID_GROUP]">
						<option value="1"<?if($group){?> selected<?}?>><?=GetMessage("BEL_VK_POSTING_OWNER_ID_GROUP")?></option>
						<option value="0"<?if(!$group){?> selected<?}?>><?=GetMessage("BEL_VK_POSTING_OWNER_ID_USER")?></option>
					</select>
					<br><br>
					<?=GetMessage("BEL_VK_POSTING_FROM_GROUP")?><br>
					<select name="VKPOST[n<?=$key?>][FROM_GROUP]"> 
							<option value="1"<?=(1==$value['FROM_GROUP'])?" selected='selected'":"";?>><?=GetMessage("BEL_VK_POSTING_FROM_GROUP_1")?></option>
							<option value="2"<?=(2==$value['FROM_GROUP'])?" selected='selected'":"";?>><?=GetMessage("BEL_VK_POSTING_FROM_GROUP_2")?></option>
							<option value="3"<?=(3==$value['FROM_GROUP'])?" selected='selected'":"";?>><?=GetMessage("BEL_VK_POSTING_FROM_GROUP_3")?></option>
					</select>										
				</td>
				<td align="center">
					<?=GetMessage("BEL_POSTING_MESSAGE")?><br>
					<select name="VKPOST[n<?=$key?>][MESSAGE]" class="n<?=$key?>_select_fields"> 
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['MESSAGE'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>
					</select>					
					<br><br>
					<?=GetMessage("BEL_POSTING_ATTACHMENTS")?><br>
					<select name="VKPOST[n<?=$key?>][ATTACHMENTS]" class="n<?=$key?>_select_fields"> 
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['ATTACHMENTS'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>
					</select>					
					<br><br>
					<?=GetMessage("BEL_VK_POSTING_PUBLISH_DATE")?><br>
					<select name="VKPOST[n<?=$key?>][PUBLISH_DATE]" class="n<?=$key?>_select_fields"> 
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['PUBLISH_DATE'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>
					</select>					
				</td>
				<td align="center">
					<?=GetMessage("BEL_POSTING_PUBLICATION")?><br>
					<select name="VKPOST[n<?=$key?>][PUBLICATION]"> 
							<option value="1"<?=(1==$value['PUBLICATION'])?" selected='selected'":"";?>><?=GetMessage("BEL_POSTING_PUBLICATION_1")?></option>
							<option value="2"<?=(2==$value['PUBLICATION'])?" selected='selected'":"";?>><?=GetMessage("BEL_POSTING_PUBLICATION_2")?></option>
							<option value="3"<?=(3==$value['PUBLICATION'])?" selected='selected'":"";?>><?=GetMessage("BEL_POSTING_PUBLICATION_3")?></option>
					</select>
					<br><br>
					<?=GetMessage("BEL_POSTING_PUBLICATION_CODE")?><br>
					<select name="VKPOST[n<?=$key?>][PUBLICATION_CODE]" data="PUBLICATION_CODE" class="n<?=$key?>_select_fields"> 
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){ if($field["PROPERTY"]=='P'){?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['PUBLICATION_CODE'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}}?>
					</select>					
					<br><br>
					<?=GetMessage("BEL_POSTING_URL")?><br>
					<select name="VKPOST[n<?=$key?>][URL]" class="n<?=$key?>_select_fields"> 
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['URL'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>
					</select>					
				</td>	
			</tr>			
			<?}$key++;?>
			<tr id="n<?=$key?>">
				<td align="center">
					<?=GetMessage("BEL_POSTING_IBLOCK")?><b style='color: #ED2121;'>*</b><br>
					<select name="VKPOST[n<?=$key?>][IBLOCK]" data="n<?=$key?>" class="iblock_selected"> 
						<option></option>
						<?foreach($IblockArray as $IblockId => $Iblock){?>
							<option value="<?=$IblockId?>">[<?=$IblockId?>] <?=$Iblock['NAME']?></option>		
						<?}?>
					</select><br><br>
					<input type="checkbox" name="VKPOST[n<?=$key?>][STOK]" value="Y" id="VKPOST[n<?=$key?>][STOK]" checked="checked" class="adm-designed-checkbox">
					<label class="adm-designed-checkbox-label" for="VKPOST[n<?=$key?>][STOK]" title=""></label> <?=GetMessage("BEL_POSTING_STOK")?>					
				</td>			
				<td align="center">
					<?=GetMessage("BEL_POSTING_TOKEN")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="VKPOST[n<?=$key?>][TOKEN]" value="">
					<br><br>
					<?=GetMessage("BEL_VK_POSTING_OWNER_ID")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" style="width: 90px;" type="text" name="VKPOST[n<?=$key?>][OWNER_ID]" value="">
					<select style="width: 150px;" name="VKPOST[n<?=$key?>][OWNER_ID_GROUP]">
						<option value="1" selected><?=GetMessage("BEL_VK_POSTING_OWNER_ID_GROUP")?></option>
						<option value="0"><?=GetMessage("BEL_VK_POSTING_OWNER_ID_USER")?></option>
					</select>					
					<br><br>
					<?=GetMessage("BEL_VK_POSTING_FROM_GROUP")?><br>
					<select name="VKPOST[n<?=$key?>][FROM_GROUP]"> 
							<option value="1"><?=GetMessage("BEL_VK_POSTING_FROM_GROUP_1")?></option>
							<option value="2"><?=GetMessage("BEL_VK_POSTING_FROM_GROUP_2")?></option>
							<option value="3"><?=GetMessage("BEL_VK_POSTING_FROM_GROUP_3")?></option>
					</select>										
				</td>
				<td align="center">
					<?=GetMessage("BEL_POSTING_MESSAGE")?><br>
					<select name="VKPOST[n<?=$key?>][MESSAGE]" class="n<?=$key?>_select_fields" disabled><option></option></select>					
					<br><br>
					<?=GetMessage("BEL_POSTING_ATTACHMENTS")?><br>
					<select name="VKPOST[n<?=$key?>][ATTACHMENTS]" class="n<?=$key?>_select_fields" disabled><option></option></select>		
					<br><br>
					<?=GetMessage("BEL_VK_POSTING_PUBLISH_DATE")?><br>
					<select name="VKPOST[n<?=$key?>][PUBLISH_DATE]" class="n<?=$key?>_select_fields" disabled><option></option></select>							
				</td>
				<td align="center">
					<?=GetMessage("BEL_POSTING_PUBLICATION")?><br>
					<select name="VKPOST[n<?=$key?>][PUBLICATION]"> 
							<option value="1"><?=GetMessage("BEL_POSTING_PUBLICATION_1")?></option>
							<option value="2"><?=GetMessage("BEL_POSTING_PUBLICATION_2")?></option>
							<option value="3" selected><?=GetMessage("BEL_POSTING_PUBLICATION_3")?></option>
					</select>
					<br><br>
					<?=GetMessage("BEL_POSTING_PUBLICATION_CODE")?><br>
					<select name="VKPOST[n<?=$key?>][PUBLICATION_CODE]" class="n<?=$key?>_select_fields" data="PUBLICATION_CODE" disabled><option></option></select>
					<br><br>
					<?=GetMessage("BEL_POSTING_URL")?><br>
					<select name="VKPOST[n<?=$key?>][URL]" class="n<?=$key?>_select_fields" disabled><option></option></select>					
				</td>	
			</tr>
			<tr>
				<td align="center" colspan="4">
					<input type="button" value="<?=GetMessage("BUT_ADD")?>" style="width: 60%;" onclick="addNewRow('VKPOSTTABLE')">
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center">
		<div class="adm-info-message" align="left">
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_VK_POSTING_TOKEN_TITLE")?></h3>
			<?=GetMessage("BEL_VK_POSTING_TOKEN_TEXT")?><br><br>
			<input class="adm-input adm-input-calendar" type="text" id='URL' value=""> <input type="button" OnClick="GetURL();" value="<?=GetMessage('BEL_VK_POSTING_URL_GET')?>" class="adm-btn-save">
			<br><br>
			<?=GetMessage("BEL_VK_POSTING_URL_GET_TEXT")?><br><br>
			<input class="adm-input adm-input-calendar" type="text" id='TOKEN' value=""> <input type="button" OnClick="GetToken();" value="<?=GetMessage('BEL_VK_POSTING_TOKEN_GET')?>" class="adm-btn-save"> <input class="adm-input adm-input-calendar" type="text" id='TOKEN_TO' value="">
			<script>
				function GetURL(){
					var id = document.getElementById('URL').value;
					window.open('http://oauth.vk.com/authorize?client_id='+id+'&scope=wall,offline,photos&redirect_uri=http://oauth.vk.com/blank.html&response_type=token','_blank');
				}
				
				function GetToken(){
					var TOKEN = document.getElementById('TOKEN').value;
					var TOKEN_NO = true
					TOKEN.split('#')[1].split('&').forEach(function(item) {
						item = item.split('=');
						if(item[0]=='access_token'){ document.getElementById('TOKEN_TO').value = item[1]; TOKEN_NO = false; }
					});
					if(TOKEN_NO) document.getElementById('TOKEN_TO').value = "";
				}
			
			</script>
			<br><br>
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_VK_POSTING_OWNER_ID")?></h3>
			<?=GetMessage("BEL_VK_POSTING_OWNER_ID_INFO")?>
			<br><br>
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_POSTING_ATTACHMENTS")?></h3>
			<?=GetMessage("BEL_VK_POSTING_ATTACHMENTS_INFO")?>
			<br><br>
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_POSTING_PUBLICATION_CODE")?></h3>
			<?=GetMessage("BEL_POSTING_PUBLICATION_CODE_INFO")?>
			<br><br>
			<h3 style="margin-top: 0;"><?=GetMessage("EDIT_DOC_TITLE")?></h3>
			<?=GetMessage("EDIT_DOC")?>		
		</div>
	</td>
</tr>
<?$tabControl->BeginNextTab();?>
<tr>
	<td align="center">
		<table border="0" cellspacing="0" cellpadding="0" class="internal tablestyleinit" id="FBPOSTTABLE">
			<tr class="heading">
				<td><?=GetMessage("BEL_POSTING_IBLOCK")?></td>
				<td><?=GetMessage("BEL_POSTING_ACCESS")?></td>
				<td><?=GetMessage("BEL_POSTING_FIELDS")?></td>
				<td><?=GetMessage("BEL_POSTING_PUBLICATION")?></td>
			</tr>
			<?$key = -1; foreach($FBPOST as $value){ $key++;?>
			<tr id="n<?=$key?>">				
				<td align="center">
					<?=GetMessage("BEL_POSTING_IBLOCK")?><b style='color: #ED2121;'>*</b><br>
					<select name="FBPOST[n<?=$key?>][IBLOCK]" data="n<?=$key?>" class="iblock_selected"> 
						<option></option>
						<?foreach($IblockArray as $IblockId => $Iblock){?>
							<option value="<?=$IblockId?>"<?=($IblockId==$value['IBLOCK'])?" selected='selected'":"";?>>[<?=$IblockId?>] <?=$Iblock['NAME']?></option>		
						<?}?>
					</select>				
				</td>			
				<td align="center">
					<?=GetMessage("BEL_FB_POSTING_APP_ID")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="FBPOST[n<?=$key?>][APP_ID]" value="<?=$value['APP_ID']?>">
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_APP_SECRET")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="FBPOST[n<?=$key?>][APP_SECRET]" value="<?=$value['APP_SECRET']?>">
					<br><br>
					<?=GetMessage("BEL_POSTING_TOKEN")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="FBPOST[n<?=$key?>][EXISTING_ACCESS_TOKEN]" value="<?=$value['EXISTING_ACCESS_TOKEN']?>">
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_PAGE")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="FBPOST[n<?=$key?>][PAGE]" value="<?=$value['PAGE']?>">
				</td>
				<td align="center">
					<?=GetMessage("BEL_POSTING_MESSAGE")?><br>
					<select name="FBPOST[n<?=$key?>][MESSAGE]" class="n<?=$key?>_select_fields">
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['MESSAGE'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>					
					</select>					
					<br><br>
					<?=GetMessage("BEL_POSTING_ATTACHMENTS")?><br>
					<select name="FBPOST[n<?=$key?>][PICTURE]" class="n<?=$key?>_select_fields">
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['PICTURE'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>					
					</select>	
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_NAME")?><br>
					<select name="FBPOST[n<?=$key?>][NAME]" class="n<?=$key?>_select_fields">
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['NAME'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>					
					</select>					
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_DESCRIPTION")?><br>
					<select name="FBPOST[n<?=$key?>][DESCRIPTION]" class="n<?=$key?>_select_fields">
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['DESCRIPTION'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>					
					</select>
				</td>
				<td align="center">
					<?=GetMessage("BEL_POSTING_PUBLICATION")?><br>
					<select name="FBPOST[n<?=$key?>][PUBLICATION]"> 
							<option value="1"<?=(1==$value['PUBLICATION'])?" selected='selected'":"";?>><?=GetMessage("BEL_POSTING_PUBLICATION_1")?></option>
							<option value="2"<?=(2==$value['PUBLICATION'])?" selected='selected'":"";?>><?=GetMessage("BEL_POSTING_PUBLICATION_2")?></option>
							<option value="3"<?=(3==$value['PUBLICATION'])?" selected='selected'":"";?>><?=GetMessage("BEL_POSTING_PUBLICATION_3")?></option>
					</select>
					<br><br>
					<?=GetMessage("BEL_POSTING_PUBLICATION_CODE")?><br>
					<select name="FBPOST[n<?=$key?>][PUBLICATION_CODE]" class="n<?=$key?>_select_fields" data="PUBLICATION_CODE">
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){ if($field["PROPERTY"]=='P'){?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['PUBLICATION_CODE'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}}?>					
					</select>
					<br><br>
					<?=GetMessage("BEL_POSTING_URL")?><br>
					<select name="FBPOST[n<?=$key?>][LINK]" class="n<?=$key?>_select_fields">
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['LINK'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>					
					</select>
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_CAPTION")?><br>
					<select name="FBPOST[n<?=$key?>][CAPTION]" class="n<?=$key?>_select_fields">
						<option></option>
						<?foreach($IblockArray[$value['IBLOCK']]['FIELDS'] as $field){?>
							<?if($field["PROPERTY"]=="P") $field["CODE"] = "PROPERTY_".$field["CODE"]?>
							<option value="<?=$field['CODE']?>"<?=($field['CODE']==$value['CAPTION'])?" selected='selected'":"";?>><?=$field['NAME']?></option>
						<?}?>					
					</select>					
				</td>	
			</tr>			
			<?}$key++;?>
			<tr id="n<?=$key?>">				
				<td align="center">
					<?=GetMessage("BEL_POSTING_IBLOCK")?><b style='color: #ED2121;'>*</b><br>
					<select name="FBPOST[n<?=$key?>][IBLOCK]" data="n<?=$key?>" class="iblock_selected"> 
						<option></option>
						<?foreach($IblockArray as $IblockId => $Iblock){?>
							<option value="<?=$IblockId?>">[<?=$IblockId?>] <?=$Iblock['NAME']?></option>		
						<?}?>
					</select>				
				</td>			
				<td align="center">
					<?=GetMessage("BEL_FB_POSTING_APP_ID")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="FBPOST[n<?=$key?>][APP_ID]" value="">
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_APP_SECRET")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="FBPOST[n<?=$key?>][APP_SECRET]" value="">
					<br><br>
					<?=GetMessage("BEL_POSTING_TOKEN")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="FBPOST[n<?=$key?>][EXISTING_ACCESS_TOKEN]" value="">
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_PAGE")?><b style='color: #ED2121;'>*</b><br>
					<input class="adm-input adm-input-calendar" type="text" name="FBPOST[n<?=$key?>][PAGE]" value="">
				</td>
				<td align="center">
					<?=GetMessage("BEL_POSTING_MESSAGE")?><br>
					<select name="FBPOST[n<?=$key?>][MESSAGE]" class="n<?=$key?>_select_fields" disabled><option></option></select>					
					<br><br>
					<?=GetMessage("BEL_POSTING_ATTACHMENTS")?><br>
					<select name="FBPOST[n<?=$key?>][PICTURE]" class="n<?=$key?>_select_fields" disabled><option></option></select>		
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_NAME")?><br>
					<select name="FBPOST[n<?=$key?>][NAME]" class="n<?=$key?>_select_fields" disabled><option></option></select>
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_DESCRIPTION")?><br>
					<select name="FBPOST[n<?=$key?>][DESCRIPTION]" class="n<?=$key?>_select_fields" disabled><option></option></select>
				</td>
				<td align="center">
					<?=GetMessage("BEL_POSTING_PUBLICATION")?><br>
					<select name="FBPOST[n<?=$key?>][PUBLICATION]"> 
							<option value="1"><?=GetMessage("BEL_POSTING_PUBLICATION_1")?></option>
							<option value="2"><?=GetMessage("BEL_POSTING_PUBLICATION_2")?></option>
							<option value="3" selected><?=GetMessage("BEL_POSTING_PUBLICATION_3")?></option>
					</select>
					<br><br>
					<?=GetMessage("BEL_POSTING_PUBLICATION_CODE")?><br>
					<select name="FBPOST[n<?=$key?>][PUBLICATION_CODE]" class="n<?=$key?>_select_fields" data="PUBLICATION_CODE" disabled><option></option></select>
					<br><br>
					<?=GetMessage("BEL_POSTING_URL")?><br>
					<select name="FBPOST[n<?=$key?>][LINK]" class="n<?=$key?>_select_fields" disabled><option></option></select>
					<br><br>
					<?=GetMessage("BEL_FB_POSTING_CAPTION")?><br>
					<select name="FBPOST[n<?=$key?>][CAPTION]" class="n<?=$key?>_select_fields" disabled><option></option></select>					
				</td>	
			</tr>
			<tr>
				<td align="center" colspan="4">
					<input type="button" value="<?=GetMessage("BUT_ADD")?>" style="width: 60%;" onclick="addNewRow('FBPOSTTABLE')">
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center">
		<div class="adm-info-message" align="left">
			<h3 style="margin-top: 0;"><?=GetMessage("BEL_POSTING_PUBLICATION_CODE")?></h3>
			<?=GetMessage("BEL_POSTING_PUBLICATION_CODE_INFO")?>
			<br><br>
			<h3 style="margin-top: 0;"><?=GetMessage("EDIT_DOC_TITLE")?></h3>
			<?=GetMessage("EDIT_DOC")?>	
		</div>
	</td>
</tr>
<?$tabControl->BeginNextTab();?>
<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights2.php");?>
<?$tabControl->Buttons();?>
<script>
	function RestoreDefaults(){
		if(confirm('<?=AddSlashes(GetMessage("BUT_DEFAULTS_WARNING"))?>'))
			window.location = "<?= $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?=LANG?>&mid=<?=urlencode($mid)?>&<?=bitrix_sessid_get()?>&mid_menu=1";
	}
	function GoStatistic(){
		if(confirm('<?=AddSlashes(GetMessage("BUT_STATISTIC_WARNING"))?>'))
			window.location = "/bitrix/admin/byteeightlab_posting.php?lang=<?=LANG?>";
	}	
</script>
<input type="submit" <?if (!$USER->CanDoOperation('byteeightlab_sitemap_edit_all_settings')) echo "disabled" ?> name="Update" value="<?=GetMessage('BUT_SAVE')?>">
<input type="hidden" name="Update" value="Y">
<input <?if (!$USER->CanDoOperation('byteeightlab_sitemap_edit_all_settings')) echo "disabled" ?> type="button" OnClick="RestoreDefaults();" value="<?=GetMessage('BUT_DEFAULTS')?>">
<input type="button" OnClick="GoStatistic();" value="<?=GetMessage('BUT_STATISTIC')?>" class="adm-btn-save">
<?$tabControl->End();?>
</form>

<script>

var iblocks = {
<?$i=0;foreach($IblockArray as $IblockId => $Iblock){?>
	<?if($i>0){?>,<?}?><?=$IblockId?>:[
		<?foreach($Iblock['FIELDS'] as $field){?>
			['<?=$field['CODE']?>','<?=$field['NAME']?>','<?=$field['PROPERTY']?>'],
		<?}?>
	]
<?$i++;}?>
};

var iblock_selected = document.getElementsByClassName('iblock_selected');
for(var i=0;i<iblock_selected.length;i++){
	iblock_selected[i].onchange = function(){
		if(this.value==''){
			var select_fields = document.getElementsByClassName(this.attributes.data.value+'_select_fields');
			for(var i0=0;i0<select_fields.length;i0++){
				select_fields[i0].disabled = true;
				select_fields[i0].innerHTML = '';
				select_fields[i0].value = '';
			}
		}else{
			var optionHTML = '<option></option>';
			for(var i1=0;i1<iblocks[this.value].length;i1++){
				if(iblocks[this.value][i1][2]=='P') iblocks[this.value][i1][0] = "PROPERTY_"+iblocks[this.value][i1][0];
				optionHTML += "<option value='"+iblocks[this.value][i1][0]+"'>"+iblocks[this.value][i1][1]+"</option>";
			}
			var optionHTML_p = '<option></option>';
			for(var i1=0;i1<iblocks[this.value].length;i1++){ if(iblocks[this.value][i1][2]=='P'){
				optionHTML_p += "<option value='"+iblocks[this.value][i1][0]+"'>"+iblocks[this.value][i1][1]+"</option>";
			}}			
			var select_fields = document.getElementsByClassName(this.attributes.data.value+'_select_fields');
			for(var i0=0;i0<select_fields.length;i0++){
				select_fields[i0].disabled = false;
				console.dir(select_fields[i0].attributes);
				
				if(select_fields[i0].attributes.data!=undefined){
					if(select_fields[i0].attributes.data.value=='PUBLICATION_CODE'){
						select_fields[i0].innerHTML = optionHTML_p;
					}else select_fields[i0].innerHTML = optionHTML;
				}else  select_fields[i0].innerHTML = optionHTML;
				
			}		
		}
		
	}
}
function addNewRow(tableID, row_to_clone)
{
	var tbl = document.getElementById(tableID);
	var cnt = tbl.rows.length;
	if(row_to_clone == null)
		row_to_clone = -2;
	var oRow = tbl.insertRow(cnt+row_to_clone+1);
	for(var i = tbl.rows[cnt+row_to_clone].cells.length-1; i>=0; i--){
		var sHTML = tbl.rows[cnt+row_to_clone].cells[i].innerHTML;
		var oCell = oRow.insertCell(0);
		oCell.align = tbl.rows[cnt+row_to_clone].cells[i].align;
		var s, e, n, p;
		p = 0;
		while(true)
		{
			s = sHTML.indexOf('[n',p);
			if(s<0)break;
			e = sHTML.indexOf(']',s);
			if(e<0)break;
			n = parseInt(sHTML.substr(s+2,e-s));
			sHTML = sHTML.substr(0, s)+'[n'+(++n)+']'+sHTML.substr(e+1);
			p=s+1;
		}
		p = 0;
		while(true)
		{
			s = sHTML.indexOf('__n',p);
			if(s<0)break;
			e = sHTML.indexOf('_',s+2);
			if(e<0)break;
			n = parseInt(sHTML.substr(s+3,e-s));
			sHTML = sHTML.substr(0, s)+'__n'+(++n)+'_'+sHTML.substr(e+1);
			p=e+1;
		}
		p = 0;
		while(true)
		{
			s = sHTML.indexOf('__N',p);
			if(s<0)break;
			e = sHTML.indexOf('__',s+2);
			if(e<0)break;
			n = parseInt(sHTML.substr(s+3,e-s));
			sHTML = sHTML.substr(0, s)+'__N'+(++n)+'__'+sHTML.substr(e+2);
			p=e+2;
		}
		p = 0;
		while(true)
		{
			s = sHTML.indexOf('xxn',p);
			if(s<0)break;
			e = sHTML.indexOf('xx',s+2);
			if(e<0)break;
			n = parseInt(sHTML.substr(s+3,e-s));
			sHTML = sHTML.substr(0, s)+'xxn'+(++n)+'xx'+sHTML.substr(e+2);
			p=e+2;
		}
		p = 0;
		while(true)
		{
			s = sHTML.indexOf('%5Bn',p);
			if(s<0)break;
			e = sHTML.indexOf('%5D',s+3);
			if(e<0)break;
			n = parseInt(sHTML.substr(s+4,e-s));
			sHTML = sHTML.substr(0, s)+'%5Bn'+(++n)+'%5D'+sHTML.substr(e+3);
			p=e+3;
		}
		oCell.innerHTML = sHTML;
	}
	var patt = new RegExp ("<"+"script"+">[^\000]*?<"+"\/"+"script"+">", "ig");
	var code = sHTML.match(patt);
	if(code)
	{
		for(var i = 0; i < code.length; i++)
		{
			if(code[i] != '')
			{
				s = code[i].substring(8, code[i].length-9);
				jsUtils.EvalGlobal(s);
			}
		}
	}
	if (BX && BX.adminPanel)
	{
		BX.adminPanel.modifyFormElements(oRow);
		BX.onCustomEvent('onAdminTabsChange');
	}
	setTimeout(function() {
		var r = BX.findChildren(oCell, {tag: /^(input|select|textarea)$/i});
		if (r && r.length > 0)
		{
			for (var i=0,l=r.length;i<l;i++)
			{
				if (r[i].form && r[i].form.BXAUTOSAVE)
					r[i].form.BXAUTOSAVE.RegisterInput(r[i]);
				else
					break;
			}
		}
	}, 10);
}
</script>