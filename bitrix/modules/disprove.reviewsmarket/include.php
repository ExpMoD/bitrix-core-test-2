<?
IncludeModuleLangFile(__FILE__);

class DRM{
	public static function GetList($nav,$set){
		global $DB;
		if($nav){
			if($nav[0])
				$count = $nav[0];
			if($nav[1])
				$page = $nav[1];
			$sql_i = ' ORDER BY id desc LIMIT '.$count*($page-1).','.$count;

		}
		$sql = 'select * from d_reviews_market'.$sql_i;
		$res = $DB->Query($sql, true);
		while ($row = $res->Fetch()){
			$arResult["arReview"][] = $row;
		}
		$sql = 'select SHOP_ID from d_reviews_settings';
		$res = $DB->Query($sql, true);
		if ($row = $res->Fetch()){
			$arResult["SHOP_ID"] = $row["SHOP_ID"];
		}
		$sql = 'select count(ID) from d_reviews_market';
		$res = $DB->Query($sql, true);

		if ($row = $res->Fetch()){
			$arResult["COUNT"] = $row["count(ID)"];
		}
		return $arResult;
	}
	public static function SAdd($key,$shop){
		global $DB;
		$sql = 'select * from d_reviews_settings';
		$results = $DB->Query($sql, true);
		if(!$row = $results->Fetch()){
			$sql = 'INSERT INTO d_reviews_settings (SHOP_KEY,SHOP_ID) VALUES ("'.$key.'",'.$shop.')';
			$result = $DB->Query($sql, true);
		}else{
			$sql = 'UPDATE `d_reviews_settings` SET `SHOP_KEY`="'.$key.'", `SHOP_ID`='.$shop.'';
			$DB->Query($sql, true);
		}
	}
	public static function GetKey(){
		global $DB;
		$sql = 'select SHOP_KEY, SHOP_ID from d_reviews_settings';
		$results = $DB->Query($sql, true);
		
		if ($row = $results->Fetch()){
			$r["SHOP_KEY"] = $row["SHOP_KEY"];
			$r["SHOP_ID"] = $row["SHOP_ID"];
		}
		return $r;
	}	
}
class Dyandexreviewsload{
  public static function cUrlYandexLoad($bAgent=true){	  
	if(function_exists('curl_init')) {
		global $DB;
		$sql = 'select SHOP_KEY, SHOP_ID from d_reviews_settings';
			$results = $DB->Query($sql, true);
			if ($row = $results->Fetch()){
				$key = $row["SHOP_KEY"];
				$shop_id = $row["SHOP_ID"];
			}
		if($key || $shop_id){
			$i = 0;
			while($i < $i+1){
				$url = "https://api.content.market.yandex.ru/v1/shop/".$shop_id."/opinion.xml?count=30&page=".$i;
				$headers = array(
				  "GET '".$url."' HTTP/1.1",
				  "Host: api.content.market.yandex.ru",
				  "Accept: */*",
				  "Authorization: ".$key
				);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				curl_setopt($ch, CURLOPT_AUTOREFERER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$data = curl_exec($ch);
				if (curl_errno($ch)) {
					$j = 0;
				} else {
					$xml = new SimpleXMLElement($data);
					$j = 0;
					foreach ($xml as $ell){
						$j++;
						$dat = $ell->date;
						$dd = explode(".",$dat[0]);
						$ddd = explode("T",$dd[0]);
						$ddate_u = $ddd[0];
						$DATE = date('Y.m.d', strtotime($ddate_u)).' '.$ddd[1];
						
						$AUTHOR = $ell->author;
						$AUTHOR ? '' : $AUTHOR = '������';
						
						$VALUE = $ell->pro;
						$NOTES = $ell->text;
						$SHORT = $ell->contra;
						
						$VALUE = str_replace('"',"'",$VALUE);
						$NOTES = str_replace('"',"'",$NOTES);
						$SHORT = str_replace('"',"'",$SHORT);
						
						$attr = $ell->attributes();
						foreach ($attr as $k=>$child){
							if($k=='id'){
								$ID_MARKET = $child[0];
							}
							if($k=='grade'){
								$RATING = $child[0];
							}
						} //foreach
						$RATING = $RATING+3;
						
						if($ID_MARKET){
							$true = true;
							$sql = 'select ID_MARKET from d_reviews_market where ID_MARKET='.$ID_MARKET;
							$results = $DB->Query($sql, true);
							if ($row = $results->Fetch()){
								$true = false;
							}
							
							if($true){
								$dsql = 'INSERT INTO d_reviews_market (ID_MARKET,RATING,AUTHOR,DATE,NOTES,VALUE,SHORT) 
									VALUES ("'.$ID_MARKET.'",'.$RATING.',"'.$AUTHOR.'","'.$DATE.'","'.$NOTES.'","'.$VALUE.'","'.$SHORT.'")';
								$DB->Query($dsql, true);
							}
						}
					}//foreach
				  curl_close($ch);
				}//if
				if($j==0){
					break;
				}else $i++;	
			}//while
		}//if
	}//function_exists
		
	if ($bAgent) {
		return 'Dyandexreviewsload::cUrlYandexLoad();';
	}
  }
}		
?>