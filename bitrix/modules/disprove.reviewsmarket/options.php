<?
if(!$USER->IsAdmin()) return;
$MODULE_ID = 'disprove.reviewsmarket';
global $DB;
if(!CModule::IncludeModule("disprove.reviewsmarket")){	
	ShowError(GetMessage("MARKETSETTINGS_ERROR"));
	return;
}
if(isset($_REQUEST['Update'])){
	$key_id = htmlspecialcharsbx ($_REQUEST["key_id"]);
    $shop_id = htmlspecialcharsbx ($_REQUEST["shop_id"]);
	$dResult = DRM::SAdd($key_id,$shop_id);
}
if(isset($_REQUEST['Apply'])){
	$lResult = Dyandexreviewsload::cUrlYandexLoad();
}
$dResult = DRM::GetKey();
$SHOP_KEY = $dResult["SHOP_KEY"];
$SHOP_ID = $dResult["SHOP_ID"];
if($_REQUEST["DPAGE"]){
	$PAGE = htmlspecialcharsbx($_REQUEST["DPAGE"]);
}else $PAGE = 1;
$REVIEWS_COUNT = 10;
$dResult = DRM::GetList(array($REVIEWS_COUNT,$PAGE),array());
	$count = $dResult["COUNT"];
	$Result["ITEMS"] = $dResult["arReview"];
	$Result["SHOP_ID"] = $dResult["SHOP_ID"];
	$Result["COUNT"] = $dResult["COUNT"];
IncludeModuleLangFile(__FILE__);
$aTabs = array(
	array(
		"DIV" => "marketsettings",
		"TAB" => GetMessage("MARKETSETTINGS_TAB_SET"),
		"TITLE" => GetMessage("MARKETSETTINGS_TAB_TITLE_SET")
	),
	array(
		"DIV" => "marketcomments",
		"TAB" => GetMessage("MARKETCOMMENTS_TAB_SET"),
		"TITLE" => GetMessage("MARKETCOMMENTS_TAB_TITLE_SET")
	),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
if($Result["COUNT"] > $REVIEWS_COUNT){
	$p = ceil($Result["COUNT"] / $REVIEWS_COUNT);
	$i=1;
	$nav = '<ul class="pagination_dreviews">';
	while($i<=$p){
		if($i == $PAGE){
			$nav .= '<li style="display:inline-block; list-style:none;margin-right: 10px;" class="dactive">'.$i.'</li>';
		}else{
			$nav .= '<li style="display:inline-block; list-style:none;margin-right: 10px;">
			<a href="'.$APPLICATION->GetCurPageParam("tabControl_active_tab=marketcomments&DPAGE=".$i, array("DPAGE"))."#authorize".'">'.$i.'</a></li>';
		}
		$i++;
	}
	$nav .= '</ul><div style="clear:both;"></div>';
	$Result["NAV_STRING"] = $nav;
}
$tabControl->Begin();
?>
<form method="post" action="" name="market_settings">
<?$tabControl->BeginNextTab();?>	
<tr class="heading">
	<td colspan="4" align="center"><?=GetMessage("MARKETSETTINGS_HEADER")?></td> 
</tr>

<tr>
    <td width="40%" class="adm-detail-content-cell-l"><?=GetMessage("MARKETSETTINGS_KEY")?>:</td>
    <td width="60%" class="adm-detail-content-cell-r">
		<input type="text" size="30" value="<? $SHOP_KEY ? print $SHOP_KEY : print $_REQUEST["key_id"];?>" name="key_id">     
    </td>
</tr>
<tr>
    <td width="40%" class="adm-detail-content-cell-l"><?=GetMessage("MARKETSETTINGS_ID_SHOP")?>:</td>
    <td width="60%" class="adm-detail-content-cell-r">
		<input type="text" size="30" value="<? $SHOP_ID ? print $SHOP_ID : print $_REQUEST["shop_id"];?>" name="shop_id">     
    </td>
</tr>
<?$tabControl->BeginNextTab();?>

<tr class="heading">
	<td colspan="4" align="center"><?=GetMessage("MARKETCOMMENTS_HEADER")?> (<?=$Result["COUNT"];?>) <a target="_blank" href="http://market.yandex.ru/shop/<?=$Result["SHOP_ID"];?>/reviews"><?=GetMessage("MARKETSETTINGS_SEE_ALL")?></a></td>
</tr>
<div class="dreview_yandex_block"><div class="dreview_yandex_block_innder">
<style>
.stars_review .link_review_starts{
	font:14px 'Open Sans', sans-serif;
	font-weight:bold;
	color:#55565a;
	padding-left:5px;
}
.review_one{
	padding-bottom:40px;	
}
.review_one p{
	padding-top:5px;
	font:14px 'Open Sans', sans-serif;
	color:#55565a;
	line-height:28px;
	padding-bottom:20px;
}
.review_one_right{text-align:right;}
.name_review_one{
	color:#55565a;
	font-style:italic;
	font:14px 'Open Sans', sans-serif;
}
.date_review_one{
	padding-left:5px;
	color:#c1c1c2;
	font-style:italic;
	font:14px 'Open Sans', sans-serif;
}
.stars_review span{
	display:inline-block;
	width:15px;
	height:12px;
	overflow:hidden;
	background:url(/bitrix/components/disprove/reviewsmarket/templates/.default/images/stars.png) no-repeat;
}
.stars_review .link_review_starts {
	font: 14px 'Open Sans', sans-serif;
	font-weight: bold;
	color: #55565a;
	padding-left: 5px;
}
.link_review_starts {
	background: none!important;
	width: auto!important;
	overflow: inherit!important;
	font: 12px 'Open Sans', sans-serif;
}
.stars_review span.stars_review_none{
	background-position-x:-16px;	
}
</style>
<?  
$arr_stars = array(GetMessage("MARKETSETTINGS_S_1"),GetMessage("MARKETSETTINGS_S_2"),GetMessage("MARKETSETTINGS_S_3"),GetMessage("MARKETSETTINGS_S_4"),GetMessage("MARKETSETTINGS_S_5"));
if($Result["ITEMS"]){
	foreach($Result["ITEMS"] as $r){?>
	<tr>
		<td class="adm-detail-content-cell">
	<div class="review_one v<?=$r["ID_MARKET"];?>" id="<?=$r["ID"];?>">
	  <div class="stars_review">
	  <? 
	  $i=0;
	  while($i < $r["RATING"]){?>
		<span class="stars_review_gold"></span>
		<? $i++;} 
		while($i<5){
			echo '<span class="stars_review_none"></span>';
			$i++;
		}
		?>                    
		<span class="link_review_starts"><?=$arr_stars[$r["RATING"]-1];?></span>
	  </div>
	<? if($r["VALUE"]){?> 
	<h2 style="font: 13px 'Open Sans', sans-serif; font-weight: bold;  color: #55565a; padding-top: 10px;"><?=GetMessage("MARKETSETTINGS_VALUE");?></h2>
	<p style="padding-bottom: 5px;"><?=$r["VALUE"];?></p>
	<? }if($r["SHORT"]){?>          
	<h2 style="font: 13px 'Open Sans', sans-serif; font-weight: bold; color: #55565a; padding-top: 10px;"><?=GetMessage("MARKETSETTINGS_SHORT");?></h2> 
	<p style="padding-bottom: 5px;"><?=$r["SHORT"];?></p>         
	<? }if($r["NOTES"]){?>                 
	<h2 style="font: 13px 'Open Sans', sans-serif;  font-weight: bold;  color: #55565a; padding-top: 10px;"><?=GetMessage("MARKETSETTINGS_NOTES");?></h2>
	<p><?=$r["NOTES"];?></p>
	<? }?> 
		<div class="review_one_right">
			<span class="name_review_one"><?=$r["AUTHOR"];?></span> <span class="date_review_one"><?=$r["DATE"];?></span>
		</div>
	</div>
	<hr  />
	</td></tr>
<?  }?>
</div></div>
<? if($Result["NAV_STRING"]) echo $Result["NAV_STRING"];
}else ShowError(GetMessage("MARKETSETTINGS_NONE"));
$tabControl->Buttons();
?>
	<input type="submit" class="adm-btn-save" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>">
	<? if($SHOP_KEY && $SHOP_ID){?>
    <input type="submit" name="Apply" value="<?=GetMessage("MARKETSETTINGS_LOAD")?>">
	<? }?>
	<?=bitrix_sessid_post();?>
<?$tabControl->End();?>
</form>
