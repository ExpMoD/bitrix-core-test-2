<?
$MESS['DISPROVE_MODULE_NAME'] = "Отзывы яндекс.Market";
$MESS['DISPROVE_MODULE_DESC'] = "Выгрузка отзывов об магазине из yandex.Market и вывод на сайте с помощью готового компонента на страницу сайта. Компонент полностью адаптируется и совместим с композитной технологией.";  
$MESS['DISPROVE_NEED_MODULES_CURL'] = 'Для установки данного решения необходимо наличие php библиотеки <a href="http://php.net/manual/ru/book.curl.php">cURL</a>.';
$MESS ['DISPROVE_NEED_RIGHT_VER'] = "Для установки данного решения необходима версия главного модуля #NEED# или выше.";
$MESS ['DISPROVE_INSTALL_MODULE_NOTIFICATION'] = 'Вы установили модуль "Отзывы яндекс.Market", для работы с модулем необходимо его <a href="/bitrix/admin/settings.php?lang=ru&mid=disprove.reviewsmarket">настроить</a>.';
?>