<?
$MESS['MARKETSETTINGS_TAB_SET'] = "Маркет";
$MESS['MARKETSETTINGS_TAB_TITLE_SET'] = "Настройка параметров yandex.Маркета";
$MESS['MARKETCOMMENTS_TAB_SET'] = "Отзывы";
$MESS['MARKETCOMMENTS_TAB_TITLE_SET'] = "Все отзывы с маркета";
$MESS['MARKETCOMMENTS_HEADER'] = "Отзывы с маркета";
$MESS['MARKETSETTINGS_HEADER'] = "Укажите Ваш ключ и ID вашего магазина";
$MESS['MARKETSETTINGS_KEY'] = "Ваш ключ";
$MESS['MARKETSETTINGS_ID_SHOP'] = "ID вашего магазина";
$MESS['MARKETSETTINGS_LOAD'] = "Выгрузить";
$MESS['MARKETSETTINGS_ERROR'] = 'У вас ошибка, обратитесь администратору по почте panaetov@disprove.ru';
$MESS['MARKETSETTINGS_SEE_ALL'] = "Посмотреть все отзывы";
$MESS['MARKETSETTINGS_S_1'] = "ужасный магазин";
$MESS['MARKETSETTINGS_S_2'] = "плохой магазин";
$MESS['MARKETSETTINGS_S_3'] = "обычный магазин";
$MESS['MARKETSETTINGS_S_4'] = "хороший магазин";
$MESS['MARKETSETTINGS_S_5'] = "отличный магазин";
$MESS['MARKETSETTINGS_VALUE'] = "Достоинства";
$MESS['MARKETSETTINGS_SHORT'] = "Недостатки";
$MESS['MARKETSETTINGS_NOTES'] = "Комментарий";
$MESS['MARKETSETTINGS_NONE'] = "Сейчас у вас нет отзывов";
?>