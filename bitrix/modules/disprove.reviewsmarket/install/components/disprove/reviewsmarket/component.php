<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;
$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;
$arNavigation = false;	
if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arNavigation, $arrFilter))){
	if(!CModule::IncludeModule("disprove.reviewsmarket")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	if($_REQUEST["DPAGE"]){
		$PAGE = htmlspecialcharsbx($_REQUEST["DPAGE"]);
	}else $PAGE = 1;
	if($arParams["REVIEWS_COUNT"] <= 1)
		$arParams["REVIEWS_COUNT"] = 1;
	$dResult = DRM::GetList(array($arParams["REVIEWS_COUNT"],$PAGE),array());
		$count = $dResult["COUNT"];
		$arResult["SHOP_ID"] = $dResult["SHOP_ID"];
		$arResult["ITEMS"] = $dResult["arReview"];
	if($count > $arParams["REVIEWS_COUNT"]){
		$p = ceil($count / $arParams["REVIEWS_COUNT"]);
		$i=1;
		$nav = '<ul class="pagination_dreviews">';
		while($i<=$p){
			if($i == $PAGE){
				$nav .= '<li class="dactive">'.$i.'</li>';
			}else $nav .= '<li><a href="'.$APPLICATION->GetCurPage().'?DPAGE='.$i.'">'.$i.'</a></li>';
			$i++;
		}
		$nav .= '</ul><div style="clear:both;"></div>';
		$arResult["NAV_STRING"] = $nav;
	}
}
$this->IncludeComponentTemplate();
?>