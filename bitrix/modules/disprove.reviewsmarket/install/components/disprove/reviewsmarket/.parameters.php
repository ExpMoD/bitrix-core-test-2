<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	
$arComponentParameters = array(
	"GROUPS" => array(),
	"PARAMETERS" => array(
		"REVIEWS_COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("T_DRM_COUNT"), 
			"TYPE" => "STRING",
			"DEFAULT" => "20",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),  
		"CACHE_GROUPS" => array(
			"PARENT" => "CACHE_SETTINGS",
			"NAME" => GetMessage("CP_BNL_CACHE_GROUPS"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
	),
);
?>
