<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_IBLOCK_DESC_LIST"),
	"DESCRIPTION" => GetMessage("T_IBLOCK_DESC_LIST_DESC"),
	"PATH" => array(
	"ID" => "DisproveStudio",
		"CHILD" => array(
			"ID" => "loadpage_reviews",
			"NAME" => GetMessage("T_COMPONENT_NAME") 
		)
	),
	"ICON" => "/images/icon.gif",
);

?>