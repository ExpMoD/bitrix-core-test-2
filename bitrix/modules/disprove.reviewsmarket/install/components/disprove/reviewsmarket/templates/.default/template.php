<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */?>
<?$this->setFrameMode(true);?>
<div class="dreview_block">
	<div class="review_h1">
    	<h1><?=GetMessage("DISPROVE_REVIEWSMARKET_OTZYVY")?></h1> 
    	<a href="http://market.yandex.ru/shop/<?=$arResult["SHOP_ID"];?>/reviews"><span><?=GetMessage("DISPROVE_REVIEWSMARKET_NAPISATQ_I_POSMOTRET")?><br /> <?=GetMessage("DISPROVE_REVIEWSMARKET_NA_ANDEKS_MARKET")?></span></a>
    </div>
    <?
	$arr = array(GetMessage("DISPROVE_REVIEWSMARKET_UJASNYY_MAGAZIN"),GetMessage("DISPROVE_REVIEWSMARKET_PLOHOY_MAGAZIN"),GetMessage("DISPROVE_REVIEWSMARKET_OBYCNYY_MAGAZIN"),GetMessage("DISPROVE_REVIEWSMARKET_HOROSIY_MAGAZIN"),GetMessage("DISPROVE_REVIEWSMARKET_OTLICNYY_MAGAZIN"));
	?>
	<div class="review_page_block">
		<? 
		if($arResult["ITEMS"]){
			foreach($arResult["ITEMS"] as $arItem):?>
			<div class="review_one">
			  <div class="stars">
			  <? $i=0;
				while($i<$arItem["RATING"]){
					echo '<span class="stars_review_gold"></span>';
					$i++;
				}
				while($i<5){
					echo '<span class="stars_review_none"></span>';
					$i++;
				}?>
				<span class="link_review_starts"><?=$arr[$arItem["RATING"]-1]?></span>
			  </div>
			<? if($arItem["VALUE"]):?> 
				<h2><?=GetMessage("DISPROVE_REVIEWSMARKET_DOSTOINSTVA")?></h2>
				<p style="padding-bottom: 5px;"><?=$arItem["VALUE"];?></p>
			<? endif;?> 
			<? if($arItem["SHORT"]):?>                
				<h2><?=GetMessage("DISPROVE_REVIEWSMARKET_NEDOSTATKI")?></h2> 
				<p style="padding-bottom: 5px;"><?=$arItem["SHORT"];?></p> 
			<? endif;?>                 
				<h2><?=GetMessage("DISPROVE_REVIEWSMARKET_KOMMENTARIY")?></h2>
				<p><?=$arItem["NOTES"];?></p>
				<div class="review_one_right">
					<span class="name_review_one"><?=$arItem["AUTHOR"];?></span> <span class="date_review_one"><?=$arItem["DATE"];?></span>
				</div>
			</div>
			<?endforeach;
        }
        ?>
	</div>
    <?if($arResult["NAV_STRING"]):?>
        <?=$arResult["NAV_STRING"]?>
    <?endif;?>
</div>