<?
global $MESS;
$PathInstall = str_replace('\\', '/', __FILE__);
$PathInstall = substr($PathInstall, 0, strlen($PathInstall)-strlen('/index.php'));
IncludeModuleLangFile($PathInstall.'/install.php');

class disprove_reviewsmarket extends CModule{
	var $MODULE_ID = "disprove.reviewsmarket";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = 'N';
    public $NEED_MAIN_VERSION = '14.0.0';
	function disprove_reviewsmarket(){
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("DISPROVE_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("DISPROVE_MODULE_DESC");
		$this->PARTNER_NAME = 'Disprove'; 
		$this->PARTNER_URI = 'http://www.disprove.ru';
    }
    public function DoInstall(){
		global $APPLICATION, $step;
		$this->errors = false;
        if(!function_exists('curl_init')) {
            ShowError(GetMessage('DISPROVE_NEED_MODULES_CURL'));
            return;
        }
		$this->InstallDB();
		if($this->errors !== false){
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}
        if (CheckVersion(SM_VERSION, $this->NEED_MAIN_VERSION)) {
			$this->InstallFiles();
            RegisterModule($this->MODULE_ID);
			CAgent::AddAgent('Dyandexreviewsload::cUrlYandexLoad();', 'disprove.reviewsmarket', 'Y', 3600);
			$ar = Array(
				"MESSAGE" => GetMessage("DISPROVE_INSTALL_MODULE_NOTIFICATION"),
				"TAG" => "DISPROVE_REVIEW_INSTALL",
				"MODULE_ID" => "disprove.reviewsmarket",
				"ENABLE_CLOSE" => "Y"
			);
			CAdminNotify::Add($ar);
        }
    }
    public function DoUninstall(){
		global $APPLICATION, $step;
		$this->errors = false;
		$this->UnInstallDB();
        $this->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
		CAgent::RemoveModuleAgents('disprove.reviewsmarket');
		CAdminNotify::DeleteByModule("disprove.reviewsmarket");
    }
	public function InstallDB(){
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		if(!$DB->Query("SELECT 'x' FROM d_reviews_market", true)){
			$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/disprove.reviewsmarket/install/db/".$DBType."/install.sql");
		}
	}
	public function UnInstallDB($arParams = array()){
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/disprove.reviewsmarket/install/db/".$DBType."/uninstall.sql");
		if($this->errors !== false){
			$APPLICATION->ThrowException(implode("", $this->errors));
			return false;
		}
	}
	public function InstallFiles(){
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/disprove.reviewsmarket/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		return true;
	}
	public function UnInstallFiles(){
		DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/disprove.reviewsmarket/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		return true;
	}
}
?>