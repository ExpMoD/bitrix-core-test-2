create table if not exists d_reviews_market
(
	ID int not null auto_increment,
	ID_MARKET int not null,
	RATING int not null,
	AUTHOR varchar(100) null,	
	DATE datetime not null,
	NOTES varchar(250) null,
	VALUE varchar(250) null,
	SHORT varchar(250) null,
	primary key (ID)
);

create table if not exists d_reviews_settings
(
	ID int not null auto_increment,
	SHOP_KEY varchar(100) null,
	SHOP_ID int not null,
	primary key (ID)
);