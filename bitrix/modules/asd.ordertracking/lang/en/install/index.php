<?php
$MESS ['ASD_ORDERTR_MODULE_NAME'] = 'Отслеживание заказов в Яндекс.Метрика и Google Analytics';
$MESS ['ASD_ORDERTR_MODULE_DESCRIPTION'] = 'Отслеживание заказов в Яндекс.Метрика и Google Analytics.';
$MESS ['ASD_ORDERTR_MODULE_SETTINGS'] = 'Перейти на <a href="settings.php?mid=asd.ordertracking&amp;lang=#LANG#">страницу настроек модуля</a>.';
$MESS ['ASD_ORDERTR_PARTNER_NAME'] = 'Долганин Антон';
$MESS ['ASD_ORDERTR_NEED_RIGHT_VER'] = 'Для установки данного решения необходима версия главного модуля #NEED# или выше.';
$MESS ['ASD_ORDERTR_NEED_MODULES'] = 'Для установки данного решения необходимо наличие модуля #MODULE#.';