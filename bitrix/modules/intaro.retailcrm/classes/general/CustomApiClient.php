<?php
/**
 * API v5 client class
 */

namespace RetailCrm;

use RetailCrm\Http\Client;
use RetailCrm\Response\ApiResponse;

class CustomApiClient extends ApiClient
{
    const VERSION = 'v5';
    protected $client;
    protected $siteCode;

    /**
     * Client creating
     *
     * @param string $url api url
     * @param string $apiKey api key
     * @param string $site site code
     *
     * @throws \InvalidArgumentException
     *
     * @return mixed
     */

    public function __construct($url, $apiKey, $site = null)
    {
        if ('/' !== $url[strlen($url) - 1]) {
            $url .= '/';
        }
        $url = $url . 'api/' . self::VERSION;
        $this->client = new Client($url, array('apiKey' => $apiKey));
        $this->siteCode = $site;
    }

    /**
     * Get order by id or externalId
     *
     * @param string $id order identificator
     * @param string $by (default: 'externalId')
     * @param string $site (default: null)
     *
     * @throws \InvalidArgumentException
     * @throws \RetailCrm\Exception\CurlException
     * @throws \RetailCrm\Exception\InvalidJsonException
     *
     * @return ApiResponse
     */
    public function ordersGet($id, $by = 'externalId', $site = null)
    {
        $this->checkIdParameter($by);

        return $this->client->makeRequest(
            "/orders/$id",
            Client::METHOD_GET,
            $this->fillSite($site, array('by' => $by))
        );
    }

    /**
     * Edit an order payment
     *
     * @param array $payment order data
     * @param string $by by key
     * @param null $site site code
     *
     * @return ApiResponse
     */
    public function ordersPaymentEdit(array $payment, $by = 'id', $site = null)
    {
        if (!count($payment)) {
            throw new \InvalidArgumentException(
                'Parameter `payment` must contains a data'
            );
        }

        $this->checkIdParameter($by);

        if (!array_key_exists($by, $payment)) {
            throw new \InvalidArgumentException(
                sprintf('Order array must contain the "%s" parameter.', $by)
            );
        }

        return $this->client->makeRequest(
            sprintf('/orders/payments/%s/edit', $payment[$by]),
            Client::METHOD_POST,
            $this->fillSite(
                $site,
                array('payment' => json_encode($payment), 'by' => $by)
            )
        );
    }

}