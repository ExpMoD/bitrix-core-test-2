<?php
/**
 * RCrmEvent
 */
class RetailCrmEvent
{    
    protected static $MODULE_ID = 'intaro.retailcrm';
    protected static $CRM_API_HOST_OPTION = 'api_host';
    protected static $CRM_API_KEY_OPTION = 'api_key';
    protected static $CRM_ORDER_TYPES_ARR = 'order_types_arr';
    protected static $CRM_DELIVERY_TYPES_ARR = 'deliv_types_arr';
    protected static $CRM_PAYMENT_TYPES = 'pay_types_arr';
    protected static $CRM_PAYMENT_STATUSES = 'pay_statuses_arr';
    protected static $CRM_PAYMENT = 'payment_arr'; //order payment Y/N
    protected static $CRM_ORDER_LAST_ID = 'order_last_id';
    protected static $CRM_ORDER_PROPS = 'order_props';
    protected static $CRM_LEGAL_DETAILS = 'legal_details';
    protected static $CRM_CUSTOM_FIELDS = 'custom_fields';
    protected static $CRM_CONTRAGENT_TYPE = 'contragent_type';
    protected static $CRM_ORDER_FAILED_IDS = 'order_failed_ids';
    protected static $CRM_SITES_LIST = 'sites_list';
    
    /**
     * OnAfterUserUpdate
     * 
     * @param mixed $arFields - User arFields
     */
    function OnAfterUserUpdate($arFields)
    {        
        if (isset($GLOBALS['RETAIL_CRM_HISTORY']) && $GLOBALS['RETAIL_CRM_HISTORY']) {
            return;
        }
        
        if (!$arFields['RESULT']) {
            return;
        }
        
        $api_host = COption::GetOptionString(self::$MODULE_ID, self::$CRM_API_HOST_OPTION, 0);
        $api_key = COption::GetOptionString(self::$MODULE_ID, self::$CRM_API_KEY_OPTION, 0);
        $api = new RetailCrm\ApiClient($api_host, $api_key);
        
        $optionsSitesList = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_SITES_LIST, 0));
        
        $resultOrder = RetailCrmUser::customerEdit($arFields, $api, $optionsSitesList);
        if (!$resultOrder) {
            RCrmActions::eventLog('RetailCrmEvent::OnAfterUserUpdate', 'RetailCrmUser::customerEdit', 'error update customer');
        }

        return true; 
    }
    
    /**
     * onBeforeOrderAdd
     * 
     * @param mixed $arFields - User arFields
     */
//    function onBeforeOrderAdd($arFields = array()) {
//        $GLOBALS['RETAILCRM_ORDER_OLD_EVENT'] = false;
//        return;
//    }
    
    /**
     * OnOrderSave
     * 
     * @param mixed $ID - Order id  
     * @param mixed $arFields - Order arFields
     */
//    function OnOrderSave($ID, $arFields, $arOrder, $isNew)
//    {
//        $GLOBALS['RETAILCRM_EVENT_OLD'] = true;
//        return;
//    }
    
    /**
     * onUpdateOrder
     * 
     * @param mixed $ID - Order id  
     * @param mixed $arFields - Order arFields
     */
    function onUpdateOrder($ID, $arFields)
    {
        if (isset($GLOBALS['RETAIL_CRM_HISTORY']) && $GLOBALS['RETAIL_CRM_HISTORY']) {
            $GLOBALS['RETAILCRM_ORDER_OLD_EVENT'] = false;            
            return;
        }  
        
        $GLOBALS['RETAILCRM_ORDER_OLD_EVENT'] = true;        
        return;
    }
    
    /**
     * orderDelete
     * 
     * @param object $event - Order object
     */
    function orderDelete($event)
    {
        $GLOBALS['RETAILCRM_ORDER_DELETE'] = true; 
        return;
    }
    
    /**
     * orderSave
     * 
     * @param object $event - Order object
     */

    function orderSave($event)
    {
        if ($GLOBALS['RETAILCRM_ORDER_OLD_EVENT'] !== false && $GLOBALS['RETAIL_CRM_HISTORY'] !== true && $GLOBALS['RETAILCRM_ORDER_DELETE'] !== true) {
            if (!CModule::IncludeModule("sale")) {
                RCrmActions::eventLog('RetailCrmEvent::orderSave', 'sale', 'module not found');
                return true;
            }

           //ïðîâåðêà íà ñóùåñòâîâàíèå getParameter("ENTITY")
            if (method_exists($event, 'getId')) {
                $obOrder = $event;
            } elseif (method_exists($event, 'getParameter')) {
                $obOrder = $event->getParameter("ENTITY");
            } else {
                RCrmActions::eventLog('RetailCrmEvent::orderSave', 'events', 'event error');
                return true;
            }

            $arOrder = RetailCrmOrder::orderObjToArr($obOrder);

            $client = new GearmanClient();
            $client->addServer('localhost');

            $orderId = $arOrder['ID'];
            $handlerUrl = "https://" . str_replace(':8081', '', $_SERVER['HTTP_HOST']) . "/gearman-send-order.php";

            if ($orderId && $orderId > 0) {
                $fileAccess = $_SERVER['DOCUMENT_ROOT'] . "/exchange/data/import/.crm_orders_send.txt";
                file_put_contents($fileAccess, date('d.m.Y H:i:s')." add to queue " . $orderId  . "\n",FILE_APPEND);

                $client->addTaskBackground("orderSaveCRM", $handlerUrl, null, $orderId);
                $client->runTasks();
            }

            return true;
        }
        
        return;
    }
}