<?php
IncludeModuleLangFile(__FILE__);
class RetailCrmOrder
{
    public static $MODULE_ID = 'intaro.retailcrm';
    public static $CRM_API_HOST_OPTION = 'api_host';
    public static $CRM_API_KEY_OPTION = 'api_key';
    public static $CRM_ORDER_TYPES_ARR = 'order_types_arr';
    public static $CRM_DELIVERY_TYPES_ARR = 'deliv_types_arr';
    public static $CRM_PAYMENT_TYPES = 'pay_types_arr';
    public static $CRM_PAYMENT_STATUSES = 'pay_statuses_arr';
    public static $CRM_PAYMENT = 'payment_arr'; //order payment Y/N
    public static $CRM_ORDER_LAST_ID = 'order_last_id';
    public static $CRM_SITES_LIST = 'sites_list';
    public static $CRM_ORDER_PROPS = 'order_props';
    public static $CRM_LEGAL_DETAILS = 'legal_details';
    public static $CRM_CUSTOM_FIELDS = 'custom_fields';
    public static $CRM_CONTRAGENT_TYPE = 'contragent_type';
    public static $CRM_ORDER_FAILED_IDS = 'order_failed_ids';
    public static $CRM_ORDER_HISTORY_DATE = 'order_history_date';
    public static $CRM_CATALOG_BASE_PRICE = 'catalog_base_price';
    public static $CRM_ORDER_NUMBERS = 'order_numbers';

    const CANCEL_PROPERTY_CODE = 'INTAROCRM_IS_CANCELED';

    /**
     *
     * Creates order or returns order for mass upload
     *
     * @param array $arFields
     * @param $api
     * @param $arParams
     * @param $send
     * @return boolean
     * @return array - array('order' = $order, 'customer' => $customer)
     */
    public static function orderSend($arFields, $api, $arParams, $send = false, $site = null, $methodApi = 'ordersEdit')
    {
        if (!$api || empty($arParams)) { // add cond to check $arParams
            return false;
        }
        if (empty($arFields)) {
            RCrmActions::eventLog('RetailCrmOrder::orderSend', 'empty($arFields)', 'incorrect order');
            return false;
        }

        $order = array(
            'number'          => $arFields['NUMBER'],
            'externalId'      => $arFields['ID'],
            'createdAt'       => new \DateTime($arFields['DATE_INSERT']),
            'customer'        => array('externalId' => $arFields['USER_ID']),
            'paymentType'     => isset($arParams['optionsPayTypes'][$arFields['PAYMENTS'][0]]) ?
                                     $arParams['optionsPayTypes'][$arFields['PAYMENTS'][0]] : '',
//            'paymentStatus'   => isset($arParams['optionsPayment'][$arFields['PAYED']]) ?
//                                     $arParams['optionsPayment'][$arFields['PAYED']] : '',
            'orderType'       => isset($arParams['optionsOrderTypes'][$arFields['PERSON_TYPE_ID']]) ?
                                     $arParams['optionsOrderTypes'][$arFields['PERSON_TYPE_ID']] : '',
            'status'          => isset($arParams['optionsPayStatuses'][$arFields['STATUS_ID']]) ?
                                     $arParams['optionsPayStatuses'][$arFields['STATUS_ID']] : '',
            'statusComment'   => $arFields['REASON_CANCELED'],
            'customerComment' => $arFields['USER_DESCRIPTION'],
            'managerComment'  => $arFields['COMMENTS'],
            'delivery' => array(
//                'cost' => $arFields['PRICE_DELIVERY']
            ),
        );

        if ($arFields['PRICE_DELIVERY'] > 0) {
            $order['delivery']['cost'] = $arFields['PRICE_DELIVERY'];
        }

        if (isset($_COOKIE['_rc']) && $_COOKIE['_rc'] != '') {
            $order['customer']['browserId'] = $_COOKIE['_rc'];
        }
        $order['contragent']['contragentType'] = $arParams['optionsContragentType'][$arFields['PERSON_TYPE_ID']];

        //��������
        foreach ($arFields['PROPS']['properties'] as $prop) {
            if ($search = array_search($prop['CODE'], $arParams['optionsLegalDetails'][$arFields['PERSON_TYPE_ID']])) {
                $order['contragent'][$search] = $prop['VALUE'][0];//�� ������ ������
            } elseif ($search = array_search($prop['CODE'], $arParams['optionsCustomFields'][$arFields['PERSON_TYPE_ID']])) {
                $order['customFields'][$search] = $prop['VALUE'][0];//��������� ��������
            } elseif ($search = array_search($prop['CODE'], $arParams['optionsOrderProps'][$arFields['PERSON_TYPE_ID']])) {//���������
                if (in_array($search, array('fio', 'phone', 'email'))) {//���, �������, �����
                    if ($search == 'fio') {
                        $order = array_merge($order, RCrmActions::explodeFIO($prop['VALUE'][0]));//��������� ���� ���
                    } else {
                        $order[$search] = $prop['VALUE'][0];//������� � �����
                    }
                } else {//��������� - �����
                    if ($prop['TYPE'] == 'LOCATION' && isset($prop['VALUE'][0]) && $prop['VALUE'][0] != '') {
                        $arLoc = \Bitrix\Sale\Location\LocationTable::getByCode($prop['VALUE'][0])->fetch();
                        if ($arLoc) {
                            $server = \Bitrix\Main\Context::getCurrent()->getServer()->getDocumentRoot();
                            $countrys = array();
                            if (file_exists($server . '/bitrix/modules/intaro.retailcrm/classes/general/config/objects.xml')) {
                                $countrysFile = simplexml_load_file($server . '/bitrix/modules/intaro.retailcrm/classes/general/config/country.xml'); 
                                foreach ($countrysFile->country as $country) {
                                    $countrys[RCrmActions::fromJSON((string) $country->name)] = (string) $country->alpha;
                                }
                            }
                            $location = \Bitrix\Sale\Location\Name\LocationTable::getList(array(
                                'filter' => array('=LOCATION_ID' => $arLoc['CITY_ID'], 'LANGUAGE_ID' => 'ru')
                            ))->fetch();
                            if (count($countrys) > 0) {
                                $countryOrder = \Bitrix\Sale\Location\Name\LocationTable::getList(array(
                                    'filter' => array('=LOCATION_ID' => $arLoc['COUNTRY_ID'], 'LANGUAGE_ID' => 'ru')
                                ))->fetch();
                                if(isset($countrys[$countryOrder['NAME']])){
                                    $order['countryIso'] = $countrys[$countryOrder['NAME']];
                                }
                            }
                            
                        }
                        $prop['VALUE'][0] = $location['NAME'];
                    }

                    $order['delivery']['address'][$search] = $prop['VALUE'][0];
                }
            }
        }
//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/crmlog.txt', print_r([$arFields['DELIVERYS'], $arParams['optionsDelivTypes']], true), FILE_APPEND);
        
        //��������
        if (array_key_exists($arFields['DELIVERYS'][0]['id'], $arParams['optionsDelivTypes'])) {
            $order['delivery']['code'] = $arParams['optionsDelivTypes'][$arFields['DELIVERYS'][0]['id']];
            if (isset($arFields['DELIVERYS'][0]['service']) && $arFields['DELIVERYS'][0]['service'] != '') {
                $order['delivery']['service']['code'] = $arFields['DELIVERYS'][0]['service'];
            }
        }

        $reserve = [];
        $purchasingPrices = [];
        //�������
        foreach ($arFields['BASKET'] as $product) {
            $item = array(
                'quantity'        => $product['QUANTITY'],
                'offer'           => array('externalId' => $product['PRODUCT_ID'],
                                           'xmlId' => $product['PRODUCT_XML_ID']
                                        ),
                'productName'     => $product['NAME']
            );

            $pp = CCatalogProduct::GetByID($product['PRODUCT_ID']);
            if (is_null($pp['PURCHASING_PRICE']) == false) {
                $item['purchasePrice'] = $pp['PURCHASING_PRICE'];
                $purchasingPrices[$product['PRODUCT_ID']] = $pp['PURCHASING_PRICE'];
            }
            $item['discount'] = (double) $product['DISCOUNT_PRICE'];
            $item['discountPercent'] = 0;
            $item['initialPrice'] = (double) $product['PRICE'] + (double) $product['DISCOUNT_PRICE'];

            if ($product['ID']) {
                $propReserve = false;
                $db_res = CSaleBasket::GetPropsList(array(), array('BASKET_ID' => $product['ID']));
                while ($ar_res = $db_res->Fetch()) {
                    if ($ar_res['CODE'] == 'reserve') {
                        $propReserve = $ar_res['VALUE'];
                    } elseif ($ar_res['CODE'] == 'product_status') {
                        $item['status'] = $ar_res['VALUE'];
                    }
                }

                if ($propReserve) {
                    $reserve[$product['PRODUCT_ID']] = $propReserve;
                }
            }

            $order['items'][] = $item;
        }

        //��������
        if (function_exists('retailCrmBeforeOrderSend')) {
            $newResOrder = retailCrmBeforeOrderSend($order, $arFields);
            if (is_array($newResOrder) && !empty($newResOrder)) {
                $order = $newResOrder;
            }
        }

        $normalizer = new RestNormalizer();
        $order = $normalizer->normalize($order, 'orders');

        if (isset($arParams['optionsSitesList']) && is_array($arParams['optionsSitesList']) &&
                array_key_exists($arFields['LID'], $arParams['optionsSitesList'])) {
            $site = $arParams['optionsSitesList'][$arFields['LID']];
        }

        $log = new Logger();
        $log->write($order, 'order');

        if($send) {
            $sendOrder = RCrmActions::apiMethod($api, $methodApi, __METHOD__, $order, $site);
            if (!$sendOrder) {
                return false;
            }

            $crmOrderId  = false;

            try {
                $crmOrderId = $sendOrder->id;
            } catch (Exception $e) {

            }

            if ($crmOrderId && $arFields['SUM_PAID'] > 0) {
                try {
                    $api_host = COption::GetOptionString(self::$MODULE_ID, self::$CRM_API_HOST_OPTION, 0);
                    $api_key = COption::GetOptionString(self::$MODULE_ID, self::$CRM_API_KEY_OPTION, 0);

                    $apiV5 = new RetailCrm\CustomApiClient($api_host, $api_key);

                    $orderV5 = $apiV5->ordersGet($crmOrderId, 'id');
                    $orderV5 = $orderV5->order;
                    $payments = $orderV5['payments'];
                    $sum = 0;
                    foreach ($arFields['PAYMENTS_COLLECTION'] as $p) {
                        if (in_array($p->getPaymentSystemId(), $arFields['PAYMENTS'])) {
                            $sum = $p->getField('SUM');
                        }
                    }

                    if (count($payments) == 1) {
                        foreach ($payments as $payment) {
                            $paymentId = $payment['id'];
                            if ($sum > 0) {
                                try {
                                    $pmnt = [
                                        'id' => $paymentId,
                                        'amount' => $sum,
                                    ];

                                    if ($arFields['SUM_PAID'] > 0) {
                                        $pmnt['status'] = 'paid';
                                        $pmnt['amount'] = $arFields['SUM_PAID'];
                                        $log->write([$crmOrderId, $arFields['SUM_PAID']], 'paidOrdersPaymentEdit');
                                    }

                                    $t = $apiV5->ordersPaymentEdit($pmnt);
                                } catch (Exception $e) {
                                    $log->write($t, 'ordersPaymentEditError');
                                }

                                $log->write(
                                    [
                                        'orderId' => $orderV5['id'],
                                        'paymentId' => $paymentId,
                                        'totalSumm' => $orderV5['totalSumm'],
                                        'paymentAmount' => $sum,
                                        'sumPaid' => $arFields['SUM_PAID']
                                    ],
                                    'paymentEdit'
                                );
                            }
                        }
                    }
                } catch (Exception $e) {
                    $log->write($orderV5, 'ordersGetV5Error');
                }
            }

            //���� ���� ������������ � �������� � ������ �� ������ ������� ������������� ������
            //���� ��������, �� ����� �� ����������.
            if (!empty($reserve) && $crmOrderId && !in_array($order['status'], ['complete', 'cancel-other'])) {
                //�������� ����� �� ��� � ����� �� ���� �� ������ � ���
                $i = [];
                try {
                    $order = $api->ordersGet($crmOrderId, 'id');
                    $order = $order->order;
                    $items = $order['items'];
                    foreach ($items as $item) {
                        $i[$item['offer']['externalId']] = $item['id'];
                    }
                } catch (Exception $e) {
                    $log->write($order, 'ordersGetError');
                }

                $reserveHelper = [];
                foreach ($reserve as $itemExternalId => $item) {
                    $item = explode(' | ', $item);
                    foreach ($item as $it) {
                        $it = explode(':', $it);
                        $reserveHelper[$itemExternalId][$it[0]] = ['store' => $it[0], 'quantity' => $it[1]];
                    }
                }

                //�������� ���� � ������������ �� ���, ����� ����� ����� ��� ����� ������������
                try {
                    $reservCrm = $api->ordersPacksList(['orderId' => $crmOrderId]);
                    $packsCRM =  $reservCrm->packs;
                    if (!empty($packsCRM)) {
                        foreach ($packsCRM as $packCRM) {
                            $reserveCRM[$packCRM['item']['offer']['externalId']][$packCRM['store']]['quantity'] = $packCRM['quantity'];
                            $reserveCRM[$packCRM['item']['offer']['externalId']][$packCRM['store']]['store'] = $packCRM['store'];
                            $reserveCRM[$packCRM['item']['offer']['externalId']][$packCRM['store']]['id'] = $packCRM['id'];
                        }
                    }
                } catch (Exception $e) {
                    $log->write($reservCrm, 'getPackError');
                }

                $log->write([$reserveHelper, $reserveCRM], 'getPack');

                if (!empty($i)) {
                    foreach ($i as $externalId => $item) {
                        foreach ($reserveHelper[$externalId] as $pck) {
                            $crmStore = $reserveCRM[$externalId][$pck['store']]['store'];
                            $crmQuantity = $reserveCRM[$externalId][$pck['store']]['quantity'];

                            // ���������� �����
                            $doNothing = $pck['store'] == $crmStore && $pck['quantity'] == $crmQuantity;
                            //������� �����
                            $methodCreate = !isset($crmQuantity) && !isset($crmStore) && empty($reserveCRM[$externalId]);
                            //����������� �����
                            $methodEdit = (isset($crmQuantity) && isset($crmStore))
                                && ($crmQuantity != $pck['quantity'] && $crmStore == $pck['store']);
                            //���� ��������������� �����
                            $editStore = !empty($reserveCRM[$externalId])
                                && (!isset($crmStore) && !isset($crmQuantity));

                            if (!$doNothing) {
                                if ($methodCreate) {
                                    $pack = [
                                        'itemId' => $item,
                                        'store' => $pck['store'],
                                        'quantity' => $pck['quantity']
                                    ];

                                    if (!empty($purchasingPrices[$externalId])) {
                                        $pack['purchasePrice'] = $purchasingPrices[$externalId];
                                    }

                                    $logInfo = [$pack, $crmOrderId];
                                    $log->write($logInfo, 'packCreate');
                                    try {
                                        $r = $api->ordersPacksCreate($pack);
                                        if (isset($r['errorMsg'])) {
                                            $log->write(array($r['errorMsg'], $r['errors'], $crmOrderId), 'packCreateApiErrors');
                                        }
                                    } catch (Exception $e) {
                                        $log->write($r, 'ordersPacksCreateError');
                                    }
                                } elseif ($methodEdit) {
                                    $pack = [
                                        'id' => $reserveCRM[$externalId][$pck['store']]['id'],
                                        'quantity' => $pck['quantity']
                                    ];

                                    if (!empty($purchasingPrices[$externalId])) {
                                        $pack['purchasePrice'] = $purchasingPrices[$externalId];
                                    }

                                    $logInfo = [$pack, $crmOrderId];
                                    $log->write($logInfo, 'packEdit');

                                    try {
                                        $r = $api->ordersPacksEdit($pack);
                                        if (isset($r['errorMsg'])) {
                                            $log->write(array($r['errorMsg'], $r['errors'], $crmOrderId), 'packEditApiErrors');
                                        }
                                    } catch (Exception $e) {
                                        $log->write($r, 'ordersPacksCreateError');
                                    }
                                } elseif ($editStore) {
                                    //������� ������ ����
                                    try {
                                        foreach ($reserveCRM[$externalId] as $oldPack) {
                                            $r = $api->ordersPacksDelete($oldPack['id']);
                                            $log->write($oldPack['id'], 'deletedPacks');
                                            if (isset($r['errorMsg'])) {
                                                $log->write(array($r['errorMsg'], $r['errors'], $crmOrderId), 'packDeleteApiErrors');
                                            }
                                        }
                                    } catch (Exception $e) {
                                        $log->write($r, 'deletePack');
                                    }

                                    $pack = [
                                        'itemId' => $item,
                                        'store' => $pck['store'],
                                        'quantity' => $pck['quantity']
                                    ];

                                    if (!empty($purchasingPrices[$externalId])) {
                                        $pack['purchasePrice'] = $purchasingPrices[$externalId];
                                    }

                                    $logInfo = [$pack, $crmOrderId];
                                    $log->write($logInfo, 'packCreateNewStore');
                                    try {
                                        $r = $api->ordersPacksCreate($pack);
                                        if (isset($r['errorMsg'])) {
                                            $log->write(array($r['errorMsg'], $r['errors'], $crmOrderId), 'packCreateNewStoreApiErrors');
                                        }
                                    } catch (Exception $e) {
                                        $log->write($r, 'ordersPacksCreateError');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $order;
    }
    
    /**
     * Mass order uploading, without repeating; always returns true, but writes error log
     * @param $pSize
     * @param $failed -- flag to export failed orders
     * @return boolean
     */
    public static function uploadOrders($pSize = 50, $failed = false, $orderList = false)
    {
        if (!CModule::IncludeModule("iblock")) {
            RCrmActions::eventLog('RetailCrmOrder::uploadOrders', 'iblock', 'module not found');
            return true;
        }
        if (!CModule::IncludeModule("sale")) {
            RCrmActions::eventLog('RetailCrmOrder::uploadOrders', 'sale', 'module not found');
            return true;
        }
        if (!CModule::IncludeModule("catalog")) {
            RCrmActions::eventLog('RetailCrmOrder::uploadOrders', 'catalog', 'module not found');
            return true;
        }

        $resOrders = array();
        $resCustomers = array();
        $orderIds = array();

        $lastUpOrderId = COption::GetOptionString(self::$MODULE_ID, self::$CRM_ORDER_LAST_ID, 0);
        $failedIds = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_ORDER_FAILED_IDS, 0));

        if ($failed == true && $failedIds !== false && count($failedIds) > 0) {
            $orderIds = $failedIds;
        } elseif ($orderList !== false && count($orderList) > 0) {
            $orderIds = $orderList;
        } else {
            $dbOrder = \Bitrix\Sale\Internals\OrderTable::GetList(array(
                'order'   => array("ID" => "ASC"),
                'filter'  => array('>ID' => $lastUpOrderId),
                'limit'   => $pSize,
                'select'  => array('ID')
            ));
            while ($arOrder = $dbOrder->fetch()) {
                $orderIds[] = $arOrder['ID'];
            }
        }

        if (count($orderIds)<=0) {
            return false;
        }

        $api_host = COption::GetOptionString(self::$MODULE_ID, self::$CRM_API_HOST_OPTION, 0);
        $api_key = COption::GetOptionString(self::$MODULE_ID, self::$CRM_API_KEY_OPTION, 0);

        $optionsSitesList = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_SITES_LIST, 0));        
        $optionsOrderTypes = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_ORDER_TYPES_ARR, 0));
        $optionsDelivTypes = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_DELIVERY_TYPES_ARR, 0));
        $optionsPayTypes = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_PAYMENT_TYPES, 0));
        $optionsPayStatuses = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_PAYMENT_STATUSES, 0)); // --statuses
        $optionsPayment = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_PAYMENT, 0));
        $optionsOrderProps = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_ORDER_PROPS, 0));
        $optionsLegalDetails = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_LEGAL_DETAILS, 0));
        $optionsContragentType = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_CONTRAGENT_TYPE, 0));
        $optionsCustomFields = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_CUSTOM_FIELDS, 0));

        $api = new RetailCrm\ApiClient($api_host, $api_key);

        $arParams = array(
            'optionsOrderTypes'     => $optionsOrderTypes,
            'optionsDelivTypes'     => $optionsDelivTypes,
            'optionsPayTypes'       => $optionsPayTypes,
            'optionsPayStatuses'    => $optionsPayStatuses,
            'optionsPayment'        => $optionsPayment,
            'optionsOrderProps'     => $optionsOrderProps,
            'optionsLegalDetails'   => $optionsLegalDetails,
            'optionsContragentType' => $optionsContragentType,
            'optionsSitesList'      => $optionsSitesList,
            'optionsCustomFields'   => $optionsCustomFields,
        );

        $recOrders = array();
        foreach ($orderIds as $orderId) {
            $id = \Bitrix\Sale\Order::load($orderId);
            if (!$id) {
                continue;
            }
            $order = self::orderObjToArr($id);
            $user = Bitrix\Main\UserTable::getById($order['USER_ID'])->fetch();
            
            $site = count($optionsSitesList) > 1 ? $optionsSitesList[$order['LID']] : null;

            $arCustomers = RetailCrmUser::customerSend($user, $api, $optionsContragentType[$order['PERSON_TYPE_ID']], false, $site);
            $arOrders = self::orderSend($order, $api, $arParams, false, $site); 

            if (!$arCustomers || !$arOrders) {
                continue;
            }
            
            $resCustomers[$order['LID']][] = $arCustomers;
            $resOrders[$order['LID']][] = $arOrders; 
            
            $recOrders[] = $orderId;
        }

        if (count($resOrders) > 0) {
            foreach ($resCustomers as $key => $customerLoad) {
                $site = count($optionsSitesList) > 1 ? $optionsSitesList[$key] : null;
                if (RCrmActions::apiMethod($api, 'customersUpload', __METHOD__, $customerLoad, $site) === false) {
                    return false;
                }
                if (count($optionsSitesList) > 1) {
                    time_nanosleep(0, 250000000);
                }
            }
            foreach ($resOrders as $key => $orderLoad) {
                $site = count($optionsSitesList) > 1 ? $optionsSitesList[$key] : null;
                if (RCrmActions::apiMethod($api, 'ordersUpload', __METHOD__, $orderLoad, $site) === false) {
                    return false;
                }
                if (count($optionsSitesList) > 1) {
                    time_nanosleep(0, 250000000);
                }
            }
            if ($failed == true && $failedIds !== false && count($failedIds) > 0) {
                COption::SetOptionString(self::$MODULE_ID, self::$CRM_ORDER_FAILED_IDS, serialize(array_diff($failedIds, $recOrders)));
            } elseif ($lastUpOrderId < max($recOrders) && $orderList === false) {
                COption::SetOptionString(self::$MODULE_ID, self::$CRM_ORDER_LAST_ID, max($recOrders));
            }
        }

        return true;
    }

    /**
     * @param \Bitrix\Sale\Order $obOrder
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function orderObjToArr($obOrder)
    {
        $arOrder = array(
            'ID'               => $obOrder->getId(),
            'NUMBER'           => $obOrder->getField('ACCOUNT_NUMBER'),
            'LID'              => $obOrder->getSiteId(),
            'DATE_INSERT'      => $obOrder->getDateInsert(),
            'STATUS_ID'        => $obOrder->getField('STATUS_ID'),
            'USER_ID'          => $obOrder->getUserId(),
            'PERSON_TYPE_ID'   => $obOrder->getPersonTypeId(),
            'CURRENCY'         => $obOrder->getCurrency(),
            'PAYMENTS'         => $obOrder->getPaymentSystemId(),
            'PAYMENTS_COLLECTION' => $obOrder->getPaymentCollection(),
            'PAYED'            => $obOrder->isPaid() ? 'Y' : 'N',
            'DELIVERYS'        => array(),
            'PRICE_DELIVERY'   => $obOrder->getDeliveryPrice(),
            'PROPS'            => $obOrder->getPropertyCollection()->getArray(),
            'DISCOUNTS'        => $obOrder->getDiscount()->getApplyResult(),
            'BASKET'           => array(),
            'USER_DESCRIPTION' => $obOrder->getField('USER_DESCRIPTION'),
            'COMMENTS'         => $obOrder->getField('COMMENTS'),
            'REASON_CANCELED'  => $obOrder->getField('REASON_CANCELED'),
            'SUM_PAID' => $obOrder->getSumPaid()
        );
        
        $shipmentList = $obOrder->getShipmentCollection();
        foreach ($shipmentList as $shipmentData) {
            if ($shipmentData->getDeliveryId()) {
                $delivery = \Bitrix\Sale\Delivery\Services\Manager::getById($shipmentData->getDeliveryId());
                if ($delivery['PARENT_ID']) {
                    $servise = explode(':', $delivery['CODE']);
                    $shipment = array('id' => $delivery['ID'], 'service' => $servise[1]);
                } else {
                    $shipment = array('id' => $delivery['ID']);
                }
                $arOrder['DELIVERYS'][] = $shipment;
            }
        }

        $basketItems = $obOrder->getBasket();
        foreach ($basketItems as $item) {
            $arOrder['BASKET'][] = $item->getFields();
        }
     
        return $arOrder;
    }

    public static function AddOrderFrom1C ($orderId)
    {
        $orderEntity = \Bitrix\Sale\Order::load($orderId);

        $arOrder = self::orderObjToArr($orderEntity);

        $client = new GearmanClient();
        $client->addServer('localhost');

        $orderId = $arOrder['ID'];
        $handlerUrl = "https://" . str_replace(':8081', '', $_SERVER['HTTP_HOST']) . "/gearman-send-order.php";

        if ($orderId && $orderId > 0) {
            $fileAccess = $_SERVER['DOCUMENT_ROOT'] . "/exchange/data/import/.crm_orders_send.txt";
            file_put_contents($fileAccess, date('d.m.Y H:i:s')." add to queue " . $orderId  . "\n",FILE_APPEND);

            $client->addTaskBackground("orderSaveCRM", $handlerUrl, null, $orderId);
            $client->runTasks();
        }

        return true;
    }

    public static function OrderSendByID ($orderId)
    {
        $orderEntity = \Bitrix\Sale\Order::load($orderId);

        $arOrder = self::orderObjToArr($orderEntity);

        $api_host = COption::GetOptionString(self::$MODULE_ID, self::$CRM_API_HOST_OPTION, 0);
        $api_key = COption::GetOptionString(self::$MODULE_ID, self::$CRM_API_KEY_OPTION, 0);
        $api = new RetailCrm\ApiClient($api_host, $api_key);

        //params
        $optionsOrderTypes = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_ORDER_TYPES_ARR, 0));
        $optionsDelivTypes = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_DELIVERY_TYPES_ARR, 0));
        $optionsPayTypes = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_PAYMENT_TYPES, 0));
        $optionsPayStatuses = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_PAYMENT_STATUSES, 0)); // --statuses
        $optionsPayment = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_PAYMENT, 0));
        $optionsSitesList = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_SITES_LIST, 0));
        $optionsOrderProps = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_ORDER_PROPS, 0));
        $optionsLegalDetails = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_LEGAL_DETAILS, 0));
        $optionsContragentType = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_CONTRAGENT_TYPE, 0));
        $optionsCustomFields = unserialize(COption::GetOptionString(self::$MODULE_ID, self::$CRM_CUSTOM_FIELDS, 0));

        $arParams = RCrmActions::clearArr(array(
            'optionsOrderTypes'     => $optionsOrderTypes,
            'optionsDelivTypes'     => $optionsDelivTypes,
            'optionsPayTypes'       => $optionsPayTypes,
            'optionsPayStatuses'    => $optionsPayStatuses,
            'optionsPayment'        => $optionsPayment,
            'optionsOrderProps'     => $optionsOrderProps,
            'optionsLegalDetails'   => $optionsLegalDetails,
            'optionsContragentType' => $optionsContragentType,
            'optionsSitesList'      => $optionsSitesList,
            'optionsCustomFields'   => $optionsCustomFields
        ));

        //���������������
        $site = count($optionsSitesList) > 1 ? $optionsSitesList[$arOrder['LID']] : null;

        //�������� �� ����� �����
        $orderCrm = RCrmActions::apiMethod($api, 'ordersGet', __METHOD__, $arOrder['ID'], $site);
        if (isset($orderCrm['order'])) {
            $methodApi = 'ordersEdit';
        } else {
            $methodApi = 'ordersCreate';
        }

        //user
        $userCrm = RCrmActions::apiMethod($api, 'customersGet', __METHOD__, $arOrder['USER_ID'], $site);
        if (!isset($userCrm['customer'])) {
            $arUser = Bitrix\Main\UserTable::getById($arOrder['USER_ID'])->fetch();
            $resultUser = RetailCrmUser::customerSend($arUser, $api, $optionsContragentType[$arOrder['PERSON_TYPE_ID']], true, $site);
            if (!$resultUser) {
                RCrmActions::eventLog('RetailCrmEvent::orderSave', 'RetailCrmUser::customerSend', 'error during creating customer');
                return true;
            }
        }

        //order
        $resultOrder = self::orderSend($arOrder, $api, $arParams, true, $site, $methodApi);
        if (!$resultOrder) {
            RCrmActions::eventLog('RetailCrmEvent::orderSave', 'RetailCrmOrder::orderSend', 'error during creating order');
            return true;
        }

        return true;
    }
}
