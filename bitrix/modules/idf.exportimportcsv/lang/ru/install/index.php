<?
$MESS["idf.massimport_MODULE_NAME"] = "Экспорт/Импорт прайс-листа в формате csv";
$MESS["idf.massimport_MODULE_DESC"] = "Компонент \"Экспорт/Импорт прайс-листа в формате csv\" позволит вам с легкостью редактировать элементы инфоблоков в вашем любимом офисном пакете.";
$MESS["idf.massimport_PARTNER_NAME"] = "IDF Company";
$MESS["idf.massimport_PARTNER_URI"] = "http://www.idfc.ru";
?>