<?

$MESS["FIELD_ID"] = "№";
$MESS["FIELD_NAME"] = "Название";
$MESS["FIELD_CODE"] = "Символьный код";
$MESS["FIELD_XML_ID"] = "Внешний ID";
$MESS["FIELD_PREVIEW_TEXT"] = "Описание в списке";
$MESS["FIELD_DETAIL_TEXT"] = "Описание на странице детального просмотра";
$MESS["FIELD_ACTIVE"] = "Активность";
$MESS["FIELD_IBLOCK_SECTION_ID"] = "ID раздела";
$MESS["FIELD_PREVIEW_PICTURE"] = "Картинка для анонса";
$MESS["FIELD_DETAIL_PICTURE"] = "Детальная картинка";


$MESS["PROPERTY"] = "Свойство";
$MESS["PRICE"] = "Цена";
$MESS["REMAINS"] = "Остатки";


$MESS["NO_MODULES_INSTALLED"] = "Не удалось подключить необходимые модули";
$MESS["EMPTY_IBLOCK_ID"] = "Не выбран инфоблок";
$MESS["EMPTY_ELEMENTS"] = "Не выбраны элементы";
$MESS["NOT_OPEN_FILE_FOR_WRITE"] = "Не удалось создать файл экспорта";



