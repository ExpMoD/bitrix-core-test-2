<?

$MESS["GROUP_FIELDS"] = "Поля";
$MESS["GROUP_FILTERS"] = "Фильтры";
$MESS["GROUP_DECOR"] = "Оформление";
$MESS["GROUP_OTHER"] = "Прочие настройки";


$MESS["IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["IBLOCK_IBLOCK"] = "Инфоблок";




$MESS["FIELD_NAME"] = "Название";
$MESS["FIELD_CODE"] = "Символьный код";
$MESS["FIELD_XML_ID"] = "Внешний ID";
$MESS["FIELD_PREVIEW_TEXT"] = "Описание в списке";
$MESS["FIELD_DETAIL_TEXT"] = "Описание на странице детального просмотра";
$MESS["FIELD_ACTIVE"] = "Активность";
$MESS["FIELD_IBLOCK_SECTION_ID"] = "ID раздела";
$MESS["FIELD_PREVIEW_PICTURE"] = "Картинка для анонса";
$MESS["FIELD_DETAIL_PICTURE"] = "Детальная картинка";


$MESS["PROP_ELEMENT_DATA"] = "Поля элемента инфоблока";
$MESS["PROP_ELEMENT_PROPS"] = "Свойства элемента инфоблока";
$MESS["PROP_SECTION"] = "Раздел";
$MESS["PROP_PRICES"] = "Цены";
$MESS["PROP_REMAINS"] = "Остатки";


$MESS["DECOR_COLOR_BUTTONS"] = "Цвет кнопок";
$MESS["DECOR_COLOR_BUTTONS_TEXT"] = "Цвет текста на кнопках";

$MESS["COLOR_RED"] = "Красный";
$MESS["COLOR_ORANGE"] = "Оранжевый";
$MESS["COLOR_YELLOW"] = "Желтый";
$MESS["COLOR_GREEN"] = "Зеленый";
$MESS["COLOR_BLUE"] = "Синий";

$MESS["IMAGES_PATH"] = "Путь к файлам на сервере";
$MESS["CODE_FROM_NAME"] = "Формировать символьный код из названия";
