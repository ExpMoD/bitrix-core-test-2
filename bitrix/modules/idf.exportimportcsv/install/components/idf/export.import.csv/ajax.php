<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
require_once('class.php');

if($_POST['action'] == 'import')
{
	$params = json_decode($_POST['params'], true);
	$responce = ExportImportCsv::getSelfInAjax($params)->importFile();
}
else
{
	$responce = ExportImportCsv::getSelfInAjax()->continueImport();
}


echo json_encode($responce);

