<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Seo\SitemapTable;

class ExportImportCsv extends CBitrixComponent
{

	const NEW_FILE_NAME = '/upload/export.csv';
	const IMPORT_FILE_KEY = 'import_file';
	const ELEMENT_BY_STEP = 50;

	protected $fields = array();
	protected $propertyesList = array();
	protected $pricesList = array();
	protected $enums = array();


	public function onPrepareComponentParams($params)
	{
		foreach($params as $code => $param)
		{
			if(in_array($code, array('IBLOCK_ID', "SECTION")))
			{
				$params[$code] = (int)$param;
			}
			elseif(is_array($param))
			{
				foreach($param as $key => $value)
				{
					if($value === '')
					{
						unset($key);
					}
				}

				$params[$code] = $param;
			}
			else
			{
				$params[$code] = trim($param);
			}
		}

		if(empty($params["DECOR_COLOR_BUTTONS"]))
		{
			$params["DECOR_COLOR_BUTTONS"] = "orange";
		}

		if(!isset($params["CODE_FROM_NAME"]))
		{
			$params["CODE_FROM_NAME"] = "Y";
		}

		return $params;
	}

	public function executeComponent()
	{
		if(!$this->check())
		{
			return false;
		}

		$this->arResult = array(
			'success' => false,
			'error' => false
		);

		if(!empty($_POST['action_idfc']) && check_bitrix_sessid() && $_POST['action_idfc'] == 'export')
		{
			$this->arResult = $this->export();
		}

		$this->includeComponentTemplate();
	}

	protected function check()
	{
		if(!CModule::IncludeModule('iblock') || !CModule::IncludeModule('catalog') || !CModule::IncludeModule('sale'))
		{
			ShowError(GetMessage('NO_MODULES_INSTALLED'));
			return false;
		}

		if(empty($this->arParams['IBLOCK_ID']))
		{
			ShowError(GetMessage('EMPTY_IBLOCK_ID'));
			return false;
		}

		return true;
	}

	protected function export()
	{
		$elements = $this->getElements();

		if(empty($elements))
		{
			return array('error' => GetMessage('EMPTY_ELEMENTS'));
		}

		$file = fopen($_SERVER['DOCUMENT_ROOT'] . static::NEW_FILE_NAME, 'w');

		if(empty($file))
		{
			return array('error' => GetMessage('NOT_OPEN_FILE_FOR_WRITE'));
		}

		$this->write($file, $this->getCsvHeader());

		foreach($elements as $element)
		{
			$this->write($file, $element);
		}

		return array(
			'success' => true,
			'filename' => static::NEW_FILE_NAME,
			'action' => 'export',
		);
	}

	protected function read($file)
	{
		$data = fgetcsv($file, 0, ';');

		if(LANG_CHARSET == 'UTF-8' && is_array($data))
		{
			$data = $this->convertCharset($data, 'cp1251', 'utf8');
		}

		return $data;
	}

	protected function write($file, array $data)
	{
		if(LANG_CHARSET == 'UTF-8')
		{
			$data = $this->convertCharset($data, 'utf8', 'cp1251');
		}

		return fputcsv($file, $data, ';');
	}

	protected function getElements()
	{
		$result = array();

		$res = CIBlockElement::GetList(
			array(),
			$this->getFilters(),
			false,
			false,
			$this->getSelect()
		);

		while($element = $res->GetNext(true, false))
		{
			$element = $this->formatElement($element);
			$result[$element['ID']] = $element;
		}

		return $result;
	}

	protected function addElement(array $element)
	{
		$iblockElement = new CIBlockElement;

		$formatResult = $this->formatFilePropertyes($element["fields"], array("PREVIEW_PICTURE", "DETAIL_PICTURE"));
		if(isset($formatResult["error"]) && $formatResult["success"] === false)
		{
			return $formatResult;
		}

		$element["fields"] = $this->formatElementCode($formatResult);

		$element["fields"]["IBLOCK_ID"] = $this->arParams['IBLOCK_ID'];
		$element["id"] = $iblockElement->Add($element["fields"]);
		if(!$element["id"])
		{
			return array(
				'success' => false,
				'error' => 'cannot add element: ' . (string)$iblockElement->LAST_ERROR
			);
		}

		if(isset($element['propertyes']) && !empty($element['propertyes']))
		{
			$formatResult = $this->formatFilePropertyes($element["propertyes"], $this->getFilesPropertyes());
			if(isset($formatResult["error"]) && $formatResult["success"] === false)
			{
				return $formatResult;
			}
			$element["propertyes"] = $formatResult;

			$formatResult = $this->formatListPropertyes($element["propertyes"], $this->getListPropertyes());
			if(isset($formatResult["error"]) && $formatResult["success"] === false)
			{
				return $formatResult;
			}
			$element["propertyes"] = $formatResult;

			$this->setElementPropertyes($element["id"], $element["propertyes"]);
		}

		if(isset($element['prices']) && !empty($element['prices']))
		{
			$result = $this->updateElementPrices($element["id"], $element["prices"]);

			if(!$result["success"])
			{
				return $result;
			}
		}

		if(isset($element['remains']) && !is_null($element['remains']))
		{
			$result = $this->addElementRemains($element["id"], $element["remains"]);

			if(!$result["success"])
			{
				return $result;
			}
		}

		return array('success_add' => true);
	}

	protected function formatElementCode(array $fields)
	{
		if(isset($fields["CODE"]) && !empty($fields["CODE"])) return $fields;

		if(isset($fields["NAME"]) && !empty($fields["NAME"]) && $this->arParams["CODE_FROM_NAME"] == "Y")
		{
			$fields["CODE"] = CUtil::translit($fields["NAME"], LANGUAGE_ID, array(
				"max_len" => 100,
				"change_case" => 'L',
				"replace_space" => '-',
				"replace_other" => '-',
				"delete_repeat_replace" => true
			));
		}

		return $fields;
	}

	protected function updateElement(array $element)
	{
		if(empty($element['id']))
		{
			return array(
				'success' => false,
				'error' => 'empty id in CSV file'
			);
		}

		$iblockElement = new CIBlockElement;

		$formatResult = $this->formatFilePropertyes($element["fields"], array("PREVIEW_PICTURE", "DETAIL_PICTURE"));
		if(isset($formatResult["error"]) && $formatResult["success"] === false)
		{
			return $formatResult;
		}
		$element['fields'] = $formatResult;

		if(!$iblockElement->Update($element['id'], $element['fields']))
		{
			$errorText = ', message: ' . $iblockElement->LAST_ERROR;
			return array(
				'success' => false,
				'error' => 'cannot update element ' . $element['id'] . $errorText
			);
		}

		if(isset($element['propertyes']) && !empty($element['propertyes']))
		{
			$formatResult = $this->formatFilePropertyes($element["propertyes"], $this->getFilesPropertyes());
			if(isset($formatResult["error"]) && $formatResult["success"] === false)
			{
				return $formatResult;
			}
			$element["propertyes"] = $formatResult;

			$formatResult = $this->formatListPropertyes($element["propertyes"], $this->getListPropertyes());
			if(isset($formatResult["error"]) && $formatResult["success"] === false)
			{
				return $formatResult;
			}
			$element["propertyes"] = $formatResult;

			$this->setElementPropertyes($element["id"], $element["propertyes"]);
		}

		if(isset($element['prices']) && !empty($element['prices']))
		{
			$result = $this->updateElementPrices($element["id"], $element["prices"]);

			if(!$result["success"])
			{
				return $result;
			}
		}

		if(isset($element['remains']) && !is_null($element['remains']))
		{
			$result = $this->updateElementRemains($element["id"], $element["remains"]);

			if(!$result["success"])
			{
				return $result;
			}
		}

		return array('success' => true);
	}

	protected function formatFilePropertyes(array $propertyes, array $filesPropertyes)
	{
		foreach($filesPropertyes as $code)
		{
			if(!isset($propertyes[$code]) || empty($propertyes[$code]))
			{
				continue;
			}

			$picture = $this->uploadFile($propertyes[$code]);

			if(isset($picture["error"]))
			{
				return array(
					'success' => false,
					'error' => "error on upload file for property \"" . $this->getPropertyName($code) . "\": " . $picture["error"]
				);
			}

			$propertyes[$code] = $picture;
		}

		return $propertyes;
	}

	protected function formatListPropertyes(array $propertyes, array $listPropertyes)
	{
		foreach($listPropertyes as $code)
		{
			if(!isset($propertyes[$code]) || empty($propertyes[$code]))
			{
				continue;
			}

			$enums = $this->getPropertyEnums($code);

			if(!isset($enums[$propertyes[$code]]))
			{
				return array(
					'success' => false,
					'error' => "error add value for property \"" . $this->getPropertyName($code) . "\": doesn`t exists \"{$propertyes[$code]}\" value"
				);
			}

			$propertyes[$code] = $enums[$propertyes[$code]];
		}

		return $propertyes;
	}

	protected function setElementPropertyes($id, array $propertyes)
	{
		CIBlockElement::SetPropertyValuesEx(
			$id,
			$this->arParams['IBLOCK_ID'],
			$propertyes
		);
	}

	protected function updateElementPrices($id, array $prices)
	{
		$dbPrices = array();
		$res = CPrice::GetList(
			array(),
			array('PRODUCT_ID' => $id, '@CATALOG_GROUP_ID' => array_keys($prices)),
			false,
			false,
			array('ID', 'CATALOG_GROUP_ID')
		);

		while($price = $res->Fetch())
		{
			$dbPrices[$price['CATALOG_GROUP_ID']] = $price['ID'];
		}

		foreach($prices as $priceId => $price)
		{
			if(!isset($dbPrices[$priceId]))
			{
				if(!CPrice::Add(array(
					"PRODUCT_ID" => $id,
					"CATALOG_GROUP_ID" => $priceId,
					"PRICE" => $price,
					"CURRENCY" => "RUB",
				)))
				{
					$errorText = ($ex = $GLOBALS['APPLICATION']->GetException()) ? ', message: ' . $ex->GetString() : '';
					return array(
						'success' => false,
						'error' => "price $priceId can not add on $id $errorText"
					);
				}
			}
			else
			{
				if(!CPrice::Update($dbPrices[$priceId], array(
					'PRICE' => $price
				)))
				{
					$errorText = ($ex = $GLOBALS['APPLICATION']->GetException()) ? ', message: ' . $ex->GetString() : '';
					return array(
						'success' => false,
						'error' => "cannot update element price $id $errorText"
					);
				}
			}
		}

		return array('success' => true);
	}

	protected function addElementRemains($id, $quantity)
	{
		if(!CCatalogProduct::Add(array(
			"ID" => $id,
			'QUANTITY' => $quantity,
		)))
		{
			$errorText = ($ex = $GLOBALS['APPLICATION']->GetException()) ? ', message: ' . $ex->GetString() : '';
			return array(
				'success' => false,
				'error' => "cannot add element remains $id $errorText"
			);
		}

		return array("success" => true);
	}

	protected function updateElementRemains($id, $quantity)
	{
		if(!CCatalogProduct::Update($id, array(
			'QUANTITY' => $quantity,
		)))
		{
			$errorText = ($ex = $GLOBALS['APPLICATION']->GetException()) ? ', message: ' . $ex->GetString() : '';
			return array(
				'success' => false,
				'error' => "cannot update element remains $id $errorText"
			);
		}

		return array("success" => true);
	}

	protected function uploadFile($file)
	{
		if(strpos($file, "http:") !== false)
		{
			$path = $file;
		}
		elseif(empty($this->arParams["IMAGES_PATH"]))
		{
			return array(
				"error" => "parameter \"Image Path\" is not defined"
			);
		}
		else
		{
			$path = $_SERVER["DOCUMENT_ROOT"] . "/" . $this->arParams["IMAGES_PATH"] . $file;
		}

		return CFile::MakeFileArray($path);
	}

	protected function formatCsvElement(array $element, array $fields)
	{
		$key = 0;

		$result = array(
			'id' => false,
			'remains' => null,
			'fields' => array(),
			'propertyes' => array(),
			'prices' => array()
		);

		foreach($fields as $field => $fieldName)
		{
			if($field == 'ID')
			{
				$result['id'] = $element[$key];
			}
			elseif($field == 'CATALOG_QUANTITY')
			{
				$result['remains'] = $element[$key];
			}
			elseif(strpos($field, 'PROPERTY_') !== false)
			{
				$result['propertyes'][str_replace('PROPERTY_', '', $field)] = $element[$key];
			}
			elseif(strpos($field, 'CATALOG_GROUP_') !== false)
			{
				$result['prices'][str_replace('CATALOG_GROUP_', '', $field)] = $element[$key];
			}
			else
			{
				$result['fields'][$field] = $element[$key];
			}

			$key++;
		}

		return $result;
	}

	protected function formatElement(array $element)
	{
		$result = array();
		$fields = $this->getFields();
		
		foreach($fields as $field => $name)
		{
			if(in_array($field, $this->getFilesFields()))
			{
				$result[$field] = "";
				continue;
			}

			if(strpos($field, 'PROPERTY_') !== false)
			{
				$result[$field] = $element[$field . '_VALUE'];
				continue;
			}

			if(strpos($field, 'CATALOG_GROUP_') !== false)
			{
				$groupId = str_replace('CATALOG_GROUP_', '', $field);
				$result[$field] = $element['CATALOG_PRICE_' . $groupId];
				continue;
			}

			$result[$field] = $element[$field];
		}

		return $result;
	}

	protected function getFilesFields()
	{
		$fields = array("DETAIL_PICTURE", "PREVIEW_PICTURE");

		$propertyes = $this->getFilesPropertyes();
		foreach($propertyes as $code)
		{
			$fields[] = "PROPERTY_$code_VALUE";
		}

		return $fields;
	}

	protected function getCsvHeader()
	{
		return array_values($this->getFields());
	}

	protected function getFilters()
	{
		if(!empty($this->arParams['SECTION']))
		{
			$filter = array(
				'SECTION_ID' => $this->arParams['SECTION'],
				'INCLUDE_SUBSECTIONS' => 'Y'
			);
		}

		if(isset($_POST['brand']) && !empty($_POST['brand']))
		{
			$filter['PROPERTY_BRAND'] = $_POST['brand'];
		}

		$filter['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];

		return $filter;
	}

	protected function getSelect()
	{
		return array_keys($this->getFields());
	}

	protected function getFields()
	{
		if(empty($this->fields))
		{
			$this->fields['ID'] = $this->headerMessage('element', 'ID');

			foreach($this->arParams['ELEMENT_DATA'] as $field)
			{
				$this->fields[$field] = $this->headerMessage('element', $field);
			}

			foreach($this->arParams['ELEMENT_PROPS'] as $field)
			{
				$this->fields['PROPERTY_' . $field] = $this->headerMessage('property', $field);
			}

			foreach($this->arParams['PRICES'] as $field)
			{
				$this->fields['CATALOG_GROUP_' . $field] = $this->headerMessage('price', $field);
			}

			if($this->arParams['REMAINS'] == 'Y')
			{
				$this->fields['CATALOG_QUANTITY'] = $this->headerMessage('remains', 'CATALOG_QUANTITY');
			}
		}

		return $this->fields;
	}

	protected function headerMessage($type, $field)
	{
		switch($type)
		{
			case 'element':
				return GetMessage('FIELD_' . $field);

			case 'property':
				return GetMessage('PROPERTY') . ' "' . $this->getPropertyName($field) . '"';

			case 'price':
				return GetMessage('PRICE') . ' "' . $this->getPriceName($field) . '"';

			case 'remains':
				return GetMessage('REMAINS');
		}

		return 'unknown';
	}

	protected function getPropertyName($field)
	{
		$property = $this->getProperty($field);
		return isset($property["NAME"]) ? $property["NAME"] : $field;
	}

	protected function getProperty($code)
	{
		$list = $this->getPropertyesList();
		return $list[$code];
	}

	protected function getPropertyEnums($code)
	{
		if(!isset($this->enums[$code]))
		{
			$prop = $this->getProperty($code);
			if($prop["PROPERTY_TYPE"] != "L") return false;

			$res = CIBlockPropertyEnum::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "CODE" => $code)
			);

			while($enum = $res->Fetch())
			{
				$this->enums[$code][$enum["VALUE"]] = $enum["ID"];
			}
		}

		return $this->enums[$code];
	}

	protected function getFilesPropertyes()
	{
		$result = array();

		$propertyes = $this->getPropertyesList();
		foreach($propertyes as $code => $value)
		{
			if($value["PROPERTY_TYPE"] == "F")
			{
				$result[] = $code;
			}
		}

		return $result;
	}

	protected function getListPropertyes()
	{
		$result = array();

		$propertyes = $this->getPropertyesList();
		foreach($propertyes as $code => $value)
		{
			if($value["PROPERTY_TYPE"] == "L")
			{
				$result[] = $code;
			}
		}

		return $result;
	}

	protected function getPropertyesList()
	{
		if(empty($this->propertyesList))
		{
			$res = CIBlockProperty::GetList(
				array(),
				array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'])
			);

			while($prop = $res->Fetch())
			{
				$this->propertyesList[$prop['CODE']] = $prop;
			}
		}

		return $this->propertyesList;
	}

	protected function getPriceName($priceId)
	{
		$price = $this->getCatalogPrice($priceId);
		return $price["NAME_LANG"];
	}

	protected function getCatalogPrice($priceId)
	{
		if(empty($this->pricesList))
		{
			$res = CCatalogGroup::GetList(
				array(),
				array(),
				false,
				false,
				array('ID', 'NAME_LANG', 'CURRENCY')
			);

			while($price = $res->Fetch())
			{
				$this->pricesList[$price['ID']] = $price;
			}
		}

		return $this->pricesList[$priceId];
	}

	public function convertCharset(array $data, $inCharset, $outCharset)
	{
		$component = $this;

		return array_map(function($element) use ($inCharset, $outCharset, $component)
		{
			return !is_array($element) ?
				iconv($inCharset, $outCharset, $element) :
				$component->convertCharset($element, $inCharset, $outCharset);
		}, $data);
	}

	/* for ajax */

	public static function getSelfInAjax($params = false)
	{
		$component = new self;

		$params = !empty($params) ? $params : $component->getCachedParams();
		$component->setParams($params);

		__IncludeLang(__DIR__ . '/lang/' . LANGUAGE_ID . '/component.php');

		return $component;
	}

	protected function setParams($params)
	{
		$this->arParams = $this->onPrepareComponentParams($params);
		return $this;
	}

	protected function getCachedParams()
	{
		$params = $_SESSION['IDF_IMPORT'][$GLOBALS['USER']->GetId()]['PARAMS'];
		unset($_SESSION['IDF_IMPORT'][$GLOBALS['USER']->GetId()]['PARAMS']);
		return $params;
	}

	public function importFile()
	{
		if(!$this->check())
		{
			return false;
		}

		if(!isset($_FILES[static::IMPORT_FILE_KEY]))
		{
			return array('error' => 'no_file');
		}

		$filename = $_FILES[static::IMPORT_FILE_KEY]['tmp_name'];

		if(!is_file($filename))
		{
			return array('error' => 'no_file');
		}

		$file = fopen($filename, 'r');

		if(empty($file))
		{
			return array('error' => 'not_open_file_for_read');
		}

		$csvHeader = $this->read($file);

		if($csvHeader != $this->getCsvHeader())
		{
			return array('error' => 'uncorrect_format');
		}

		$result = $this->importBySteps($file);

		if(feof($file))
		{
			return $this->getSuccessResult($result);
		}

		$fileIndex = ftell($file);
		$persent = ($fileIndex / filesize($filename)) * 100;

		move_uploaded_file($filename, $this->getTempFileName());

		$_SESSION['IDF_IMPORT'][$GLOBALS['USER']->GetId()]['PARAMS'] = $this->arParams;
		$_SESSION['IDF_IMPORT'][$GLOBALS['USER']->GetId()]['FILE_INDEX'] = $fileIndex;

		return $this->getContinueResult($result, $persent);
	}

	public function continueImport()
	{
		if(!$this->check())
		{
			return false;
		}

		$filename = $this->getTempFileName();

		if(!is_file($filename))
		{
			return array('error' => 'no_file');
		}

		$file = fopen($filename, 'r');

		if(empty($file))
		{
			return array('error' => 'not_open_file_for_read');
		}

		fseek(
			$file,
			$_SESSION['IDF_IMPORT'][$GLOBALS['USER']->GetId()]['FILE_INDEX']
		);

		$result = $this->importBySteps($file);

		if(feof($file))
		{
			unlink($filename);
			return $this->getSuccessResult($result);
		}

		$fileIndex = ftell($file);
		$persent = ($fileIndex / filesize($filename)) * 100;

		$_SESSION['IDF_IMPORT'][$GLOBALS['USER']->GetId()]['PARAMS'] = $this->arParams;
		$_SESSION['IDF_IMPORT'][$GLOBALS['USER']->GetId()]['FILE_INDEX'] = ftell($file);

		return $this->getContinueResult($result, $persent);
	}

	protected function getTempFileName()
	{
		return $_SERVER['DOCUMENT_ROOT'] . '/upload/import_' . $GLOBALS['USER']->GetId() . '.csv';
	}

	protected function getSuccessResult($result)
	{
		// for json_encode bag
		if(LANG_CHARSET != 'UTF-8')
		{
			$result = $this->convertCharset($result, 'cp1251', 'utf8');
		}

		return array_merge($result, array(
			'success' => true,
			'action' => 'import'
		));
	}

	protected function getContinueResult($result, $persent)
	{
		// for json_encode bag
		if(LANG_CHARSET != 'UTF-8')
		{
			$result = $this->convertCharset($result, 'cp1251', 'utf8');
		}

		return array_merge($result, array(
			'success' => true,
			'action' => 'continue',
			'persent' => $persent
		));
	}

	protected function importBySteps($file)
	{
		$correct = 0;
		$new = 0;
		$uncorrect = 0;
		$errors = array();

		$counter = 0;

		$sitemap = $this->sitemapBegin();

		while(($data = $this->read($file)) !== false)
		{
			$element = $this->formatCsvElement($data, $this->getFields());
			if($element["id"] == "new")
			{
				$updateResult = $this->addElement($element);
			}
			else
			{
				$updateResult = $this->updateElement($element);
			}

			if($updateResult['success'])
			{
				$correct++;
			}
			elseif($updateResult['success_add'])
			{
				$new++;
			}
			else
			{
				$errors[] = $updateResult['error'];
				$uncorrect++;
			}

			if(++$counter == static::ELEMENT_BY_STEP)
			{
				break;
			}
		}

		$this->sitemapEnd($sitemap);

		return array(
			'correct' => $correct,
			'uncorrect' => $uncorrect,
			'new' => $new,
			'errors' => $errors
		);
	}

	protected function sitemapBegin()
	{
		if(!CModule::IncludeModule("seo"))
			return false;

		$sitemap = $this->getSitemap();

		if(empty($sitemap))
			return false;

		if(!$this->checkSitemap($sitemap, $this->arParams["IBLOCK_ID"]))
			return false;

		$settings = unserialize($sitemap["SETTINGS"]);

		$settings["IBLOCK_ELEMENT"][$this->arParams["IBLOCK_ID"]] = "N";

		$sitemap["SETTINGS"] = serialize($settings);

		return SitemapTable::update($sitemap["ID"], $sitemap) ? $sitemap : false;
	}

	protected function sitemapEnd($sitemap)
	{
		if(empty($sitemap))
			return false;

		$settings = unserialize($sitemap["SETTINGS"]);

		$settings["IBLOCK_ELEMENT"][$this->arParams["IBLOCK_ID"]] = "Y";

		$sitemap["SETTINGS"] = serialize($settings);

		return SitemapTable::update($sitemap["ID"], $sitemap);
	}

	protected function getSitemap()
	{
		return SitemapTable::getList(array("filter" => array("SITE_ID" => SITE_ID, "ACTIVE" => "Y")))->fetch();
	}

	protected function checkSitemap($sitemap, $iblockId)
	{
		if(empty($sitemap) || empty($iblockId))
			return false;

		$settings = unserialize($sitemap["SETTINGS"]);

		if($settings["IBLOCK_ACTIVE"][$iblockId] == "N")
			return false;

		if($settings["IBLOCK_ELEMENT"][$iblockId] == "N")
			return false;

		return true;
	}

}
