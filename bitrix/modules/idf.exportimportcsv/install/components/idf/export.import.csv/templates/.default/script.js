

$(function(){

	$('form.csv-form .csv-form-export').bind('click', function(){
		$(this).parent().find('[name="action_idfc"]').val('export');
		$(this).parent().submit();
	});

	$('form.csv-form .csv-form-import').bind('click', function(){
		$(this).parent().find('[name="action_idfc"]').val('import');
		$(this).parent().find('[name="import_file"]').click();
	});

	$('form.csv-form [name="import_file"]').bind('change', function(){
		if($(this).val() == '')
			return false;

		$(this).parent().submit();
	});

	$('form.csv-form').bind('submit', function(){

		if($(this).find('[name="action_idfc"]').val() == 'import') {

			var data = new FormData();
			data.append('params', $('[name="params"]').val());
			data.append('action', $('[name="action_idfc"]').val());
			data.append('import_file', $('[name="import_file"]')[0].files[0]);

			$('form.csv-form [name="import_file"]').val('');

			$('.corrent-elements-count span').html('0');
			$('.added-elements-count span').html('0');
			$('.uncorrent-elements-count span').html('0');

			ajaxImportHandler(data);

			return false;
		}

		return true;

	});

});

var ajaxImportHandler = function(data){

	$('.process-box').show();

	$.ajax({
		url: importConteiner.getData('url'),
		type: 'POST',
		processData: false,
		contentType: false,
		data: data,
		success: function(response){
			try{
				response = $.parseJSON(response);
			} catch(err) {
				response = {'error': 'php error: ' + response};
			}

			if(typeof response['error'] != 'undefined'){
				$('.error-box .error-container').append('<p>' + response['error'] + '</p>');
				console.log(response['error']);
				return false;
			}

			if(response['errors'].length != 0){
				// $('.error-box').show();

				for(var key in response['errors']){
					$('.error-box .error-container').append('<p>' + response['errors'][key] + '</p>');
					console.log(response['errors'][key]);
				}
			}

			if(typeof response['persent'] != 'undefined'){
				$('.loading-content').css('width', parseInt(response['persent']) + '%');
			}

			var correct = $('.corrent-elements-count span').html();
			correct = correct == '' ? 0 : parseInt(correct);
			$('.corrent-elements-count span').html(correct + parseInt(response['correct']));

			var added = $('.added-elements-count span').html();
			added = added == '' ? 0 : parseInt(added);
			$('.added-elements-count span').html(added + parseInt(response['new']));

			var uncorrect = $('.uncorrent-elements-count span').html();
			uncorrect = uncorrect == '' ? 0 : parseInt(uncorrect);
			$('.uncorrent-elements-count span').html(uncorrect + parseInt(response['uncorrect']));

			if(response['action'] == 'continue'){
				ajaxImportHandler({action: 'continue'});
			} else if(response['action'] == 'import') {
				$('.process-box').hide();
				$('.success-box').show();
			}
		},
		error: function(jqXHR, textStatus){
			console.log(jqXHR, textStatus);
		}
	});
}

var IdfImportConteiner = function(){

	this.data = [];

	this.getData = function(key){
		return this.data[key];
	}

	this.setData = function(key, data){
		this.data[key] = data;
		return this;
	}
}

var importConteiner = new IdfImportConteiner;


