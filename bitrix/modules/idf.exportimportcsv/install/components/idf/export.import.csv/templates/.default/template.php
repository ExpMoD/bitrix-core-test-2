<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
CJSCore::Init(array("jquery"));

if(in_array($arParams["DECOR_COLOR_BUTTONS"], array("red", "yellow", "orange", "green", "blue")))
{
	$buttonClass = "button-{$arParams["DECOR_COLOR_BUTTONS"]}";
	$buttonStyle = "";
}
else
{
	$buttonClass = "";
	$buttonStyle = "background-color: {$arParams["DECOR_COLOR_BUTTONS"]};";
}

if(!empty($arParams["DECOR_COLOR_BUTTONS_TEXT"]))
{
	$buttonStyle .= "color: {$arParams["DECOR_COLOR_BUTTONS_TEXT"]};";
}

if($arResult['success'])
{
	if($arResult['action'] == 'export') { ?>

		<p><?=GetMessage('SUCCESS_TEXT')?> <a href="<?=$arResult['filename']?>"><?=GetMessage('SUCCESS_LINK')?></a></p>

	<? } elseif($arResult['action'] == 'import') { ?>

		<p><?=GetMessage('SUCCESS_MESSAGE')?></p>

		<p><?=GetMessage('CORRENT_ELEMENTS_COUNT')?><?=$arResult['correct']?></p>
		<p><?=GetMessage('UNCORRENT_ELEMENTS_COUNT')?><?=$arResult['uncorrect']?></p>
		<?if(!empty($arResult['errors'])) { ?>
			<p><?=GetMessage('ERRORS_TITLE')?></p>
			<?foreach($arResult['errors'] as $error) { ?>
				<p><?=$error?></p>
			<? } ?>
		<? } ?>

	<? }
}
elseif(!empty($arResult['error']))
{
	ShowError($arResult['error']);
}

?>

<form action="" method="post" enctype="multipart/form-data" class="csv-form">
	<?=bitrix_sessid_post()?>
	<input type="file" name="import_file">
	<input type="hidden" name="action_idfc">
	<input type="hidden" name="params" value="<?=htmlspecialcharsbx(json_encode($arParams))?>">

	<input type="button" class="csv-form-export <?=$buttonClass?>" style="<?=$buttonStyle?>" value="<?=GetMessage('EXPORT_BUTTON')?>">
	<input type="button" class="csv-form-import <?=$buttonClass?>" style="<?=$buttonStyle?>" value="<?=GetMessage('IMPORT_BUTTON')?>">

</form>

<div class="process-box" style="display: none;">
	<h4 id="loading_process"><?=GetMessage("LOADING_MESSAGE")?></h4>

	<div class="loading-conteiner">
		<div class="loading-content"></div>
	</div>
</div>


<div class="success-box" style="display: none;">
	<p><?=GetMessage('SUCCESS_MESSAGE')?></p>

	<p class="corrent-elements-count"><?=GetMessage('CORRENT_ELEMENTS_COUNT')?><span></span></p>
	<p class="added-elements-count"><?=GetMessage('ADDED_ELEMENTS_COUNT')?><span></span></p>
	<p class="uncorrent-elements-count"><?=GetMessage('UNCORRENT_ELEMENTS_COUNT')?><span></span></p>
</div>

<div class="error-box" style="display: none;">
	<p><?=GetMessage('ERRORS_TITLE')?></p>
	<span class="error-container"></span>
</div>


<script type="text/javascript">
	importConteiner.setData('url', '<?=$component->getPath() . "/ajax.php"?>');
</script>
