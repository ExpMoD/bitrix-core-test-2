<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!\Bitrix\Main\Loader::includeModule("iblock") || !\Bitrix\Main\Loader::includeModule("catalog"))
	return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();
$arIBlock = array();
$elementData = array();
$elementProps = array();
$elementPrices = array();
$sections = array(0 => '');

$arCurrentValues["IBLOCK_TYPE"] = !empty($arCurrentValues["IBLOCK_TYPE"]) ? $arCurrentValues["IBLOCK_TYPE"] : key($arIBlockType);

$rsIBlock = CIBlock::GetList(array("sort" => "asc"), array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$haveIblock = isset($arIBlock[$arCurrentValues['IBLOCK_ID']]);
$haveCatalog = false;

if($haveIblock)
{
	$elementData = array(
		'NAME' => GetMessage('FIELD_NAME'),
		'CODE' => GetMessage('FIELD_CODE'),
		'XML_ID' => GetMessage('FIELD_XML_ID'),
		'DETAIL_TEXT' => GetMessage('FIELD_DETAIL_TEXT'),
		'PREVIEW_TEXT' => GetMessage('FIELD_PREVIEW_TEXT'),
		'ACTIVE' => GetMessage('FIELD_ACTIVE'),
		'IBLOCK_SECTION_ID' => GetMessage('FIELD_IBLOCK_SECTION_ID'),
		'PREVIEW_PICTURE' => GetMessage('FIELD_PREVIEW_PICTURE'),
		'DETAIL_PICTURE' => GetMessage('FIELD_DETAIL_PICTURE'),
	);

	$res = CIBlockProperty::GetList(
		array(),
		array(
			'IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'],
			'ACTIVE' => 'Y',
			'MULTIPLE' => 'N'
		)
	);

	while($prop = $res->Fetch())
	{
		if(!in_array($prop['PROPERTY_TYPE'], array('S', 'N', 'F', 'L')))
			continue;

		$elementProps[$prop['CODE']] = $prop['NAME'];
	}

	$res = CIBlockSection::GetTreeList(
		array('IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'], 'GLOBAL_ACTIVE' => "Y"),
		array()
	);

	$sortingSections = array();
	while($section = $res->Fetch())
	{
		$sortingSections[$section["IBLOCK_SECTION_ID"]][$section["ID"]] = array(
			"ID" => $section["ID"],
			"SORT" => $section["SORT"],
			"DEPTH_LEVEL" => $section["DEPTH_LEVEL"],
			"NAME" => $section["NAME"],
			"IBLOCK_SECTION_ID" => $section["IBLOCK_SECTION_ID"],
		);
	}

	foreach($sortingSections as $depth => $sortingSection)
	{
		uasort($sortingSections, function($a, $b){
			if($a["SORT"] == $b["SORT"]) return 0;
			return $a["SORT"] < $b["SORT"] ? -1 : 1;
		});

		$sortingSections[$depth] = $sortingSection;
	}

	if(!function_exists("sectionAssembly"))
	{
		function sectionAssembly($sortingSections, $parent)
		{
			if(!isset($sortingSections[$parent]))
				return array();

			$result = array();

			foreach($sortingSections[$parent] as $sectionId => $section)
			{
				$result[] = $section;
				$result = array_merge($result, sectionAssembly($sortingSections, $section["ID"]));
			}

			return $result;
		};
	}

	$newOrigin = sectionAssembly($sortingSections, "");

	foreach($newOrigin as $id => $section)
	{
		$tab = '';
		for($i = 0; $i < $section['DEPTH_LEVEL']; $i++)
		{
			$tab .= '-';
		}

		$sections[(string)$section["ID"] . "m"] = $tab . $section['NAME'];
	}

	$haveCatalog = CCatalog::GetByID($arCurrentValues['IBLOCK_ID']);

	if($haveCatalog)
	{
		$res = CCatalogGroup::GetList(
			array(),
			array(),
			false,
			false,
			array('ID', 'NAME', 'NAME_LANG')
		);

		while($price = $res->Fetch())
		{
			$elementPrices[$price['ID']] = $price['NAME_LANG'];
		}
	}

}


$arComponentParameters = array(
	"GROUPS" => array(
		"FIELDS" => array(
			"NAME" => GetMessage("GROUP_FIELDS"),
		),
		"FILTERS" => array(
			"NAME" => GetMessage("GROUP_FILTERS"),
		),
		"DECOR" => array(
			"NAME" => GetMessage("GROUP_DECOR"),
		),
		"OTHER" => array(
			"NAME" => GetMessage("GROUP_OTHER"),
		),
	),
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("IBLOCK_IBLOCK"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		),
		"DECOR_COLOR_BUTTONS" => array(
			"PARENT" => "DECOR",
			"NAME" => GetMessage("DECOR_COLOR_BUTTONS"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"DEFAULT" => "orange",
			"VALUES" => array(
				"red" => GetMessage("COLOR_RED"),
				"orange" => GetMessage("COLOR_ORANGE"),
				"yellow" => GetMessage("COLOR_YELLOW"),
				"green" => GetMessage("COLOR_GREEN"),
				"blue" => GetMessage("COLOR_BLUE"),
			),
		),
		"DECOR_COLOR_BUTTONS_TEXT" => array(
			"PARENT" => "DECOR",
			"NAME" => GetMessage("DECOR_COLOR_BUTTONS_TEXT"),
			"TYPE" => "STRING",
		),
		"IMAGES_PATH" => array(
			"PARENT" => "OTHER",
			"NAME" => GetMessage("IMAGES_PATH"),
			"TYPE" => "STRING",
			"ADDITIONAL_VALUES" => "N",
			"DEFAULT" => ""
		),
		"CODE_FROM_NAME" => array(
			"PARENT" => "OTHER",
			"NAME" => GetMessage("CODE_FROM_NAME"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y"
		),
	)
);


if($haveIblock)
{
	$arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
		"ELEMENT_DATA" => array(
			"PARENT" => "FIELDS",
			"NAME" => GetMessage('PROP_ELEMENT_DATA'),
			"TYPE" => "LIST",
			"VALUES" => $elementData,
			"MULTIPLE" => "Y",
			"REFRESH" => "N",
		),
		"ELEMENT_PROPS" => array(
			"PARENT" => "FIELDS",
			"NAME" => GetMessage('PROP_ELEMENT_PROPS'),
			"TYPE" => "LIST",
			"VALUES" => $elementProps,
			"MULTIPLE" => "Y",
			"REFRESH" => "N",
		),
		"SECTION" => array(
			"PARENT" => "FILTERS",
			"NAME" => GetMessage('PROP_SECTION'),
			"TYPE" => "LIST",
			"VALUES" => $sections,
			"MULTIPLE" => "N",
			"REFRESH" => "N",
		),
	));
}


if($haveCatalog)
{
	$arComponentParameters['PARAMETERS'] = array_merge($arComponentParameters['PARAMETERS'], array(
		"PRICES" => array(
			"PARENT" => "FIELDS",
			"NAME" => GetMessage('PROP_PRICES'),
			"TYPE" => "LIST",
			"VALUES" => $elementPrices,
			"MULTIPLE" => "Y",
			"REFRESH" => "N",
		),
		"REMAINS" => array(
			"PARENT" => "FIELDS",
			"NAME" => GetMessage('PROP_REMAINS'),
			"TYPE" => "CHECKBOX",
			"REFRESH" => "N",
		),
	));
}

