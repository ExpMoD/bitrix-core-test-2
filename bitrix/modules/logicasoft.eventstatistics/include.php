<?php

Class CLogicasoftEventstatistics
{
	public function OnBeforeEventSend(&$arFields, &$arTemplate) {
		$fields = $arFields;
		$template = $arTemplate;

		$rsEM = CEventMessage::GetByID($arTemplate['ID']);
		$arEM = $rsEM->Fetch();

		$arParamsTranslit = array(
			'max_len' => 100,
			'change_case' => 'L',
			'replace_space' => '_',
			'replace_other' => '_',
			'delete_repeat_replace' => 'true'
		);

		$utm_source = mb_convert_case($arEM['EVENT_NAME'], MB_CASE_LOWER, 'utf-8');
		$utm_medium = 'email';
		$utm_campaign = Cutil::translit($arEM['SUBJECT'], 'ru', $arParamsTranslit);
		$utm_content = array();

		//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/event_'.$arTemplate['ID'].'.txt', date('d.m.Y H:i')."\nDATA:\n".print_r($arFields, true)."\n".print_r($arTemplate, true)."\n".print_r($arEM, true)."\n", FILE_APPEND);

		if ($arTemplate['BODY_TYPE'] == 'text') {
			$reg = '/(https?:\/\/(www.)?(('.SITE_SERVER_NAME.')|(#[^#]+#))([^\s]+))/i';
		} else {
			/*$reg = '/href="(https?:\/\/(www.)?(('.SITE_SERVER_NAME.')|(#[^#]+))([^"]+)?)"([^>]+)?>([^<]+)</i';*/
			$reg = '/href="(https?:\/\/(www.)?(('.SITE_SERVER_NAME.')|(#[^#]+))([^"]+)?)"([^>]+)?>(([^<]+|<img([^>]+)>)+)</i';
		}

		foreach ($arFields as $field=>$value) {
			preg_match_all($reg, $fields, $out, PREG_OFFSET_CAPTURE);
			//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/event_'.$arTemplate['ID'].'.txt', "arFields ".$field.":\n\n".print_r($out, true), FILE_APPEND);
		}

		preg_match_all($reg, $arTemplate['MESSAGE'], $out, PREG_OFFSET_CAPTURE);
		//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/event_'.$arTemplate['ID'].'.txt', "\nMESSAGE:\n\n".print_r($out, true)."\n", FILE_APPEND);
		$emailMessage = $arTemplate['MESSAGE'];

		$delta = 0;
		foreach ($out[1] as $key=>$ar) {

			if ($arTemplate['BODY_TYPE'] == 'text') {
				$utm_content[$key] = $out[6][$key][0];
				$link = $ar[0];
			} else {
				$utm_content[$key] = strip_tags($out[8][$key][0]);
				if (!strlen(trim($utm_content[$key])) && preg_match_all('/(alt="([^"]+)"|title="([^"]+)")/i', $out[8][$key][0], $_out, PREG_OFFSET_CAPTURE)) {
					if (is_array($_out[2])) {
						if (isset($_out[2][1])) {
							$utm_content[$key] = '[IMG]: '.$_out[2][1][0];
						} else if (isset($_out[2][0][0]) && strlen($_out[2][0][0])) {
							$utm_content[$key] = '[IMG]: '.$_out[2][0][0];
						} else if (isset($_out[3][0])) {
							$utm_content[$key] = '[IMG]: '.$_out[3][0][0];
						} else {
							$utm_content[$key] = '[IMG]: '.($key + 1);
						}
					}
				} else if (!strlen(trim($utm_content[$key])) && preg_match_all('/src="([^"]+)"/i', $out[8][$key][0], $_out, PREG_OFFSET_CAPTURE)) {
					$utm_content[$key] = '[IMG]: '.($key + 1);
				}
				$link = $ar[0];
			}

			if (mb_substr($link, 0, 7, 'utf-8') == 'mailto:') {
				continue;
			}

			if (strpos($link, '?') === false) {
				$symbol = '?';
			} else {
				$symbol = '&';
			}
			$link .= $symbol.'utm_source='.$utm_source.'&utm_medium='.$utm_medium.'&utm_campaign='.$utm_campaign.'&utm_content='.htmlentities(urlencode($utm_content[$key]));

			if ($arTemplate['BODY_TYPE'] == 'text') {
				$emailMessage = substr_replace($emailMessage, $link, ($ar[1] + $delta), mb_strlen($ar[0], 'utf-8'));
			} else {
				$emailMessage = substr_replace($emailMessage, $link, ($ar[1] + $delta), mb_strlen($ar[0], 'utf-8'));
			}

			$delta += mb_strlen($link, 'utf-8') - mb_strlen($ar[0], 'utf-8');
		}
		//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/event_'.$arTemplate['ID'].'.txt', "FINAl MESSAGE:\n".$emailMessage."\n#####################################\n\n\n", FILE_APPEND);
		$arTemplate['MESSAGE'] = $emailMessage;

		return true;
	}

	public function BeforePostingSendMail($arFields) {
		$utm_source = 'posting_'.$arFields['POSTING_ID'];
		$utm_medium = 'email';
		$utm_campaign = urlencode(mb_convert_case(base64_decode(mb_substr($arFields['SUBJECT'], 10, -2, 'utf-8')), MB_CASE_LOWER, 'utf-8'));
		$utm_content = array();

		$reg = '/href="(https?:\/\/(www.)?(('.SITE_SERVER_NAME.')|(#[^#]+))([^"]+)?)"([^>]+)?>(([^<]+|<img([^>]+)>)+)</i';
		preg_match_all($reg, $arFields['BODY'], $out, PREG_OFFSET_CAPTURE);
		//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/posting_'.$arFields['POSTING_ID'].'.txt', "\nLINKS:\n\n".print_r($out, true)."\n", FILE_APPEND);
		$emailMessage = $arFields['BODY'];

		$delta = 0;
		foreach ($out[1] as $key=>$ar) {
			$utm_content[$key] = strip_tags($out[8][$key][0]);
			if (!strlen(trim($utm_content[$key])) && preg_match_all('/(alt="([^"]+)"|title="([^"]+)")/i', $out[8][$key][0], $_out, PREG_OFFSET_CAPTURE)) {
				//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/posting_'.$arFields['POSTING_ID'].'.txt', "\nIMG:\n\n".print_r($_out, true)."\n", FILE_APPEND);
				if (is_array($_out[2])) {
					if (isset($_out[2][1])) {
						$utm_content[$key] = '[IMG]: '.$_out[2][1][0];
					} else if (isset($_out[2][0][0]) && strlen($_out[2][0][0])) {
						$utm_content[$key] = '[IMG]: '.$_out[2][0][0];
					} else if (isset($_out[3][0])) {
						$utm_content[$key] = '[IMG]: '.$_out[3][0][0];
					} else {
						$utm_content[$key] = '[IMG]: '.($key + 1);
					}
				}
			} else if (!strlen(trim($utm_content[$key])) && preg_match_all('/src="([^"]+)"/i', $out[8][$key][0], $_out, PREG_OFFSET_CAPTURE)) {
				$utm_content[$key] = '[IMG]: '.($key + 1);
			}

			$link = $ar[0];

			if (mb_substr($link, 0, 7, 'utf-8') == 'mailto:') {
				continue;
			}

			if (strpos($link, '?') === false) {
				$symbol = '?';
			} else {
				$symbol = '&';
			}
			$link .= $symbol.'utm_source='.$utm_source.'&utm_medium='.$utm_medium.'&utm_campaign='.$utm_campaign.'&utm_content='.htmlentities(urlencode($utm_content[$key]));

			$emailMessage = substr_replace($emailMessage, $link, ($ar[1] + $delta), mb_strlen($ar[0], 'utf-8'));

			$delta += mb_strlen($link, 'utf-8') - mb_strlen($ar[0], 'utf-8');
		}
		//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/posting_'.$arFields['POSTING_ID'].'.txt', date('d.m.Y H:i')."\nBODY:\n".$emailMessage."\n", FILE_APPEND);
		$arFields['BODY'] = $emailMessage;

		return $arFields;
	}
}