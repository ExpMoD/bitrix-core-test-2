<?
$MESS["logicasoft.eventstatistics_MODULE_NAME"] = "Отслеживание статистики по переходам из рассылок";
$MESS["logicasoft.eventstatistics_MODULE_DESC"] = "Модуль позволяет через Google Analytics отслеживать переходы по всем ссылкам из писем отправляемых сайтом";
$MESS["logicasoft.eventstatistics_PARTNER_NAME"] = "LogicaSoft LLC";
$MESS["logicasoft.eventstatistics_PARTNER_URI"] = "http://logicasoft.pro";
?>