<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
		die();


		if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog") || !CModule::IncludeModule("sale"))
			return;
		if ($this->StartResultCache()){
			$arSelect = Array("ID", "NAME", "IBLOCK_ID", "DETAIL_PICTURE", "CATALOG_QUANTITY", "DETAIL_PAGE_URL", "PROPERTY_MARKER", "PROPERTY_RATING");
			$arFilter = Array("IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"], "IBLOCK_ID" => $arParams["IBLOCK_ID"], "PROPERTY_".$arParams["PROP_NAME"]."_VALUE" => "Y", "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			$res = CIBlockElement::GetList(array("DATE_MODIFY_FROM" => "ASC"), $arFilter, false, Array(), $arSelect);
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				$arButtons = CIBlock::GetPanelButtons(
					$arFields["IBLOCK_ID"],
					$arFields["ID"],
					$arFields["ID"],
					array("SECTION_BUTTONS" => false, 
						  "SESSID" => false, 
						  "CATALOG" => true
					)
				);
				$arFields["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
				$arFields["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
				$arFields["IMG"] = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width' => $arParams["PICTURE_WIDTH"], 'height' => $arParams["PICTURE_HEIGHT"]), BX_RESIZE_IMAGE_PROPORTIONAL, true); 
				$arResult["ITEMS"][] = $arFields;
			}

			$this->IncludeComponentTemplate();
		}

?>