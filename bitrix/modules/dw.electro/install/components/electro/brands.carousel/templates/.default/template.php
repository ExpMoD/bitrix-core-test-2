<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult["ITEMS"])):?>
	<div id="brandCarousel">
		<div class="wrap">
			<ul>
				<?foreach($arResult["ITEMS"] as $arElement):?>
					<?
						$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array());
					?>
					<li id="<?=$this->GetEditAreaId($arElement['ID']);?>"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["IMG"]["src"]?>" alt="<?=$arElement["NAME"]?>"></a></li>	
				<?endforeach;?>
			</ul>
    	</div>
	    <a href="#" class="brandBtnLeft"></a>
    	<a href="#" class="brandBtnRight"></a>
	</div>
<?endif;?>