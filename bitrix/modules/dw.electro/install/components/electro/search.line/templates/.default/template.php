<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true);?>
<div id="topSearch">
	<div id="topSearchLine">
		<div id="reloadSearch"></div>
		<form action="<?=SITE_DIR?>search/" method="GET" id="searchForm">
			<table id="searchTable">
				<tr>
					<td class="inputSelect">
						<select name="where" id="searchLineSelectParse">
							<option value="0"><?=GetMessage("ALL")?></option>
							<?if(!empty($arResult["SECTIONS"])):?>
								<?foreach ($arResult["SECTIONS"] as $index => $arSection):?>
									<option value="<?=$arSection["ID"]?>"<?if($arSection["SELECTED"]):?> selected <?endif;?>><?=$arSection["NAME"]?></option>
								<?endforeach;?>
							<?endif;?>
						</select>
						<div id="topSearchSelect">
							<div class="slide">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/searchElement1.png">
								<span><?=GetMessage("ALL")?></span>
								<img src="<?=SITE_TEMPLATE_PATH?>/images/arrow_s.png">
							</div>
							<ul class="searchSlide"></ul>
						</div>
					</td>
					<td class="inputData">
						<input type="text" name="q" value="" data-id="<?=$arParams["IBLOCK_ID"]?>" autocomplete="off" placeholder="<?=!empty($arResult["q"]) ? $arResult["q"] : GetMessage("SMART_SEARCH")?>" id="topQ" />
					</td>
					<td class="inputSubmit">
						<a href="#" id="goTopSearch"><img src="<?=SITE_TEMPLATE_PATH?>/images/searchElement2.png"><span><?=GetMessage("GO")?></span></a>
					</td>
				</tr>
				<tr>
					<td id="searchMenu" colspan="3">
						<ul>
							<li><a href="<?=SITE_DIR?>contact/"><?=GetMessage("STORES")?></a></li>
						</ul>
					</td>
				</tr>
			</table>
			<input type="hidden" name="r" value="Y">
		</form>
	</div>
</div>

