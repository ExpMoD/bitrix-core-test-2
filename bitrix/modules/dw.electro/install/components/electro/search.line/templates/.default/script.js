var searchTimeout;

$(window).bind("load", function(){

	var changeSearch = function(event){
		var $this = (event.data.direction == "input") ? $(this) : $("#topQ");

		if($this.val() != ""){
			$sectionMenu = $("#searchLineSelectParse").find("option:selected");
			clearTimeout(searchTimeout);
			searchTimeout = setTimeout(function(){
				sendDataSearch($this.val(), $sectionMenu.val(), $this.val(), $this.data("id"), $("#searchForm").attr("action"));
			}, 250);
		}else{
			clearTimeout(searchTimeout);
			makeObject(false);
		}

	};

	var sendDataSearch = function(value, section, q, iblock_id, action){
		$.getJSON(ajaxPath + "?act=search&name=" + encodeURI(value) + "&section=" + section + "&iblock_id=" + iblock_id, function(data){
			makeObject(data, q, section, action);
		});
	};

	var submitSearch = function(event){
		$("#searchForm").submit();
	};

	var closeSearch = function(event){
		$("#reloadSearch").stop().empty().hide();
		clearTimeout(searchTimeout);
	};

	var makeObject = function(data, q, section, action){
		$main = $("#reloadSearch");
		$main.empty().show();
		$mainTable = $("#reloadSearch").append($("<table/>")).find("table");
		if(data != ""){
			$main.append('<a href="'+ action + '?q=' + q + '&where=' + section + '&r=Y" class="allResult">' + LANG["ALL_RESULT"] + '</a>');
			for (var i = 0; i <= data.length -1; i++){
				$mainTable.append(
					$("<tr/>").append(
						'<td class="photo"><a href="' + data[i]["DETAIL_PAGE_URL"] + '"><img src="' + data[i]["DETAIL_PICTURE"] + '" title="' + data[i]["NAME"] + '"></a></td>' + 
						'<td class="name"><a href="' + data[i]["DETAIL_PAGE_URL"] + '">' + data[i]["NAME"] + '</a></td>' +
						'<td class="quantity">' + (data[i]["CATALOG_QUANTITY"] > 0 ? '<span class="available">' + LANG["AVAILABLE"] + '</span>' : '<span class="noAvailable">' + LANG["NOAVAILABLE"] + '</span>') + '</td>' + 
						'<td class="price"><span class="price"><s>' + (data[i]["DISCONT_PRICE"] != 0 ? data[i]["DISCONT_PRICE"] : "") + ' </s>' + data[i]["PRICE"] + ' </td>' + 
						'<td class="bask"><a href="#" class="addCart" data-id="' + data[i]["ID"] + '">' + LANG["ADD_CART"] + '</a></td>'
					)
				);
			}
		}else{
			$main.hide();
		}
	};

	$(document).on("keyup change paste focus", "#topQ", {direction: "input"}, changeSearch);
	$(document).on("change", "#searchLineSelectParse", {direction: "select"}, changeSearch);
	$(document).on("click", "#topSearchLine", function(event){event.stopImmediatePropagation();});
	$(document).on("click", "#goTopSearch", submitSearch);
	$(document).on("click", closeSearch);

});

$(document).ready(function() {

	var link = $("#topSearchLine");

	link.find("option").each(function() {
		link.find(".searchSlide").append(
			$("<li>").html($(this).text())
		);
	});
	if (link.find("option:selected").length) {
		link.find(".slide span").text($(".searchSlide li").eq(link.find("option:selected").index()).text().substr(0, 5));
	}

	$(document).on('click', '#topSearchSelect', function(e) {
		link.find(".searchSlide").stop().slideToggle("fast");
	});

	$(document).on('click', '#topSearchSelect ul li', function(e) {
		link.find(".slide span").text($(this).text().substr(0, 5));
		link.find("option").removeAttr("selected").eq($(this).index()).attr("selected", "selected").trigger("change");
		link.find(".searchSlide").stop().hide("fast");
	});

	$(document).keydown(function(e) {
		var el = $("#topSearchSelect ul");
		if (e.keyCode == 27 && el.is(":visible")) {
			link.find(".searchSlide").slideToggle("fast");
		}
	});

	$(document).click(function(e) {
		if ($(e.target).parent().attr("class") != "slide" && $(e.target).parent().attr("class") != "searchSlide" && $(e.target).attr("id") != "topSearchSelect") {
			link.find(".searchSlide").stop().slideUp("fast");
		}
	});

});