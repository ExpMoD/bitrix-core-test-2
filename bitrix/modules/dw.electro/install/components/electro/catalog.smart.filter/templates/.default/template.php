<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter" id="smartFilterForm">
	<?foreach($arResult["HIDDEN"] as $arItem):?>
		<input
			type="hidden"
			name="<?echo $arItem["CONTROL_NAME"]?>"
			id="<?echo $arItem["CONTROL_ID"]?>"
			value="<?echo $arItem["HTML_VALUE"]?>"
		/>
	<?endforeach;?>
	<div class="filtren" id="smartFilter">
		<span class="hd2"><?echo GetMessage("CT_BCSF_FILTER_TITLE")?></span>
		<ul>
		<?sort($arResult["ITEMS"]);?>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?if($arItem["PROPERTY_TYPE"] == "N" || isset($arItem["PRICE"])):?>
			<?if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
				continue;
			?>
			<li class="lvl1"> <a href="#" onclick="return false;" class="showchild hd3" style="cursor:default"><?=preg_replace("/\[.*\]/", "", $arItem["NAME"])?></a>
				<ul id="ul_<?=$arItem["ID"]?>">
					<li class="lvl2">
						<div class="rangeSlider" id="sl_<?=$arItem["ID"]?>">
							<label><?=GetMessage("CT_BCSF_FILTER_IN")?></label><input name="<?=$arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>" type="text" value="<?=empty($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? round($arItem["VALUES"]["MIN"]["VALUE"]) : round($arItem["VALUES"]["MIN"]["HTML_VALUE"])?>" id="<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>" onchange="smartFilter.keyup(this)">
							<label><?=GetMessage("CT_BCSF_FILTER_OUT")?></label><input name="<?=$arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>" type="text" value="<?=empty($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? round($arItem["VALUES"]["MAX"]["VALUE"]) : round($arItem["VALUES"]["MAX"]["HTML_VALUE"])?>" id="<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>" onchange="smartFilter.keyup(this)">
							<div class="slider">
								<div class="handler">
									<div class="blackoutLeft"><ins id="s_<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>" class="left"></ins></div>
									
									<div class="blackoutRight"><ins id="s_<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>" class="right"></ins></div>
								    
								</div>
							</div>
						</div>
						<script>
							$(function(){
								$("#sl_<?=$arItem["ID"]?>").rangeSlider({
									min: <?=round($arItem["VALUES"]["MIN"]["VALUE"])?>,
									max: <?=round($arItem["VALUES"]["MAX"]["VALUE"])?>,
									step: 1,
									leftButton: "#s_<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>",
									rightButton: "#s_<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>",
									inputLeft: "#<?=$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>",
									inputRight: "#<?=$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
								});
							});
						</script>
					</li>
				</ul>
			</li>
			<?elseif(!empty($arItem["VALUES"])):;?><?$i = 0;?>
			<li class="lvl1"> <a href="#" onclick="return false;" style="cursor:default"  class="showchild hd3"><?=preg_replace("/\[.*\]/", "", $arItem["NAME"])?></a>
				<ul id="ul_<?echo $arItem["ID"]?>">
					<?foreach($arItem["VALUES"] as $val => $ar):?><?$i++;?> 
					<li class="lvl2<?if($i > 5):?> off<?endif;?><?echo $ar["DISABLED"]? ' lvl2_disabled': ''?>"><input
						type="checkbox"
						value="<?echo $ar["HTML_VALUE"]?>"
						name="<?echo $ar["CONTROL_NAME"]?>"
						id="<?echo $ar["CONTROL_ID"]?>"
						<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
						class="electroCheck"
						data-class="electroCheck_div"
						<?echo $ar["DISABLED"]? ' disabled': ''?>
					/><label for="<?echo $ar["CONTROL_ID"]?>"><?=preg_replace("/\[.*\]/", "", $ar["VALUE"])?></label></li>
					<?endforeach;?>
				</ul>
				<?if(count($arItem["VALUES"]) > 5):?>
					<a href="#" class="showALL"><?echo GetMessage("CT_BCSF_FILTER_SHOW_ALL")?> (<?=count($arItem["VALUES"])?>)</a>
				<?endif;?>
			</li>
			<?endif;?>
		<?endforeach;?>
		</ul>


		<a href="javascript:void(0)" id="set_filter"><img src="<?=SITE_TEMPLATE_PATH?>/images/filter.png"><?=GetMessage("CT_BCSF_SET_FILTER")?></a>
		<a href="javascript:void(0)" id="del_filter"><img src="<?=SITE_TEMPLATE_PATH?>/images/clear.png"><?=GetMessage("CT_BCSF_DEL_FILTER")?></a>

		<div class="modef" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?>>
			<a href="#" class="close"></a>
			<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
			<a href="javascript:void(0)" class="showchild" id="modef_send"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
		</div>
	</div>
</form>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
</script>
<?endif;?>