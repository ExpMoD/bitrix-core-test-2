<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if (!empty($arResult)):?>
<td id="looked">
	<div class="right">
		<span class="heading"><?=GetMessage("HEADING")?></span>
		<ul>
			<?foreach($arResult as $arValues):?>
			<li>
				<a href="<?=$arValues["DETAIL_PAGE_URL"]?>" class="pic" target="_blank">
					<img src="<?=!empty($arValues["PICTURE"]["src"]) ? $arValues["PICTURE"]["src"] : SITE_TEMPLATE_PATH."/images/empty.png"?>" title="<?=$arValues["NAME"]?>"></a>
				<a href="<?=$arValues["DETAIL_PAGE_URL"]?>" class="name" target="_blank"><?=substr($arValues["NAME"], 0, 80)?></a>
				<span class="price">		      
					<?=str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arValues["PRICE_FORMATED"]);?>
					<?=!empty($arValues["OLD_PRICE"]) ? '<s>'.$arValues["OLD_PRICE"].'</s>' : ''?>
				</span>
				<a href="#" class="addCart" data-reload="Y" data-id="<?=$arValues["PRODUCT_ID"]?>"><?=GetMessage("ADDCART")?></a>
			</li>
			<?endforeach;?>
		</ul>
	</div>
</td>
<?endif;?>