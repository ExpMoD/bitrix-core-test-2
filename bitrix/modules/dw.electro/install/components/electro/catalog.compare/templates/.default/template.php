<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); $this->setFrameMode(true);?>
<?if(!empty($arResult["ITEMS"])):?>
<div id="compare">
	<ul id="compareCheck">
		<?foreach($arResult["PROPERTIES"] as $index => $arValues):?>
			<?if($arValues["SORT"] <= 500):?>
				<li>
					<input name="<?=$arValues["ID"]?>" type="checkbox" value="asdasd" class="electroCheck" id="<?=$arValues["ID"]?>" data-class="electroCheck_div"> 
					<label> <?=$arValues["NAME"];?> </label>
				</li>
			<?endif;?>
		<?endforeach;?>
	</ul>
	<div id="compareTools">
		<a href="#" class="hide"><?=GetMessage("HIDE");?></a>
		<a href="#" class="show"><?=GetMessage("SHOW");?></a>
	</div>
	<div id="compareBlock">

		<table>
			<tr>
				<td class="left">
					<div class="wrap">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/empty.png" alt="" class="compareImage">
						<div class="headingTools">
							<div class="leftTools">
								<a href="#" class="all"><?=GetMessage("ALLFEATURES");?></a>
								<a href="#" class="different"><?=GetMessage("DISTINGUISHED");?></a>
							</div>
						</div>
						<ul class="propList">
							<?foreach ($arResult["PROPERTIES"] as $index => $arProp):?>
								<li data-id="<?=$arProp["ID"]?>" title="<?=$arProp["NAME"]?>"><?=$arProp["NAME"]?></li>
							<?endforeach;?>
						</ul>
					</div>
				</td>
				<td class="right">
					<div id="topScroll">
						<div id="fakeScroll"></div>
					</div>
					<div id="scrollTable">
						<ul>
							<?foreach($arResult["ITEMS"] as $index => $arElement):?>
							<li>
								<div class="scrollElement">
									<ins data-id="<?=$arElement[0]["ID"]?>"></ins>
									<div class="imgBlock"><img src="<?=!empty($arElement[0]["PICTURE"]["src"]) ? $arElement[0]["PICTURE"]["src"] : SITE_TEMPLATE_PATH."/images/empty.png" ?>" alt="<?=$arElement[0]["NAME"]?>"></div>
									<a href="<?=$arElement[0]["DETAIL_PAGE_URL"]?>" class="name"><?=$arElement[0]["NAME"]?></a>
									<span class="price"><?=str_replace(GetMessage("RUB"), "<span class=\"rouble\">P<i>-</i></span>", $arElement[0]["PRICE"])?></span>
									<a href="#" class="addCart" data-id="<?=$arElement[0]["ID"]?>"><?=GetMessage("ADDCART");?></a>
								</div>	
								<ul class="propList check">
									<?foreach ($arElement["PROPERTIES"] as $index => $arProp):?>
										<li data-id="<?=$arProp["ID"]?>"><?=$arProp["VALUE"]?></li>
									<?endforeach;?>
								</ul>
							</li>
							<?endforeach;?>
						</ul>
					</div>
				</td>
			</tr>
		</table>
</div>
</div>
<?else:?>
<div id="empty">
	<img src="<?=SITE_TEMPLATE_PATH?>/images/emptyCompare.png" alt="<?=GetMessage("EMPTY_HEAD");?>" class="emptyImg">
	<div class="info">
		<h3><?=GetMessage("EMPTY_HEAD");?></h3>
		<p><?=GetMessage("EMPTY");?></p>
		<a href="<?=SITE_DIR?>" class="back"><?=GetMessage("MAIN");?></a>
	</div>
</div>
<?endif;?>