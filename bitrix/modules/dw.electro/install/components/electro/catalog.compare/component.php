<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true){
		die();
	}
	if(!CModule::IncludeModule("iblock") || !CModule::IncludeModule("catalog") || !CModule::IncludeModule("sale"))
	return;
?>
<?	
	$arResult = array();
	if(!empty($_SESSION["COMPARE_LIST"]["ITEMS"])){
		$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "IBLOCK_ID", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "CATALOG_GROUP_1");
		$arFilter = Array('IBLOCK_TYPE' => $arParams["IBLOCK_TYPE"], 'IBLOCK_ID' => $arParams["IBLOCK_ID"], "ID" => $_SESSION["COMPARE_LIST"]["ITEMS"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()){ 
			$arResult["ITEMS"][] = array(
				$ob->GetFields(), 
				"PROPERTIES" => $ob->GetProperties()
			);
		}
		foreach ($arResult["ITEMS"] as $index => $arElement) {
			foreach ($arElement["PROPERTIES"] as $i => $arValue) {
				if(!empty($arValue["VALUE"]) && $arValue["SORT"] <= 500){
					if($arValue["PROPERTY_TYPE"] != "S" && $arValue["MULTIPLE"] != "Y"){
						$arValue["NAME"] = preg_replace("/\[.*\]/", "", trim($arValue["NAME"]));
						$arResult["PROPERTIES"][$i] = $arValue;
					}	
				}
			}
		}
		foreach ($arResult["ITEMS"]  as $index => $arValue){
			$arResult["ITEMS"][$index][0]["PICTURE"] = CFile::ResizeImageGet($arValue[0]["DETAIL_PICTURE"], array("width" => 200, 'height' => 140), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
			$arFields["DISCONT_PRICE"] = CCatalogDiscount::GetDiscountByProduct(
		        $arValue[0]["ID"],
		        $USER->GetUserGroupArray(),
		        "N",
		        0,
		        SITE_ID
		    );
			if($arFields["DISCONT_PRICE"][0]["VALUE_TYPE"] == "S"){
				$arResult["ITEMS"][$index][0]["PRICE"] = SaleFormatCurrency($arFields["DISCONT_PRICE"][0]["VALUE"], RUB, false);
			}
			elseif($arFields["DISCONT_PRICE"][0]["VALUE_TYPE"] == "F"){ // fixed discont
			    $arResult["ITEMS"][$index][0]["PRICE"] = SaleFormatCurrency(($arValue[0]["CATALOG_PRICE_1"] - $arFields["DISCONT_PRICE"][0]["VALUE"]), RUB, false);
			}elseif($arFields["DISCONT_PRICE"][0]["VALUE_TYPE"] == "P"){
			    $arResult["ITEMS"][$index][0]["PRICE"] = SaleFormatCurrency($arValue[0]["CATALOG_PRICE_1"] - ($arValue[0]["CATALOG_PRICE_1"] * $arFields["DISCONT_PRICE"][0]["VALUE"] / 100), RUB, false);
			}else{
				$arResult["ITEMS"][$index][0]["PRICE"] = SaleFormatCurrency($arValue[0]["CATALOG_PRICE_1"], RUB);
			}
			foreach ($arValue["PROPERTIES"] as $key => $arProp) {
				if(empty($arResult["PROPERTIES"][$key])){
					unset($arResult["ITEMS"][$index]["PROPERTIES"][$key]);
				}
			}
		}
	}
	$this->IncludeComponentTemplate();
?>