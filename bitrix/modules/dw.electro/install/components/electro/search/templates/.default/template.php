<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true);?>
<?if (!empty($arResult["ITEMS"])):?>
<?if(!empty($arResult["SECTIONS"])):?>
<ul id="searchSection">
	<?foreach ($arResult["SECTIONS"] as $i => $arSection):?>
		<li><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a></li>
	<?endforeach;?>
</ul>
<?endif;?>
<?$i=0;?>
<div id="catalogLines">
	<?foreach($arResult["ITEMS"] as $arElement):?>
		<div class="section">
			<table class="item">
				<tbody>
				<?$img = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array('width'=>320, 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
					<tr>
						<td class="picture">
							<?=(!empty($arElement["PROPERTIES"]["MARKER"]["VALUE"]) ? '<ins class="marker">'.$arElement["PROPERTIES"]["MARKER"]["VALUE"].'</ins>' : '')?>

							<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=(!empty($img["src"]) ? $img["src"] : SITE_TEMPLATE_PATH.'/images/empty.png')?>" alt="<?=$arElement["NAME"]?>"></a>
						
						</td>
						<td class="name">
							<div class="wrap">
								<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="elementName"><?=$arElement["NAME"]?></a>
				                <div class="infoBox">
					                <span class="label"><?=GetMessage("RATING")?></span> <span class="rating"> 
					                	<i class="m" style="width:<?=($arElement["PROPERTIES"]["RATING"]["VALUE"] * 100 / 5)?>%"></i><i class="h"></i>
					                </span>
				                	<?=(!empty($arElement["PROPERTIES"]["ARTICLE"]["VALUE"]) ? "<span class=\"article\">".GetMessage("ART")." ".$arElement["PROPERTIES"]["ARTICLE"]["VALUE"]."</span>" : "" )?>
				                </div>
								<table class="prop">
									<tbody>
										<?foreach ($arElement["PROPERTIES"] as $key => $arProp):?>
											<?if(!empty($arProp["VALUE"]) && $arProp["SORT"] <= 500):?>
												<?if($i++ == 5){ $i = 0; break;	}?>
												<tr>
													<td><span><?=preg_replace("/\[.*\]/", "", $arProp["NAME"])?></span></td>
													<td><?if(is_array($arProp["VALUE"])){$arProp["VALUE"] = implode(" /", $arProp["VALUE"]);}?><?=$arProp["VALUE"]?></td>
												</tr>
											<?endif;?>
										<?endforeach;?>
									</tbody>
								</table>
							</div>
						</td>
						<td class="tools">
							<div class="wrap">
								<ul>
									<li class="price">
										<span><?=GetMessage("PRICE")?> </span>
										<?=($arElement["DISCONT_PRICE"] != $arElement["PRICE"] ? '<s>'.str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arElement["DISCONT_PRICE"]).'</s> ' : '')?>
									   <?=str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arElement["PRICE"]);?>								
									</li>
									<li><a href="#" class="addCart" data-id="<?=$arElement["ID"]?>"><?=GetMessage("ADDCART")?></a></li>
									<li>
										<?if($arElement["CATALOG_QUANTITY"] > 0):?>
											<span class="available">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/available.png" alt="<?=GetMessage("AVAILABLE")?>"> <?=GetMessage("AVAILABLE")?>
											</span>
										<?else:?>
											<span class="noAvailable">
											<img src="<?=SITE_TEMPLATE_PATH?>/images/iconNoAvailable.png" alt="<?=GetMessage("AVAILABLE")?>"> <?=GetMessage("NOAVAILABLE")?>
											</span>			  
										<?endif;?>
									</li>
									<li><a href="#" class="addCompare" data-id="<?=$arElement["ID"]?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/iconCompare.png" alt="<?=GetMessage("ADDCOMPARE")?>"><?=GetMessage("ADDCOMPARE")?></a></li>
								</ul>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?endforeach;?>
</div>
<? echo $arResult["NAV_STRING"]; ?>
<?else:?>
<div id="empty">
	<img src="<?=SITE_TEMPLATE_PATH?>/images/emptySearch.png" alt="<?=GetMessage("Q")?>" class="emptyImg">
	<div class="info">
		<h3><?=GetMessage("Q")?></h3>
		<p><?=GetMessage("EMPTY")?></p>
		<a href="<?=SITE_DIR?>" class="back"><?=GetMessage("MAIN")?></a>
	</div>
</div>
<?endif;?>