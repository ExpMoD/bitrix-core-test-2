<?
	$MESS["ART"] = "Арт.:";
	$MESS["PRICE"] = "Цена:";
	$MESS["ADDCART"] = "Добавить в корзину";
	$MESS["AVAILABLE"] = "Товар есть в наличии";
	$MESS["NOAVAILABLE"] = "Доступно под заказ";
	$MESS["ADDCOMPARE"] = "Добавить к сравнению";
	$MESS["RATING"] = "Рейтинг:";
	$MESS["MAIN"] ="Главная страница";
	$MESS["Q"] = 'По вашему запросу "'.htmlspecialcharsbx($_GET["q"]).'" ничего не найдено';
	$MESS["EMPTY"] = 'Попробуйте написать ваш поисковый запрос по-другому или воспользоваться <a href="'.SITE_DIR.'catalog/">каталогом</a> ;)';
	$MESS["RUB"] = "руб."
?>