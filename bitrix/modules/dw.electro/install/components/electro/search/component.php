<?
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
		die();
?>
<?
	$arResult["ITEMS"] = array();
	if(CModule::IncludeModule("iblock") && CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")){
		if(!empty($_GET["q"])){

			//sections
			if(empty($_GET["where"])){
				$arFilter = Array('IBLOCK_TYPE' => $arParams["IBLOCK_TYPE"], 'IBLOCK_ID' => $arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE' => 'Y', '?NAME' => htmlspecialcharsbx($_GET["q"]));
				$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
				while($arRes = $db_list->GetNext()){
					$arResult["SECTIONS"][] = $arRes;
					$sectionsID[] = $arRes["ID"];
				}
			}else{
				$arFilter = Array('IBLOCK_TYPE' => $arParams["IBLOCK_TYPE"], 'IBLOCK_ID' => $arParams["IBLOCK_ID"], 'SECTION_ID' => intval($_GET["where"]), 'GLOBAL_ACTIVE'=>'Y');
				$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
				$sectionsID[] = intval($_GET["where"]);
				while($arRes = $db_list->GetNext()){
					$arResult["SECTIONS"][] = $arRes;
					$sectionsID[] = $arRes["ID"];
				}
			}
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_PAGE_URL", "DETAIL_PICTURE","CATALOG_GROUP_1", "QUANTITY", "PROPERTY_*");
			$arFilter = Array('IBLOCK_TYPE' => $arParams["IBLOCK_TYPE"], 'IBLOCK_ID' => $arParams["IBLOCK_ID"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
			if(empty($_GET["where"])){
				$arFilter[] = array('LOGIC' => 'OR', "SECTION_ID" => $sectionsID, "?NAME" => htmlspecialcharsbx($_GET["q"]));
			}else{
				$arFilter[] = array('LOGIC' => 'AND', "SECTION_ID" => $sectionsID, "?NAME" => htmlspecialcharsbx($_GET["q"]));
			}
			$res = CIBlockElement::GetList(Array(), $arFilter, false, array("nPageSize" => '30'), $arSelect);
			$arResult["NAV_STRING"] = $res->GetPageNavStringEx($navComponentObject, '�������� ������', 'visual', 'Y');
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				$dbPrice = CPrice::GetList(
			        array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"),
			        array("PRODUCT_ID" => $arFields["ID"]),
			        false,
			        false,
			        array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
				);
				while ($arPrice = $dbPrice->Fetch()){
				    $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
			            $arPrice["ID"],
			            $USER->GetUserGroupArray(),
			            "N",
			            SITE_ID
				    );
				    $arFields["PRICE"] = CCatalogProduct::CountPriceWithDiscount(
			            $arPrice["PRICE"],
			            $arPrice["CURRENCY"],
			            $arDiscounts
				    );
				    $arFields["PRICE"] = CurrencyFormat($arFields["PRICE"], $arPrice["CURRENCY"]);
				    $arFields["DISCONT_PRICE"] = $arFields["PRICE"] != $arPrice["PRICE"] ? CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]) : "" ;
				}
				$arResult["ITEMS"][] = $arFields + array("PROPERTIES" => $ob->GetProperties());
			}
		}
	}
	$this->IncludeComponentTemplate();

?>

