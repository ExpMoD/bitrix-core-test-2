<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (CModule::IncludeModule("sale")){
	$arID = array();
	$arBasketItems = array();
	$arBasketOrder = array("NAME" => "ASC", "ID" => "ASC");
	$arBasketUser = array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "LID" => SITE_ID, "ORDER_ID" => "NULL");
	$arBasketSelect = array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS","PRICE","PRODUCT_ID");
	$dbBasketItems = CSaleBasket::GetList($arBasketOrder, $arBasketUser, false, false, $arBasketSelect);

	$arResult["SUM"]          = 0;
	$arResult["WEIGHT"]       = 0;
	$arResult["SUM_DELIVERY"] = 0;

	while ($arItems = $dbBasketItems->Fetch()){

		CSaleBasket::UpdatePrice(
			$arItems["ID"],
			$arItems["CALLBACK_FUNC"],
			$arItems["MODULE"],
			$arItems["PRODUCT_ID"],
			$arItems["QUANTITY"],
			"N",
			$arItems["PRODUCT_PROVIDER_CLASS"]
		);

		array_push($arID, $arItems["ID"]);
	}

	if (!empty($arID)){

		$dbBasketItems = CSaleBasket::GetList(
			$arBasketOrder,
			array(
				"ID" => $arID,
				"ORDER_ID" => "NULL"
			),
			false,
			false,
			$arBasketSelect
		);

		while ($arItems = $dbBasketItems->Fetch()){
		    $arResult["SUM"]    += ($arItems["PRICE"]  * $arItems["QUANTITY"]);
		    $arResult["WEIGHT"] += ($arItems["WEIGHT"] * $arItems["QUANTITY"]);
		    $arResult["ITEMS"][$arItems["PRODUCT_ID"]] = $arItems;
		    $arResult["ID"][] = $arItems["PRODUCT_ID"];
		}
	}

	// add fields
	if(!empty($arResult["ID"])){

		$arSelect = Array(
			"ID",
			"NAME",
			"DETAIL_PAGE_URL",
			"DETAIL_PICTURE",
			"CATALOG_GROUP_1",
			"CATALOG_QUANTITY"
		);

		$res = CIBlockElement::GetList(
			Array(),
			Array(
				"ID" => $arResult["ID"]
			),
			false,
			false,
			$arSelect
		);

		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			$arFields["OLD_PRICE"] = $arFields["CATALOG_PRICE_1"];
			$arFields["PICTURE"] = CFile::ResizeImageGet(
				$arFields["DETAIL_PICTURE"],
				array(
					"width"  => $arParams["BASKET_PICTURE_WIDTH"],
					"height" => $arParams["BASKET_PICTURE_HEIGHT"]
				),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);
			$arResult["ITEMS"][$arFields["ID"]]["INFO"] =  $arFields;
		}

	}

	if(!empty($arResult["ID"])){

		$arResult["ACCESSORIES"] = array();

		$arSelect = Array(
			"ID",
			"PROPERTY_SIMILAR_PRODUCT"
		);

		$arFilter = Array(
			"ID" => $arResult["ID"]
		);

		$res = CIBlockElement::GetList(
			Array(),
			$arFilter,
			false,
			false,
			$arSelect
		);

		while($ob = $res->GetNextElement()){
			$arFields = $ob->GetFields();
			if(!empty($arFields["PROPERTY_SIMILAR_PRODUCT_VALUE"])){
				array_push($arResult["ACCESSORIES"], $arFields["PROPERTY_SIMILAR_PRODUCT_VALUE"]);
			}
		}

		if(!empty($arResult["ACCESSORIES"])){

			shuffle($arResult["ACCESSORIES"]);
			$arSelect = Array(
				"ID",
				"NAME",
				"DETAIL_PAGE_URL",
				"DETAIL_PICTURE",
				"CATALOG_GROUP_1",
				"CATALOG_QUANTITY",
				"PROPERTY_RATING",
				"PROPERTY_MARKER"
			);

			$arFilter = Array(
				"ID" => $arResult["ACCESSORIES"]
			);

			$res = CIBlockElement::GetList(
				Array(),
				$arFilter,
				false,
				false,
				$arSelect
			);

			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();

				$dbPrice = CPrice::GetList(
			        array(
			        	"QUANTITY_FROM" => "ASC",
			        	"QUANTITY_TO"   => "ASC",
			        	"SORT"          => "ASC"
			        ),
			        array(
			        	"PRODUCT_ID" => $arFields["ID"]
			        ),
			        false,
			        false,
			        array(
			        	"ID",
			        	"CATALOG_GROUP_ID",
			        	"PRICE",
			        	"CURRENCY",
			        	"QUANTITY_FROM",
			        	"QUANTITY_TO"
			        )
				);

				while ($arPrice = $dbPrice->Fetch()){

				    $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
			            $arPrice["ID"],
			            $USER->GetUserGroupArray(),
			            "N",
			            SITE_ID
				    );

				    $arFields["PRICE"] = CCatalogProduct::CountPriceWithDiscount(
			            $arPrice["PRICE"],
			            $arPrice["CURRENCY"],
			            $arDiscounts
				    );

				    $arFields["OLD_PRICE"] = ($arPrice["PRICE"] != $arFields["PRICE"] ? CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]) : 0);
				    $arFields["PRICE"] = CurrencyFormat($arFields["PRICE"], $arPrice["CURRENCY"]);

				}

				$arFields["PICTURE"] = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array("width" => 300, "height" => 300), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				$arResult["ACCESSORIES"]["ITEMS"][] = $arFields;
			}
		}

		###
		#############
		############# SALE ORDER #############
								 #############
									       ###


		global $USER;

		$rsUser = CUser::GetByID($USER->GetID());
		$arResult["USER"] = $rsUser->Fetch();



		$dbPerson = CSalePersonType::GetList(
			Array(
				"SORT" => "ASC"
				),
			Array(
				"LID" => SITE_ID
			)
		);

		while ($arPersonType = $dbPerson->Fetch()){

			if(empty($arResult["PERSON_TYPE"])){
				$arPersonType["FIRST"] = "Y";
			}

			$arResult["PERSON_TYPE"][$arPersonType["ID"]] =  $arPersonType;

			$dbGrop = CSaleOrderPropsGroup::GetList(
		        array(
		        	"SORT" => "ASC"
		        ),
		        array(
		        	"PERSON_TYPE_ID" => $arPersonType["ID"]
		        ),
		        false,
		        false,
		        array()
			);

			while ($arGroups = $dbGrop->Fetch()){
				$arResult["PROP_GROUP"][$arPersonType["ID"]][$arGroups["ID"]] = $arGroups;

				$dbProps = CSaleOrderProps::GetList(
			        array(
			        	"SORT" => "ASC"
			        ),
			        array(
			            "PROPS_GROUP_ID" => $arGroups["ID"],
			            "PERSON_TYPE_ID" => $arPersonType["ID"],
			            "ACTIVE" => "Y",
			            "UTIL" => "N",
			            "RELATED" => false
			        ),
			        false,
			        false,
			        array()
			    );

				while ($arProps = $dbProps->Fetch()){
					$arResult["PROPERTIES"][$arGroups["ID"]][] = $arProps;
				}
			}

			$dbPay = CSalePaySystem::GetList(
				$arOrder = Array(
					"SORT"=>"ASC",
					"PSA_NAME"=>"ASC"
				),
				Array(
					"ACTIVE" => "Y",
					"PERSON_TYPE_ID" => $arPersonType["ID"]
				)
			);

			while ($arPay = $dbPay->Fetch()){
				$arResult["PAYSYSTEM"][$arPersonType["ID"]][] = $arPay;
			}

		}

		$dbDelivery = CSaleDelivery::GetList(
		    array(
		            "SORT" => "ASC",
		            "NAME" => "ASC"
		        ),
		    array(
		            "LID" => SITE_ID,
		            "ACTIVE" => "Y"
		        ),
		    false,
		    false,
		    array()
		);

		while($arDelivery = $dbDelivery->Fetch()){

			if(empty($arResult["DELIVERY"])){
				$arResult["SUM_DELIVERY"] = $arDelivery["PRICE"];
			}

			$arResult["DELIVERY"][] = $arDelivery;

				$dbProps = CSaleOrderProps::GetList(
			        array(
			        	"SORT" => "ASC"
			        ),
			        array(
			            "ACTIVE" => "Y",
			            "UTIL" => "N",
			            "RELATED" => array("DELIVERY_ID" =>  $arDelivery["ID"])
			        ),
			        false,
			        false,
			        array()
			    );

				while ($arProps = $dbProps->Fetch()){
					$arProps["DELIVERY_ID"] = $arDelivery["ID"];
					$arResult["DELIVERY_PROPS"][] = $arProps;
				}

		}

	}

	$this->IncludeComponentTemplate();
}
?>
