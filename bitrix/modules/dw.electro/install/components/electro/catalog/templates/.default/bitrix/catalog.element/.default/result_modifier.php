<?
$arElement = CIblockElement::GetById($arResult["ID"])->GetNext();
$arResult['DETAIL_PAGE_URL_TMP'] = $arResult['DETAIL_PAGE_URL'];
$arResult['DETAIL_PAGE_URL'] = $arElement['DETAIL_PAGE_URL'];
$cp = $this->__component;
if (is_object($cp))
    $cp->SetResultCacheKeys(array('DETAIL_PAGE_URL'));

$db_old_groups = CIBlockElement::GetElementGroups($arResult["ID"], true);
while($ar_group = $db_old_groups->Fetch()){
	$arSection[$ar_group["DEPTH_LEVEL"]] = $ar_group["ID"];
}
krsort($arSection);

$arResult["LAST_SECTION"] = array_slice($arSection, 0, 1);
$res = CIBlockSection::GetByID($arResult["LAST_SECTION"][0]);
if($arSec = $res->GetNext()){
	$arResult["LAST_SECTION"] = $arSec;
}


$arParams["SHOW_REVIEW_FORM"] = true;
if(CModule::IncludeModule("iblock")){
	if(!empty($arResult["PROPERTIES"]["ATT_BRAND"])){
		$res = CIBlockElement::GetByID($arResult["PROPERTIES"]["ATT_BRAND"]["VALUE"]);
		if($arRes = $res->GetNext()){
			$arResult["PROPERTIES"]["ATT_BRAND"] = array(
				"IMAGE" =>CFile::GetPath($arRes["DETAIL_PICTURE"]),
				"LINK" => $arRes["DETAIL_PAGE_URL"],
				"NAME" => $arRes["NAME"]
			);
		}
	}
	if(!empty($arResult["PROPERTIES"]["MARKER_PHOTO"]["VALUE"])){
		foreach ($arResult["PROPERTIES"]["MARKER_PHOTO"]["VALUE"] as $key => $elementID) {
			$res = CIBlockElement::GetByID($elementID);
			if($arRes = $res->GetNext()){
				$arResult["PROPERTIES"]["MARKER_PHOTO"]["VALUE"][$key] = array(
					"IMAGE" =>CFile::GetPath($arRes["DETAIL_PICTURE"])
				);
			}
		}
	}
}
foreach ($arResult["PROPERTIES"] as $index => $arProp) {
	if($arProp["CODE"] == "MORE_PROPERTIES"){
		if(!empty($arProp["VALUE"])){
			foreach ($arProp["VALUE"] as $i => $arValue) {
				$arResult["PROPERTIES"][] = array(
					"CODE" => $arProp["PROPERTY_VALUE_ID"][$i],
					"SORT" => 500,
					"VALUE" => $arProp["DESCRIPTION"][$i],
					"NAME" => $arValue
				);
			}
		}
		unset($arResult["PROPERTIES"][$index]);
	}
}

$i = 0;
$index = 0;
foreach($arResult["PROPERTIES"] as $arProp){
	if(!empty($arProp["VALUE"]) && $arProp["SORT"] <= 500){
		if($i == 5){ $index++; $i = 0; }
		$arResult["TOP_PROPERTIES"][$index][] = $arProp;
		$i++;
	}
}
//REVIEW
$arSelect = Array("ID", "DATE_CREATE", "DETAIL_TEXT", "PROPERTY_DIGNITY", "PROPERTY_SHORTCOMINGS", "PROPERTY_EXPERIENCE", "PROPERTY_GOOD_REVIEW", "PROPERTY_BAD_REVIEW", "PROPERTY_NAME", "PROPERTY_RATING");
$arFilter = Array("IBLOCK_ID" => $arParams["REVIEW_IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "CODE" => $arResult["ID"]);
$res = CIBlockElement::GetList(Array("SORT" => "ASC", "CREATED_DATE"), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()){
	$arResult["REVIEWS"][] = $ob->GetFields();
}

//RATING
$expEnums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID" => $arParams["REVIEW_IBLOCK_ID"], "CODE" => "EXPERIENCE"));
while($enumValues = $expEnums->GetNext()){
	$arResult["NEW_REVIEW"]["EXPERIENCE"][] = array(
		"ID" => $enumValues["ID"],
		"VALUE" => $enumValues["VALUE"]
	);
}

$USER_ID = $USER->GetID();
$res = CIBlockElement::GetList(
	Array(), 
	Array(
		"ID" => intval($arResult["ID"]),
		"ACTIVE_DATE" => "Y",
		"ACTIVE" => "Y"
	), 
	false, 
	false, 
	Array(
		"ID", 
		"IBLOCK_ID", 
		"PROPERTY_USER_ID", 
	)
);
while($ob = $res->GetNextElement()){ 
	$arFields = $ob->GetFields();
	if($USER_ID == $arFields["PROPERTY_USER_ID_VALUE"]){
		$arParams["SHOW_REVIEW_FORM"] = false;
		break;
	}
}  

//RELATED_PRODUCT
if(!empty($arResult["IBLOCK_SECTION_ID"]) || !empty($arResult["PROPERTIES"]["RELATED_PRODUCT"]["VALUE"])){
	$arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "CATALOG_GROUP_1", "DETAIL_PAGE_URL", "PROPERTY_MARKER", "PROPERTY_RATING");
	if(empty($arResult["PROPERTIES"]["RELATED_PRODUCT"]["VALUE"])){
		$db_old_groups = CIBlockElement::GetElementGroups($arResult["ID"], true);
		while($ar_group = $db_old_groups->Fetch()){
			$arSection[$ar_group["DEPTH_LEVEL"]] = $ar_group["ID"];
		}
		krsort($arSection);
		$arFilter = Array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID" => array_slice($arSection, 0, 1), "!ID" => $arResult["ID"]);
		$res = CIBlockElement::GetList(array("RAND" => "ASC"), $arFilter, false, array("nTopCount" => 10), $arSelect);
	}else{
		$arFilter = Array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" => $arResult["PROPERTIES"]["RELATED_PRODUCT"]["VALUE"]);
		$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
	}
	while($ob = $res->GetNextElement()){
		$arValue = $ob->GetFields();
		$dbPrice = CPrice::GetList(
	        array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"),
	        array("PRODUCT_ID" => $arValue["ID"]),
	        false,
	        false,
	        array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
		);
		while ($arPrice = $dbPrice->Fetch()){
		    $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
	            $arPrice["ID"],
	            $USER->GetUserGroupArray(),
	            "N",
	            SITE_ID
		    );
		    $arValue["PRICE"] = CCatalogProduct::CountPriceWithDiscount(
	            $arPrice["PRICE"],
	            $arPrice["CURRENCY"],
	            $arDiscounts
		    );
		    $arValue["OLD_PRICE"] = ($arPrice["PRICE"] != $arValue["PRICE"] ? CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]) : 0);
		    $arValue["PRICE"] = CurrencyFormat($arValue["PRICE"], $arPrice["CURRENCY"]);
		}
		$arValue["PICTURE"] = !empty($arValue["DETAIL_PICTURE"]) ? CFile::ResizeImageGet(CFile::GetFileArray($arValue["DETAIL_PICTURE"]), array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array("src" => SITE_TEMPLATE_PATH."/images/empty.png");
		$arResult["RELATED"][] = $arValue;
	}
}


//SIMILAR_PRODUCT
if(!empty($arResult["PROPERTIES"]["SIMILAR_PRODUCT"]["VALUE"])){
	$arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_MARKER", "PROPERTY_RATING");
	$arFilter = Array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" => $arResult["PROPERTIES"]["SIMILAR_PRODUCT"]["VALUE"]);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement()){
		$arValue = $ob->GetFields();
		$dbPrice = CPrice::GetList(
	        array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"),
	        array("PRODUCT_ID" => $arValue["ID"]),
	        false,
	        false,
	        array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
		);
		while ($arPrice = $dbPrice->Fetch()){
		    $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
	            $arPrice["ID"],
	            $USER->GetUserGroupArray(),
	            "N",
	            SITE_ID
		    );
		    $arValue["PRICE"] = CCatalogProduct::CountPriceWithDiscount(
	            $arPrice["PRICE"],
	            $arPrice["CURRENCY"],
	            $arDiscounts
		    );
		    $arValue["OLD_PRICE"] = ($arPrce["PRICE"] != $arValue["PRICE"] ? CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]) : 0);
		    $arValue["PRICE"] = CurrencyFormat($arValue["PRICE"], $arPrice["CURRENCY"]);
		}
		$arValue["PICTURE"] = !empty($arValue["DETAIL_PICTURE"]) ? CFile::ResizeImageGet(CFile::GetFileArray($arValue["DETAIL_PICTURE"]), array('width'=>200, 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array("src" => SITE_TEMPLATE_PATH."/images/empty.png");
		$arResult["SIMILAR"][] = $arValue;
	}
}

  $arResult["PICTURES"] = array(
  	"PREVIEW" => !empty($arResult['DETAIL_PICTURE']) ? CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array("src" => SITE_TEMPLATE_PATH."/images/empty.png"),
  	"PREVIEW_BIG" => !empty($arResult['DETAIL_PICTURE']) ? CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width'=>1500, 'height'=>1500), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array("src" => SITE_TEMPLATE_PATH."/images/empty.png"),
  	"PREVIEW_SMALL" => !empty($arResult['DETAIL_PICTURE']) ? CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array("src" => SITE_TEMPLATE_PATH."/images/empty.png"),
  	"DETAIL" =>  !empty($arResult['DETAIL_PICTURE']) ? CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width'=>40, 'height'=>40), BX_RESIZE_IMAGE_PROPORTIONAL, true) : array("src" => SITE_TEMPLATE_PATH."/images/empty.png")
  );
  $arResult["OLD_CAP"] = "";
?>