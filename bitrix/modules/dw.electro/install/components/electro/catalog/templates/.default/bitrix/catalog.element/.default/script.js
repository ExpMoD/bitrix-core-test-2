	function cartSlider() {
		$(window).bind("ready", function() {
			var link = $("#elementTableImg")
			var itworkSlide = $("#itworkSlide");
			var mover = $("#itworkSlide");
			var elements = mover.find("li");
			var elementsMore = $("#elementMorePhoto li");
			var elementsSelected = $("#elementMorePhoto span.selected");
			var moreHeight = $(window).width() > 1000 ? 120 - (120 * (100 - 100 * $(window).width() / 1920) / 100) : 120 - (120 * (100 - 100 * 1000 / 1920) / 100) + 5;

			mover.width(100 * elements.length + "%");
			elements.width(100 / elements.length + "%");
			var moverHeight = $(window).width() > 1200 ? $("#elementTableInfo td").outerHeight() - $("#elementMorePhoto").outerHeight() - 48 : 200;
			//elements.css({"height": moverHeight + "px", "line-height": moverHeight + "px"});
			elementsMore.find("a").css({
				"height": moreHeight + "px",
				"line-height": moreHeight + "px"
			})
			elementsSelected.css({
				"height": moreHeight + "px",
				"line-height": moreHeight + "px"
			});

			var leftBtn = $("#elementTableImg a.prev");
			var rightBtn = $("#elementTableImg a.next");
			var borderBlock = $("#elementMorePhoto span.selected");
			var moveValue = 0;
			var curPos = 0;
			var topMove = 0;
			var boxPos = 0;
			var cursorPos = 0;
			var touchMove = 0;
			var touchVal = 0;
			var slidePos = 0;
			var mouseDown = false;
			var returnSlide = false;
			var curLine = elementsMore.eq(0).offset().top;

			//slide
			$(window).resize(function() {
				moreHeight = $(window).width() > 1000 ? 120 - (120 * (100 - 100 * $(window).width() / 1920) / 100) : 120 - (120 * (100 - 100 * 1000 / 1920) / 100) + 5;
				moverHeight = $(window).width() > 1300 ? 350 : 200;
				//elements.css({"height": moverHeight + "px", "line-height": moverHeight + "px"});
				elementsMore.find("a").css({
					"height": moreHeight + "px",
					"line-height": moreHeight + "px"
				})
				elementsSelected.css({
					"height": moreHeight + "px",
					"line-height": moreHeight + "px"
				});
				borderBlock.css({
					left: elementsMore.eq(curPos).offset().left - elementsMore.eq(0).offset().left + "px",
					top: elementsMore.eq(curPos).offset().top - elementsMore.eq(0).offset().top + 12 + "px"
				});
			});

			var boxMove = function(left) {
				moveBoxValue = elementsMore.eq(curPos).offset().left - elementsMore.eq(0).offset().left;
				boxPos = elementsMore.eq(curPos).offset().top;
				if (curLine != boxPos) {
					topMove = boxPos - elementsMore.eq(0).offset().top + 12;
					if (left === false) {
						borderBlock.finish().animate({
							left: moveBoxValue
						}, 150, function() {
							borderBlock.finish().animate({
								top: topMove
							}, 150);
						});
					} else {
						borderBlock.finish().animate({
							top: topMove
						}, 150, function() {
							borderBlock.finish().animate({
								left: moveBoxValue
							}, 150);
						});
					}
					curLine = elementsMore.eq(curPos).offset().top;

				} else {
					borderBlock.animate({
						left: moveBoxValue
					}, 150);
				}
				return 0;
			}
			if (elements.length > 1) {
				leftBtn.on("click", function(e) {
					if (curPos == 0) {
						curPos = elements.length;
					}
					moveValue = "-" + --curPos * 100 + "%";
					boxMove(true);
					mover.data("number", curPos).finish().animate({
						left: moveValue
					}, 150);
					e.preventDefault();
				});

				rightBtn.on("click", function(e) {
					if (curPos == elements.length - 1) {
						moveValue = curPos = 0;
					} else {
						moveValue = "-" + ++curPos * 100 + "%";
					}

					boxMove(false);
					mover.data("number", curPos).finish().animate({
						left: moveValue
					}, 150);
					e.preventDefault();
				});
			} else {
				rightBtn.remove();
				leftBtn.remove();
			}
			elementsMore.on("click", function(e) {
				curPos = parseInt($(this).index());
				moveValue = "-" + curPos * 100 + "%";
				boxMove(true);
				mover.data("number", curPos).finish().animate({
					left: moveValue
				}, 150);
				e.preventDefault();
			});
			elementsSelected.on("click", function(e) {
				$("#itworkSlide a").trigger("click");
			});
		}($));
	}

	var lastMove = 0;

	$(window).bind("ready", function() {
		//images carousel

		cartSlider();

		//morePhoto tools

		$("#qtyLink").on("click", function(e) {
			$("#quantityModal").toggle();
			e.preventDefault();
		});

		//propSlider
		(function($) {

			var propSlider = $("#propSlider");
			var sliderElements = propSlider.find("li");
			var controls = {
				left: $(".prop .controls .prev"),
				right: $(".prop .controls .next")
			}
			var settings = {
				pos: 0,
				current: 0,
				animateValue: 0
			}
			if (sliderElements.length >= 2) {
				$(".prop .controls a").show();
			}
			propSlider.width(sliderElements.length * 100 + "%");
			sliderElements.width(100 / sliderElements.length + "%");

			controls.left.click(function(e) {
				settings.animateValue = !settings.current ? "-" + (100 * (sliderElements.length - 1)) + "%" : "-" + --settings.current * 100 + "%";
				propSlider.finish().animate({
					"left": settings.animateValue
				}, 350, function() {
					settings.current = Math.abs(parseInt(settings.animateValue) / 100);
				});

				e.preventDefault();
			});
			controls.right.click(function(e) {
				settings.animateValue = settings.current == (sliderElements.length - 1) ? 0 : "-" + ++settings.current * 100 + "%";
				propSlider.finish().animate({
					"left": settings.animateValue
				}, 350, function() {
					settings.current = Math.abs(parseInt(settings.animateValue) / 100);
				});
				e.preventDefault();
			});

		}($));

		//reviewSlider
		(function($) {

			var reviewSlider = $("#reviewSlider");
			var sliderElements = reviewSlider.find("li");
			var controls = {
				left: $(".review .controls .prev"),
				right: $(".review .controls .next")
			}
			var settings = {
				pos: 0,
				current: 0,
				animateValue: 0
			}
			if (sliderElements.length >= 2) {
				$(".review .controls a").show();
			}
			reviewSlider.width(sliderElements.length * 100 + "%");
			sliderElements.width(100 / sliderElements.length + "%");
			reviewSlider.height(sliderElements.eq(0).height());

			controls.left.click(function(e) {
				settings.animateValue = !settings.current ? "-" + (100 * (sliderElements.length - 1)) + "%" : "-" + --settings.current * 100 + "%";
				settings.height = sliderElements.eq(Math.abs(parseInt(settings.animateValue) / 100)).height();
				reviewSlider.finish().animate({
					"left": settings.animateValue,
					"height": settings.height
				}, 350, function() {
					settings.current = Math.abs(parseInt(settings.animateValue) / 100);
				});

				e.preventDefault();
			});
			controls.right.click(function(e) {
				settings.animateValue = settings.current == (sliderElements.length - 1) ? 0 : "-" + ++settings.current * 100 + "%";
				settings.height = sliderElements.eq(Math.abs(parseInt(settings.animateValue) / 100)).height();
				reviewSlider.finish().animate({
					"left": settings.animateValue,
					"height": settings.height
				}, 350, function() {
					settings.current = Math.abs(parseInt(settings.animateValue) / 100);
				});
				e.preventDefault();
			});

		}($));

		//morePhoto slider

		(function($) {
			var link = $("#zoomPhotoLeft ul");
			var linkElements = link.find("li");
			var linkMoreElements = $("#zoomMorePhoto li");
			var leftButton = $("#zoomPrev, #zoomMorePhoto a.prev");
			var rightButton = $("#zoomNext, #zoomMorePhoto a.next");
			var activeMoreBox = 0;
			var currentPosition = $("#itworkSlide").data("number") != undefined ? ($("#itworkSlide").data("number") + 1) * 100 : 0;
			var moveBoxVal = 0;

			//touch
			var clickControl = false;
			var slidePosition = 0;
			var slidePositionStart = 0;
			var clickDirection = 0;
			var clickPosition = 0;
			var slideMove = 0;

			link.width(linkElements.length * 100 + "%");
			linkElements.width(100 / linkElements.length + "%");

			var leftMove = function() {
				currentPosition = $("#zoomMorePhoto .active").index() * 100;
				if (currentPosition == 0) {
					moveBoxVal = (linkElements.length - 1) * 100;
					currentPosition = (linkElements.length - 1) * 100;
				} else {
					moveBoxVal = currentPosition - 100;
					currentPosition = currentPosition - 100;
				}

				link.finish().animate({
					"left": "-" + moveBoxVal + "%"
				});
				linkMoreElements.removeClass("active").eq(currentPosition / 100).addClass("active");
				$("#mover").find("li").eq(currentPosition / 100).trigger("click");
			}
			var rightMove = function() {
				currentPosition = $("#zoomMorePhoto .active").index() * 100;
				moveBoxVal = currentPosition + 100;
				if (moveBoxVal == linkElements.length * 100) {
					moveBoxVal = currentPosition = 0;
				} else {
					currentPosition = currentPosition + 100;
				}
				link.finish().animate({
					"left": "-" + moveBoxVal + "%"
				});
				linkMoreElements.removeClass("active").eq(currentPosition / 100).addClass("active");
				$("#mover").find("li").eq(currentPosition / 100).trigger("click");
			}

			leftButton.on("click", function(e) {
				leftMove();
				e.preventDefault();
			});

			rightButton.on("click", function(e) {
				rightMove();
				e.preventDefault();
			});

			$(document).keydown(function(e) {
				if ($("#zoomPhoto").is(":visible")) {
					if (e.which == 37 || e.which == 40 && $('#zoomPhoto').is(':visible')) {
						leftMove();
						e.preventDefault();
					} else if (e.which == 39 || e.which == 38 && $('#zoomPhoto').is(':visible')) {
						rightMove();
						e.preventDefault();
					} else if (e.which == 27 && $('#zoomPhoto').is(':visible')) {
						$("#zoomPhoto, #blackout, #quantityModal").hide();
					}
				}
			});

			linkMoreElements.on("click", function(e) {
				activeMoreBox = $(e.target).parent().parent().index();
				$("#mover").find("li").eq(activeMoreBox).trigger("click");
				linkMoreElements.removeClass("active").eq(activeMoreBox).addClass("active");
				link.finish().animate({
					"left": "-" + activeMoreBox * 100 + "%"
				});
				currentPosition = activeMoreBox * 100;
				e.preventDefault();
			});

			link.on("mousedown touchstart", function(e) {
				clickControl = true;
				clickPosition = e.type == "touchstart" ? e.originalEvent.touches[0].pageX : e.pageX;
				slidePositionStart = slidePosition = link.css("left").replace("px", "");
				e.preventDefault();
			});

			$(document).on("mouseup touchend", function(e) {
				if (clickControl == true) {

					clickControl = false;
					slidePosition = link.css("left").replace("px", "");
					if (Math.abs(Math.abs(slidePosition) - Math.abs(slidePositionStart)) > 20) {
						if (Math.abs(slidePositionStart) < Math.abs(slidePosition) && slidePosition < 0 && (Math.abs(slidePosition) < (linkElements.length - 1) * parseInt(linkElements.width()))) {

							link.finish().animate({
								left: "-" + (Math.floor(Math.abs(slidePosition) / parseInt(linkElements.width())) + 1) * 100 + "%"
							}, 200);

							linkMoreElements.removeClass("active").eq((Math.ceil(Math.abs(slidePosition) / parseInt(linkElements.width())))).addClass("active");
							$("#mover").find("li").eq((Math.ceil(Math.abs(slidePosition) / parseInt(linkElements.width())))).trigger("click");
						} else {

							if (slidePosition <= 0) {
								if (Math.abs(slidePosition) > linkElements.width() * (linkElements.length - 1)) {
									link.finish().animate({
										left: "-" + (linkElements.length - 1) * 100 + "%"
									}, 200);
								} else {
									link.finish().animate({
										left: "-" + (Math.ceil(Math.abs(slidePosition) / parseInt(linkElements.width())) - 1) * 100 + "%"
									}, 200);
									linkMoreElements.removeClass("active").eq((Math.ceil(Math.abs(slidePosition) / parseInt(linkElements.width())) - 1)).addClass("active");
									$("#mover").find("li").eq((Math.ceil(Math.abs(slidePosition) / parseInt(linkElements.width())) - 1)).trigger("click");
								}
							}

						}

						if (slidePosition > 0) {
							link.finish().animate({
								left: "0px"
							}, 200);

						}

					} else {
						link.finish().animate({
							left: "-" + Math.round(Math.abs(slidePosition) / parseInt(linkElements.width())) * 100 + "%"
						}, 200);
					}

					e.preventDefault();
				}

			});

			$(document).on("mousemove touchmove", function(e) {

				e.pageX = e.type == "touchmove" ? e.originalEvent.touches[0].pageX : e.pageX;
				if (clickControl == true) {
					if (slidePosition - (clickPosition - e.pageX) < 0) {
						slideMove = slidePosition - (clickPosition - e.pageX);
					} else {
						slideMove = slidePosition - (clickPosition - e.pageX);
					}
					link.css({
						"left": slideMove + "px"
					});
				}
			});



			//after load page
			var reParams = function(flag) {

				$("#zoomPhoto").css({
					"top": $(window).scrollTop() + $(window).height() / 2 + "px",
					"width": +parseInt($(document).width() - $(document).width() * 24 / 100) + 1 + "px",
					"height": +($(document).width() - $(document).width() * 24 / 100) * 60 / 100 + "px",
					"margin-left": "-" + ($(document).width() - $(document).width() * 24 / 100) / 2 + "px",
					"margin-top": "-" + ($(document).width() - $(document).width() * 24 / 100) * 60 / 100 / 2 + "px"

				});

				$("#zoomMorePhoto").css({
					"width": $("#zoomPhoto").width() - 240 + "px"
				});

				$("#zoomPhotoLeft").css({
					"width": $("#zoomPhoto").width() - 250 + "px",
					"height": $("#zoomPhoto").height() - 35 + "px",
				});

				$("#zoomPhotoRight").css({
					"height": $("#zoomPhoto").height() + "px"
				});

				$("#zoomPhotoLeft li").css({
					"height": $("#zoomPhoto").height() - 35 + "px",
					"line-height": $("#zoomPhoto").height() - 35 + "px"
				});

				if (flag == true) {
					var eq = $("#itworkSlide").data("number") != undefined ? $("#itworkSlide").data("number") : 0;
					$("#zoomMorePhoto li").removeClass("active").eq(eq).addClass("active");
					$("#zoomPhotoLeft ul").css({
						"left": "-" + eq * 100 + "%"
					});
					$("#zoomPhoto, #blackout").show();
				}
				$("#zoomMorePhoto div").width((48 * linkElements.length) - 8 + "px");
			}

			$("#itworkSlide a, .zoom").on("click", function(e) {
				$("#blackout").css({
					"height": $(document).height() + "px",
					"width": $("html").width() + "px",
					"min-width": "1000px"
				});
				reParams(true);

				e.preventDefault();

			});
			$("#zoomPhotoExit, #blackout").on("click", function(event) {
				$("#zoomPhoto, #blackout, #quantityModal").hide();
				event.preventDefault();
			});

			$(document).on("click", function(e) {
				if (e.target.id != "qtyLink" && e.target.id != "quantityModal") {
					$("#quantityModal").hide();
				}
			});

			$(window).on("resize orientationchange", function() {
				$("#blackout").css({
					"height": $(document).height() + "px",
					"width": $("html").width() + "px"
				});

				reParams(false);

			});

		}($));

	});

	(function($) {
		$(document).on("click", ".question", function(e) {
			e.preventDefault();
			$("#hint").remove();
			$("#catalogElement").append(
				$('<div id="hint">').html("<span>" + $(this).siblings().text() + "</span><ins></ins><p>" + $(this).data("description") + "</p>").css({
					"top": ($(this).offset().top - 20) + "px",
					"left": ($(this).offset().left + 40) + "px"
				})
			);
		});
		$(document).on("click", "#hint ins", function(e) {
			$("#hint").remove();
		});
	}($));

	//tabs
	(function($) {
		$(document).on("click", ".allProp, #propTAB", function(e) {
			scrollElement($(".stats").offset().top - 38);
			e.preventDefault();
		});
		$(document).on("click", ".allReview, #reviewsTAB", function(e) {
			scrollElement($("#reviews").offset().top - 38);
			e.preventDefault();
		});
		$(document).on("click", "#descTAB", function(e) {
			scrollElement($(".description").offset().top - 48);
			e.preventDefault();
		});
		$(document).on("click", "#relatedTAB", function(e) {
			scrollElement($("#relatedCarousel").offset().top - 38);
			e.preventDefault();
		});
		$(document).on("click", "#similarTAB", function(e) {
			scrollElement($("#similarCarousel").offset().top - 38);
			e.preventDefault();
		});
		$(document).on("click", "#browseTAB", function(e) {
			scrollElement($("body").offset().top - 38);
			e.preventDefault();
		});
	})($);

	var sendRating = function(event) {
		var $this = $(this);
		var $win = $("#elementError");
		var trig = event.data.dest == "good" ? true : false;

		$.getJSON(ajaxPath + "?act=rating&id=" + $this.data("id") + "&trig=" + trig, function(data) {
			if (data["result"]) {
				$this.find("span").html(
					parseInt($this.find("span").html()) + 1
				);
			} else {
				$win.show().find("p").text(data["error"]).parent().find(".heading").text(data["heading"]);
			}
		});
		event.preventDefault();
	};

	var scrollElement = function(dest) {
		$("html, body").animate({
			scrollTop: dest
		}, 500);
	};

	var calcRating = function(event) {
		var $this = $(this);
		var $mover = $this.find(".m");
		var $ratingInput = $("#ratingInput");
		var position = $this.offset().left;
		var curWidth = $this.width() / 5;
		var value = Math.ceil((event.pageX - position) / curWidth);

		$mover.stop().css({
			"width": (value * 20) + "%"
		});

		if (event.data.action) {
			$ratingInput.val(value);
		};

	};

	var callRating = function(event) {
		var $this = $(this);
		var $ratingInput = $("#ratingInput");
		var value = $ratingInput.val() != "" ? parseInt($ratingInput.val()) : 0;

		clearTimeout(flushTimeout);
		flushTimeout = setTimeout(function() {
			$this.find(".m").css({
				"width": (value * 20) + "%"
			})
		}, 500);
	};

	var usedSelect = function(event) {
		var $this = $(this);
		var $ul = $(".usedSelect");
		var usedInput = $("#usedInput");

		$ul.find("a").removeClass("selected");
		$this.addClass("selected");
		$("#usedInput").val($this.data("id"));

		event.preventDefault();
	};

	var reviewSubmit = function(event) {
		var $this = $(this);
		var $form = $(this).parents("form");
		var formData = $form.serialize();
		var $win = $("#elementError");

		$.getJSON(ajaxPath + "?act=newReview&" + formData + "&iblock_id=" + $this.data("id"), function(data) {
			$win.show().find("p").text(data["message"]).parent().find(".heading").text(data["heading"]);
			data["reload"] ? $win.data("reload", 1) : void 0;
		});

		event.preventDefault();
	};

	var windowClose = function(event) {
		var $win = $("#elementError");
		$win.data("reload") ? document.location.reload() : $("#elementError").hide();
		event.preventDefault();
	};

	var showReview = function(event) {
		var $this = $(this);
		var $reviews = $("#reviews");
		if ($this.data("open") == "N") {
			$reviews.children("li").removeClass("hide");
			$this.data("open", "Y").html("������ ������");
		} else {
			$reviews.children("li").slice(3).addClass("hide")
			$this.data("open", "N").html("�������� ��� ������");
		}
		event.preventDefault();
	};

	$(document).on("click", ".showReviewDetail", function(event) {
		var $this = $(this);
		var $reviewContainer = $("#reviews");

		scrollElement(
			$reviewContainer.children("li").eq(
				$this.data("cnt")
			).offset().top
		);
		event.preventDefault();
	});

	$(document).on("click", ".good", {
		dest: "good"
	}, sendRating);
	$(document).on("click", ".bad", {
		dest: "bad"
	}, sendRating);

	//rating review
	$(document).on("mousemove", "#newRating .rating", {action: false}, calcRating);
	$(document).on("mouseleave", "#newRating .rating", callRating)
	$(document).on("click", "#newRating .rating", {action: true}, calcRating);
	$(document).on("click", ".usedSelect a", usedSelect);
	$(document).on("click", "#showallReviews", showReview);
	$(document).on("click", "#newReview .submit", reviewSubmit);
	$(document).on("click", "#elementErrorClose, #elementError .close", windowClose);