<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?	$this->SetViewTarget("hide");?> 
	allowHide
	<?$this->EndViewTarget();
	
	$this->SetViewTarget("filter");

if (CModule::IncludeModule("iblock"))
{
   $arFilter = array(
      "ACTIVE" => "Y",
      "GLOBAL_ACTIVE" => "Y",
      "IBLOCK_ID" => $arParams["IBLOCK_ID"],
   );
   if(strlen($arResult["VARIABLES"]["SECTION_CODE"])>0)
   {
      $arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
   }
   elseif($arResult["VARIABLES"]["SECTION_ID"]>0)
   {
      $arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
   }
      
   $obCache = new CPHPCache;
   if($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
   {
      $arCurSection = $obCache->GetVars();
   }
   else
   {
      $arCurSection = array();
      $dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));
      $dbRes = new CIBlockResult($dbRes);

      if(defined("BX_COMP_MANAGED_CACHE"))
      {
         global $CACHE_MANAGER;
         $CACHE_MANAGER->StartTagCache("/iblock/catalog");

         if ($arCurSection = $dbRes->GetNext())
         {
            $CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
         }
         $CACHE_MANAGER->EndTagCache();
      }
      else
      {
         if(!$arCurSection = $dbRes->GetNext())
            $arCurSection = array();
      }

      $obCache->EndDataCache($arCurSection);
   }
   ?>
	   <?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"level2",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
			"TOP_DEPTH" => 1,
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
			"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
			"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
		),
		$component
	);?>
	   <?$APPLICATION->IncludeComponent(
	"electro:catalog.smart.filter", 
	"", 
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"SECTION_ID" => $arCurSection["ID"],
		"FILTER_NAME" => "arrFilter",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SAVE_IN_SESSION" => "N",
		"INSTANT_RELOAD" => "Y",
		"PRICE_CODE" => array($arParams["PRICE_CODE"][0]),
		"HIDE_NOT_AVAILABLE" => "N",
		"XML_EXPORT" => "N",
		"SECTION_TITLE" => "-",
		"SECTION_DESCRIPTION" => "-",
		"TEMPLATE_THEME" => "blue"
	),
	false
);?>
<?
}


	$this->EndViewTarget();


?><?global $APPLICATION;$view = $APPLICATION->get_cookie("VIEW") == "" && !isset($_GET["view"]) ? "line" : $APPLICATION->get_cookie("VIEW")?>
<h1><?$APPLICATION->ShowTitle()?></h1>
<div id="catalog">
	<div id="catalogLine">
		<div class="left">
			<span><?=GetMessage("SORT");?></span> 
			<a href="<?=$APPLICATION->GetCurPageParam("order=shows", array("order"))?>" <?=isset($_GET["order"]) && $_GET["order"] =="shows" || !isset($_GET["order"]) ? 'class="active"' : '' ?>><?=GetMessage("SORT_POPULAR");?></a> 
			<a href="<?=$APPLICATION->GetCurPageParam("order=price", array("order"))?>"<?=isset($_GET["order"]) && $_GET["order"] =="price" ? 'class="active"' : '' ?>><?=GetMessage("SORT_PRICE");?></a> 
			<a href="<?=$APPLICATION->GetCurPageParam("order=rating", array("order"))?>"<?=isset($_GET["order"]) && $_GET["order"] =="rating" ? 'class="active"' : '' ?>><?=GetMessage("SORT_RATING");?></a>
			<div id="on">
				<a href="<?=$APPLICATION->GetCurPageParam("on=asc", array("on"))?>" class="asc<?=isset($_GET["on"]) && $_GET["on"] =="asc" ? " active" : "" ?>"></a>
				<a href="<?=$APPLICATION->GetCurPageParam("on=desc", array("on"))?>" class="desc<?=isset($_GET["on"]) && $_GET["on"] =="desc" || !isset($_GET["on"]) ? " active" : "" ?>"></a>
			</div>
			<span class="count"><?=GetMessage("SORT_TO");?></span> 
			<a href="<?=$APPLICATION->GetCurPageParam("show=30", array("show"))?>" <?=isset($_GET["show"]) && $_GET["show"] =="30" || !isset($_GET["show"]) ? 'class="active"' : '' ?>><?=GetMessage("SORT_PAGE_FIRST");?></a> 
			<a href="<?=$APPLICATION->GetCurPageParam("show=60", array("show"))?>" <?=isset($_GET["show"]) && $_GET["show"] =="60" ? 'class="active"' : '' ?>><?=GetMessage("SORT_PAGE_SECOND");?></a> 
			<a href="<?=$APPLICATION->GetCurPageParam("show=90", array("show"))?>" <?=isset($_GET["show"]) && $_GET["show"] =="90" ? 'class="active"' : '' ?>><?=GetMessage("SORT_PAGE_THIRD");?></a>
		</div>
		<div class="right">
			<a href="<?=$APPLICATION->GetCurPageParam("view=line", array("view"))?>" class="type1<?=isset($_GET["view"]) && $_GET["view"] =="line" || ($view == "line" && !isset($_GET["view"])) ? ' active' : '' ?>"></a>
			<a href="<?=$APPLICATION->GetCurPageParam("view=squares", array("view"))?>" class="type2<?=isset($_GET["view"]) && $_GET["view"] =="squares" || $view  == "squares" && empty($_GET["view"]) ? ' active' : '' ?>"></a>
			<a href="<?=$APPLICATION->GetCurPageParam("view=table", array("view"))?>" class="type3<?=isset($_GET["view"]) && $_GET["view"] =="table" || $view  == "table" && empty($_GET["view"]) ? ' active' : '' ?>"></a>
		</div>
	</div>
<?$intSectionID = 0;
if(isset($_GET["order"])){
	if($_GET["order"] == "price"){
		$arParams["ELEMENT_SORT_FIELD"] = "catalog_PRICE_1";
	}else if($_GET["order"] == "shows"){
		$arParams["ELEMENT_SORT_FIELD"] = "shows";
	}else if($_GET["order"] == "rating"){
		$arParams["ELEMENT_SORT_FIELD"] = "PROPERTY_RATING";
	}
}
if(isset($_GET["on"]) && $_GET["on"] == "asc"){
	$arParams["ELEMENT_SORT_ORDER"] = "asc";
}elseif(isset($_GET["on"]) && $_GET["on"] == "desc"){
	$arParams["ELEMENT_SORT_ORDER"] = "desc";
}else{
	$arParams["ELEMENT_SORT_ORDER"] = "desc";
}
if(isset($_GET["show"])){
	$arParams["PAGE_ELEMENT_COUNT"] = intval($_GET["show"]);
}
if(isset($_GET["view"]) && $_GET["view"] == "squares"){
	$APPLICATION->set_cookie("VIEW", "squares", time()+69992);
	$tName = "squares";
}else if(isset($_GET["view"]) && $_GET["view"] == "line"){
	$APPLICATION->set_cookie("VIEW", "line", time()+69992);
	$tName = "line";
}else if(isset($_GET["view"]) && $_GET["view"] == "table"){
	$APPLICATION->set_cookie("VIEW", "table", time()+69992);
	$tName = "table";
}
$templateName = $APPLICATION->get_cookie("VIEW") ? $APPLICATION->get_cookie("VIEW") : "line";?>
<?$intSectionID = $APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	 !empty($tName) ? $tName : $templateName,
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"] ,
		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
		"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
		"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

		'LABEL_PROP' => $arParams['LABEL_PROP'],
		'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
		'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

		'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
		'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
		'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
		'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
		'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
		'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
		'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
		'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
		'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
		'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

		'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
		"ADD_SECTIONS_CHAIN" => "N"
	),
	$component
);?></div>