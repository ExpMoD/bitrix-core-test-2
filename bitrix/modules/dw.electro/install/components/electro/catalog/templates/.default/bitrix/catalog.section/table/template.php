<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>

<?if (!empty($arResult['ITEMS'])):?>
<?
	if ($arParams["DISPLAY_TOP_PAGER"]){
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
?>

	<ul id="catalogTableList">
		<?foreach($arResult["ITEMS"] as $arElement):?>
			<li>
				<?$img = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
				<table>
					<tbody>
				    	<tr>
				    		<td class="imgLink"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=(!empty($img["src"]) ? $img["src"] : SITE_TEMPLATE_PATH.'/images/empty.png')?>" alt="<?=$arElement["NAME"]?>"></a></td>
				    		<td class="link"><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></td>
				    		<?=!empty($arElement["PROPERTIES"]["ARTICLE"]["VALUE"]) ? '<td class="art"><span>'.GetMessage("ART").'&nbsp;'.$arElement["PROPERTIES"]["ARTICLE"]["VALUE"].'</span></td>' : ''?>
				    		<td class="pr">
					    		<span class="price">
									<?=($arElement["PRICES"][$arParams["PRICE_CODE"][0]]["DISCOUNT_VALUE_VAT"] != $arElement["PRICES"][$arParams["PRICE_CODE"][0]]["VALUE"] ? '<s>'.str_replace(GetMessage("RUB"), "", $arElement["PRICES"][$arParams["PRICE_CODE"][0]]["PRINT_VALUE"]).'</s> ' : '')?>
									<?=str_replace(GetMessage("RUB"),'<span class="rouble">Р<i>-</i></span>', $arElement["PRICES"][$arParams["PRICE_CODE"][0]]["PRINT_DISCOUNT_VALUE_VAT"]);?>
								</span>
							</td>
				    		<td class="cart"><a href="#" class="addCart" data-id="<?=$arElement["ID"]?>"><span><?=GetMessage("ADDCART");?></span></a></td>
				    		<td class="quantity">
			    				<div>
				    				<?if($arElement["CATALOG_QUANTITY"] > 0):?>
								      <span class="available">
								        <img src="<?=SITE_TEMPLATE_PATH?>/images/iconAvailable.png" alt="<?=GetMessage("AVAILABLE");?>">
								       <?=GetMessage("AVAILABLE");?>
								      </span>
								  <?else:?>
								      <span class="noAvailable">
								        <img src="<?=SITE_TEMPLATE_PATH?>/images/iconNoAvailable.png" alt="<?=GetMessage("NOAVAILABLE");?>">
								        <?=GetMessage("NOAVAILABLE");?>
								      </span>			  
							      <?endif;?>
							      <a href="#" class="addCompare" data-id="<?=$arElement["ID"]?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/iconCompare.png" alt=""> <?=GetMessage("ADDCOMPARE");?></a>
						      </div>	
				    		</td>
				    	</tr>
				    </tbody>
				</table>
			</li>
		<?endforeach;?>
	</ul>	
<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"]){
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
?>

<?if(empty($_GET["PAGEN_1"])):?>
	<p><?=$arResult["DESCRIPTION"]?></p>
<?endif;?>

<?else:?>
	<div id="empty">
		<img src="<?=SITE_TEMPLATE_PATH?>/images/emptyFolder.png" alt="<?=GetMessage("EMPTYDIR");?>" class="emptyImg">
		<div class="info">
			<h3><?=GetMessage("EMPTYDIR");?></h3>
			<p><?=GetMessage("EMPTY");?></p>
			<a href="/" class="back"><?=GetMessage("MAIN");?></a>
		</div>
	</div>
<?endif;?>