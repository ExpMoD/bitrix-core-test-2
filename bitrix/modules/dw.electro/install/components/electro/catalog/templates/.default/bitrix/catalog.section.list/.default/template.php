<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

	$i = 0; 
	$b = 0; 
	foreach($arResult["SECTIONS"] as $arElement){ 
		if($arElement["DEPTH_LEVEL"] == 1){
			$i++;
			$result[$i] = array(
				"NAME" => $arElement["NAME"],
				"SECTION_PAGE_URL" => $arElement["SECTION_PAGE_URL"],
				"DETAIL_PICTURE" => CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array('width'=>360, 'height'=>300), BX_RESIZE_IMAGE_PROPORTIONAL, true)
			);
		}
		elseif($arElement["DEPTH_LEVEL"] == 2){
			$b++;
			if($arElement["SORT"] <= 50){
				$from = 1;	
			}
			elseif($arElement["SORT"] <= 150){
				$from = 2;	
			}else{
				$from = 3;
			}
			$result[$i]["ELEMENTS"][$from][$b] = array(
				"NAME" => $arElement["NAME"],
				"SECTION_PAGE_URL" => $arElement["SECTION_PAGE_URL"]
			);
		}elseif($arElement["DEPTH_LEVEL"] == 3){
			$result[$i]["ELEMENTS"][$from ][$b]["ELEMENTS"][] = array(
				"NAME" => $arElement["NAME"],
				"SECTION_PAGE_URL" => $arElement["SECTION_PAGE_URL"]
			);	
		}
	} 
	?>
<?if(count($result) > 1):?>
	<?foreach($result as $nextElement):?>
		<div class="catalogSection">
			<table width="100%">
				<tbody>
						<tr>
							<td><a href="<?=$nextElement["SECTION_PAGE_URL"]?>"><img src="<?=$nextElement["DETAIL_PICTURE"]["src"]?>" alt="<?=$nextElement["NAME"]?>"></a><a href="<?=$nextElement["SECTION_PAGE_URL"]?>"><span><?=$nextElement["NAME"]?></span></a></td>
							<td>
								<table width="100%">
									<tbody>
										<tr>
											<td>
												<?if(count($nextElement["ELEMENTS"][1])):?>
													<?foreach($nextElement["ELEMENTS"][1] as $next2Elements):?>
															<a href="<?=$next2Elements["SECTION_PAGE_URL"]?>" class="hd"><?=$next2Elements["NAME"]?></a>
															<ul>
																<?if(!empty($next2Elements["ELEMENTS"])):?>
																	<?foreach($next2Elements["ELEMENTS"] as $next3Elements):?>
																		<li><a href="<?=$next3Elements["SECTION_PAGE_URL"]?>"><?=$next3Elements["NAME"]?></a></li>
																	<?endforeach;?>
																<?endif;?>
															</ul>
													<?endforeach;?>						
												<?endif;?>
											</td>
										    <td>
											   	<?if(count($nextElement["ELEMENTS"][2])):?>
													<?foreach($nextElement["ELEMENTS"][2] as $next2Elements):?>
															<a href="<?=$next2Elements["SECTION_PAGE_URL"]?>" class="hd"><?=$next2Elements["NAME"]?></a>
															<ul>
																<?if(!empty($next2Elements["ELEMENTS"])):?>
																	<?foreach($next2Elements["ELEMENTS"] as $next3Elements):?>
																		<li><a href="<?=$next3Elements["SECTION_PAGE_URL"]?>"><?=$next3Elements["NAME"]?></a></li>
																	<?endforeach;?>
																<?endif;?>
															</ul>
													<?endforeach;?>						
												<?endif;?>
										    </td>
										    <td>
												<?if(count($nextElement["ELEMENTS"][3])):?>
													<?foreach($nextElement["ELEMENTS"][3] as $next2Elements):?>
															<a href="<?=$next2Elements["SECTION_PAGE_URL"]?>" class="hd"><?=$next2Elements["NAME"]?></a>
															<ul>
																<?if(!empty($next2Elements["ELEMENTS"])):?>
																	<?foreach($next2Elements["ELEMENTS"] as $next3Elements):?>
																		<li><a href="<?=$next3Elements["SECTION_PAGE_URL"]?>"><?=$next3Elements["NAME"]?></a></li>
																	<?endforeach;?>
																<?endif;?>
															</ul>
													<?endforeach;?>						
												<?endif;?>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
				</tbody>
			</table>
		</div>
	<?endforeach;?>	
<?endif;?>	