<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>

<?if (!empty($arResult['ITEMS'])):?>
<?
	if ($arParams["DISPLAY_TOP_PAGER"]){
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
?>

	<ul class="productList">
		<?foreach($arResult["ITEMS"] as $arElement):?>
			<?$img = CFile::ResizeImageGet($arElement['DETAIL_PICTURE'], array('width'=>230, 'height'=>170), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
			<li class="product">
				<div class="wrap">
					<?=(!empty($arElement["PROPERTIES"]["MARKER"]["VALUE"]) ? '<ins class="marker">'.$arElement["PROPERTIES"]["MARKER"]["VALUE"].'</ins>' : '')?>
					<span class="rating"> 
						<i class="m" style="width:<?=($arElement["PROPERTIES"]["RATING"]["VALUE"] * 100 / 5)?>%"></i><i class="h"></i>
					</span>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="pic">
						<img src="<?=(!empty($img["src"]) ? $img["src"] : SITE_TEMPLATE_PATH.'/images/empty.png')?>" alt="<?=$arElement["NAME"]?>">
					</a>
					<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="name"><?=$arElement["NAME"]?></a>
					<span class="price">
						<?=($arElement["PRICES"][$arParams["PRICE_CODE"][0]]["DISCOUNT_VALUE_VAT"] != $arElement["PRICES"][$arParams["PRICE_CODE"][0]]["VALUE"] ? '<s>'.str_replace(GetMessage("RUB"), "", $arElement["PRICES"][$arParams["PRICE_CODE"][0]]["PRINT_VALUE"]).'</s> ' : '')?>
						<?=str_replace(GetMessage("RUB"),'<span class="rouble">Р<i>-</i></span>', $arElement["PRICES"][$arParams["PRICE_CODE"][0]]["PRINT_DISCOUNT_VALUE_VAT"]);?>
					</span>
					<a href="#" class="addCart" data-id="<?=$arElement["ID"]?>"><?=GetMessage("ADDCART");?></a>
					<div class="hr">
						<a href="#" class="addCompare" data-id="<?=$arElement["ID"]?>">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/iconCompare.png" alt=""> <?=GetMessage("ADDCOMPARE");?>
						</a>
								<?if($arElement["CATALOG_QUANTITY"] > 0):?>
								      <span class="available">
								        <img src="<?=SITE_TEMPLATE_PATH?>/images/iconAvailable.png" alt="<?=GetMessage("AVAILABLE");?>">
								        <?=GetMessage("AVAILABLE");?>
								      </span>
								  <?else:?>
								      <span class="noAvailable">
								        <img src="<?=SITE_TEMPLATE_PATH?>/images/iconNoAvailable.png" alt="<?=GetMessage("NOAVAILABLE");?>">
								        <?=GetMessage("NOAVAILABLE");?>
								      </span>			  
							      <?endif;?>		  
					</div>
				</div>
			</li>
		<?endforeach;?>	
	</ul>

<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"]){
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
?>

<?if(empty($_GET["PAGEN_1"])):?>
	<p><?=$arResult["DESCRIPTION"]?></p>
<?endif;?>

<?else:?>
	<div id="empty">
		<img src="<?=SITE_TEMPLATE_PATH?>/images/emptyFolder.png" alt="<?=GetMessage("EMPTYDIR");?>" class="emptyImg">
		<div class="info">
			<h3><?=GetMessage("EMPTYDIR");?></h3>
			<p><?=GetMessage("EMPTY");?></p>
			<a href="/" class="back"><?=GetMessage("MAIN");?></a>
		</div>
	</div>
<?endif;?>