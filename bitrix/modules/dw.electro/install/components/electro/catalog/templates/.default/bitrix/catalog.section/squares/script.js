var cartResize = function() {
	var width = $(window).outerWidth() < 1000 ? 1000 : $(window).outerWidth();
	var $productList = $(".productList");
	var $elements = $productList.children("li");
	var allWidth = $productList.width() - 1;

	var setWidth = function(count) {
		var elementWidth = (allWidth - ((count - 1) * 27)) / count;
		$elements.removeAttr("style").css({
			width: elementWidth
		}).filter(":nth-child(" + (count) + "n)").css({
			"margin-right": 0
		});
	
	};
	
	if (width > 1800) {
		setWidth(6);
	} else if (width < 1799 && width > 1470) {
		setWidth(5);
	} else if (width < 1469 && width > 1125) {
		setWidth(4);
	} else {
		setWidth(3);
	}

}

$(window).on("ready resize", cartResize);
