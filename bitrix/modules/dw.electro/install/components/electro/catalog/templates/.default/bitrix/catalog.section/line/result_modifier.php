<?
	if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
	foreach ($arResult["ITEMS"] as $index => $arElement){
		foreach ($arElement["DISPLAY_PROPERTIES"] as $key => $arValue) {
			if($arValue["CODE"] == "MORE_PROPERTIES"){
				foreach ($arValue["VALUE"] as $i => $arProp) {
					$arResult["ITEMS"][$index]["DISPLAY_PROPERTIES"][$arValue["PROPERTY_VALUE_ID"][$i]] = array(
						"CODE" => $arValue["PROPERTY_VALUE_ID"][$i],
						"SORT" => 500,
						"DISPLAY_VALUE" => $arValue["DESCRIPTION"][$i],
						"NAME" => $arProp
					);
				}
				unset($arResult["ITEMS"][$index]["DISPLAY_PROPERTIES"][$key]);
			}
		}
	}
?>