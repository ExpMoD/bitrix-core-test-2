<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>
	<div id="secLevel2">
		<ul>
			<?foreach($arResult["SECTIONS"] as $arElement):?>
	    		<?if($arElement["ELEMENT_CNT"] > 0):?>
	    			<li><a href="<?=$arElement["SECTION_PAGE_URL"]?>" class="<?=!empty($arElement["SELECTED"]) ? 'selected' : ''?>"><?=$arElement["NAME"]?> (<?=$arElement["ELEMENT_CNT"]?>)</a></li>
	    		<?endif;?>
		    <?endforeach;?>	
		</ul>
	</div>

	
