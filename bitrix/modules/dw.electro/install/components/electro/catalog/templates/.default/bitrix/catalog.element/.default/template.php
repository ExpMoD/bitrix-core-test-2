<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
global $USER;
?>
<div id="catalogElement">
    <?if(!empty($arResult["PROPERTIES"]["ATT_BRAND"]["IMAGE"])):?>
        <div class="manufacturer">
            <?if(!empty($arResult["PROPERTIES"]["ATT_BRAND"]["LINK"])):?><a href="<?=$arResult["PROPERTIES"]["ATT_BRAND"]["LINK"]?>"><?endif;?><?=!empty($arResult["PROPERTIES"]["ATT_BRAND"]["IMAGE"]) ? '<img src="'.$arResult["PROPERTIES"]["ATT_BRAND"]["IMAGE"].'" alt="'.$arResult["PROPERTIES"]["ATT_BRAND"]["NAME"].'" title="'.$arResult["PROPERTIES"]["ATT_BRAND"]["NAME"].'">' : $arResult["PROPERTIES"]["ATT_BRAND"]["NAME"] ?><?if(!empty($arResult["PROPERTIES"]["ATT_BRAND"]["LINK"])):?></a><?endif;?>
        </div>
    <?endif;?>

    <h1><?=$arResult["NAME"]?></h1>
    <div class="caption">
        <ul>
            <li class="active"><a href="#" id="browseTAB"><?=GetMessage("BROWSE")?></a></li>
            <?if(!empty($arResult["DETAIL_TEXT"])):?><li><a href="#" id="descTAB"><?=GetMessage("DESC")?></a></li><?endif;?>
            <?if(isset($arResult["DISPLAY_PROPERTIES"])):?><li><a href="#" id="propTAB"><?=GetMessage("STATS")?></a></li><?endif;?>
            <?if(isset($arResult["SIMILAR"])):?><li><a href="#" id="similarTAB"><?=GetMessage("ACCESSORIES")?></a></li><?endif;?>
            <?if(isset($arResult["REVIEWS"])):?><li><a href="#" id="reviewsTAB"><?=GetMessage("REVIEW")?></a></li><?endif;?>
            <?if(isset($arResult["RELATED"])):?><li><a href="#" id="relatedTAB"><?=GetMessage("SIMILAR")?></a></li><?endif;?>
        </ul>
    </div>
    <div id="blackout"></div>
    <div id="zoomPhoto">
        <div id="zoomPhotoLeft">
            <ul>
                <li><img src="<?=$arResult["PICTURES"]["PREVIEW_BIG"]["src"]?>" alt="" title=""></li>
                <?if(!empty($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"])):?>
                    <?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $arPhoto):?>
                        <?$morePicture = CFile::ResizeImageGet($arPhoto, array('width'=>1500, 'height'=>1500), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                        <li><img src="<?=$morePicture["src"]?>" alt="" title=""></li>
                    <?endforeach;?>
                <?endif;?>
            </ul>
            <a href="#" id="zoomPrev"></a>
            <a href="#" id="zoomNext"></a>
        </div>
        <div id="zoomPhotoRight">
            <div id="zoomPhotoRightInfo">
                <a href="#" id="zoomPhotoExit"></a>
                <span class="heading"><span><?=substr($arResult["NAME"], 0, 50)?></span></span>
                <i id="zoomDecorLine"></i>
                <div id="zoomPhotoTools">
                    <div id="zoomPhotoToolsKit">
                    <?=(!empty($arResult["PROPERTIES"]["MARKER"]["VALUE"]) ? '<i id="zoomPromo">'.$arResult["PROPERTIES"]["MARKER"]["VALUE"].'</i>' : '')?>
                    <ul>
                        <li><?=str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arResult["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"]);?>  
                            <?=($arResult["PRICES"]["BASE"]["DISCOUNT_VALUE_VAT"] != $arResult["PRICES"]["BASE"]["VALUE"] ? '<s>'.str_replace(GetMessage("RUB"), "", $arResult["PRICES"]["BASE"]["PRINT_VALUE"]).'</s> ' : '')?>
                        </li>
                        <li>
                            <?if($arResult["CATALOG_QUANTITY"] > 0):?>
                                <span class="available">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/iconAvailable.png" alt="<?=GetMessage("AVAILABLE")?>">
                                <?=GetMessage("AVAILABLE")?>
                                </span>
                            <?else:?>
                                <span class="noAvailable">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/iconNoAvailable.png" alt="<?=GetMessage("NOAVAILABLE")?>">
                                <?=GetMessage("NOAVAILABLE")?>
                                </span>       
                            <?endif;?>
                        </li>
                        <li><a href="#" class="addCart" data-id="<?=$arResult["ID"]?>"><span><?=GetMessage("ADDCART")?></span></a></li>
                        <li><a href="#" class="addCompare" data-id="<?=$arResult["ID"]?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/iconCompare.png" alt="<?=GetMessage("ADDCOMPARE")?>"> <?=GetMessage("ADDCOMPARE")?></a></li>
                    </ul>
                </div>
                <ul id="zoomPhotoMoreInfo">
                    <?if(!empty($arResult["REVIEWS"])):?>
                    <li><sub><?=GetMessage("EVALUATION")?></sub>
                        <span class="rating"> 
                        <i class="m" style="width:<?=($arResult["PROPERTIES"]["RATING"]["VALUE"] * 100 / 5)?>%"></i><i class="h"></i>
                        </span>
                       <span class="reviewCnt"><?=GetMessage("REVIEWS")?> <?=count($arResult["REVIEWS"])?></span>
                    </li><?endif;?>
                    <?if(!empty($arResult["PROPERTIES"]["WARRANTY"]["VALUE"])):?>
                        <li><span><?=GetMessage("WARRANTY")?></span> <?=$arResult["PROPERTIES"]["WARRANTY"]["VALUE"]?></li>
                    <?endif;?>
                    <?if(!empty($arResult["PROPERTIES"]["ARTICLE"]["VALUE"])):?>
                        <li><span><?=GetMessage("ARTICLE")?></span> <?=$arResult["PROPERTIES"]["ARTICLE"]["VALUE"]?></li>
                    <?endif;?>
                    </ul>
                </div>  
            </div>
        </div>
        <div id="zoomMorePhoto">
            <div>
                <ul>
                    <li class="active"><a href="#"><img src="<?=$arResult["PICTURES"]["DETAIL"]["src"]?>" alt="" title="" class="largePicture"></a></li>
                    <?if(!empty($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"])):?>
                        <?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $arPhoto):?>
                            <?$morePicture = CFile::ResizeImageGet($arPhoto, array('width'=>40, 'height'=>40), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                            <li><a href="#"><img src="<?=$morePicture["src"]?>" alt="" title="" class="largePicture"></a></li>
                        <?endforeach;?>
                    <?endif;?>
                </ul>
                <a href="" class="prev"></a>
                <a href="" class="next"></a>
            </div>
        </div>      
    </div>
    <div class="layout">
        <table id="elementTableInfo">
            <tbody>
            <tr>
                <td id="photo">
                    <div id="zoomer">
                        <a href="<?=SITE_TEMPLATE_PATH?>/images/imgProduct.jpg" class="zoom" alt="<?=GetMessage("ZOOM")?>"></a>
                        <?=(!empty($arResult["PROPERTIES"]["MARKER"]["VALUE"]) ? '<ins class="marker">'.$arResult["PROPERTIES"]["MARKER"]["VALUE"].'</ins>' : '')?>
                        <div id="elementTableImg">
                            <ul id="itworkSlide">
                                <li><a href="#"><img src="<?=$arResult["PICTURES"]["PREVIEW"]["src"]?>" alt="" title="" class="largePicture"></a></li>
                                <?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photoElement):?>
                                <?$morePicture = CFile::ResizeImageGet($photoElement, array('width'=>500, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                                <li><a href="#"><img src="<?=$morePicture["src"]?>" alt="" title="" class="largePicture"></a></li>
                                <?endforeach;?>
                            </ul>
                            <?if(!empty($arResult["PROPERTIES"]["MARKER_PHOTO"]["VALUE"])):?>
                                <ul class="stickers">
                                    <?foreach($arResult["PROPERTIES"]["MARKER_PHOTO"]["VALUE"] as $i => $arFile):?>
                                    <?if($i == 4) break;?>
                                    <li><img src="<?=$arFile["IMAGE"]?>" alt="" title=""></li>
                                    <?endforeach;?>
                                </ul>
                            <?endif;?>
                            <a href="#" class="prev noActive"></a>
                            <a href="#" class="next"></a>
                         </div>
                         <div id="elementMorePhoto">
                            <span class="selected"></span>
                            <ul id="mover">
                                <li><a href="<?=$arResult["PICTURES"]["PREVIEW_SMALL"]["src"]?>"><img src="<?=$arResult["PICTURES"]["PREVIEW_SMALL"]["src"]?>" alt=""></a></li>
                                <?if(!empty($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"])):?>
                                    <?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photoElement):?>
                                        <?$morePicture = CFile::ResizeImageGet($photoElement, array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
                                        <li><a href="<?=$morePicture["src"]?>"><img src="<?=$morePicture["src"]?>" alt=""></a></li>
                                    <?endforeach;?>
                                <?endif;?>
                            </ul>
                        </div>
                    </div>
                </td>
                <td>
                    <table id="additional">
                        <tbody>
                            <tr>
                                <td class="main">
                                    <?if(!empty($arResult["TOP_PROPERTIES"])):?>
                                    <div class="wrap prop">
                                        <span class="heading"><?=GetMessage("PARAMETERS")?></span>
                                        <ul id="propSlider">
                                            <?foreach($arResult["TOP_PROPERTIES"] as $arProp):?>
                                                <li>                                    
                                                    <table class="shortProp">
                                                        <tbody>
                                                            <?foreach($arProp as $arValues):?>
                                                                <?if(gettype($arValues["VALUE"]) == "array"){$arValues["VALUE"] = implode(" /", $arValues["VALUE"]); $arValues["FILTRABLE"] = false;}?>
                                                                <tr>
                                                                    <td><?=preg_replace("/\[.*\]/", "", $arValues["NAME"])?></td>
                                                                    <td><?if($arValues["FILTRABLE"] =="Y" && !empty($arValues["VALUE_ENUM_ID"])):?><a href="<?=$arResult["LAST_SECTION"]["SECTION_PAGE_URL"]?>?arrFilter_<?=$arValues["ID"]?>_<?=abs(crc32($arValues["VALUE_ENUM_ID"]))?>=Y&set_filter=Y" class="analog"><?endif;?><?=$arValues["VALUE"]?><?if($arValues["FILTRABLE"] =="Y"):?></a><?endif;?></td>
                                                                </tr>
                                                            <?endforeach;?>
                                                        </tbody>
                                                    </table> 
                                                </li>
                                            <?endforeach?>
                                        </ul>
                                        <a href="#" class="allProp"><?=GetMessage("ALLFEATURES")?></a>
                                        <div class="controls">
                                            <a href="#" class="prev"></a>
                                            <a href="#" class="next"></a>
                                        </div>
                                    </div>
                                    <?endif;?>
                                    <?if(isset($arResult["REVIEWS"])):?>
                                    <div class="wrap review">
                                        <span class="heading"><?=GetMessage("FEEDBACK")?></span>
                                        <ul id="reviewSlider">
                                            <?foreach($arResult["REVIEWS"] as $index => $arReview):?>
                                                <li><ins><?=$arReview["PROPERTY_NAME_VALUE"]?></ins><p><?=(strlen($arReview["DETAIL_TEXT"]) > 250) ? preg_replace('/\s[^\s]+$/', '', mb_substr($arReview["DETAIL_TEXT"], 0, 270, 'CP1251')) : $arReview["DETAIL_TEXT"]?><?if(strlen($arReview["DETAIL_TEXT"]) > 250):?> <br /><a href="#" class="showReviewDetail" data-cnt="<?=$index?>"><?=GetMessage("READMORE")?></a><?endif;?></p></li>
                                            <?endforeach;?>
                                        </ul>
                                        <a href="#" class="allReview"><?=GetMessage("ALLREVIEWS")?></a>
                                        <div class="controls">
                                            <a href="#" class="prev"></a>
                                            <a href="#" class="next"></a>
                                        </div>
                                    </div>    
                                    <?endif;?>
                                </td>
                                <td class="extra">
                                    <div class="wrap">
                                        <ul class="sectional">
                                            <li class="price">
                                                <span><?=GetMessage("PRICE")?></span>
                                                <?=str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arResult["PRICES"]["BASE"]["PRINT_DISCOUNT_VALUE_VAT"]);?>  
                                                <?=($arResult["PRICES"]["BASE"]["DISCOUNT_VALUE_VAT"] != $arResult["PRICES"]["BASE"]["VALUE"] ? '<s>'.str_replace(GetMessage("RUB"), "", $arResult["PRICES"]["BASE"]["PRINT_VALUE"]).'</s> ' : '')?>
                                            </li>
                                            <li class="inCart"><a href="#" class="addCart" data-id="<?=$arResult["ID"]?>"><?=GetMessage("ADDCART_BIG")?></a></li>
                                            <li class="fastBay"><a href="#" class="oneClick" data-id="794"><?=GetMessage("ONCLICK_BIG");?></a></li>
                                            <li>
                                                <?if($arResult["CATALOG_QUANTITY"] > 0):?>
                                                    <span class="available">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/available.png" alt="<?=GetMessage("AVAILABLE_BIG")?>">
                                                    <?=GetMessage("AVAILABLE_BIG")?>
                                                    </span>
                                                <?else:?>
                                                    <span class="noAvailable">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/iconNoAvailable.png" alt="<?=GetMessage("NOAVAILABLE_BIG")?>">
                                                    <?=GetMessage("NOAVAILABLE_BIG")?>
                                                    </span>       
                                                <?endif;?>
                                            </li>
                                            <li class="compare"><a href="#" class="addCompare" data-id="<?=$arResult["ID"]?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/iconCompare.png" alt=""> <?=GetMessage("ADDCOMPARE_BIG")?></a></li>
                                            <li class="rat">
                                                <span class="score"><?=GetMessage("RATING")?>&nbsp;</span>            
                                                <span class="rating"> 
                                                <i class="m" style="width:<?=($arResult["PROPERTIES"]["RATING"]["VALUE"] * 100 / 5)?>%"></i><i class="h"></i>
                                                </span>
                                            </li>
                                            <?if(!empty($arResult["PROPERTIES"]["WARRANTY"]["VALUE"])):?>
                                                <li><span><?=GetMessage("WARRANTY")?></span> <?=$arResult["PROPERTIES"]["WARRANTY"]["VALUE"]?></li>
                                            <?endif;?>
                                            <?if(!empty($arResult["PROPERTIES"]["ARTICLE"]["VALUE"])):?>
                                                <li><span><?=GetMessage("ARTICLE")?></span> <?=$arResult["PROPERTIES"]["ARTICLE"]["VALUE"]?></li>
                                            <?endif;?>
                                            <?if(!empty($arResult["PROPERTIES"]["DELIVERY"]["VALUE"]) || !empty($arResult["PROPERTIES"]["PICKUP"]["VALUE"])):?>
                                                <li class="method"><span><?=GetMessage("OBTAININGGOODS")?></span></li>
                                            <?if(!empty($arResult["PROPERTIES"]["DELIVERY"]["VALUE"])):?>
                                                <li class="shipping"><img src="<?=SITE_TEMPLATE_PATH?>/images/delivery.png"> <span><?=GetMessage("SHIPPING")?></span> <ins><?=$arResult["PROPERTIES"]["DELIVERY"]["VALUE"]?></ins></li>
                                            <?endif;?>
                                            <?if(!empty($arResult["PROPERTIES"]["PICKUP"]["VALUE"])):?>
                                                <li class="shipping"><img src="<?=SITE_TEMPLATE_PATH?>/images/pickup.png"> <span><?=GetMessage("PICKUP")?></span> <ins><?=$arResult["PROPERTIES"]["PICKUP"]["VALUE"]?></ins></li>
                                            <?endif;?>
                                            <?endif;?>
                                            <?if(!empty($arResult["PROPERTIES"]["DELIVERY_DESC"]["VALUE"])):?>
                                            <li class="infoText">   
                                                <p><?=$arResult["PROPERTIES"]["DELIVERY_DESC"]["VALUE"]?></p>
                                            </li>
                                            <?endif;?>
                                        </ul>
                                    </div>
                                </td> 
                            </tr>
                        </tbody>  
                    </table>
                </td>
                </tr>
            </tbody>                
        </table>
        <?if(!empty($arResult["DETAIL_TEXT"])):?>
            <span class="heading"><?=GetMessage("PRODUCTDESCRIPTION")?></span>
            <p class="description"><?=$arResult["DETAIL_TEXT"]?></p>
        <?endif;?>
        <?if(!empty($arResult["TOP_PROPERTIES"])):?>
        <span class="heading"><?=GetMessage("SPECS")?></span>
        <table class="stats">
            <tbody>
                                <?foreach($arResult["PROPERTIES"] as $arProp):?> 
                    <?if(!empty($arProp["VALUE"]) && $arProp["SORT"] <= 500):?>
                        <?
                            $set++;
                            $PROP_NAME = $arProp["NAME"];
                        ?>
                        <?if(gettype($arProp["VALUE"]) == "array"){$arProp["VALUE"] = implode(" /", $arProp["VALUE"]); $arProp["FILTRABLE"] = false;}?>
                        <?if(preg_match("/\[.*\]/", trim($arProp["NAME"]), $res, PREG_OFFSET_CAPTURE)):?>
                            <?$arProp["NAME"] = substr($arProp["NAME"], 0, $res[0][1]);?>
                            <?if($res[0][0] != $arResult["OLD_CAP"]):?>
                                <?
                                    $arResult["OLD_CAP"] = $res[0][0];
                                    $set = 1;
                                ?>
                                <tr class="cap">
                                    <td colspan="3"><?=substr($res[0][0], 1, -1)?></td>
                                </tr>
                            <?endif;?>
                            <tr<?if($set % 2):?> class="gray"<?endif;?>>
                                <td class="name"><span><?=preg_replace("/\[.*\]/", "", trim($PROP_NAME))?></span><?if(!empty($arProp["HINT"])):?><a href="#" class="question" title="<?=$arProp["HINT"]?>"data-description="<?=$arProp["HINT"]?>"></a><?endif;?></td>
                                <td><?=$arProp["VALUE"]?></td>
                                <td class="right"><?if($arProp["FILTRABLE"] =="Y" && !is_array($arProp["VALUE"])):?><a href="<?=$arResult["LAST_SECTION"]["SECTION_PAGE_URL"]?>?arrFilter_<?=$arProp["ID"]?>_<?=abs(crc32($arProp["VALUE_ENUM_ID"]))?>=Y&set_filter=Y" class="analog"><?=GetMessage("OTHERITEMS")?></a><?endif;?></td>
                            </tr>
                        <?else:?>
                            <? $arSome[] = $arProp?> 
                        <?endif;?>
                    <?endif;?>
                <?endforeach;?>
                <?if(!empty($arSome)):?>
                <?$set = 0;?>
                     <tr class="cap">
                        <td colspan="3"><?=GetMessage("CHARACTERISTICS")?></td>
                    </tr>
                    <?foreach($arSome as $arProp):?>
                        <?$set++?>
                        <tr<?if($set % 2 ):?> class="gray"<?endif;?>>
                            <td class="name"><span><?=$arProp["NAME"]?></span></td>
                            <td><?=$arProp["VALUE"]?></td>
                            <td class="right"><?if($arProp["FILTRABLE"] =="Y" && !is_array($arProp["VALUE"]) && !empty($arProp["VALUE_ENUM_ID"])):?><a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>?arrFilter_<?=$arProp["ID"]?>_<?=abs(crc32($arProp["VALUE_ENUM_ID"]))?>=Y&set_filter=Y" class="analog"><?=GetMessage("OTHERITEMS")?></a><?endif;?></td>
                        </tr>
                    <?endforeach;?>
                <?endif;?>
            </tbody>
        </table>
        <?endif;?>
        <?if(isset($arResult["SIMILAR"])):?>
            <script>
                $(window).bind('load', function(){
                    $("#similarCarousel").electroCarousel({
                        speed: 500,
                        countElement: 5,
                        leftButton: ".similarLeft",
                        rightButton: ".similarRight",
                        resizeElement: true
                    });
                });
            </script>
        <span class="heading"><?=GetMessage("ACCESSORIES")?></span>
        <div id="similarCarousel">
            <ul class="productList">
                <?foreach ($arResult["SIMILAR"] as $key => $arSimilar):?> 
                <li class="product">
                    <div class="wrap">
                        <?=(!empty($arSimilar["PROPERTY_MARKER_VALUE"]) ? '<ins class="marker">'.$arSimilar["PROPERTY_MARKER_VALUE"].'</ins>' : '')?>
                        <span class="rating"> 
                        <i class="m" style="width:<?=($arSimilar["PROPERTY_RATING_VALUE"] * 100 / 5)?>%"></i><i class="h"></i>
                        </span>
                        <a href="<?=$arSimilar["DETAIL_PAGE_URL"]?>" class="pic" target="_blank">
                        <img src="<?=$arSimilar["PICTURE"]["src"]?>" alt="<?=$arSimilar["NAME"]?>">
                        </a>
                        <a href="<?=$arSimilar["DETAIL_PAGE_URL"]?>" class="name" target="_blank"><?=$arSimilar["NAME"]?></a>
                        <span class="price">
                        <?=str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arSimilar["PRICE"]);?>
                        <?if($arSimilar["OLD_PRICE"]):?>
                            <s><?=$arSimilar["OLD_PRICE"]?></s>
                        <?endif;?>
                        </span>
                        <a href="#" class="addCart" data-id="<?=$arSimilar["ID"]?>"><?=GetMessage("ADD_CART_MIN")?></a>
                    </div>
                </li>
                <?endforeach;?>
            </ul>
            <a href="#" class="similarLeft"></a>
            <a href="#" class="similarRight"></a>
        </div>
        <?endif;?>
       <?if(isset($arResult["REVIEWS"])):?>
        <span class="heading"><?=GetMessage("REVIEW")?></span>
        <ul id="reviews">
            <?foreach($arResult["REVIEWS"] as $i => $arReview):?>
                <li<?if($i > 2):?> class="hide"<?endif?>>
                    <ul>
                        <li>

                            <span class="name"><?=$arReview["PROPERTY_NAME_VALUE"]?>:</span>
                               <small>                        
                                <?=FormatDate(array(
                                "tommorow" => "tommorow",
                                "today" => "today",  
                                "yesterday" => "yesterday", 
                                "d" => 'j F',  
                                 "" => 'j F Y',  
                                ), MakeTimeStamp($arReview["DATE_CREATE"], "DD.MM.YYYY HH:MI:SS"));
                                ?>
                            </small>
                        </li>
                        <li>
                            <span class="label"><?=GetMessage("USERRATINGS")?> </span> <span class="rating"><i class="m" style="width:<?=($arReview["PROPERTY_RATING_VALUE"] * 100 / 5)?>%"></i><i class="h"></i></span>
                        </li>
                        <li>
                            <span class="label"><?=GetMessage("DIGNIFIED")?> </span>
                            <p><?=$arReview["~PROPERTY_DIGNITY_VALUE"]?></p>
                        </li>
                        <li>
                            <span class="label"><?=GetMessage("FAULTY")?> </span>
                            <p><?=$arReview["~PROPERTY_SHORTCOMINGS_VALUE"]?></p>
                        </li>
                        <li>
                            <span class="label"><?=GetMessage("IMPRESSION")?> </span>
                            <p class="comment"> <?=$arReview["~DETAIL_TEXT"]?></p>
                        </li>
                        <li>
                            <span class="label"><?=GetMessage("EXPERIENCE")?> </span>
                            <p><?=$arReview["~PROPERTY_EXPERIENCE_VALUE"]?></p>
                        </li>
                    </ul>
                    <div class="controls">
                        <span><?=GetMessage("REVIEWSUSEFUL")?></span>
                        <a href="#" class="good" data-id="<?=$arReview["ID"]?>"><?=GetMessage("YES")?> (<span><?=!empty($arReview["PROPERTY_GOOD_REVIEW_VALUE"]) ? $arReview["PROPERTY_GOOD_REVIEW_VALUE"] : 0 ?></span>)</a>
                        <a href="#" class="bad" data-id="<?=$arReview["ID"]?>"><?=GetMessage("NO")?> (<span><?=!empty($arReview["PROPERTY_BAD_REVIEW_VALUE"]) ? $arReview["PROPERTY_BAD_REVIEW_VALUE"] : 0 ?></span>)</a>
                    </div>
                </li>
            <?endforeach;?>
        </ul>
        <?if(count($arResult["REVIEWS"]) > 3):?><a href="#" id="showallReviews" data-open="N"><?=GetMessage("SHOWALLREVIEWS")?></a><?endif;?>
        <?endif;?>
        <?if($USER->IsAuthorized()):?>
            <?if($arParams["SHOW_REVIEW_FORM"]):?>
            <div id="newReview">
                <span class="heading"><?=GetMessage("ADDAREVIEW")?></span>
                <form action="" method="GET">
                    <table>
                        <tbody>
                            <tr>
                                <td class="left">
                                    <div id="newRating"><ins><?=GetMessage("YOURRATING")?></ins><span class="rating"><i class="m" style="width:0%"></i><i class="h"></i></span></div>
                                    <label><?=GetMessage("EXPERIENCE")?></label>
                                    <?if(!empty($arResult["NEW_REVIEW"]["EXPERIENCE"])):?>
                                        <ul class="usedSelect">
                                            <?foreach ($arResult["NEW_REVIEW"]["EXPERIENCE"] as $arExp):?>
                                                <li><a href="#" data-id="<?=$arExp["ID"]?>"><?=$arExp["VALUE"]?></a></li>
                                            <?endforeach;?>
                                        </ul>
                                    <?endif;?>
                                    <label><?=GetMessage("DIGNIFIED")?></label>
                                    <textarea rows="10" cols="45" name="DIGNITY"></textarea>
                                </td>
                                <td class="right">
                                    <label><?=GetMessage("FAULTY")?></label>
                                    <textarea rows="10" cols="45" name="SHORTCOMINGS"></textarea> 
                                    <label><?=GetMessage("IMPRESSION")?></label>
                                    <textarea rows="10" cols="45" name="COMMENT"></textarea>   
                                    <label><?=GetMessage("INTRODUCEYOURSELF")?></label>
                                    <input type="text" name="NAME"><a href="#" class="submit" data-id="<?=$arParams["REVIEW_IBLOCK_ID"]?>"><?=GetMessage("SENDFEEDBACK")?></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="USED" id="usedInput" value="" />
                    <input type="hidden" name="RATING" id="ratingInput" value="0"/>
                    <input type="hidden" name="PRODUCT_NAME" value="<?=$arResult["NAME"]?>"/>
                    <input type="hidden" name="PRODUCT_ID" value="<?=$arResult["ID"]?>"/>
                </form>
            </div>
            <?else:?>
            <div id="newReview">
                <div class="reviewError">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/addReview.png">
                    <span class="cap"><?=GetMessage("REVIEW_OK")?></span>
                    <p><?=GetMessage("REVIEW_BAD")?></p>
                </div>
            </div>
            <?endif;?>
        <?endif;?>
        <?if(isset($arResult["RELATED"])):?>
            <script>
                $(window).bind('load', function(){
                    $("#relatedCarousel").electroCarousel({
                        speed: 500,
                        countElement: 5,
                        leftButton: ".relatedLeft",
                        rightButton: ".relatedRight",
                        resizeElement: true
                    });
                });
            </script>
        <span class="heading"><?=GetMessage("SIMILAR")?></span>
        <div id="relatedCarousel">
            <ul class="productList">
                <?foreach ($arResult["RELATED"] as $key => $arRelated):?>
                <li class="product">
                    <div class="wrap">
                        <?=(!empty($arRelated["PROPERTY_MARKER_VALUE"]) ? '<ins class="marker">'.$arRelated["PROPERTY_MARKER_VALUE"].'</ins>' : '')?>
                        <span class="rating"> 
                        <i class="m" style="width:<?=($arRelated["PROPERTY_RATING_VALUE"] * 100 / 5)?>%"></i><i class="h"></i>
                        </span>
                        <a href="<?=$arRelated["DETAIL_PAGE_URL"]?>" class="pic" target="_blank">
                        <img src="<?=$arRelated["PICTURE"]["src"]?>" alt="<?=$arRelated["NAME"]?>">
                        </a>
                        <a href="<?=$arRelated["DETAIL_PAGE_URL"]?>" class="name" target="_blank"><?=$arRelated["NAME"]?></a>
                        <span class="price">
                           <?=str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arRelated["PRICE"]);?>
                            <?if($arRelated["OLD_PRICE"]):?>
                                <s><?=$arRelated["OLD_PRICE"]?></s>
                            <?endif;?>
                        </span>
                        <a href="#" class="addCart" data-id="<?=$arRelated["ID"]?>"><?=GetMessage("ADD_CART_MIN")?></a>
                    </div>
                </li>
                <?endforeach;?>
            </ul>
            <a href="#" class="relatedLeft"></a>
            <a href="#" class="relatedRight"></a>
        </div>
        <?endif;?>
    </div>
    <div id="elementError">
      <div id="elementErrorContainer">
        <span class="heading"><?=GetMessage("ERROR")?></span>
        <a href="#" id="elementErrorClose"></a>
        <p class="message"></p>
        <a href="#" class="close"><?=GetMessage("CLOSE")?></a>
      </div>
    </div>
</div>
<div id="fastBack">
    <div id="fastBackWrapper">
        <span class="heading"><?=GetMessage("FASTBAY")?></span>
        <a href="#" class="fastBackClose" data-id="fastBack"></a>
        <form name="" action="" method="post" id="fastBackForm" data-product-id="<?=$arResult["ID"]?>">
            <input name="name" type="text" value="" placeholder="<?=GetMessage("PERS_INFO")?>" id="fastBackName">
            <input name="phone" type="text" value="" placeholder="<?=GetMessage("PERS_PHONE")?>" id="fastBackPhone">
            <textarea name="message" rows="5" cols="20" wrap="off" placeholder="<?=GetMessage("PERS_MESSAGE")?>" spellcheck="false" id="fastBackMessage"></textarea>
            <a href="#" class="fastBackSubmit" id="fastBackSubmit"><?=GetMessage("BAY")?></a>
        </form>
        <div id="fastBackErrors"></div>
        <div id="fastBackSuccess">
          <p class="message"></p>
          <a href="#" class="close"><?=GetMessage("CLOSE_WINDOW")?></a>
        </div>
    </div>
</div>