<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if(!empty($arResult)):?>
<?$picture = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], Array("width" => 250, "height" => 180));?>
<h1><?=$arResult["NAME"]?></h1>
	<div id="brandDetail">
		<img src="<?=$picture["src"]?>" class="brandLogo"><p><?=$arResult["DETAIL_TEXT"]?></p>
	</div>
<?endif;?>