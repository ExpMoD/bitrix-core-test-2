<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if(!empty($arResult["ITEMS"])):?>
	<div id="bestsellers">
	    <div class="offersLine">
			<span><?=GetMessage("HEADING_HITS")?></span>
		</div>
		<div id="hitsCarousel">
	 		<ul class="productList">
				<?foreach($arResult["ITEMS"] as $arElement):?>
				<?
					$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array());
				?>
					<li class="product" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
					  <div class="wrap">
					    <?=(!empty($arElement["PROPERTY_MARKER_VALUE"]) ? '<ins class="marker">'.$arElement["PROPERTY_MARKER_VALUE"].'</ins>' : '')?>
					    <span class="rating"> 
					      <i class="m" style="width:<?=($arElement["PROPERTY_RATING_VALUE"] * 100 / 5)?>%"></i><i class="h"></i>
					    </span>
					    <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="pic">
					      <img src="<?=(!empty($arElement["IMG"]["src"]) ? $arElement["IMG"]["src"] : SITE_TEMPLATE_PATH.'/images/empty.png')?>" alt="<?=$arElement["NAME"]?>">
					    </a>
					    <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="name"><?=$arElement["NAME"]?></a>
					    <span class="price">
					      <?=($arElement["OLD_PRICE"] ? '<s>'.$arElement["OLD_PRICE"].'</s>' : '')?>
					      <?=str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arElement["PRICE"]);?>
					    </span>
					    <a href="#" class="addCart" data-id="<?=$arElement["ID"]?>"><?=GetMessage("ADDCART")?></a>
					    <div class="hr">
					      <a href="#" class="addCompare" data-id="<?=$arElement["ID"]?>">
					        <img src="/bitrix/templates/electro/images/iconCompare.png" alt="<?=GetMessage("ADDCOMPARE")?>">
					         <?=GetMessage("ADDCOMPARE")?>
					      </a>
					      <?if($arElement["CATALOG_QUANTITY"] > 0):?>
						      <span class="available">
						        <img src="<?=SITE_TEMPLATE_PATH?>/images/iconAvailable.png" alt="<?=GetMessage("AVAILABLE")?>">
						        <?=GetMessage("AVAILABLE")?>
						      </span>
						  <?else:?>
						      <span class="noAvailable">
						        <img src="<?=SITE_TEMPLATE_PATH?>/images/iconNoAvailable.png" alt="<?=GetMessage("NOAVAILABLE")?>">
						        <?=GetMessage("NOAVAILABLE")?>
						      </span>			  
					      <?endif;?>
					    </div>
					  </div>
					</li>
				<?endforeach;?>
			</ul>
		    <a href="#" class="hitsBtnLeft"></a>
        	<a href="#" class="hitsBtnRight"></a>	
	    </div>
 	</div>
<?endif;?>