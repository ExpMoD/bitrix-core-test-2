<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?error_reporting(0);?>
<?if(!empty($_GET["act"])){
	if (CModule::IncludeModule("catalog") && CModule::IncludeModule("sale")){
		if($_GET["act"] == "add"){
			if(Add2BasketByProductID(intval($_GET['id']), intval($_GET["q"]), false)){
				global $USER;
				$getList = CIBlockElement::GetList(
					Array(),
					array(
						"ID" => intval($_GET['id'])
					),
					false,
					false,
					array(
						"ID",
						"NAME",
						"DETAIL_PICTURE",
						"DETAIL_PAGE_URL"
					)
				);
				$obj = $getList->GetNextElement();
				$arResult = $obj->GetFields();
				$arResult["DETAIL_PICTURE"] = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width' => 300, 'height' => 300), BX_RESIZE_IMAGE_PROPORTIONAL, true);
				$arResult["DETAIL_PICTURE"] = !empty($arResult["DETAIL_PICTURE"]["src"]) ? $arResult["DETAIL_PICTURE"]["src"] : SITE_TEMPLATE_PATH."/images/empty.png";
				foreach ($arResult as $index => $arValues) {
					$arJsn[] = '"'.$index.'":"'.addslashes($arValues).'"';
				}
				$dbBasketItems = CSaleBasket::GetList(
					false,
					array(
						"FUSER_ID" => CSaleBasket::GetBasketUserID(),
						"ORDER_ID" => "NULL",
						"PRODUCT_ID" => intval($_GET["id"])
					),
					false,
					false,
					array(
						"ID",
						"QUANTITY",
						"PRICE",
						"PRODUCT_ID"
					)
				);
				$basketQty = $dbBasketItems->Fetch();
				$arJsn[] = '"PRODUCT_ID":"'.intval($basketQty["PRODUCT_ID"]).'","CART_ID":"'.intval($basketQty["ID"]).'","QUANTITY":"'.intval($basketQty["QUANTITY"]).'","~PRICE":"'.round($basketQty["PRICE"]).'","PRICE":"'.priceFormat(round($basketQty["PRICE"])).'","SUM":"'.priceFormat((round($basketQty["PRICE"]) * intval($basketQty["QUANTITY"]))).'"';
				echo "{".implode($arJsn, ",")."}";
			}
		}
		elseif($_GET["act"] == "del"){
			echo CSaleBasket::Delete(intval($_GET["id"]));
		}elseif($_GET["act"] == "upd"){
			
			$dbBasketItems = CSaleBasket::GetList(
				false, 
				array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"ORDER_ID" => "NULL",
					"PRODUCT_ID" => intval($_GET["id"])
				), 
				false, 
				false, 
				array("ID")
			);
			
			$basketRES = $dbBasketItems->Fetch();
			
			echo CSaleBasket::Update(
					$basketRES["ID"],
					 array(
					 	"QUANTITY" => intval($_GET["q"])
					)
				);
		}
		elseif($_GET["act"] == "compADD"){
			if(!empty($_GET["id"])){
				
				$res = CIBlockElement::GetList(
					Array(), 
					Array(
						"ID" => IntVal($_GET["id"])
					), 
					false, 
					false, 
					Array(
						"ID", 
						"NAME", 
						"IBLOCK_ID"
					)
				);

				$ob = $res->GetNextElement();
			 	$arFields = $ob->GetFields();
				$_SESSION["COMPARE_LIST"]["ITEMS"][$_GET["id"]] = $_GET["id"];
				echo '{"NAME":"'.$arFields["NAME"].'"}';
			}
		}elseif($_GET["act"] == "compDEL"){
			if(!empty($_GET["id"])){
				foreach ($_SESSION["COMPARE_LIST"]["ITEMS"] as $key => $arValue){
					if($arValue == $_GET["id"]){
						echo true;
						unset($_SESSION["COMPARE_LIST"]["ITEMS"][$key]);
						break;
					}
				}
			}
		}elseif($_GET["act"] == "search"){
			$_GET["name"] = BX_UTF !== 1 ? htmlspecialcharsbx(iconv("UTF-8", "CP1251//IGNORE", $_GET["name"])) : htmlspecialcharsbx($_GET["name"]);
			
			if(!empty($_GET["name"]) && !empty($_GET["iblock_id"])){
				$section = !empty($_GET["section"]) ? intval($_GET["section"]) : 0;
				$arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "CATALOG_QUANTITY");
				$arFilter = Array("?NAME" => $_GET["name"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_ID" => intval($_GET["iblock_id"]));
				if($section){
					 $arFilter["SECTION_ID"] = $section;
				}
				$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 4), $arSelect);
				while($ob = $res->GetNextElement()){ 
					$arFields = $ob->GetFields(); 
					$dbPrice = CPrice::GetList(
				        array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"),
				        array("PRODUCT_ID" => $arFields["ID"]),
				        false,
				        false,
				        array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
					);
					while ($arPrice = $dbPrice->Fetch()){
					    $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
				            $arPrice["ID"],
				            $USER->GetUserGroupArray(),
				            "N",
				            SITE_ID
					    );
					    $arFields["PRICE"] = CCatalogProduct::CountPriceWithDiscount(
				            $arPrice["PRICE"],
				            $arPrice["CURRENCY"],
				            $arDiscounts
					    );
					    $arFields["DISCONT_PRICE"] = $arFields["PRICE"] != $arPrice["PRICE"] ? CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]) : 0;
					    $arFields["PRICE"] = CurrencyFormat($arFields["PRICE"], $arPrice["CURRENCY"]);
					}
					$picture = CFile::ResizeImageGet($arFields['DETAIL_PICTURE'], array('width' => 50, 'height' => 50), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$arFields["DETAIL_PICTURE"] = !empty($picture["src"]) ? $picture["src"] : SITE_TEMPLATE_PATH."/images/empty.png";
					foreach ($arFields as $key => $arProp){
						$arJsn[] = '"'.$key.'" : "'.addslashes($arProp).'"';
					}
					$arReturn[] = '{'.implode($arJsn, ",").'}';
				}

				echo "[".implode($arReturn, ",")."]";
			}
		}elseif($_GET["act"] == "flushCart"){
		   ?>
		   <ul>
			   <li class="dl">      
			       <?$APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket.small",
						"topCart",
						Array(),
						false
					);?>
				</li>
				<li class="dl">
			       <?$APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket.small",
						"footCart",
						Array(),
						false
					);?>				
				</li>
			</ul><?
		}elseif($_GET["act"] == "rating"){
			global $USER;
			if ($USER->IsAuthorized()){
				if(!empty($_GET["id"])){
					$arUsers[] = $USER->GetID();
					$res = CIBlockElement::GetList(Array(), Array("ID" => intval($_GET["id"]), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y"), false, false, Array("ID", "IBLOCK_ID", "PROPERTY_USER_ID", "PROPERTY_GOOD_REVIEW", "PROPERTY_BAD_REVIEW"));
					while($ob = $res->GetNextElement()){ 
						$arFields = $ob->GetFields();  
						if($arFields["PROPERTY_USER_ID_VALUE"] == $arUsers[0]){
							$result = array(
								"result" => false,
								"error" => "Вы уже голосовали!",
								"heading" => "Ошибка"
							);
							break;
						}
					}
					if(!$result){
						$propCODE = $_GET["trig"] ? "GOOD_REVIEW" : "BAD_REVIEW";
						$propVALUE = $_GET["trig"] ? $arFields["PROPERTY_GOOD_REVIEW_VALUE"] + 1 : $arFields["PROPERTY_BAD_REVIEW_VALUE"] + 1;
						$db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"], array("sort" => "asc"), Array("CODE" => "USER_ID"));
						if($arProps = $db_props->Fetch()){
							$arUsers[] = $arProps["VALUE"];
						}
						CIBlockElement::SetPropertyValuesEx($arFields["ID"], $arFields["IBLOCK_ID"], array($propCODE => $propVALUE, "USER_ID" => $arUsers));
						$result = array(
							"result" => true
						);
					}
				}else{
					$result = array(
						"result" => false,
						"error" => "Элемент не найден",
						"heading" => "Ошибка"
					);
				}
			}
			else{
				$result = array(
					"error" => "Для голосования вам необходимо авторизаваться",
					"result" => false,
					"heading" => "Ошибка"
				);
			}
			echo jsonEn($result);
		
		}elseif($_GET["act"] == "newReview"){
			global $USER;
			if ($USER->IsAuthorized()){
				if(!empty($_GET["DIGNITY"])      && 
				   !empty($_GET["SHORTCOMINGS"]) && 
				   !empty($_GET["COMMENT"])      && 
				   !empty($_GET["NAME"])         && 
				   !empty($_GET["USED"])         && 
				   !empty($_GET["RATING"])       && 
				   !empty($_GET["PRODUCT_NAME"]) && 
				   !empty($_GET["PRODUCT_ID"])
				  ){
					$arUsers = array($USER->GetID());
					$res = CIBlockElement::GetList(
						Array(), 
						Array(
							"ID" => intval($_GET["PRODUCT_ID"]),
							"ACTIVE_DATE" => "Y",
							"ACTIVE" => "Y"
						), 
						false, 
						false, 
						Array(
							"ID", 
							"IBLOCK_ID", 
							"PROPERTY_USER_ID", 
							"PROPERTY_VOTE_SUM", 
							"PROPERTY_VOTE_COUNT"
						)
					);
					while($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields();
						if($arFields["PROPERTY_USER_ID_VALUE"] == $arUsers[0]){
							$result = array(
								"heading" => "Ошибка",
								"message" => "Вы уже оставляли отзыв к этому товару."
							);
							break;
						}
						$arUsers[] = $arFields["PROPERTY_USER_ID_VALUE"];
					}
					if(empty($result)){
						$newElement = new CIBlockElement;

						// DIGNITY - достоинства
						// SHORTCOMINGS - недостатки
						// RATING - рейтинг
						// EXPERIENCE - опыт использования
						// NAME - Имя

						$PROP = array(
							"DIGNITY" => iconv("UTF-8","windows-1251//IGNORE", htmlspecialchars($_GET["DIGNITY"])),
							"SHORTCOMINGS" => iconv("UTF-8","windows-1251//IGNORE", htmlspecialchars($_GET["SHORTCOMINGS"])),
							"NAME" => iconv("UTF-8","windows-1251//IGNORE", htmlspecialchars($_GET["NAME"])),
							"EXPERIENCE" => intval($_GET["USED"]),
							"RATING" => intval($_GET["RATING"])
						);

						$arLoadProductArray = Array(
							"MODIFIED_BY"    => $USER->GetID(),
							"IBLOCK_SECTION_ID" => false,
							"IBLOCK_ID"      => intval($_GET["iblock_id"]),
							"PROPERTY_VALUES"=> $PROP,
							"NAME"           => iconv("UTF-8","windows-1251//IGNORE", htmlspecialchars($_GET["PRODUCT_NAME"])),
							"ACTIVE"         => "N",
							"DETAIL_TEXT"    => iconv("UTF-8","windows-1251//IGNORE", htmlspecialchars($_GET["COMMENT"])),
							"CODE"           => intval($_GET["PRODUCT_ID"])
						);

						if($PRODUCT_ID = $newElement->Add($arLoadProductArray)){
							$result = array(
								"heading" => "Отзыв добавлен",
								"message" => "Ваш отзыв будет опубликован после модерации.",
								"reload" => true
							);

							$VOTE_SUM   = $arFields["PROPERTY_VOTE_SUM_VALUE"] + intval($_GET["RATING"]);
							$VOTE_COUNT = $arFields["PROPERTY_VOTE_COUNT_VALUE"] + 1;
							$RATING = ($VOTE_SUM / $VOTE_COUNT);

							CIBlockElement::SetPropertyValuesEx(
								intval($_GET["PRODUCT_ID"]),
								$arFields["IBLOCK_ID"], 
								array(
									"VOTE_SUM" => $VOTE_SUM,
									"VOTE_COUNT" => $VOTE_COUNT,
									"RATING" => $RATING,
									"USER_ID" => $arUsers
								)
							);

						}
						else{
							$result = array(
								"heading" => "Ошибка",
								"message" => "error(1)"
							);
						}
					}
				}else{
					$result = array(
						"heading" => "Ошибка",
						"message" => "Заполните все поля!"
					);
				}
			}else{
				$result = array(
					"heading" => "Ошибка",
					"message" => "Ошибка авторизации"
				);
			}

			echo jsonEn($result);

		}elseif($_GET["act"] === "fastBack"){
			
			if(!empty($_GET["phone"]) && !empty($_GET["id"])){

				if(CModule::IncludeModule("iblock")){
					$arElement = CIBlockElement::GetByID(intval($_GET["id"]))->GetNext();
					if(!empty($arElement)){
						
						$dbPrice = CPrice::GetList(
					        array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"),
					        array("PRODUCT_ID" => $arElement["ID"]),
					        false,
					        false,
					        array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
						);
						
						while ($arPrice = $dbPrice->Fetch()){
							
							$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
								$arPrice["ID"],
								$USER->GetUserGroupArray(),
								"N",
								SITE_ID
							);
							
							$arElement["PRICE"] = CCatalogProduct::CountPriceWithDiscount(
								$arPrice["PRICE"],
								$arPrice["CURRENCY"],
								$arDiscounts
							);
						
							$arElement["PRICE"] = CurrencyFormat($arElement["PRICE"], $arPrice["CURRENCY"]);
						
						}

						$postMess = CEventMessage::GetList($by = "site_id", $order = "desc", array("TYPE" => "SALE_ELECTRO_FASTBACK_SEND"))->GetNext();

						if(empty($postMess)){
							
							$MESSAGE = "<h3>С сайта #SITE# поступил новый заказ в 1 клик. </h3> <p> Товар: <b>#PRODUCT#</b>  <br /> Имя: <b>#NAME#</b> <br /> Телефон: <b>#PHONE#</b> <br /> Комментарий: #COMMENT#";
							$FIELDS = "#SITE# \n #PRODUCT# \n #NAME# \n #PHONE# \n #COMMENT# \n";

							$et = new CEventType;
						    $et->Add(
						    	array(
							        "LID"           => "ru",
							        "EVENT_NAME"    => "SALE_ELECTRO_FASTBACK_SEND",
							        "NAME"          => "Купить в один клик",
							        "DESCRIPTION"   => $FIELDS
						        )
						    );

							$arr["ACTIVE"] = "Y";
							$arr["EVENT_NAME"] = "SALE_ELECTRO_FASTBACK_SEND";
							$arr["LID"] = SITE_ID;
							$arr["EMAIL_FROM"] = COption::GetOptionString('main', 'email_from', 'webmaster@webmaster.com');
							$arr["EMAIL_TO"] = COption::GetOptionString("sale", "order_email");
							$arr["BCC"] = COption::GetOptionString('main', 'email_from', 'webmaster@webmaster.com');
							$arr["SUBJECT"] = "Покупка товара в один клик";
							$arr["BODY_TYPE"] = "html";
							$arr["MESSAGE"] = $MESSAGE;

							$emess = new CEventMessage;
							$emess->Add($arr);

						}						

						$arMessage = array(
							"SITE" => SITE_SERVER_NAME,
							"PRODUCT" => $arElement["NAME"]." (ID:".$arElement["ID"]." )"." - ".$arElement["PRICE"],
							"NAME" => iconv("UTF-8","windows-1251//IGNORE", htmlspecialcharsbx($_GET["name"])),
							"PHONE" => iconv("UTF-8","windows-1251//IGNORE", htmlspecialcharsbx($_GET["phone"])),
							"COMMENT" => iconv("UTF-8","windows-1251//IGNORE", htmlspecialcharsbx($_GET["message"]))
						);

						CEvent::SendImmediate("SALE_ELECTRO_FASTBACK_SEND", SITE_ID, $arMessage, "Y", false);


						$result = array(
							"heading" => "Ваш заказ успешно отправлен",
							"message" => "В ближайшее время Вам перезвонит наш менеджер для уточнения деталей заказа.",
							"success" => true
						);
					}else{

						$result = array(
							"heading" => "Ошибка",
							"message" => "Ошибка, товар не найден!",
							"success" => false
						);

					}

				}

			}else{
				$result = array(
					"heading" => "Ошибка",
					"message" => "Ошибка, заполните обязательные поля!",
					"success" => false
				);
			}
			
			echo jsonEn($result);

		}
	}
	else{
		die(false);
	}
}

function priceFormat($data, $str = ""){
	$price = explode(".", $data);
	$strLen = strlen($price[0]);
	for ($i = $strLen; $i > 0 ; $i--) { 
		$str .=	(!($i%3) ? " " : "").$price[0][$strLen - $i];
	}
	return $str.($price[1] > 0 ? ".".$price[1] : "");
}

function jsonEn($data){
	foreach ($data as $index => $arValue) {
		$arJsn[] = '"'.$index.'" : "'.addslashes($arValue).'"';
	}
	return  "{".implode($arJsn, ",")."}";
}

?>