<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<h1><?=GetMessage("HEADING")?></h1>
<?if(!empty($arResult["ITEMS"])):?>
<div id="newsList">
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?>
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<?$detailPicture = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 250, "height" => 180));?>
			<table class="item">
				<tbody>
					<tr>
						<?if(!empty($detailPicture["src"])):?>
						<td class="left">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
								<img src="<?=$detailPicture["src"]?>" title="<?=$arItem["NAME"]?>" alt="<?=$arItem["NAME"]?>">
							</a>
						</td>
						<?endif;?>
						<td class="right"<?if(empty($detailPicture["src"])):?> colspan=2<?endif;?>>
							<ins><?=substr($arItem["DATE_CREATE"], 0, strpos($arItem["DATE_CREATE"], " "))?></ins>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="name"><?=$arItem["NAME"]?></a>
							<p><?=$arItem["PREVIEW_TEXT"]?></p>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="more"><?=GetMessage("DETAIL")?></a>
						</td>
					</tr>
				</tbody>
			</table>				
		<?endforeach;?>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
<?else:?>
<div id="empty">
  <img src="<?=SITE_TEMPLATE_PATH?>/images/emptyFolder.png" alt="<?=GetMessage("EMPTY_HEADING")?>" class="emptyImg">
  <div class="info">
    <h3><?=GetMessage("EMPTY_HEADING")?></h3>
    <p>
      <?=GetMessage("EMPTY_TEXT")?>
    </p>
    <a href="<?=SITE_DIR?>" class="back"><?=GetMessage("MAIN_PAGE")?></a>
  </div>
</div>	
<?endif;?>
