<?
	$MESS["MAGAZINE"] = "Интернет-магазин";
	$MESS["ABOUT"] = "О компании";
	$MESS["SERVICE"] = "Услуги и сервисы";
	$MESS["SMM"] = "Мы в cоциальных сетях";
	$MESS["ADD_CART"] = "В корзину";
	$MESS["CART_ADDED"] = "В корзине";
	$MESS["COMP_ADDED"] = " В списке сравнения";
	$MESS["LOADING"] = "Загрузка";
	$MESS["NOAVAILABLE"] = "Под заказ";
	$MESS["AVAILABLE"] = "В наличии";
	$MESS["ALL_RESULT"] = "Все результаты";
	$MESS["ORDER"] = "Оформление";
	$MESS["HIDE_ALL"] = "Скрыть";
	$MESS["SHOW_ALL"] = "Показать все";
?>