<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<h1><?=GetMessage("HEADING");?></h1>
<?if(!empty($arResult["ITEMS"])):?>
<div id="brandList">
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?>
		<ul>
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?$picture = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], Array("width" => 250, "height" => 180));?>
				<li><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?if(!empty($picture["src"])):?><img src="<?=$picture["src"]?>"><?else:?><span><?=$arItem["NAME"]?></span><?endif;?></a></li>
			<?endforeach;?>
		</ul>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
<?else:?>
<div id="empty">
  <img src="<?=SITE_TEMPLATE_PATH?>/images/emptyFolder.png" alt="<?=GetMessage("EMPTY_HEADING");?>" class="emptyImg">
  <div class="info">
    <h3><?=GetMessage("EMPTY_HEADING");?></h3>
    <p>
      <?=GetMessage("EMPTY_TEXT");?>
    </p>
    <a href="<?=SITE_DIR?>" class="back"><?=GetMessage("MAIN_PAGE");?></a>
  </div>
</div>	
<?endif;?>
