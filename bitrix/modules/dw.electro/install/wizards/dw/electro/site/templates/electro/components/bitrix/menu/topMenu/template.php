<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if (!empty($arResult)):?>
<ul id="topMenu">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li><a href="<?=$arItem["LINK"]?>" class="selected<?=$arItem["LINK"] == "/" ? ' home' : ''?>"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
	<li><a href="<?=$arItem["LINK"]?>"<?=$arItem["LINK"] == "/" ? ' class="home"' : ''?>><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>

</ul>
<?endif?>