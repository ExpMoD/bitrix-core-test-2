<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if (!empty($arResult)):?>
<script>
    $(window).bind('load', function(){
        $("#showCarousel").electroCarousel({
            speed: 500,
            countElement: 4,
            leftButton: ".showLeft",
            rightButton: ".showRight",
            resizeElement : true
        });
    });
</script>
<div id="showCarousel">
	<span class="heading"><?=GetMessage("HEADING_PROFILE")?></span>
	<ul class="productList">
		<?foreach($arResult as $arElement):?>
		<li class="product">
			<div class="wrap">
				 <?=(!empty($arElement["PROPERTIES"]["MARKER"]["VALUE"]) ? '<ins class="marker">'.$arElement["PROPERTIES"]["MARKER"]["VALUE"].'</ins>' : '')?>
				<span class="rating"><i class="m" style="width:<?=($arElement["PROPERTIES"]["RATING"]["VALUE"] * 100 / 5)?>%"></i><i class="h"></i></span>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="pic" target="_blank">
					<img src="<?=$arElement["PICTURE"]["src"]?>" alt="<?=$arElement["NAME"]?>">
				</a>
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="name" target="_blank" title="<?=$arElement["NAME"]?>"><?=$arElement["NAME"]?></a>
				<span class="price">
					 <?=str_replace(GetMessage("RUB"),'<span class="rouble">P<i>-</i></span>', $arElement["PRICE_FORMATED"]);?>
				</span>
				<a href="#" class="addCart" data-id="<?=$arElement["PRODUCT_ID"]?>"><?=GetMessage("ADDCART")?></a>
			</div>
		</li>
		<?endforeach;?>
	</ul>
	 <a href="#" class="showLeft"></a>
	 <a href="#" class="showRight"></a>
</div>
<?endif;?>