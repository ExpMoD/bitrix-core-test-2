 <div id="inBasket" data-load="<?=SITE_TEMPLATE_PATH?>/images/picLoad.gif">
    <div id="inBasketContainer">
        <span class="heading">Товар добавлен в корзину.</span>
        <a href="#" id="inBasketClose"></a>
        <table>
            <tbody>
            <tr>
                <td><a href="" class="basketIMG"><img src="" alt="Изображение товара"></a></td>
                <td>
                    <span class="basketNAME"></span>
                    <table id="inBasketToolsTable">
                        <tr>
                            <td>Кол-во:  
                            <div id="inBasketQty">
                                <button class="minus"></button>
                                <input name="qty" type="text" value="1" class="qty"/>
                                <button class="plus"></button>
                            </div>
                        </td>
                            <td>Стоимость: <span id="inBasketPrice"></span> <span class="rouble">Р<i>-</i></span></td>
                            <td><a href="#" id="inBasketDelete"></a></td>
                        </tr>
                    </table>
                    <div id="inBasketTotal">Итого:&nbsp;<span class="all"></span> <span class="rouble">Р<i>-</i></span></div>
                    <div id="inBasketOrder">
                        <a href="#" class="inBasketContinue">Продолжить покупки</a>
                        <a href="<?=SITE_DIR?>personal/cart/" class="inBasketMake">Перейти в корзину</a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>	