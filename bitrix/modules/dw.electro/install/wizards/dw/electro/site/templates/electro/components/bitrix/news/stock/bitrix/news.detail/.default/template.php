<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if(!empty($arResult)):?>
	<div id="newsDetail">
		<h1><?=$arResult["NAME"]?></h1>
		<ins><?=substr($arResult["DATE_CREATE"], 0, strpos($arResult["DATE_CREATE"], " "))?></ins>
		<p><?=$arResult["DETAIL_TEXT"]?></p>
	</div>
<?endif;?>