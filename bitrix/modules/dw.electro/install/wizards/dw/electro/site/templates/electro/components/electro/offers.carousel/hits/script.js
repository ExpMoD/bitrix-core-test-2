$(window).on("ready", function(){
	$("#hitsCarousel").electroCarousel({
		speed: 500,
		leftButton: ".hitsBtnLeft",
		rightButton: ".hitsBtnRight",
		resizeElement: true
	});
});
