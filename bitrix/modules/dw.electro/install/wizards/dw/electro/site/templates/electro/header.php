<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html>
	<head>
		<?$APPLICATION->ShowHead();?>
		<meta name="viewport" content="width=1366px">
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/images/favicon.ico" /> 
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/font/bender/bender.css");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-1.11.0.min.js");?>
  		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/system.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/electroSlider.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/rotate.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/headerMenu.js");?>
  		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/percent.js");?>
  		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/electroSelect.js");?>
  		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/electroCheckBox.js");?>
  		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/electroCarousel.js");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/rangeSlider.js");?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/leftMenu.js");?>
		<script type="text/javascript">
			var ajaxPath = "<?=SITE_DIR?>ajax.php";
			var SITE_DIR = "<?=SITE_DIR?>";
			var SITE_ID  = "<?=SITE_ID?>";
		</script>
	</head>
	<body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
     	<div id="headerLine">
     		<div class="wrapper">
             	<div id="headerLineLeft">
             	 	<?$APPLICATION->IncludeComponent(
						"bitrix:sale.basket.basket.small",
						"topCart",
						array(
							"PATH_TO_BASKET" => SITE_DIR."personal/cart/",
							"PATH_TO_ORDER" => SITE_DIR."personal/order/",
							"SHOW_DELAY" => "Y",
							"SHOW_NOTAVAIL" => "Y",
							"SHOW_SUBSCRIBE" => "Y"
						),
						false
					);?>
				</div>
             	<div id="headerLineRight">
                	<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"topMenu",
						array(
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "top",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
						),
						false
					);?>
             	</div>
     		</div>
     	</div>
     	<div id="header">
     		<div class="wrapper">
	     		<div id="headerLeft">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						".default",
						array(
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "logo",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE" => ""
						),
						false
					);?>
	     		</div>
				<?$APPLICATION->IncludeComponent(
					"electro:search.line",
					".default",
					array(
						"CACHE_TYPE" => "Y",
						"CACHE_TIME" => "3600000",
						"PROP_NAME" => "",
						"IBLOCK_TYPE" => "catalog",
						"IBLOCK_ID" => "8"
					),
					false
				);?>
	     		<div id="headerRight">
		     		<table>
		     			<tbody>
		     				<tr>
		     					<td>
			     					<span class="telephone">
				     					<?$APPLICATION->IncludeComponent(
											"bitrix:main.include",
											".default",
											array(
												"AREA_FILE_SHOW" => "sect",
												"AREA_FILE_SUFFIX" => "telephone",
												"AREA_FILE_RECURSIVE" => "Y",
												"EDIT_TEMPLATE" => ""
											),
											false
										);?>
									</span>
								</td>
		     					<td>
			     					<span class="telephone">
				     					<?$APPLICATION->IncludeComponent(
											"bitrix:main.include",
											".default",
											array(
												"AREA_FILE_SHOW" => "sect",
												"AREA_FILE_SUFFIX" => "telephone2",
												"AREA_FILE_RECURSIVE" => "Y",
												"EDIT_TEMPLATE" => ""
											),
											false
										);?>
									</span>
								</td>
		     				</tr>
		     				<tr>
		     					<td>
			     					<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "top", Array(
										"REGISTER_URL" => "",
											"FORGOT_PASSWORD_URL" => "",
											"PROFILE_URL" => "",
											"SHOW_ERRORS" => "N"
										),
										false
									);?>
								</td>
		     					<td><a href="<?=SITE_DIR?>callback/" class="oCallback"><?=GetMessage("CALLBACK")?></a></td>
		     				</tr>
		     			</tbody>
		     		</table>
	     		</div>
	     	</div>
     	</div>
     	<div id="mainContent">
	            	<div id="mainContentleft">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "inc",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_TEMPLATE" => ""
							),
							false
						);?>
	            	</div>
	            	<div id="mainContentRight">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "cart",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_TEMPLATE" => ""
							),
							false
						);?>
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							array(
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "comp",
								"AREA_FILE_RECURSIVE" => "Y",
								"EDIT_TEMPLATE" => ""
							),
							false
						);?>
	            		<?if (INDEX_PAGE === "Y"):?>
	      				<div id="advBlock">

							<?$APPLICATION->IncludeComponent(
								"electro:slider",
								".default",
								array(
									"IBLOCK_TYPE" => "slider",
									"IBLOCK_ID" => "3",
									"CACHE_TYPE" => "Y",
									"CACHE_TIME" => "3600000",
									"PICTURE_WIDTH" => "950",
									"PICTURE_HEIGHT" => "380"
								),
								false
							);?>
									<div id="advRight">
									<ul>
										<li>
											<?$APPLICATION->IncludeComponent(
												"bitrix:main.include",
												".default",
												array(
													"AREA_FILE_SHOW" => "sect",
													"AREA_FILE_SUFFIX" => "banner1",
													"AREA_FILE_RECURSIVE" => "Y",
													"EDIT_TEMPLATE" => ""
												),
												false
											);?>
										</li>
										<li>
											<?$APPLICATION->IncludeComponent(
												"bitrix:main.include",
												".default",
												array(
													"AREA_FILE_SHOW" => "sect",
													"AREA_FILE_SUFFIX" => "banner2",
													"AREA_FILE_RECURSIVE" => "Y",
													"EDIT_TEMPLATE" => ""
												),
												false
											);?>
										</li>
									</ul>
								</div>
								<div class="clear"></div>
							</div>

                 			<?$APPLICATION->IncludeComponent("electro:offers.carousel", "hits", array(
								"CACHE_TYPE" => "Y",
									"CACHE_TIME" => "3600000",
									"PROP_NAME" => "POPULAR",
									"IBLOCK_TYPE" => "catalog",
									"IBLOCK_ID" => "8",
									"PICTURE_WIDTH" => "200",
									"PICTURE_HEIGHT" => "140"
								),
								false,
								array(
								"ACTIVE_COMPONENT" => "Y"
								)
							);?>

							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								".default",
								array(
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "banner3",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => ""
								),
								false
							);?>

	             			<?$APPLICATION->IncludeComponent(
								"electro:offers.carousel",
								"sale",
								array(
									"CACHE_TYPE" => "Y",
									"CACHE_TIME" => "3600000",
									"PROP_NAME" => "SALE",
									"IBLOCK_TYPE" => "catalog",
									"IBLOCK_ID" => "8",
									"PICTURE_WIDTH" => "200",
									"PICTURE_HEIGHT" => "140"
								),
								false
							);?>


							<?$APPLICATION->IncludeComponent(
								"electro:brands.carousel", 
								"main", 
								array(
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "360000",
									"IBLOCK_TYPE" => "brand_desc",
									"IBLOCK_ID" => "1",
									"PICTURE_WIDTH" => "200",
									"PICTURE_HEIGHT" => "140"
								),
								false
							);?>


                 			<?$APPLICATION->IncludeComponent(
								"electro:offers.carousel",
								"new",
								array(
									"CACHE_TYPE" => "Y",
									"CACHE_TIME" => "3600000",
									"PROP_NAME" => "NEW",
									"IBLOCK_TYPE" => "catalog",
									"IBLOCK_ID" => "8",
									"PICTURE_WIDTH" => "200",
									"PICTURE_HEIGHT" => "140"
								),
								false
							);?>

							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								".default",
								array(
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "banner4",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => ""
								),
								false
							);?>

		            		<div id="ancillary">
		            			<ul>
									<li>
										<?$APPLICATION->IncludeComponent(
											"bitrix:news.list", 
											"news", 
											array(
												"IBLOCK_TYPE" => "news",
												"IBLOCK_ID" => "7",
												"NEWS_COUNT" => "3",
												"SORT_BY1" => "TIMESTAMP_X",
												"SORT_ORDER1" => "DESC",
												"SORT_BY2" => "SORT",
												"SORT_ORDER2" => "ASC",
												"FILTER_NAME" => "",
												"FIELD_CODE" => array(
													0 => "",
													1 => "",
												),
												"PROPERTY_CODE" => array(
													0 => "",
													1 => "",
												),
												"CHECK_DATES" => "Y",
												"DETAIL_URL" => "",
												"AJAX_MODE" => "N",
												"AJAX_OPTION_JUMP" => "N",
												"AJAX_OPTION_STYLE" => "Y",
												"AJAX_OPTION_HISTORY" => "N",
												"CACHE_TYPE" => "A",
												"CACHE_TIME" => "36000000",
												"CACHE_FILTER" => "N",
												"CACHE_GROUPS" => "Y",
												"PREVIEW_TRUNCATE_LEN" => "",
												"ACTIVE_DATE_FORMAT" => "d.m.Y",
												"SET_STATUS_404" => "N",
												"SET_TITLE" => "Y",
												"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
												"ADD_SECTIONS_CHAIN" => "Y",
												"HIDE_LINK_WHEN_NO_DETAIL" => "N",
												"PARENT_SECTION" => "",
												"PARENT_SECTION_CODE" => "",
												"INCLUDE_SUBSECTIONS" => "Y",
												"PAGER_TEMPLATE" => ".default",
												"DISPLAY_TOP_PAGER" => "N",
												"DISPLAY_BOTTOM_PAGER" => "Y",
												"PAGER_TITLE" => "�������",
												"PAGER_SHOW_ALWAYS" => "Y",
												"PAGER_DESC_NUMBERING" => "N",
												"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
												"PAGER_SHOW_ALL" => "Y",
												"DISPLAY_DATE" => "Y",
												"DISPLAY_NAME" => "Y",
												"DISPLAY_PICTURE" => "Y",
												"DISPLAY_PREVIEW_TEXT" => "Y",
												"AJAX_OPTION_ADDITIONAL" => ""
											),
											false
										);?>
									</li>
										<li>
											<?$APPLICATION->IncludeComponent(
												"bitrix:news.list",
												"stock",
												array(
													"IBLOCK_TYPE" => "news",
													"IBLOCK_ID" => "6",
													"NEWS_COUNT" => "3",
													"SORT_BY1" => "TIMESTAMP_X",
													"SORT_ORDER1" => "DESC",
													"SORT_BY2" => "SORT",
													"SORT_ORDER2" => "ASC",
													"FILTER_NAME" => "",
													"FIELD_CODE" => array(
														0 => "",
														1 => "",
													),
													"PROPERTY_CODE" => array(
														0 => "",
														1 => "",
													),
													"CHECK_DATES" => "Y",
													"DETAIL_URL" => "",
													"AJAX_MODE" => "N",
													"AJAX_OPTION_JUMP" => "N",
													"AJAX_OPTION_STYLE" => "Y",
													"AJAX_OPTION_HISTORY" => "N",
													"CACHE_TYPE" => "A",
													"CACHE_TIME" => "36000000",
													"CACHE_FILTER" => "N",
													"CACHE_GROUPS" => "Y",
													"PREVIEW_TRUNCATE_LEN" => "",
													"ACTIVE_DATE_FORMAT" => "d.m.Y",
													"SET_STATUS_404" => "N",
													"SET_TITLE" => "Y",
													"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
													"ADD_SECTIONS_CHAIN" => "Y",
													"HIDE_LINK_WHEN_NO_DETAIL" => "N",
													"PARENT_SECTION" => "",
													"PARENT_SECTION_CODE" => "",
													"INCLUDE_SUBSECTIONS" => "Y",
													"PAGER_TEMPLATE" => ".default",
													"DISPLAY_TOP_PAGER" => "N",
													"DISPLAY_BOTTOM_PAGER" => "Y",
													"PAGER_TITLE" => "�������",
													"PAGER_SHOW_ALWAYS" => "Y",
													"PAGER_DESC_NUMBERING" => "N",
													"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
													"PAGER_SHOW_ALL" => "Y",
													"DISPLAY_DATE" => "Y",
													"DISPLAY_NAME" => "Y",
													"DISPLAY_PICTURE" => "Y",
													"DISPLAY_PREVIEW_TEXT" => "Y",
													"AJAX_OPTION_ADDITIONAL" => ""
												),
												false
											);?>
									</li>
									<li>
										<?$APPLICATION->IncludeComponent(
											"bitrix:news.list",
											"survey",
											array(
												"IBLOCK_TYPE" => "news",
												"IBLOCK_ID" => "4",
												"NEWS_COUNT" => "3",
												"SORT_BY1" => "TIMESTAMP_X",
												"SORT_ORDER1" => "DESC",
												"SORT_BY2" => "SORT",
												"SORT_ORDER2" => "ASC",
												"FILTER_NAME" => "",
												"FIELD_CODE" => array(
													0 => "",
													1 => "",
												),
												"PROPERTY_CODE" => array(
													0 => "",
													1 => "",
												),
												"CHECK_DATES" => "Y",
												"DETAIL_URL" => "",
												"AJAX_MODE" => "N",
												"AJAX_OPTION_JUMP" => "N",
												"AJAX_OPTION_STYLE" => "Y",
												"AJAX_OPTION_HISTORY" => "N",
												"CACHE_TYPE" => "A",
												"CACHE_TIME" => "36000000",
												"CACHE_FILTER" => "N",
												"CACHE_GROUPS" => "Y",
												"PREVIEW_TRUNCATE_LEN" => "",
												"ACTIVE_DATE_FORMAT" => "d.m.Y",
												"SET_STATUS_404" => "N",
												"SET_TITLE" => "Y",
												"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
												"ADD_SECTIONS_CHAIN" => "Y",
												"HIDE_LINK_WHEN_NO_DETAIL" => "N",
												"PARENT_SECTION" => "",
												"PARENT_SECTION_CODE" => "",
												"INCLUDE_SUBSECTIONS" => "Y",
												"PAGER_TEMPLATE" => ".default",
												"DISPLAY_TOP_PAGER" => "N",
												"DISPLAY_BOTTOM_PAGER" => "Y",
												"PAGER_TITLE" => "�������",
												"PAGER_SHOW_ALWAYS" => "Y",
												"PAGER_DESC_NUMBERING" => "N",
												"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
												"PAGER_SHOW_ALL" => "Y",
												"DISPLAY_DATE" => "Y",
												"DISPLAY_NAME" => "Y",
												"DISPLAY_PICTURE" => "Y",
												"DISPLAY_PREVIEW_TEXT" => "Y",
												"AJAX_OPTION_ADDITIONAL" => ""
											),
											false
										);?>
									</li>
									<li>
										<?$APPLICATION->IncludeComponent(
											"bitrix:main.include",
											".default",
											array(
												"AREA_FILE_SHOW" => "sect",
												"AREA_FILE_SUFFIX" => "bottomMenu",
												"AREA_FILE_RECURSIVE" => "Y",
												"EDIT_TEMPLATE" => ""
											),
											false
										);?>
									</li>
		            			</ul>
		            			<div class="clear"></div>
		            		</div>
						<?else:?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:breadcrumb",
								"breadcrumb",
								array(
									"START_FROM" => "0",
									"PATH" => "",
									"SITE_ID" => "s1"
								),
								false
							);?>
						<?endif;?>