(function($) {
	jQuery.fn.electroCarousel = function(options) {

		var options = $.extend({
			resizeElement: false,
			countElement: 6,
			speed: 400,
			severity: 8
		}, options);

		var $main = new Main($(this));

		function Main(obj) {
			this.ul = obj.find("ul");
			this.li = this.ul.find("li");
			this.qtyLI = this.li.length;
			this.curPos = null;
			this.startTouch = false;
			this.startTouchPos = false;
			this.clicking = false;
			this.active = false;
		}

		// main functions
		var bindEvents = function(e) {
			if (e) {
				$(document).on("click", options.leftButton, {
					direction: "left"
				}, moveUL);
				$(document).on("click", options.rightButton, {
					direction: "right"
				}, moveUL);
				$(document).on("mouseup touchend", touchEnd);

				$main.ul.on("mousedown touchstart", touchStart);
				$main.ul.on("mousemove touchmove", touchMove);
				$main.ul.on("click", clickMove);
				$main.active = true;
			}
		}

		var resizeElements = function(count) {
			$main.ul.css({
				width: ($main.qtyLI * 100) + "%"
			});

			$main.li.css({
				width: (100 / $main.qtyLI / count) + "%"
			});
		};

		var calculateParams = function(windowSize) {
			var currentCount = options.countElement;

			if (options.resizeElement === true) {
				if (windowSize >= 1833) {
					options.countElement = 6;
				} else if (windowSize >= 1490 && windowSize <= 1832) {
					options.countElement = 5;
				} else if (windowSize >= 1151 && windowSize <= 1489) {
					options.countElement = 4;
				} else if (windowSize <= 1150) {
					options.countElement = 3;
				}
			}

			if (options.countElement < $main.qtyLI) {

				$(options.leftButton).show();
				$(options.rightButton).show();

				if (!$main.active) {
					$main.active = true;
					bindEvents(true);
				}

			} else {

				$(options.leftButton).hide();
				$(options.rightButton).hide();
				$main.ul.unbind();
				$main.active = false;

			}

			$main.ul.css("left", 0);

		};

		var moveUL = function(event) {
			var direction = event.data.direction == "left" ? "left" : "right",
				maxPos = $main.qtyLI - options.countElement,
				animateValue = null

			if (direction == "left") {
				if (!$main.curPos) {
					animateValue = "-" + (100 / options.countElement * maxPos) + "%";
					$main.curPos = maxPos;
				} else {
					animateValue = "-" + (100 / options.countElement * --$main.curPos) + "%";
				}
			} else {
				if ($main.curPos == maxPos) {
					$main.curPos = maxPos;
					$main.curPos = animateValue = 0;
				} else {
					animateValue = "-" + (100 / options.countElement * ++$main.curPos) + "%";
				}
			}

			$main.ul.finish().animate({
				"left": animateValue
			}, options.speed);

			event.preventDefault();
		};

		var touchStart = function(event) {
			$main.startTouch = event.type == "touchstart" ? event.originalEvent.touches[0].pageX : event.pageX;
			$main.startTouchPos = Math.abs(parseInt($main.ul.css("left"), 10));
			if(event.type !== "touchstart"){
				event.preventDefault();
			}
		};

		var touchMove = function(event) {
			if ($main.startTouch) {
				event.pageX = event.type == "touchmove" ? event.originalEvent.touches[0].pageX : event.pageX;
				var animateValue = (-$main.startTouchPos - ($main.startTouch - event.pageX));
				var maxPos = ($main.li.outerWidth() * $main.qtyLI) - (options.countElement * $main.li.outerWidth());

				if (animateValue > 0) {
					animateValue /= 8;
				} else if (maxPos < Math.abs(parseInt($main.ul.css("left")))) {
					animateValue = -(maxPos + ((Math.abs(animateValue) - maxPos) / 8));
				}

				$main.ul.stop().css({
					"left": animateValue
				});

				$main.clicking = true;
			}
		};

		var touchEnd = function(event) {
			if ($main.startTouch) {
				var maxPos = ($main.li.outerWidth() * $main.qtyLI) - (options.countElement * $main.li.outerWidth()),
					posNow = parseInt($main.ul.css("left")),
					animateValue = null;
				if (posNow > 0) {
					animateValue = 0;
				} else if (Math.abs($main.startTouchPos - Math.abs(posNow)) < 30) {
					animateValue = "-" + $main.startTouchPos;
				} else if (maxPos < Math.abs(posNow)) {
					animateValue = "-" + maxPos;
				} else {
					animateValue = "-" + (Math.abs(posNow) > $main.startTouchPos ? Math.ceil(Math.abs(posNow) / $main.li.outerWidth()) : Math.floor(Math.abs(posNow) / $main.li.outerWidth())) * $main.li.outerWidth();
				}

				$main.ul.finish().animate({
					"left": animateValue
				}, options.speed);

				$main.startTouch = false;
				$main.clicking = false;
			}
		};

		var clickMove = function(event) {
			if ($main.clicking) {
				$main.clicking = false;
				event.preventDefault();
			}
		};

		$(window).resize(function(e) {
			calculateParams($(window).width());
			resizeElements(options.countElement);
		});

		calculateParams($(window).width());
		resizeElements(options.countElement);
		bindEvents();

	};

})($);