(function($) {
	jQuery.fn.electroSlider = function(options) {
		var options = $.extend({
			speed: 500,
			severity: 8
		}, options);

		var link = this;
		var slideBox = link.find("ul");
		var slideElements = slideBox.find("li");
		var leftButton = $(options["leftButton"]);
		var rightButton = $(options["rightButton"]);
		var currentPosition = 0;
		var moveValue = 0;
		var moveValueFx = 0;
		var timeoutID = 0;

		slideBox.width(slideElements.length * 100 + "%");
		slideElements.width(100 / slideElements.length + "%");


		link.append(
			$("<ol>").append(
				function() {
					var str = "";
					for (var i = 1; i <= slideElements.length; i++) {
						if (i == 1) {
							str = str + '<li class="selected"></li>';
						} else {
							str = str + '<li></li>';
						}
					}
					return str;
				}
			)
		);

		var slideDotBox = link.find("ol");
		var slideDotElements = slideDotBox.find("li");
		slideDotBox.css("margin-left", "-" + (slideElements.length * 14 / 2) + "px");

		var slideAuto = function() {
			slideMove(false);

			timeoutID = setTimeout(function() {
				slideAuto();
			}, 3500);
		}

		var slideMove = function(left) {

			if (left == true) {
				if (-1 == --currentPosition) {
					currentPosition = slideElements.length - 1;
				}
				moveValue = "-" + currentPosition * 100 + "%";
			} else {
				if (slideElements.length == ++currentPosition) {
					currentPosition = 0;
					moveValue = 0;
				} else {
					moveValue = "-" + (currentPosition * 100) + "%"
				}
			}
			slideAnimate(moveValue, moveValueFx);
		}

		var slideAnimate = function(value, fxValue) {
			slideBox.finish().animate({
				"left": value
			}, 350, function() {
				slideDotElements.removeAttr("class").eq(currentPosition).addClass("selected");
			});
		}

		leftButton.on("click", function(e) {
			clearTimeout(timeoutID);
			slideMove(true);
			e.preventDefault();
		});

		rightButton.on("click", function(e) {
			clearTimeout(timeoutID);
			slideMove(false);
			e.preventDefault();
		});

		slideDotElements.on("click", function(e) {
			clearTimeout(timeoutID);
			currentPosition = $(this).index() - 1;
			slideMove(false);
		});

		timeoutID = setTimeout(function() {
			slideAuto();
		}, 3500);

	};
})(jQuery);