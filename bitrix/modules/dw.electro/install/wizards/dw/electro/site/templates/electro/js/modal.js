(function($) {
	$(document).on('click', '.activeWindow', function(e) {
		e.preventDefault();
		$("#" + $(this).data("id")).fadeToggle("fast");
	});
})($);