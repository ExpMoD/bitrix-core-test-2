<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$allPrice = 0;
$allQuantity = 0;
if(!empty($arResult["ITEMS"])){
	foreach ($arResult["ITEMS"] as $index => $arValue) {
		$allPrice += ($arValue["PRICE"] * $arValue["QUANTITY"]);
		$allQuantity += intval($arValue["QUANTITY"]);
	}
	$allPrice = round($allPrice);
}
if(!function_exists('priceFormat')){
		function priceFormat($data, $str = ""){
		$price = explode(".", $data);
		$strLen = strlen($price[0]);
		for ($i = $strLen; $i > 0 ; $i--) { 
			$str .=	(!($i%3) ? " " : "").$price[0][$strLen - $i];
		}
		return $str.($price[1] > 0 ? ".".$price[1] : "");
	}
}
?>

<ul id="topCart">
	<li><a href="<?=SITE_DIR?>personal/cart/"><img src="<?=SITE_TEMPLATE_PATH?>/images/cartElement1.png" alt="<?=GetMessage("CART");?>"> <?=GetMessage("CART");?> <span<?=$allQuantity ? ' class="full"' : false?> id="topQty"><?=$allQuantity?></span></a></li>
	<li><img src="<?=SITE_TEMPLATE_PATH?>/images/cartElement2.png" alt="<?=GetMessage("SUM");?>"><?=GetMessage("SUM");?> <span<?=$allQuantity ? ' class="full"' : false?> id="topSum"><?=priceFormat($allPrice)?></span></li>
	<?if($allQuantity):?><li><a href="<?=SITE_DIR?>personal/cart/#order" class="order"><img src="<?=SITE_TEMPLATE_PATH?>/images/cartElement3.png" alt="<?=GetMessage("ORDER");?>"><?=GetMessage("ORDER");?></a></li><?endif;?>
</ul>