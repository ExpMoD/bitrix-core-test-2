  $(window).bind('load', function() {



    var elements = $(".electroCheck");
    var elementsChecked = $(".electroCheck:checked");
    var elementsDisabled = $(".electroCheck:disabled");

    //set styles


    $(elements).each(function(eIndex) {
      var element = $(this);

      if (!element.is(":checked") && !element.is(":disabled")) {
        element.css("opacity", "0");
        element.parent().html('<div style="position:relative;"><div class="' + element.data("class") + '" style="width:' + element.outerWidth() + 'px;height:' + element.outerHeight() + 'px;"></div> ' + element.parent().html() + ' </div>'); //add style div
      }

    });

    //if checked

    $(elementsDisabled).each(function(eIndex) {

      var element = $(this);

      element.css("opacity", "0");
      element.parent().html('<div style="position:relative;"><div class="' + element.data("class") + '_disabled" style="width:' + element.outerWidth() + 'px;height:' + element.outerHeight() + 'px;"></div> ' + element.parent().html() + ' </div>'); //add style div

    });

    $(elementsChecked).each(function(eIndex) {

      var element = $(this);

      element.css("opacity", "0");
      element.parent().html('<div style="position:relative;"><div class="' + element.data("class") + '_active" style="width:' + element.outerWidth() + 'px;height:' + element.outerHeight() + 'px;"></div> ' + element.parent().html() + ' </div>'); //add style div

    });


    //set active

    $("div[class^='electroCheck_']").click(function(event) {

      var activeElement = $(this);
      var selectElement = activeElement.siblings(".electroCheck");

      if (!selectElement.is(":disabled")) {

        if (selectElement.is(':checked')) {
          var activeClassName = activeElement.attr('class');
          var passiveClassName = activeElement.attr('class').replace('_active', '');

          activeElement.addClass(passiveClassName).removeClass(activeClassName);
          selectElement.prop('checked', false).removeAttr("checked").trigger("change");;

        } else {
          var passiveClassName = activeElement.attr('class');
          var activeClassName = activeElement.attr('class') + "_active";

          activeElement.addClass(activeClassName).removeClass(passiveClassName);
          selectElement.prop('checked', true).attr("checked", "checked").trigger("change");

        }
      }

    });

    //

    $("label").click(function(e) {
      $this = $(this);
      $element = $("#" + $this.attr("for"));
      $activeElement = $this.siblings("div[class^='electroCheck_']");
      if ($element.attr("class") == "electroCheck") {
        if (!$element.is(":disabled")) {
          if ($element.is(':checked')) {
            var activeClassName = $activeElement.attr('class');
            var passiveClassName = $activeElement.attr('class').replace('_active', '');

            $activeElement.addClass(passiveClassName).removeClass(activeClassName);
            $element.prop('checked', false).removeAttr("checked").trigger("change");;

          } else {
            var passiveClassName = $activeElement.attr('class');
            var activeClassName = $activeElement.attr('class') + "_active";

            $activeElement.addClass(activeClassName).removeClass(passiveClassName);
            $element.prop('checked', true).attr("checked", "checked").trigger("change");

          }
        }
        e.preventDefault();
      }
    });

  });