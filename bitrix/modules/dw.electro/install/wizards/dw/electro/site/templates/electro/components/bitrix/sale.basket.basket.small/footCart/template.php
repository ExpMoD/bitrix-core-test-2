<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$allQuantity = 0;
if(!empty($arResult["ITEMS"])){
	foreach ($arResult["ITEMS"] as $index => $arValue) {
		$allQuantity += intval($arValue["QUANTITY"]);
	}
	$allPrice = explode(".", $allPrice);
}
?>
<table>
	<tr>
		<td><a href="<?=SITE_DIR?>personal/cart/" class="footerCart<?=!$allQuantity ? " empty" : false?>"><?=GetMessage("CART")?></a><span id="footCart"><?=$allQuantity?></span></td>
		<td><a href="<?=SITE_DIR?>compare/" class="footerCompare<?=!count($_SESSION["COMPARE_LIST"]["ITEMS"]) ? " empty" : false?>"><?=GetMessage("COMPARE")?></a><span id="footComp"><?=count($_SESSION["COMPARE_LIST"]["ITEMS"])?></span></td>
		<td><a href="<?=SITE_DIR?>callback/" class="oCallback"><?=GetMessage("CALLBACK")?></a></td>
	</tr>
</table>



