<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["FORM_TYPE"] == "login"):?>
<a href="<?=SITE_DIR?>auth/?backurl=<?=$APPLICATION->GetCurPageParam();?>"><?=GetMessage("LOGIN")?></a> | <a href="<?=SITE_DIR?>auth/?register=yes&backurl=<?=$APPLICATION->GetCurPageParam();?>"><?=GetMessage("REGISTER")?></a>
<?else:?>
<a href="<?=SITE_DIR?>personal/"><?=GetMessage("PERSONAL")?></a> | <a href="<?=SITE_DIR?>exit/"><?=GetMessage("EXIT")?></a>
<?endif?>
