<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if (!empty($arResult)):?>
	<? $i = 0; $b = 0; ?>
	<?foreach($arResult as $arElement){
		if($arElement["DEPTH_LEVEL"] == 1){
			$i++;
			$result[$i] = array(
				"TEXT" => $arElement["TEXT"],
				"LINK" => $arElement["LINK"],
				"SELECTED" => $arElement["SELECTED"],
				"IBLOCK_ID" => $arElement["PARAMS"]["IBLOCK_ID"]
			);
		}
		elseif($arElement["DEPTH_LEVEL"] == 2){
			$b++;
			if($arElement["PARAMS"]["FROM_IBLOCK"] <= 100){
				$from = 1;
			}
			else{
				$from = 2;
			}
			$result[$i]["ELEMENTS"][$from][$b] = array(
				"TEXT" => $arElement["TEXT"],
				"LINK" => $arElement["LINK"],
				);
		}elseif($arElement["DEPTH_LEVEL"] == 3){
			$result[$i]["ELEMENTS"][$from ][$b]["ELEMENTS"][] = array(
				"TEXT" => $arElement["TEXT"],
				"LINK" => $arElement["LINK"],
				"SELECTED" => $arElement["SELECTED"],
			);
		}
	}
	?>

	<ul id="leftMenu">
		<?foreach($result as $nextElement):?>
			<?if(CModule::IncludeModule("iblock")){
				$arFilter = array("IBLOCK_ID" => $nextElement["IBLOCK_ID"], "CODE" => str_replace(array("catalog", SITE_DIR, "/"), "", $nextElement["LINK"]));
				$rsSections = CIBlockSection::GetList(array(), $arFilter, false, array("PICTURE", "DETAIL_PICTURE", "UF_DESC"));
				$arSction = $rsSections->GetNext();
				if($arSction){
					$smallImage = CFile::ResizeImageGet($arSction["PICTURE"], array("width" => 30, "height" => 20));
					$bigImage = CFile::ResizeImageGet($arSction["DETAIL_PICTURE"], array("width" => 180, "height" => 180));
				}else{
					$smallImage = "";
					$bigImage = "";
				}
			}
			?>
		<li<?if($nextElement["SELECTED"]):?> class="selected"<?endif;?>><a href="<?=$nextElement["LINK"]?>"><ins><?=(!empty($smallImage["src"]) ? '<img src="'.$smallImage["src"].'" alt="">' : '')?><span><?=$nextElement["TEXT"]?></span></ins></a>
				<div class="drop">
					<?if(count($nextElement["ELEMENTS"][1])):?>
						<ul>
							<?foreach($nextElement["ELEMENTS"][1] as $next2Elements):?>
								<li><a href="<?=$next2Elements["LINK"]?>"><span><?=$next2Elements["TEXT"]?></span></a></li>
									<?if(!empty($next2Elements["ELEMENTS"])):?>
										<?foreach($next2Elements["ELEMENTS"] as $next3Elements):?>
											<li><a href="<?=$next3Elements["LINK"]?>"><?=$next3Elements["TEXT"]?></a></li>
										<?endforeach;?>
									<?endif;?>
							<?endforeach?>
						</ul>
					<?endif;?>
					<?if(count($nextElement["ELEMENTS"][2])):?>
						<ul>
							<?foreach($nextElement["ELEMENTS"][2] as $next2Elements):?>
								<li><a href="<?=$next2Elements["LINK"]?>"><span><?=$next2Elements["TEXT"]?></span></a></li>
									<?if(!empty($next2Elements["ELEMENTS"])):?>
										<?foreach($next2Elements["ELEMENTS"] as $next3Elements):?>
											<li><a href="<?=$next3Elements["LINK"]?>"><?=$next3Elements["TEXT"]?></a></li>
										<?endforeach;?>
									<?endif;?>
							<?endforeach?>
						</ul>
					<?endif;?>
					<?if($arSction):?>
					<ol>
						<li><span><?=$nextElement["TEXT"]?></span></li>
						<li><a href="<?=$nextElement["LINK"]?>" class="pic"><img src="<?=$bigImage["src"]?>" alt="<?=$nextElement["TEXT"]?>"></a></li>
						<?=str_replace("#SITE_DIR#", SITE_DIR, $arSction["~UF_DESC"])?>
					</ol>
					<?endif;?>
				</div>
			</li>
		<?endforeach;?>
	</ul>
<?endif?>