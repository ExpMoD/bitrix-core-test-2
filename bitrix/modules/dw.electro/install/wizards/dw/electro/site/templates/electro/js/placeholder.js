$(window).bind('load', function() {


	var elements = $("input[data-holder], textarea[data-holder]"); // find holder elements and paste in val holder val ;)

	$(elements).each(function(element) {
		$(this).val($(this).data("holder"));
	});

	$("input[data-holder], textarea[data-holder]").click(function(event) {
		if ($(this).val() == $(this).data("holder")) {
			$(this).val("");
		}

	});

	$("input[data-holder], textarea[data-holder]").blur(function(event) {
		if ($(this).val() == "") {
			$(this).val($(this).data("holder"));
		}
	});

});