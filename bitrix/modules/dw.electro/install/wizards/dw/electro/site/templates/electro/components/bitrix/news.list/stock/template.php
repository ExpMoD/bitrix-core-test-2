<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<div class="ancillaryTitle newsColor">
	<span><?=GetMessage("HEADING_STOCK")?></span><?if(!empty($arResult["ITEMS"])):?><a href="<?=SITE_DIR?>stock/"><?=GetMessage("ALL")?></a><?endif;?>
</div>
<?if(!empty($arResult["ITEMS"])):?>
	<ul>
		<?foreach ($arResult["ITEMS"] as $key => $arElement):?>
			<?$previewPicture = CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"], Array("width" => 50, "height" => 50));?>
			<li>
				<table>
					<tr>
						<td><a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="ftPic"><?if(!empty($previewPicture["src"])):?><img src="<?=$previewPicture["src"]?>" title="<?=$arElement["NAME"]?>" alt="<?=$arElement["NAME"]?>"><?endif;?></a></td>
						<td><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></td>
					</tr>
				</table>
			</li>
		<?endforeach;?>
	</ul>
<?endif;?>