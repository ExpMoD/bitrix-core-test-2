<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="forgot">
	<?ShowMessage($arParams["~AUTH_RESULT"]);?>
	<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
	<?
	if (strlen($arResult["BACKURL"]) > 0)
	{
	?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?
	}
	?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="SEND_PWD">
		<p class="inf"><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></p>

	<table class="data-table bx-forgotpass-table">
		<thead>
			<tr> 
				<td colspan="2"><b><?=GetMessage("AUTH_GET_CHECK_STRING")?></b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><label><?=GetMessage("AUTH_LOGIN")?></label><input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" />&nbsp;<?=GetMessage("AUTH_OR")?>
				</td>
			</tr>
			<tr> 
				<td>
					<label><?=GetMessage("AUTH_EMAIL")?></label><input type="text" name="USER_EMAIL" maxlength="255" />
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr> 
				<td colspan="2">
					<input type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" class="submit" />
				</td>
			</tr>
		</tfoot>
	</table>
	</form>
	<script type="text/javascript">
	document.bform.USER_LOGIN.focus();
	</script>
</div>