<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Каталог товаров");
$APPLICATION->SetTitle("Каталог");
?><?$APPLICATION->IncludeComponent(
	"electro:catalog", 
	".default", 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "#CATALOG_IBLOCK_ID#",
		"HIDE_NOT_AVAILABLE" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "#SITE_DIR#catalog/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "Y",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_FILTER" => "Y",
		"USE_REVIEW" => "Y",
		"USE_COMPARE" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"CONVERT_CURRENCY" => "N",
		"BASKET_URL" => "#SITE_DIR#personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(
			0 => "GRAF_PROC",
			1 => "DIAGONAL",
			2 => "CORES",
			3 => "LINE_PROC",
			4 => "HARD_DRIVE",
			5 => "AMOUNT_MEMORY",
			6 => "MEMORY_CAPACITY",
			7 => "HARD_DRIVE_SPACE",
			8 => "MEMORY",
			9 => "OS",
			10 => "PROCESSOR",
			11 => "VIDEO",
			12 => "RESOLUTION",
			13 => "TOUCH",
			14 => "FINGERPRINT",
			15 => "HARD_DRIVE_TYPE",
			16 => "TYPE_PROCESSOR",
			17 => "SUPPORT_3G",
			18 => "SPEED_WIFI",
			19 => "SOCKET",
			20 => "CAMERA",
			21 => "SIM",
			22 => "MEMORY_CARD",
			23 => "TYPE_DISPLAY",
			24 => "FM",
			25 => "GPS",
			26 => "DISPLAY",
			27 => "SUPPORT_2SIM",
			28 => "MP3",
			29 => "GSM",
			30 => "TYPE",
			31 => "ZOOM2",
			32 => "FLASH",
			33 => "FULL_HD_VIDEO_RECORD",
			34 => "Number_of_memory_slots",
			35 => "MAX_RESOLUTION_VIDEO",
			36 => "MAX_RESOLUTION",
			37 => "ZOOM",
			38 => "ROTARY_DISPLAY",
			39 => "MATRIX",
			40 => "PHOTOSENSITIVITY",
			41 => "IMAGE_STABILIZER",
			42 => "TYPE_MEMORY",
			43 => "SWITCH",
			44 => "MORE_PROPERTIES",
			45 => "INTERFACE",
			46 => "SUPPORTED_STANDARTS",
			47 => "ETHERNET_PORTS",
			48 => "TYPE_BODY",
			49 => "TYPE_MOUSE",
			50 => "TYPE_PRINT",
			51 => "CONNECTION",
			52 => "TYPE2",
			53 => "FORM_FAKTOR",
			54 => "RANGE",
			55 => "NEW",
			56 => "SALE",
			57 => "SIMILAR_PRODUCT",
			58 => "POPULAR",
		),
		"SHOW_TOP_ELEMENTS" => "N",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"SECTION_COUNT_ELEMENTS" => "Y",
		"SECTION_TOP_DEPTH" => "3",
		"SECTIONS_VIEW_MODE" => "LIST",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"PAGE_ELEMENT_COUNT" => "30",
		"LINE_ELEMENT_COUNT" => "3",
		"ELEMENT_SORT_FIELD" => "shows",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "CATALOG_AVAILABLE",
		"ELEMENT_SORT_ORDER2" => "desc",
		"LIST_PROPERTY_CODE" => array(
			0 => "DIAGONAL",
			1 => "CORES",
			2 => "MEMORY_CAPACITY",
			3 => "HARD_DRIVE_SPACE",
			4 => "MEMORY",
			5 => "OS",
			6 => "PROCESSOR",
			7 => "VIDEO",
			8 => "RESOLUTION",
			9 => "TOUCH",
			10 => "FINGERPRINT",
			11 => "HARD_DRIVE_TYPE",
			12 => "TYPE_PROCESSOR",
			13 => "PROCESSOR_SPEED",
			14 => "MORE_PROPERTIES",
			15 => "ATT_BRAND",
			16 => "COLOR",
			17 => "BLOG_POST_ID",
			18 => "ARTICLE",
			19 => "BLOG_COMMENTS_CNT",
			20 => "VOTE_COUNT",
			21 => "MARKER_PHOTO",
			22 => "NEW",
			23 => "RELATED_PRODUCT",
			24 => "SALE",
			25 => "RATING",
			26 => "SIMILAR_PRODUCT",
			27 => "VOTE_SUM",
			28 => "MARKER",
			29 => "POPULAR",
			30 => "WEIGHT",
			31 => "vote_count",
			32 => "rating",
			33 => "vote_sum",
			34 => "",
		),
		"INCLUDE_SUBSECTIONS" => "N",
		"LIST_META_KEYWORDS" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_BROWSER_TITLE" => "NAME",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "BLUETOOTH",
			1 => "GRAF_PROC",
			2 => "DIAGONAL",
			3 => "CORES",
			4 => "LINE_PROC",
			5 => "HARD_DRIVE",
			6 => "AMOUNT_MEMORY",
			7 => "MEMORY_CAPACITY",
			8 => "HARD_DRIVE_SPACE",
			9 => "MEMORY",
			10 => "OS",
			11 => "PROCESSOR",
			12 => "VIDEO",
			13 => "RESOLUTION",
			14 => "TOUCH",
			15 => "FINGERPRINT",
			16 => "HARD_DRIVE_TYPE",
			17 => "TYPE_PROCESSOR",
			18 => "PROCESSOR_SPEED",
			19 => "HDMI",
			20 => "USB",
			21 => "WIFI",
			22 => "SUPPORT_3G",
			23 => "SPEED_WIFI",
			24 => "SOCKET",
			25 => "CAMERA",
			26 => "SIM",
			27 => "MEMORY_CARD",
			28 => "TYPE_DISPLAY",
			29 => "FM",
			30 => "GPS",
			31 => "DISPLAY",
			32 => "SUPPORT_2SIM",
			33 => "MP3",
			34 => "GSM",
			35 => "TOTAL_OUTPUT_POWER",
			36 => "TYPE",
			37 => "ZOOM2",
			38 => "FLASH",
			39 => "FULL_HD_VIDEO_RECORD",
			40 => "Number_of_memory_slots",
			41 => "MAX_RESOLUTION_VIDEO",
			42 => "MAX_RESOLUTION",
			43 => "ZOOM",
			44 => "ROTARY_DISPLAY",
			45 => "MATRIX",
			46 => "PHOTOSENSITIVITY",
			47 => "IMAGE_STABILIZER",
			48 => "TYPE_MEMORY",
			49 => "SMART_TV",
			50 => "POWER_SUB",
			51 => "POWER",
			52 => "VIDEO_FORMAT",
			53 => "SUPPORT_3D",
			54 => "REFRESH_RATE",
			55 => "SWITCH",
			56 => "MORE_PROPERTIES",
			57 => "GENRE",
			58 => "INTERFACE",
			59 => "COMPRESSORS",
			60 => "Number_of_Outlets",
			61 => "FREEZER",
			62 => "PAPER_FEED",
			63 => "SUPPORTED_STANDARTS",
			64 => "ETHERNET_PORTS",
			65 => "ATT_BRAND",
			66 => "DEFROST",
			67 => "PRINT_SPEED",
			68 => "TYPE_BODY",
			69 => "TYPE_MOUSE",
			70 => "TYPE_PRINT",
			71 => "CONNECTION",
			72 => "TYPE2",
			73 => "FORM_FAKTOR",
			74 => "COLOR",
			75 => "RANGE",
			76 => "BLOG_POST_ID",
			77 => "ARTICLE",
			78 => "WARRANTY",
			79 => "DELIVERY",
			80 => "BLOG_COMMENTS_CNT",
			81 => "VOTE_COUNT",
			82 => "MARKER_PHOTO",
			83 => "NEW",
			84 => "DELIVERY_DESC",
			85 => "RELATED_PRODUCT",
			86 => "SALE",
			87 => "RATING",
			88 => "PICKUP",
			89 => "SIMILAR_PRODUCT",
			90 => "VOTE_SUM",
			91 => "MARKER",
			92 => "POPULAR",
			93 => "WEIGHT",
			94 => "HEIGHT",
			95 => "DEPTH",
			96 => "WIDTH",
			97 => "Clock_speed",
			98 => "Battery_life",
			99 => "Length_of_cord",
			100 => "Maximum_memory_frequency",
			101 => "max_bus_frequency",
			102 => "With_cooler",
			103 => "",
		),
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_BROWSER_TITLE" => "NAME",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_PROPERTY_SID" => "",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"USE_ALSO_BUY" => "N",
		"USE_STORE" => "N",
		"PAGER_TEMPLATE" => "visual",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"TEMPLATE_THEME" => "blue",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => "-",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"DETAIL_SHOW_MAX_QUANTITY" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_USE_VOTE_RATING" => "Y",
		"DETAIL_USE_COMMENTS" => "Y",
		"DETAIL_BRAND_USE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"FILTER_NAME" => "",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "DIAGONAL",
			1 => "CORES",
			2 => "MEMORY_CAPACITY",
			3 => "HARD_DRIVE_SPACE",
			4 => "MEMORY",
			5 => "OS",
			6 => "PROCESSOR",
			7 => "VIDEO",
			8 => "RESOLUTION",
			9 => "TOUCH",
			10 => "FINGERPRINT",
			11 => "HARD_DRIVE_TYPE",
			12 => "TYPE_PROCESSOR",
			13 => "PROCESSOR_SPEED",
			14 => "MORE_PROPERTIES",
			15 => "ATT_BRAND",
			16 => "COLOR",
			17 => "VOTE_COUNT",
			18 => "MARKER_PHOTO",
			19 => "NEW",
			20 => "RELATED_PRODUCT",
			21 => "SALE",
			22 => "RATING",
			23 => "SIMILAR_PRODUCT",
			24 => "VOTE_SUM",
			25 => "MARKER",
			26 => "POPULAR",
			27 => "WEIGHT",
			28 => "",
		),
		"FILTER_PRICE_CODE" => array(
			0 => "BASE",
		),
		"MESSAGES_PER_PAGE" => "10",
		"USE_CAPTCHA" => "Y",
		"REVIEW_AJAX_POST" => "Y",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"FORUM_ID" => "1",
		"URL_TEMPLATES_READ" => "",
		"SHOW_LINK_TO_FORUM" => "Y",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"COMPARE_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"COMPARE_ELEMENT_SORT_FIELD" => "sort",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"CURRENCY_ID" => "RUB",
		"ALSO_BUY_ELEMENT_COUNT" => "5",
		"ALSO_BUY_MIN_BUYES" => "1",
		"USE_STORE_PHONE" => "N",
		"USE_STORE_SCHEDULE" => "N",
		"USE_MIN_AMOUNT" => "Y",
		"MIN_AMOUNT" => "10",
		"STORE_PATH" => "/store/#store_id#",
		"MAIN_TITLE" => "Наличие на складах",
		"DETAIL_VOTE_DISPLAY_AS_RATING" => "rating",
		"DETAIL_BLOG_USE" => "Y",
		"DETAIL_BLOG_URL" => "catalog_comments",
		"DETAIL_VK_USE" => "N",
		"DETAIL_FB_USE" => "N",
		"FILTER_OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_OFFERS_PROPERTY_CODE" => array(
			0 => "HARD_DRIVE_SPACE",
			1 => "COLOR",
			2 => "",
		),
		"OFFERS_CART_PROPERTIES" => array(
			0 => "HARD_DRIVE_SPACE",
			1 => "COLOR",
		),
		"LIST_OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"LIST_OFFERS_PROPERTY_CODE" => array(
			0 => "HARD_DRIVE_SPACE",
			1 => "MORE_PHOTO",
			2 => "",
		),
		"LIST_OFFERS_LIMIT" => "5",
		"DETAIL_OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_OFFERS_PROPERTY_CODE" => array(
			0 => "CHISLO",
			1 => "HARD_DRIVE_SPACE",
			2 => "TEST",
			3 => "MORE_PHOTO",
			4 => "COLOR",
			5 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"PRODUCT_DISPLAY_MODE" => "N",
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "-",
			1 => "HARD_DRIVE_SPACE",
			2 => "TEST",
			3 => "COLOR",
		),
		"REVIEW_IBLOCK_TYPE" => "#REVIEW_IBLOCK_TYPE#",
		"REVIEW_IBLOCK_ID" => "#REVIEW_IBLOCK_ID#",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#.html",
			"compare" => "/compare/",
		)
	),
	false
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>