$(window).bind('load', function() {

	var elements = $(".electroSelect");


	$(elements).each(function(eIndex) {

		var thisClass = $(this).data("class");
		var thisImg = $(this).data("img");

		thisImg = thisImg != undefined ? '<img src="' + thisImg + '" alt="">' : '';

		var DOM = '<div style="position:relative"><div id="' + thisClass + '"> <ins> ' + $(this).find("option:first").html() + ' </ins> ' + thisImg + '<ul>';
		var option = $(this).find("option");

		$(option).each(function(opIndex) {
			//if(opIndex != 0){
			DOM = DOM + '<li data-val="' + $(this).val() + '">' + $(this).html() + '</li>';
			//}
		});

		DOM = DOM + "</ul></div>";

		$(this).before(DOM);
		$(this).after("</div>");
		$(this).css({
			"width": $("#" + thisClass).outerWidth() + "px",
			"opacity": 0
		}); //set select width

	});

	$("div[id*=electroSelect] > ins , div[id*=electroSelect] > img").click(function(event) { // open UL
		var element = $(this).parent("div").find("ul");
		element.fadeToggle("fast");
	});

	$("div[id*=electroSelect] > ul > li").click(function(event) { // ul li click and select

		var elementClick = $(this);
		var elementClickParent = elementClick.parent("ul");
		var elementClickParentID = elementClickParent.parent().attr("id");

		elementClickParent.siblings("ins").html(elementClick.html());
		elementClickParent.fadeToggle("fast");

		var activeSelect = $("select[data-class=" + elementClickParentID + "]");
		$(activeSelect).find("option").removeAttr("selected").eq(elementClick.index()).attr("selected", "selected");

	});


});