<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?><h1>Вакансии</h1>
<b>Администратор интернет-магазина.</b></br>
Работа в интернет магазине!</br>
Зарплата от 10 000 до 30 000 рублей</br>
<b>Основные требования</b>
<ul>
<li>1. Знание на уровне продвинутого пользователя 1с-Битрикс</li>
<li>2. Опыт общения с клиентами</li>
<li>3. инициатность</li>
<li>4. продвинутый пользователь компьютера и интернета</li>
</ul>
<b>Обязанности:</b>
</ul>
<li>1.Прием заказа, обработка, отгрузка</li>
<li>2.Прием входящих звонков</li>
<li>3.Оформление заказа в интернет-магазине с телефона</li>
<li>4.Консультации по товарам, наличию, доставке, оплате</li>
<li>5.Обработка заказа, поступившего в интернет-магазин</li>
<li>6.Проверка наличия на складе</li>
<li>7.Звонки покупателям. Подтвердить заказ с сайта</li>
<li>8. сделать заявку поставщикам</li>
<li>9. получить счета</li>
<li>10. получить товар</li>
<li>11. рассортировать по посылкам и отправить их покупателям.</li>
</ul>
<b>Условия:</b>
<ul>
<li>Постоянная работа</li>
<li>График работы: с 9:00 до 20:00,  2/2</li>
<li>Работа на территории работодателя</li>
</ul><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>