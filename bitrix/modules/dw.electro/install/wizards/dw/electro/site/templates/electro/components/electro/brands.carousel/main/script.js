$(document).ready(function(){
	$("#brandCarousel").electroCarousel({
		speed: 500,
		leftButton: ".brandBtnLeft",
		rightButton: ".brandBtnRight",
		countElement: 6,
		resizeElement: true
	});
});
