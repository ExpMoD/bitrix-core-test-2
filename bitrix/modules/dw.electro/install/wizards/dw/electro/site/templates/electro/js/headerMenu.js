$(function() {
	$(document).on("click", "#headerLineRight li.slide > a", function(e) {
		$("#headerLineRight li.slide ul").toggle();
		e.preventDefault();
	});

	$(document).on("click", function(e) {
		var trg = $(e.target);
		if (trg.parent().parent().parent().attr("class") != "slide" && trg.parent().attr("class") != "slide") {
			$("#headerLineRight li.slide ul").hide();
		}
	});
});