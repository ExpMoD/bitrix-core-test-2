<?IncludeTemplateLangFile(__FILE__);?>
                </div>
            </div>
            <div id="footer">

                  <div id="footerLeft">
                    <?$APPLICATION->IncludeComponent(
                      "bitrix:main.include",
                      ".default",
                      array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "footer",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "EDIT_TEMPLATE" => ""
                      ),
                      false
                    );?>
                    <a href="http://dw24.su/" class="copyrightRef"><img src="<?=SITE_TEMPLATE_PATH?>/images/dw.png"></a>
                  </div>

                  <div id="footerRight">
                      <ul>
                        <li>
                          <span><?=GetMessage("MAGAZINE")?></span>
                          <?$APPLICATION->IncludeComponent(
                            "bitrix:menu", 
                            "footer", 
                            array(
                              "ROOT_MENU_TYPE" => "footer1",
                              "MENU_CACHE_TYPE" => "N",
                              "MENU_CACHE_TIME" => "3600",
                              "MENU_CACHE_USE_GROUPS" => "Y",
                              "MENU_CACHE_GET_VARS" => array(
                              ),
                              "MAX_LEVEL" => "1",
                              "CHILD_MENU_TYPE" => "left",
                              "USE_EXT" => "N",
                              "DELAY" => "N",
                              "ALLOW_MULTI_SELECT" => "N"
                            ),
                            false
                          );?>
                        </li>
                          <li>
                          <span><?=GetMessage("ABOUT")?></span>
                          <?$APPLICATION->IncludeComponent(
                            "bitrix:menu", 
                            "footer", 
                            array(
                              "ROOT_MENU_TYPE" => "footer2",
                              "MENU_CACHE_TYPE" => "N",
                              "MENU_CACHE_TIME" => "3600",
                              "MENU_CACHE_USE_GROUPS" => "Y",
                              "MENU_CACHE_GET_VARS" => array(
                              ),
                              "MAX_LEVEL" => "1",
                              "CHILD_MENU_TYPE" => "left",
                              "USE_EXT" => "N",
                              "DELAY" => "N",
                              "ALLOW_MULTI_SELECT" => "N"
                            ),
                            false
                          );?>
                        </li>
                          <li>
                          <span><?=GetMessage("SERVICE")?></span>
                          <?$APPLICATION->IncludeComponent(
                            "bitrix:menu", 
                            "footer", 
                            array(
                              "ROOT_MENU_TYPE" => "footer3",
                              "MENU_CACHE_TYPE" => "N",
                              "MENU_CACHE_TIME" => "3600",
                              "MENU_CACHE_USE_GROUPS" => "Y",
                              "MENU_CACHE_GET_VARS" => array(
                              ),
                              "MAX_LEVEL" => "1",
                              "CHILD_MENU_TYPE" => "left",
                              "USE_EXT" => "N",
                              "DELAY" => "N",
                              "ALLOW_MULTI_SELECT" => "N"
                            ),
                            false
                          );?>
                        </li>
                          <li class="footerTools">
                          <span><?=GetMessage("SMM")?></span>
                         <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include", 
                            ".default", 
                            array(
                              "AREA_FILE_SHOW" => "sect",
                              "AREA_FILE_SUFFIX" => "sn",
                              "AREA_FILE_RECURSIVE" => "Y",
                              "EDIT_TEMPLATE" => ""
                            ),
                            false
                          );?>
                        </li>
                      </ul>
                    </div>  
              </div>
            <div id="footerCart">
              <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "footCart", Array(
              ),
              false
              );?>
            </div>
      <script type="text/javascript">
          var LANG = {
            "ADD_CART"    :  "<?=GetMessage("ADD_CART");?>",
            "CART_ADDED"  :  "<?=GetMessage("CART_ADDED");?>",
            "COMP_ADDED"  :  "<?=GetMessage("COMP_ADDED");?>",
            "LOADING"     :  "<?=GetMessage("LOADING");?>",
            "AVAILABLE"   :  "<?=GetMessage("AVAILABLE");?>",
            "NOAVAILABLE" :  "<?=GetMessage("NOAVAILABLE");?>",
            "ALL_RESULT"  :  "<?=GetMessage("ALL_RESULT");?>",
            "ORDER"       :  "<?=GetMessage("ORDER");?>",
            "SHOW_ALL"    :  "<?=GetMessage("SHOW_ALL");?>",
            "HIDE_ALL"    :  "<?=GetMessage("HIDE_ALL");?>",
          };
      </script>
  </body>
</html>