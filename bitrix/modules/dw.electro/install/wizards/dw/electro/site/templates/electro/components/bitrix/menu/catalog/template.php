<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if (!empty($arResult)):?>
	<? $i = 0; $b = 0; ?>
	<?foreach($arResult as $arElement){ 
		if($arElement["DEPTH_LEVEL"] == 1){
			$i++;
			$result[$i] = array(
				"TEXT" => $arElement["TEXT"],
				"LINK" => $arElement["LINK"],
				 "SELECTED" => $arElement["SELECTED"]
			);
		}
		elseif($arElement["DEPTH_LEVEL"] == 2){
			$b++;
			if($arElement["PARAMS"]["FROM_IBLOCK"] <= 50){
				$from = 1;	
			}elseif($arElement["PARAMS"]["FROM_IBLOCK"] <= 149){
				$from = 2;	
			}elseif($arElement["PARAMS"]["FROM_IBLOCK"] >= 150){
				$from = 3;	
			}
			$result[$i]["ELEMENTS"][$from][$b] = array(
				"TEXT" => $arElement["TEXT"],
				"LINK" => $arElement["LINK"],
				"SELECTED" => $arElement["SELECTED"]
			);
		}elseif($arElement["DEPTH_LEVEL"] == 3){
			$result[$i]["ELEMENTS"][$from][$b]["ELEMENTS"][] = array(
				"TEXT" => $arElement["TEXT"],
				"LINK" => $arElement["LINK"],
				"SELECTED" => $arElement["SELECTED"]
			);	
		}
	} 
	?>
	
	<ul id="catalogMenu">
		<?foreach($result as $nextElement):?>
			<?if(CModule::IncludeModule("iblock")){
					$arSelect = Array("ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "DETAIL_TEXT");
					$arFilter = Array("IBLOCK_ID" => 81, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "CODE" => str_replace(array("catalog","/"),"",$nextElement["LINK"]));
					$res = CIBlockElement::GetList(Array(), $arFilter, false,array(), $arSelect);
					$ob = $res->GetNextElement();
					if($ob){
						$arFields = $ob->GetFields();
						$bigImage = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array("width" => 360, "height" => 300));
					}else{
						$bigImage = "";
					}
				}
			?> 
			<div class="catalogSection">
				<table width="100%">
					<tbody>
						<tr>
							<td><a href="<?=$nextElement["LINK"]?>"><img src="<?=$bigImage["src"]?>" alt="<?=$nextElement["TEXT"]?>"></a><a href="<?=$nextElement["LINK"]?>"><span><?=$nextElement["TEXT"]?></span></a></td>
							<td>
								<table width="100%">
									<tbody>
										<tr>
											<td>
											  <?foreach($nextElement["ELEMENTS"][1] as $elements1):?>
											  		<a href="<?=$elements1["LINK"]?>" class="hd"><?=$elements1["TEXT"]?></a>
											  		<ul>
												  		<?foreach($elements1["ELEMENTS"] as $elements1Drop):?>
												  			<li><a href="<?=$elements1Drop["LINK"]?>"><?=$elements1Drop["TEXT"]?></a></li>
												  		<?endforeach;?>
											  		</ul>
											  <?endforeach;?>
											</td>
										    <td>
												  <?foreach($nextElement["ELEMENTS"][2] as $elements2):?>
											  		<a href="<?=$elements2["LINK"]?>" class="hd"><?=$elements2["TEXT"]?></a>
											  		<ul>
												  		<?foreach($elements2["ELEMENTS"] as $elements1Drop):?>
												  			<li><a href="<?=$elements1Drop["LINK"]?>"><?=$elements1Drop["TEXT"]?></a></li>
												  		<?endforeach;?>
											  		</ul>
											  <?endforeach;?>
										    </td>
										    <td>
											  <?foreach($nextElement["ELEMENTS"][3] as $elements3):?>
											  		<a href="<?=$elements3["LINK"]?>" class="hd"><?=$elements3["TEXT"]?></a>
											  		<ul>
												  		<?foreach($elements3["ELEMENTS"] as $elements1Drop):?>
												  			<li><a href="<?=$elements1Drop["LINK"]?>"><?=$elements1Drop["TEXT"]?></a></li>
												  		<?endforeach;?>
											  		</ul>
											  <?endforeach;?>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>	
		<?endforeach;?>
	</ul>
<?endif?>