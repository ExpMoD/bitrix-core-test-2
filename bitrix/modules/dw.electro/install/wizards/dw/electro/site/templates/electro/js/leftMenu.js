$(window).on("load", function() {

	var $fixMenu = $("#fixMenu");
	var $mainContent = $("#mainContent");
	var $mainLeft = $("#mainContentleft");
	var $marg = $("#marg");
	var $menu = $("#leftMenu");
	var menuOffset = $fixMenu.offset().top;
	var allowFix = $fixMenu.hasClass("allowHide") ? false : true;

	$mainLeft.find(".drop").css({
		"left": $mainLeft.outerWidth() + "px"
	});

	$mainLeft.find(".drop").each(function(i) {
		var ulCount = $(this).find("ul").length;
		var olCount = $(this).find("ol").length;
		$(this).width(((ulCount + olCount) * 230 + $(this).find("ul").length) + "px");
	});

	$('a.allowHide').click(function(event) {
		event.preventDefault();
		if ($menu.css("display") == "block") {
			$('a.mainContentleftArrow sub').stop().animate({
				rotate: "0deg"
			}, 350);
		} else {
			$('a.mainContentleftArrow sub').stop().animate({
				rotate: "90deg"
			}, 350);
		}
		$("#leftMenu").stop().slideToggle();
	});

	var timeoutID = 0;
	var $link = $menu.children("li");

	$link.hover(function() {

		$link.find("div").hide();
		$internal = $(this);
		$internal.find("div").stop().show();
		allowFix = false;

		clearTimeout(timeoutID);

	}, function() {
		timeoutID = setTimeout(function() {
			$internal.find("div").hide();
			allowFix = $fixMenu.hasClass("allowHide") ? false : true;
			moveMenu();
		}, 600);
	});

	var moveMenu = function(event) {
		if (allowFix) {
			var maxContHeight = $mainContent.outerHeight() - $fixMenu.outerHeight();
			var moveValueEx = menuOffset - window.pageYOffset;
			var moveValue;

			if (moveValueEx <= 0) {
				moveValue = Math.abs(moveValueEx) > maxContHeight ? maxContHeight : Math.abs(moveValueEx);
			} else {
				moveValue = 0;
			}
			$fixMenu.css({
				"top": moveValue
			});
		}
	};

	$(window).on("scroll load", moveMenu);
	$(window).resize(function() {
		$mainLeft.find(".drop").css({
			"left": $mainLeft.outerWidth() + "px"
		});
	});

});