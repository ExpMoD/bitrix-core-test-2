<?define("NEED_AUTH", true);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
?><h1>Авторизация</h1>
<div id="empty">
	<img src="<?=SITE_TEMPLATE_PATH?>/images/register.png" alt="" class="emptyImg">
	<div class="info">
		<h3>Вы зарегистрированы и успешно авторизовались</h3>
		<p>Вы можете вернуться на главную страницу или воспользоваться навигацией или поиском по сайту.</p>
		<a href="<?=SITE_DIR?>" class="back">Главная страница</a>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>