
// #global vars 

var flushTimeout;
var $topCart;
var $footCart;

$(window).on('load', function() {

	$topCart = {
		qty: $("#topQty"),
		sum: $("#topSum")
	}
	$footCart = {
		cart: $("#footCart"),
		comp: $("#footComp")
	}

	//#opera scroll

	if(navigator.appName == "Opera"){
		$(window).bind('mousewheel', oScroll);
	}

});


var topSearch = function(flag) {

	var width = $(window).outerWidth() < 1000 ? 1000 : $(window).outerWidth();
	var $searchTable = $("#searchTable");

	width < 1200 ? $searchTable.addClass("small") : $searchTable.removeClass("small");

};

var topMenu = function(flag) {

	var width = $(document).width();
	var leftBlock = $("#headerLineLeft");
	var maxWidth = ((width - leftBlock.outerWidth()) - ((width - leftBlock.outerWidth()) * 45 / 100));
	var menu = $("#topMenu");
	var menuElements = $("#topMenu > li:not(.slide)");
	var currentWidth = 0;

	$("#headerLine").css("overflow", "visible");

	menuElements.each(function(index) {
		currentWidth = currentWidth + $(this).outerWidth();
		if (currentWidth > maxWidth) {

			if (flag == true) {
				menuElements.show();
				menu.find(".slide").remove();
			}

			menuElements.slice(index).hide();
			menu.append(
				$('<li class="slide">').html('<a href="#"></a>').append(
					$("<ul>").append(
						menuElements.filter(":hidden").clone().removeAttr("style")
					)
				)
			);

			return false;
		}
	});

}

var offersComp = function() {
	var width = document.body.clientWidth;

	if (width >= 1200 && width <= 1920) {
		$("#ancillary > ul > li").css({
			"width": "23.5%",
			"margin-left": "2%"
		});
		$("#ancillary > ul > li:first-child").css("margin-left", "0px");
	} else {
		$("#ancillary > ul > li:nth-of-type(2n+1)").css("margin-left", "0px");
		$("#ancillary > ul > li").css("width", "49%");
	}
}

var picResize = function(width) {
	var $advBlock = $("#advBlock");
	if (width > 1300) {
		$advBlock.removeClass("small");
	} else {
		$advBlock.addClass("small");
	}
};

var initialization = function(event) {
	var width = $(window).outerWidth() < 1000 ? 1000 : $(window).outerWidth();
	width = width > 1920 ? 1920 : width;
	offersComp();
	topMenu(event.data.flag);
	topSearch(event.data.flag);
	picResize(width);
};

$(document).keypress(function(e) {
	if (e.keyCode == 27 || e.keyCode == 13) {
		var $basket = $("#inBasket");
		var $compare = $("#comparison");
		if ($basket.is(":visible")) {
			$basket.hide();
		} else if ($compare.is(":visible")) {
			$compare.hide();
		}
	}
});

//addCompare
$(document).on("click", ".addCompare", function(e) {
	$this = $(this);
	if ($this.data("id") != "" && $this.attr("href") == "#") {
		$.getJSON(ajaxPath + "?act=compADD&id=" + $(this).data("id"), function(data) {
			if (data != "") {
				$img = $this.find("img");
				$this.html(" В списке сравнения").prepend($img).addClass("added").attr("href", SITE_DIR + "compare/");
				$("#compName").text(data["NAME"]);
				$("#comparison").show();
				cartReload();
			}
		});
		e.preventDefault();
	} else {

	}
});

$(document).on("click", "#comparisonClose, #comparisonOrder .comparisonContinue", function(e) {
	$("#comparison").hide();
	e.preventDefault();
});


//addCart
$(document).on("click", ".addCart", function(e) {
	$this = $(this);
	if ($this.data("id") != "" && $this.attr("href") == "#") {
		$this.addClass("loading").text("Загрузка");
		$.getJSON(ajaxPath + "?act=add&id=" + $this.data("id") + "&q=1", function(data) {
			if (data != "") {

				$this.text("В корзине").attr("href", SITE_DIR + "personal/cart/").removeClass("loading").addClass("added");
				if ($this.data("reload") != "Y") {

					var $basket = $("#inBasket");
					$basket.find(".basketIMG").html('<img src="' + $basket.data("load") + '">');

					var basketImage = new Image() ;
				    basketImage.src = data["DETAIL_PICTURE"];
				    basketImage .onload = function() {
				        $basket.find(".basketIMG").html(basketImage).attr("href", data["DETAIL_PAGE_URL"]);
				    };

					$basket.find(".basketNAME").html(data["NAME"]);
					$basket.find("#inBasketPrice").html(data["PRICE"]).data("price", data["~PRICE"]);
					$basket.find("#inBasketTotal").find(".all").html(data["SUM"]);
					$basket.find(".qty").val(data["QUANTITY"]);
					$basket.find("#inBasketDelete").data({
						"id" : data["CART_ID"],
						"product-id" : data["PRODUCT_ID"]
					});
					$basket.find(".plus, .minus").data("id", data["ID"]);
					$basket.show();

					cartReload();

				} else {
					document.location.reload();
				}
			}
		});

		e.preventDefault();
	}
});

$(document).on("click", "#inBasketDelete", function(e) {
	var obj = $(this);
	obj.addClass("loaderMin");
	$.get(ajaxPath + "?act=del&id=" + obj.data("id"), function(data) {
		if (data != "") {
			obj.removeClass("loaderMin");
			$("#inBasket").hide();
			var price = parseInt($("#inBasketPrice").text().replace(/\s+/g, ''));
			var qty = parseInt($("#inBasketQty").find(".qty").val());
			$(".addCart.added").filter(function( index ) {
				return $(this).data("id") == obj.data("product-id");
			}).removeClass("added").text("В корзину").attr("href", "#");
			cartReload();
		} else {
			alert("error; [data-id] not found!");
		}
	});
	e.preventDefault();
});

$(document).on("click", "#inBasket .plus", function(e) {
	var $this = $(this);
	var bask = $("#inBasketQty").find(".qty");
	var price = parseInt($("#inBasketPrice").data("price"));
	var qty = parseInt(bask.val());
	var sum = $("#inBasketTotal").find(".all");
	bask.val(qty + 1);
	sum.text(formatPrice(price * parseInt(bask.val())));
	clearTimeout(flushTimeout);
	flushTimeout = setTimeout(function() {
		flushCart($this.data("id"), bask.val())
	}, 500);
});

$(document).on("click", "#inBasket .minus", function(e) {
	var $this = $(this);
	var bask = $("#inBasketQty").find(".qty");
	var price = parseInt($("#inBasketPrice").data("price"));
	var qty = parseInt(bask.val());
	if (bask.val() > 1) {
		bask.val(qty - 1);
		$("#inBasketTotal").find(".all").text(formatPrice(price * parseInt(bask.val())));
		clearTimeout(flushTimeout);
		flushTimeout = setTimeout(function() {
			flushCart($this.data("id"), bask.val())
		}, 500);
	}
});

$(document).on("click", "#inBasketClose, .inBasketContinue", function(e) {
	$("#inBasket").hide();
	e.preventDefault();
});

var cartReload = function() {
	$.get(ajaxPath + "?act=flushCart", function(data) {
		var $items = $(data).find(".dl");
		$("#headerLineLeft").html($items.eq(0).html());
		$("#footerCart").html($items.eq(1).html());
	});
}

var formatPrice = function(data) {
	var price = String(data).split('.');
	var strLen = price[0].length;
	var str = "";

	for (var i = strLen; i > 0; i--) {
		str = str + ((!(i % 3) ? " " : "") + price[0][strLen - i]);
	}

	return str + (price[1] != undefined ? "." + price[1] : "");
}

var flushCart = function(id, q) {
	$.get(ajaxPath + "?act=upd&id=" + id + "&q=" + q, function(data) {
		data == "" ? alert("error; [data-id] not found!") : cartReload();
	});
}

var loader = function(dest) {
	var $loader = $("#loader");
	dest == "show" ? $loader.show() : $loader.hide();
};

var oScroll = function(event){
	var $this = $(this);
	var winPos = $this.scrollTop();
	if(event.originalEvent.wheelDelta > 0){
		if(winPos > event.originalEvent.wheelDelta){
			winPos -= event.originalEvent.wheelDelta;
		}else{
			winPos = 0;
		}
	}else{
		if((winPos + Math.abs(event.originalEvent.wheelDelta)) > $(document).height()){
			winPos =  $(document).height();
		}else{
			winPos -= event.originalEvent.wheelDelta;
		}
	}
	$("html").scrollTop(winPos);
	event.preventDefault();
};

var fastBackSend = function(event){
	var $_this = $(this);
	var $_form = $_this.parent("form");
	var $_err  = $("#fastBackErrors");
	var $_succ = $("#fastBackSuccess");
	var $_self = $("#fastBack");

	$.getJSON(ajaxPath + "?act=fastBack&" + $_form.serialize() + "&id=" + $_form.data("product-id"), function(resp){
		if(resp["success"]){
			$_form.siblings(".heading").html(resp["heading"]);
			$_succ.show().find(".message").html(resp["message"]);
			$_form.hide(); $_err.hide(); $_self.addClass("reload");
		}else{
			$_err.show().html(resp["message"]);
		}
	});
	event.preventDefault();
};

var fastBackOpen = function(event){
	$("#fastBackErrors, #fastBackSuccess").hide();
	$("#fastBack").show().find("form").show();
	event.preventDefault();
};

var fastBackClose = function(event){
	$_self = $("#fastBack");
	$_self.hasClass("reload") ? document.location.reload() : void 0;
	$_self.hide()
	event.preventDefault();
};


$(document).on("click", ".oneClick", fastBackOpen);
$(document).on("click", "#fastBack .close, #fastBack .fastBackClose", fastBackClose);
$(document).on("click", ".fastBackSubmit", fastBackSend);

$(window).on("ready",  {flag: false}, initialization);
$(window).on("resize", {flag: true}, initialization);