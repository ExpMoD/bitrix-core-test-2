<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?

foreach($arResult as $key => $val){
	$img = "";
	if ($val["DETAIL_PICTURE"] > 0)
		$img = $val["DETAIL_PICTURE"];
	elseif ($val["PREVIEW_PICTURE"] > 0)
		$img = $val["PREVIEW_PICTURE"];

	$file = CFile::ResizeImageGet($img, array('width'=>$arParams["VIEWED_IMG_WIDTH"], 'height'=>$arParams["VIEWED_IMG_HEIGHT"]), BX_RESIZE_IMAGE_PROPORTIONAL, true);

	$val["PICTURE"] = !empty($file) ? $file : array("src" => SITE_TEMPLATE_PATH."/images/empty.png");
	
	$arSelect = Array("ID", "IBLOCK_ID","PROPERTY_*");
	$arFilter = Array("ID" => $val["PRODUCT_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$ob = $res->GetNextElement();
	$val["PROPERTIES"] = $ob->GetProperties();
	
	$arResult[$key] = $val;

}

?>