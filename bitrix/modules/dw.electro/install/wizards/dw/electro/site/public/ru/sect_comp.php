 <div id="comparison">
    <div id="comparisonContainer">
        <span class="heading">Товар добавлен к сравнению.</span>
        <a href="#" id="comparisonClose"></a>
        <p><ins id="compName"></ins> <span>Добавлен к сравнению.</span></p>
        <div id="comparisonOrder">
        <a href="#" class="comparisonContinue">Продолжить покупки</a>
        <a href="<?=SITE_DIR?>compare/" class="comparisonMake">Сравнить</a>
        </div>
    </div>
</div>