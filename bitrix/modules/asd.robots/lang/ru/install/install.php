<?php
$MESS ['ASD_ROBOTS_MODULE_NAME'] = "Поиск + robots.txt";
$MESS ['ASD_ROBOTS_MODULE_DESCRIPTION'] = "Модуль, позволяющий индексировать сайт согласно правилам из robots.txt.";
$MESS ['ASD_ROBOTS_PARTNER_NAME'] = "Долганин Антон";
$MESS ['ASD_ROBOTS_NEED_RIGHT_VER'] = "Для установки данного решения необходима версия главного модуля #NEED# или выше.";
$MESS ['ASD_ROBOTS_NEED_MODULES'] = "Для установки данного решения необходимо наличие модуля #MODULE#.";