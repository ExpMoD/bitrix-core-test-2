<?php

class CASDRobots {

	public static function BeforeIndex($arFields) {
		if ($arFields['MODULE_ID'] != 'main') {
			if (strlen($arFields['URL'])) {//iblock and other
				$url = self::BuildURL($arFields);
				if (substr($url, 0, 1)!='/' && substr($url, 0, 4)!='http') {
					$url = '/'.$url;
				}
				if (CSearch::CheckPath($url) === false) {
					$arFields['TITLE'] = $arFields['BODY'] = '';
				}
			} elseif (is_array($arFields['SITE_ID'])) {//forum, blogs
				foreach ($arFields['SITE_ID'] as $url) {
					if (CSearch::CheckPath($url) === false) {
						$arFields['TITLE'] = $arFields['BODY'] = '';
						break;
					}
				}
			}
		}
		return $arFields;
	}

	public static function BuildURL($arFields) {
		static $index = false;
		static $arEvents = false;
		$url = $arFields['URL'];

		if (!is_array($arFields) || empty($arFields)) {
			return $url;
		}
		if ($arEvents === false) {
			$arEvents = array();
			$events = GetModuleEvents('search', 'OnSearchGetURL');
			while ($arEvent = $events->Fetch()) {
				$arEvents[] = $arEvent;
			}
		}

		if (strlen($arFields['SITE_URL']) > 0) {
			$url = $arFields['SITE_URL'];
		}
		if (substr($url, 0, 1) == '=') {
			foreach($arEvents as $arEvent) {
				$url = ExecuteModuleEventEx($arEvent, array($arFields));
			}
		}
		$url = str_replace(
			array('#LANG#', '#SITE_DIR#', '#SERVER_NAME#'),
			array($arFields['DIR'], $arFields['DIR'], $arFields['SERVER_NAME']),
			$url
		);
		$url = preg_replace("'(?<!:)/+'s", "/", $url);
		if (defined('BX_DISABLE_INDEX_PAGE') && BX_DISABLE_INDEX_PAGE) {
			if (!$index) {
				$index = '#/('.str_replace(' ', '|', preg_quote(implode(' ', GetDirIndexArray()), '#')).')$#';
			}
			$url = preg_replace($index, '/', $url);
		}
		if (($p = strpos($url, '#')) !== false) {
			$url = substr($url, 0, $p);
		}
		if(!preg_match("/^[a-z]+:\\/\\//", $url)) {
			$url = $arFields['SERVER_NAME'].$url;
		}
		$url = CAllSiteMap::LocationEncode(CAllSiteMap::URLEncode($url, 'UTF-8'));
		return $url;
	}

	public static function OnAfterUnRegisterModule($id) {
		if ($id == 'search') {
			include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/asd.robots/install/index.php');
			$module = new asd_robots();
			$module->bNotOutput = true;
			$module->DoUninstall();
		}
	}
}