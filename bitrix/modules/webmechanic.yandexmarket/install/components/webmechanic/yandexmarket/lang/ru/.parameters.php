<?

$MESS['WM_YANDEXMARKET_YANDEX_AUTHORIZATION'] = "Ключ авторизации API Yandex.Маркет";
$MESS['WM_YANDEXMARKET_MARKET_ID'] = "Идентификатор магазина в API Yandex.Маркет";
$MESS['WM_YANDEXMARKET_GOOD_ID'] = "Идентификатор товара в API Yandex.Маркет";

$MESS['WM_YANDEXMARKET_COUNT'] = "Количество отзывов";
$MESS['WM_YANDEXMARKET_SORT_NAME'] = "Тип сортировки отзывов";
    $MESS['WM_YANDEXMARKET_SORT_date'] = "сортировка по дате написания отзыва";
    $MESS['WM_YANDEXMARKET_SORT_grade'] = "сортировка по оценке пользователем магазина";
    $MESS['WM_YANDEXMARKET_SORT_rank'] = "сортировка по полезности отзыва";
$MESS['WM_YANDEXMARKET_SORT_TYPE'] = "Направление сортировки отзывов";    
    $MESS['WM_YANDEXMARKET_SORT_TYPE_asc'] = "сортировка по возрастанию";    
    $MESS['WM_YANDEXMARKET_SORT_TYPE_desc'] = "сортировка по убыванию";    

$MESS['WM_YANDEXMARKET_LINK_JQUERY'] = "Подключить библиотеку jQuery";
$MESS['WM_YANDEXMARKET_LINK_JCAROUSEL'] = "Подключить библиотеку jCarousel";

?>