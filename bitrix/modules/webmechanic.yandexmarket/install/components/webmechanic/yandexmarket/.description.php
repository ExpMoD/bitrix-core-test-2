<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage('WM_YANDEXMARKET_COMPONENT_NAME'),
	"DESCRIPTION" => GetMessage('WM_YANDEXMARKET_COMPONENT_DESCRIPTION'),
	"ICON" => "/images/icon.png",
        "CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => GetMessage("WM_YANDEXMARKET_COMPONENT_ID"),
		"NAME" => GetMessage("WM_YANDEXMARKET_COMPONENT_PATHNAME"),
	)	
);

?>