<?
//Demo data in the absence of an Yandex authorization key
$result_demo = array();

$result_demo['opinion'] = array(
    
    0 => array(
        'date' => time() - (86400 * rand (0,100)),
        'grade' => rand(0,2),
        'delivery' => 'DELIVERY',
        'author' => 'Lorem Ipsum',
        'authorInfo' => array ('grades' => rand(0,100)),
        'region_name' => 'Neque porro',        
        'pro' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quis eros sem. Ut a gravida arcu. Pellentesque non magna a diam fermentum ornare. Aenean augue enim, tincidunt ut elit bibendum, luctus dictum ex. Suspendisse mi purus, scelerisque vel mattis eu, faucibus eget leo. In bibendum gravida placerat. Nunc orci enim, volutpat nec diam ut, ornare iaculis ex. Maecenas interdum nulla et ante posuere posuere. Nullam vel sapien vitae nisl eleifend cursus sed eu augue. Aliquam blandit elementum feugiat. Nunc vestibulum mi vel elementum sollicitudin.',
        'contra' => 'Nulla laoreet ex ac nulla pharetra eleifend. Duis vel tempus dui, eget pretium tellus. Nullam convallis erat ac sollicitudin consequat. Donec maximus imperdiet mollis. Mauris sed lectus ac libero iaculis sodales.',
        'text' => 'Vestibulum sodales pulvinar nulla eget pulvinar. Aenean vestibulum ex eget pulvinar congue. Phasellus consequat magna sit amet bibendum ultricies. Maecenas laoreet sem vel euismod gravida. Proin quam enim, fringilla ut iaculis id, mollis posuere nisl.',        
    ),
    
    1 => array(
        'date' => time() - (86400 * rand (0,100)),
        'grade' => rand(0,2),
        'delivery' => 'PICKUP',
        'author' => 'Lorem Ipsum',
        'authorInfo' => array ('grades' => rand(0,100)),
        'region_name' => 'Neque porro',        
        'pro' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quis eros sem. Ut a gravida arcu. Pellentesque non magna a diam fermentum ornare. Aenean augue enim, tincidunt ut elit bibendum, luctus dictum ex. Suspendisse mi purus, scelerisque vel mattis eu, faucibus eget leo. In bibendum gravida placerat. Nunc orci enim, volutpat nec diam ut, ornare iaculis ex. Maecenas interdum nulla et ante posuere posuere. Nullam vel sapien vitae nisl eleifend cursus sed eu augue. Aliquam blandit elementum feugiat. Nunc vestibulum mi vel elementum sollicitudin.',
        'contra' => 'Nulla laoreet ex ac nulla pharetra eleifend. Duis vel tempus dui, eget pretium tellus. Nullam convallis erat ac sollicitudin consequat. Donec maximus imperdiet mollis. Mauris sed lectus ac libero iaculis sodales.',
        'text' => 'Vestibulum sodales pulvinar nulla eget pulvinar. Aenean vestibulum ex eget pulvinar congue. Phasellus consequat magna sit amet bibendum ultricies. Maecenas laoreet sem vel euismod gravida. Proin quam enim, fringilla ut iaculis id, mollis posuere nisl.',        
    ),
    
    2 => array(
        'date' => time() - (86400 * rand (0,100)),
        'grade' => rand(0,2),
        'delivery' => 'INSTORE',
        'author' => 'Lorem Ipsum',
        'authorInfo' => array ('grades' => rand(0,100)),
        'region_name' => 'Neque porro',        
        'pro' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quis eros sem. Ut a gravida arcu. Pellentesque non magna a diam fermentum ornare. Aenean augue enim, tincidunt ut elit bibendum, luctus dictum ex. Suspendisse mi purus, scelerisque vel mattis eu, faucibus eget leo. In bibendum gravida placerat. Nunc orci enim, volutpat nec diam ut, ornare iaculis ex. Maecenas interdum nulla et ante posuere posuere. Nullam vel sapien vitae nisl eleifend cursus sed eu augue. Aliquam blandit elementum feugiat. Nunc vestibulum mi vel elementum sollicitudin.',
        'contra' => 'Nulla laoreet ex ac nulla pharetra eleifend. Duis vel tempus dui, eget pretium tellus. Nullam convallis erat ac sollicitudin consequat. Donec maximus imperdiet mollis. Mauris sed lectus ac libero iaculis sodales.',
        'text' => '�������� ��������� �� ������� ����� Vestibulum sodales pulvinar nulla eget pulvinar. Aenean vestibulum ex eget pulvinar congue. Phasellus consequat magna sit amet bibendum ultricies. Maecenas laoreet sem vel euismod gravida. Proin quam enim, fringilla ut iaculis id, mollis posuere nisl.',        
    )
        
);

?>