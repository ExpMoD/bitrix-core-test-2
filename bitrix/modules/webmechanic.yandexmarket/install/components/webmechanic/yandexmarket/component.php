<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arCodePages = array ("windows-1251" => "CP1251", "UTF-8" => "UTF-8");
$arRecodingFields = array('author', 'region_name', 'pro', 'contra', 'text');
$arResult = array();
$result = array();

if ($this->StartResultCache()) {

    $arParams['COUNT'] = intval($arParams['COUNT']);
    if ($arParams['COUNT'] > 30)
        $arParams['COUNT'] = 30;

    $mode_ops = 'shopOpinions';

    $arParams['MARKET_ID'] = intval($arParams['MARKET_ID']);
    if ($arParams['MARKET_ID']) {
        //Reviews about shop
        $json_url = "https://api.content.market.yandex.ru/v1/shop/" . $arParams['MARKET_ID'] . "/opinion.json?sort=" . $arParams['SORT_NAME'] . "&how=" . $arParams['SORT_TYPE'] . "&count=" . $arParams['COUNT'] . "";
        $arResult['MORE_LINK'] = "http://market.yandex.ru/shop/" . $arParams['MARKET_ID'] . "/reviews/";
        $mode_ops = 'shopOpinions';
    };

    $arParams['GOOD_ID'] = intval($arParams['GOOD_ID']);
    if ($arParams['GOOD_ID']) {
        //Reviews about good
        $json_url = "https://api.content.market.yandex.ru/v1/model/" . $arParams['GOOD_ID'] . "/opinion.json?sort=" . $arParams['SORT_NAME'] . "&how=" . $arParams['SORT_TYPE'] . "&count=" . $arParams['COUNT'] . "";
        $arResult['MORE_LINK'] = "http://market.yandex.ru/product/" . $arParams['GOOD_ID'] . "/reviews/";
        $mode_ops = 'modelOpinions';
    };

    $arResult['MODE'] = $mode_ops;

    //If there is an Yanfdex Authorization key, theg try to link Yandex.Market API
    if ($arParams['YANDEX_AUTHORIZATION'] && $json_url) {
        $ch = curl_init($json_url);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (��� ��� ���..) "); 

        $headers = array('Authorization: ' . $arParams['YANDEX_AUTHORIZATION']);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        # ��������� ���������� � ������ �������. ���� ��������� �� ���������
        //curl_setopt($ch, CURLOPT_REFERER, "http://php.su/forum/loginout.php");
        # ����������� �������� - ������ ������ ������.
        //curl_setopt($ch, CURLOPT_POSTFIELDS, 'action=login&imembername=valenok&ipassword=ne_skaju&submit=%C2%F5%EE%E4');
        # post ������.
        # ����� libcurl ���� ������� ���������
        # Content-Type: application/x-www-form-urlencoded � Content-Length: 71

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        # ������� ����� ������ � �������. ����� ������� �� ���������� � �� �������

        $result = curl_exec($ch); // ��������� ������ curl
        curl_close($ch);
    }
    
    //Else load our demo data
    else
    {
        include_once('demo_result.php');                       
        
        //Recoding demo data to UTF-8, if necessary
        if ($arCodePages[LANG_CHARSET] != 'UTF-8') {
            foreach ($result_demo['opinion'] as &$arRes) {
                foreach ($arRecodingFields as $arField) {
                    $arRes[$arField] = iconv($arCodePages[LANG_CHARSET], "UTF-8", $arRes[$arField]);
                };
                unset($arRes);
            };
        };

        $result[$mode_ops] = $result_demo;        
        $result = json_encode($result);
        $arResult_items = json_decode($result, TRUE);
        $arResult['ITEMS'] = $arResult_items[$mode_ops]['opinion'];
    };

    $arResult_items = json_decode($result, TRUE);

    if (is_array($arResult_items['errors']))
        $arResult['ERROR'] = implode('; ', $arResult_items['errors']);       

    $arResult['ITEMS'] = $arResult_items[$mode_ops]['opinion'];

    if (is_array($arResult['ITEMS']))
        foreach ($arResult['ITEMS'] as &$arItem):

            $arItem['date'] = date("d.m.Y", mb_substr($arItem['date'], 0, 10));

            $arItem['grade'] = $arItem['grade'] + 3;

            if ($arParams['MARKET_ID']) {
                $arItem['grade_text'] = GetMessage('WM_YANDEXMARKET_STORE_' . $arItem['grade']);
            };

            if ($arParams['GOOD_ID']) {
                $arItem['grade_text'] = GetMessage('WM_YANDEXMARKET_MODEL_' . $arItem['grade']);
            };

            $arItem['delivery'] = GetMessage('WM_YANDEXMARKET_DLVR_' . $arItem['delivery']);                        
            
            //Recoding issuing comments from UTF-8, if necessary
            if ($arCodePages[LANG_CHARSET] != 'UTF-8')
                {           
                    foreach($arRecodingFields as $arField) {
                        $arItem[$arField] = iconv ("UTF-8", $arCodePages[LANG_CHARSET], $arItem[$arField]);
                    };                
                };   
            
            unset($arItem);
        endforeach;

    $arResult['COMPONENT_PATH'] = $componentPath;

    $this->IncludeComponentTemplate();
};

return $arResult;

//Getting the correct end of a noun depending on the number
function webmechanic_yandexmarket_get_correct_str($num, $str1, $str2, $str3) {
    $val = $num % 100;

    if ($val > 10 && $val < 20)
        return $num . ' ' . $str3;
    else {
        $val = $num % 10;
        if ($val == 1)
            return $num . ' ' . $str1;
        elseif ($val > 1 && $val < 5)
            return $num . ' ' . $str2;
        else
            return $num . ' ' . $str3;
    }
}

?>