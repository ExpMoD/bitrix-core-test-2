<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
    
    "GROUPS" => array(),               

    "PARAMETERS" => array(              
                       
        "YANDEX_AUTHORIZATION" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("WM_YANDEXMARKET_YANDEX_AUTHORIZATION"),
            "TYPE" => "STRING"            
        ),
        "MARKET_ID" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("WM_YANDEXMARKET_MARKET_ID"),
            "TYPE" => "STRING"            
        ),
        "GOOD_ID" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("WM_YANDEXMARKET_GOOD_ID"),
            "TYPE" => "STRING"            
        ),
        
        "COUNT" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("WM_YANDEXMARKET_COUNT"),
            "TYPE" => "STRING",
            "DEFAULT" => 10
        ),
        "SORT_NAME" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("WM_YANDEXMARKET_SORT_NAME"),
            "TYPE" => "LIST",
            "VALUES" => array(  "date" => GetMessage("WM_YANDEXMARKET_SORT_date"),
                                "grade" => GetMessage("WM_YANDEXMARKET_SORT_grade"),
                                "rank" => GetMessage("WM_YANDEXMARKET_SORT_rank")
                             ),
            "DEFAULT" => 'date'
        ),
        "SORT_TYPE" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("WM_YANDEXMARKET_SORT_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => array(  "desc" => GetMessage("WM_YANDEXMARKET_SORT_TYPE_desc"),
                                "asc" => GetMessage("WM_YANDEXMARKET_SORT_TYPE_asc")                                
                             ),
            "DEFAULT" => 'desc'            
        ),
        
        "LINK_JQUERY" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("WM_YANDEXMARKET_LINK_JQUERY"),
            "TYPE" => "CHECKBOX"            
        ),
        "LINK_JCAROUSEL" => array(
            "PARENT" => "VISUAL",
            "NAME" => GetMessage("WM_YANDEXMARKET_LINK_JCAROUSEL"),
            "TYPE" => "CHECKBOX" 
        ),                       
                
    "CACHE_TYPE" => array(),
    "CACHE_TIME" => array()    
    )    
        
);

?>