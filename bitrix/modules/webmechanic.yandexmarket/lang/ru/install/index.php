<?
$MESS["webmechanic.yandexmarket_MODULE_NAME"] = "Отзывы о магазине или товаре с Yandex.Маркет";
$MESS["webmechanic.yandexmarket_MODULE_DESC"] = "Компонент отображает отзывы о магазине или товаре (по коду) с Yandex.Маркет по ключу доступа к контентому API.";
$MESS["webmechanic.yandexmarket_PARTNER_NAME"] = "РА «Webmechanic»";
$MESS["webmechanic.yandexmarket_PARTNER_URI"] = "http://studio.webmechanic.ru";
?>