<?
use Bitrix\Main\Config\Option,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/options.php');

$module_id = 'bitrix.map';
Bitrix\Main\Loader::includeModule($module_id);

CJSCore::Init(array('jquery'));
$APPLICATION->AddHeadScript("/bitrix/js/bitrix.map/jscolor/jscolor.js");
$APPLICATION->AddHeadScript("/bitrix/js/bitrix.map/admin.js");
$APPLICATION->AddHeadString("<script src=\"/bitrix/js/bitrix.map/geocoder_cond.js\" charset=\"utf-8\"></script>");

$MOD_RIGHT = $APPLICATION->GetGroupRight($module_id);
if($MOD_RIGHT>='R')
{
    if (Bitrix\Main\Loader::includeModule("iblock"))
    {
        $siteList = array();
        $rsSites = CSite::GetList($by="sort", $order="asc", Array());
        while($arRes = $rsSites->Fetch())
        {
            $siteList[$arRes["ID"]] = $arRes["NAME"];
        }

        $versionsList = array(
            "desktop" => Loc::getMessage("BITRIX_MAP_V_DESKTOP"),
            "mobile"  => Loc::getMessage("BITRIX_MAP_V_MOBILE"),
        );

        $iblockList = array();
        $rsIBlocks = Bitrix\Iblock\IblockTable::getList(array(
            "select" => array("ID", "NAME"),
        ));
        while($arRes = $rsIBlocks->Fetch())
        {
            $iblockList[$arRes["ID"]] = $arRes["NAME"];
        }

        $obDataMixer = new \Bitrix\InteractiveMap\DataMixer();

        $arAllOptions = array();
        $arIntOptions = array();

        $arAllOptions["main"][] = Loc::getMessage('BITRIX_MAP_DEF_SETTINGS');
        $arAllOptions["main"][] = Array('def_map_height', Loc::getMessage('BITRIX_MAP_HEIGHT') . " <sup>1</sup>", '', Array('text', 4));
        $arAllOptions["main"][] = Array('def_bar_height', Loc::getMessage('BITRIX_BAR_HEIGHT') . " <sup>2</sup>", '', Array('text', 4));
        $arAllOptions["main"][] = Array('def_plate_height', Loc::getMessage('BITRIX_PLATE_HEIGHT') . " <sup>2</sup>", '', Array('text', 4));
        $arAllOptions["main"][] = Array('def_few_objects_height', Loc::getMessage('BITRIX_FEW_OBJECTS_HEIGHT') . " <sup>2</sup>", '', Array('text', 4));

        $arAllOptions["main"][] = Loc::getMessage('BITRIX_MAP_ANIMATION');
        $arAllOptions["main"][] = Array('animation_time', Loc::getMessage('BITRIX_MAP_ANIMATION_TIME') . " <sup>1</sup>", '', Array('text', 4));
        $arAllOptions["main"][] = Array('item_time', Loc::getMessage('BITRIX_MAP_ITEM_TIME') . " <sup>2</sup>", '', Array('text', 4));
        $arAllOptions["main"][] = Array('list_time', Loc::getMessage('BITRIX_BAR_LIST_TIME') . " <sup>2</sup>", '', Array('text', 4));

        $arAllOptions["main"][] = Loc::getMessage('BITRIX_MAP_SCRIPTS_LOAD');
        $arAllOptions["main"][] = Array('response_time', Loc::getMessage('BITRIX_MAP_RESPONSE_TIME') . " <sup>1,2</sup>", '', Array('text', 4));
        $arAllOptions["main"][] = Array('load_time', Loc::getMessage('BITRIX_MAP_LOAD_TIME') . " <sup>1,2</sup>", '', Array('text', 4));
        $arAllOptions["main"][] = Array('without_map', Loc::getMessage('BITRIX_MAP_WITHOUT_MAP') . " <sup>2</sup>", '', Array('checkbox'));
        $arAllOptions["main"][] = Array('note' => Loc::getMessage('BITRIX_MAP_WITHOUT_MAP_DESCR'));

        $arAllOptions["main"][] = Loc::getMessage('BITRIX_MAP_ROUTE_TYPE_YANDEX');
        $arAllOptions["main"][] = Array('route_type_yandex_driving', Loc::getMessage('BITRIX_MAP_ROUTE_TYPE_DRIVING'), '', Array('checkbox'), 'Y');

        $arAllOptions["main"][] = Loc::getMessage('BITRIX_MAP_ROUTE_TYPE_GOOGLE');
        $arAllOptions["main"][] = Array('route_type_google_driving', Loc::getMessage('BITRIX_MAP_ROUTE_TYPE_DRIVING'), '', Array('checkbox'), 'Y');
        $arAllOptions["main"][] = Array('route_type_google_walking', Loc::getMessage('BITRIX_MAP_ROUTE_TYPE_WALKING'), '', Array('checkbox'), 'Y');
        $arAllOptions["main"][] = Array('route_type_google_transit', Loc::getMessage('BITRIX_MAP_ROUTE_TYPE_TRANSIT'), '', Array('checkbox'));
        $arAllOptions["main"][] = Array('route_type_google_bicycle', Loc::getMessage('BITRIX_MAP_ROUTE_TYPE_BYCICLE'), '', Array('checkbox'));

        $arAllOptions["interface"][] = Array('different_int_settings', Loc::getMessage('BITRIX_MAP_DIFFERENT_INT_SETTINGS'), 'N', Array('checkbox'));
        $arAllOptions["interface"][] = Array('site_id_settings', Loc::getMessage('BITRIX_MAP_SITE_ID_SETTINGS'), '', Array('selectbox', $siteList));

        $arIntOptions[] = Loc::getMessage('BITRIX_MAP_INT_MAIN');
        $obDataMixer->BuildInterfacePropertiesList($arIntOptions, "main");

        $arIntOptions[] = Loc::getMessage('BITRIX_MAP_INT_ROUTES');
        $obDataMixer->BuildInterfacePropertiesList($arIntOptions, "routes");

        $arIntOptions[] = Loc::getMessage('BITRIX_MAP_INT_ERRORS');
        $obDataMixer->BuildInterfacePropertiesList($arIntOptions, "errors");
        $arAllOptions["icons"][] = Array('version_settings', Loc::getMessage('BITRIX_MAP_VERSION'), '', Array('selectbox', $versionsList));

        $version_settings = "desktop";
        $opacityList = array();
        for ($i = 0.1; $i <= 1; $i+=0.1)
        {
            $opacityList[$i.""] = ($i * 100) . " %";
        }
        $weightList = array();
        for ($i = 1; $i <= 15; $i++)
        {
            $weightList[$i] = $i;
        }

        foreach ($versionsList as $versionId => $versionName)
        {
            $arAllOptions["cluster_" . $versionId][] = Loc::getMessage('BITRIX_MAP_CLUSTER') . " (" . $versionName . ")";
            $arAllOptions["cluster_" . $versionId][] = Array('cluster_grid_size_' . $versionId, Loc::getMessage('BITRIX_MAP_CLUSTER_GRID_SIZE'), '', Array('text', 8));
            $arAllOptions["cluster_" . $versionId][] = Array('cluster_color_' . $versionId, Loc::getMessage('BITRIX_MAP_CLUSTER_COLOR'), '', Array('text', 8));

            $arAllOptions["cluster__" . $versionId][] = Array('cluster_size1_' . $versionId, "", '', Array('text', 8));
            $arAllOptions["cluster__" . $versionId][] = Array('cluster_size2_' . $versionId, "", '', Array('text', 8));
            $arAllOptions["cluster__" . $versionId][] = Array('cluster_size3_' . $versionId, "", '', Array('text', 8));
            $arAllOptions["cluster__" . $versionId][] = Array('cluster_size4_' . $versionId, "", '', Array('text', 8));
            $arAllOptions["cluster__" . $versionId][] = Array('cluster_anchor_x_' . $versionId, "", '', Array('text', 8));
            $arAllOptions["cluster__" . $versionId][] = Array('cluster_anchor_y_' . $versionId, "", '', Array('text', 8));

            $arAllOptions["direction_opt_" . $versionId][] = Array('dir_stroke_weight_' . $versionId, Loc::getMessage('BITRIX_MAP_STROKE_WEIGHT'), '', Array('selectbox', $weightList));
            $arAllOptions["direction_opt_" . $versionId][] = Array('dir_stroke_color_' . $versionId, Loc::getMessage('BITRIX_MAP_STROKE_COLOR'), '', Array('text', 8));
            $arAllOptions["direction_opt_" . $versionId][] = Array('dir_stroke_opacity_' . $versionId, Loc::getMessage('BITRIX_MAP_STROKE_OPACITY'), '', Array('selectbox', $opacityList));
            if ($versionId == "desktop")
            {
                $arAllOptions["direction_opt_" . $versionId][] = Array('dir_stroke_opacity_hover_' . $versionId, Loc::getMessage('BITRIX_MAP_STROKE_OPACITY_HOVER'), '', Array('selectbox', $opacityList));
                $arAllOptions["direction_opt_" . $versionId][] = Array('dir_stroke_color_active_' . $versionId, Loc::getMessage('BITRIX_MAP_STROKE_COLOR_ACTIVE'), '', Array('text', 8));
            }

            $arAllOptions["icon_" . $versionId][] = Array('objects_icon_size_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('objects_icon_size_y_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('objects_icon_anchor_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('objects_icon_anchor_y_' . $versionId, '', '', Array('text', 8));
            if ($versionId == "desktop")
            {
                $arAllOptions["icon_" . $versionId][] = Array('objects_icon_logo_x_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('objects_icon_logo_y_' . $versionId, '', '', Array('text', 8));
            }

            $arAllOptions["icon_" . $versionId][] = Array('events_icon_size_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('events_icon_size_y_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('events_icon_anchor_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('events_icon_anchor_y_' . $versionId, '', '', Array('text', 8));
            if ($versionId == "desktop")
            {
                $arAllOptions["icon_" . $versionId][] = Array('events_icon_logo_x_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('events_icon_logo_y_' . $versionId, '', '', Array('text', 8));
            }

            $arAllOptions["icon_" . $versionId][] = Array('routes_icon_size_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('routes_icon_size_y_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('routes_icon_anchor_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('routes_icon_anchor_y_' . $versionId, '', '', Array('text', 8));
            if ($versionId == "desktop")
            {
                $arAllOptions["icon_" . $versionId][] = Array('routes_icon_logo_x_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('routes_icon_logo_y_' . $versionId, '', '', Array('text', 8));
            }

            if ($versionId == "mobile")
            {
                $arAllOptions["icon_" . $versionId][] = Array('categories_icon_size_x_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('categories_icon_size_y_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('categories_icon_anchor_x_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('categories_icon_anchor_y_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('categories_icon_scale_' . $versionId, '', '', Array('text', 8));

                $arAllOptions["icon_" . $versionId][] = Array('direction_icon_size_x_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('direction_icon_size_y_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('direction_icon_anchor_x_' . $versionId, '', '', Array('text', 8));
                $arAllOptions["icon_" . $versionId][] = Array('direction_icon_anchor_y_' . $versionId, '', '', Array('text', 8));
            }


            $arAllOptions["icon_" . $versionId][] = Array('path_def_size_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('path_def_size_y_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('path_def_anchor_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('path_def_anchor_y_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('path_def_offset_x_' . $versionId, '', '', Array('text', 8));
            $arAllOptions["icon_" . $versionId][] = Array('path_def_offset_y_' . $versionId, '', '', Array('text', 8));

            $arAllOptions["path_stroke_" . $versionId][] = Array('path_stroke_weight_' . $versionId, Loc::getMessage("BITRIX_MAP_STROKE_WEIGHT"), '', Array('selectbox', $weightList));
            $arAllOptions["path_stroke_" . $versionId][] = Array('path_stroke_color_' . $versionId, Loc::getMessage("BITRIX_MAP_STROKE_COLOR"), '', Array('text', 8));
            $arAllOptions["path_stroke_" . $versionId][] = Array('path_stroke_opacity_' . $versionId, Loc::getMessage("BITRIX_MAP_STROKE_OPACITY"), '', Array('selectbox', $opacityList));
            if ($versionId == "desktop")
            {
                $arAllOptions["path_stroke_" . $versionId][] = Array('path_stroke_opacity_hover_' . $versionId, Loc::getMessage("BITRIX_MAP_STROKE_OPACITY_HOVER"), '', Array('selectbox', $opacityList));
                $arAllOptions["path_stroke_" . $versionId][] = Array('path_stroke_color_active_' . $versionId, Loc::getMessage("BITRIX_MAP_STROKE_COLOR_ACTIVE"), '', Array('text', 8));
            }
        }

        if ($MOD_RIGHT>='Y' || $USER->IsAdmin()) {

            if ($REQUEST_METHOD=='GET' && strlen($RestoreDefaults)>0 && check_bitrix_sessid())
            {
                $iconsResult = $obDataMixer->ClearIcons();
                Option::delete($module_id);
                $z = CGroup::GetList($v1='id',$v2='asc', array('ACTIVE' => 'Y', 'ADMIN' => 'N'));
                while($zr = $z->Fetch())
                {
                    $APPLICATION->DelGroupRight($module_id, array($zr['ID']));
                }
            }

            if($REQUEST_METHOD=='POST' && strlen($Update)>0 && check_bitrix_sessid())
            {
                $arExcludeOption = array("site_id_settings", "route_type_yandex_driving", "route_type_google_driving", "route_type_google_walking");

                $arOptions = $arAllOptions;

                foreach($arOptions as $arOptionSection)
                {
                    foreach($arOptionSection as $option)
                    {
                        if(!is_array($option) || isset($option['note']) || in_array($option[0], $arExcludeOption))
                            continue;

                        $name = $option[0];
                        $val = ${$name};
                        if($option[3][0] == 'checkbox' && $val != 'Y')
                            $val = 'N';
                        if($option[3][0] == 'multiselectbox')
                            $val = @implode(',', $val);

                        Option::set($module_id, $name, $val);
                    }
                }

                $arOptions = $arIntOptions;

                foreach($arOptions as $option)
                {
                    if(!is_array($option) || isset($option['note']))
                        continue;

                    $name = $option[0];
                    $val = ${$name};

                    Option::set($module_id, $name, $val["common"]);
                }

                if ($different_int_settings == "Y")
                {
                    foreach ($siteList as $siteCode => $siteName)
                    {
                        foreach($arOptions as $option)
                        {
                            if(!is_array($option) || isset($option['note']))
                                continue;

                            $name = $option[0];
                            $val = ${$name};

                            Option::set($module_id, $name, $val[$siteCode], $siteCode);
                        }
                    }
                }
                else
                {
                    foreach ($siteList as $siteCode => $siteName)
                    {
                        Option::delete($module_id, array("site_id" => $siteCode));
                    }
                }


                $iconsResult = $obDataMixer->SaveIcons();
                if ($iconsResult !== true)
                {
                    CAdminMessage::ShowMessage(array(
                        "TYPE" => "ERROR",
                        "MESSAGE" => Loc::getMessage("BITRIX_MAP_FILES_SAVE_ERROR"),
                        "DETAILS" => implode("<br>", $iconsResult),
                        "HTML" => true,
                    ));
                }

                $geocoderResult = $obDataMixer->SaveGeocoderConditions();
                if ($geocoderResult !== true)
                {
                    CAdminMessage::ShowMessage(array(
                        "TYPE" => "ERROR",
                        "MESSAGE" => Loc::getMessage("BITRIX_MAP_GEOCODER_SAVE_ERROR"),
                        "DETAILS" => implode("<br>", $geocoderResult),
                        "HTML" => true,
                    ));
                }

                $obDataMixer->ClearComponentsCache();
            }

        }

        $aTabs = array();
        $aTabs[] = array('DIV' => 'set', 'TAB' => Loc::getMessage('MAIN_TAB_SET'), 'ICON' => 'map_settings', 'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_SET'));
        $aTabs[] = array('DIV' => 'interface', 'TAB' => Loc::getMessage('MAIN_TAB_INTERFACE'), 'ICON' => 'map_settings', 'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_INTERFACE'));
        $aTabs[] = array('DIV' => 'icon', 'TAB' => Loc::getMessage('MAIN_TAB_ICONS'), 'ICON' => 'map_settings', 'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_ICONS'));
        $aTabs[] = array('DIV' => 'geocoder', 'TAB' => Loc::getMessage('MAIN_TAB_GEOCODER'), 'ICON' => 'map_settings', 'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_GEOCODER'));
        $aTabs[] = array('DIV' => 'rights', 'TAB' => Loc::getMessage('MAIN_TAB_RIGHTS'), 'ICON' => 'map_settings', 'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_RIGHTS'));

        $diffSettings = COption::GetOptionString($module_id, "different_int_settings") == "Y";
        $diffSettingsSite = COption::GetOptionString($module_id, "site_id_settings");

        $tabControl = new CAdminTabControl('tabControl', $aTabs);
        $tabControl->Begin();
        ?>
        <form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&lang=<?=LANGUAGE_ID?>" name="bitrix_map_settings" enctype="multipart/form-data">

        <?$tabControl->BeginNextTab();?>

        <?__AdmSettingsDrawList($module_id, $arAllOptions["main"]);?>
        <tr class="bx-map-table-divider">
            <td colspan="2">
                <small><sup>1</sup> &mdash; <?=Loc::getMessage('BITRIX_MAP_DESKTOP_ONLY')?><br/><sup>2</sup> &mdash; <?=Loc::getMessage('BITRIX_MAP_MOBILE_ONLY')?></small>
            </td>
        </tr>

        <?$tabControl->BeginNextTab();?>

        <?__AdmSettingsDrawList($module_id, $arAllOptions["interface"]);?>

        <tr class="lang-setting" id="lang-setting-common"<?echo $diffSettings ? " style='display:none'" : "" ?>>
            <td colspan="2">
                <table cellspacing="4" cellpadding="0" width="100%">
                    <? foreach ($arIntOptions as $mxOption) { ?>
                        <? if (!is_array($mxOption)) { ?>
                            <tr class="heading">
                                <td colspan="2"><?=$mxOption?></td>
                            </tr>
                        <? } else { ?>
                            <tr>
                                <td width="50%" class="adm-detail-content-cell-l"><?=$mxOption[1]?></td>
                                <td width="50%" class="adm-detail-content-cell-r"><input type="text" size="<?=$mxOption[3][1]?>" maxlength="255" value="<?=htmlspecialcharsbx(Option::get($module_id, $mxOption[0], ''))?>" name="<?=$mxOption[0]?>[common]"></td>
                            </tr>
                        <? } ?>
                    <? } ?>
                </table>
            </td>
        </tr>

        <? $i = 0; foreach ($siteList as $siteId => $siteName) { ?>
            <tr class="lang-setting" id="lang-setting-<?=$siteId?>"<?echo $diffSettings ? ($i++ == 0 ? "" : " style='display:none'") : " style='display:none'" ?>>
                <td colspan="2">
                    <table cellspacing="4" cellpadding="0" width="100%">
                        <? foreach ($arIntOptions as $mxOption) { ?>
                            <? if (!is_array($mxOption)) { ?>
                                <tr class="heading">
                                    <td colspan="2"><?=$mxOption?> (<?=$siteName?>)</td>
                                </tr>
                            <? } else { ?>
                                <tr>
                                    <td width="50%" class="adm-detail-content-cell-l"><?=$mxOption[1]?></td>
                                    <td width="50%" class="adm-detail-content-cell-r"><input type="text" size="<?=$mxOption[3][1]?>" maxlength="255" value="<?=htmlspecialcharsbx(Option::get($module_id, $mxOption[0], '', $siteId))?>" name="<?=$mxOption[0]?>[<?=$siteId?>]"></td>
                                </tr>
                            <? } ?>
                        <? } ?>
                    </table>
                </td>
            </tr>
        <? } ?>

        <?$tabControl->BeginNextTab();?>

        <?__AdmSettingsDrawList($module_id, $arAllOptions["icons"]);?>

        <? foreach ($versionsList as $versionId => $versionName) { ?>
            <tr class="version-setting" id="version-setting-<?=$versionId?>"<?echo $versionId == "desktop" ? "" : " style='display:none'" ?>>
            <td colspan="2">
            <table cellspacing="4" cellpadding="0" width="100%">

                <tr class="heading">
                    <td colspan="2"><?=Loc::getMessage('BITRIX_MAP_SPRITES_OBJECTS') . " (" . $versionName . ")"?></td>
                </tr>
                <?
                $iconSizeX = IntVal(Option::get($module_id, "objects_icon_size_x_" . $versionId));
                $iconSizeY = IntVal(Option::get($module_id, "objects_icon_size_y_" . $versionId));
                $iconAnchorX = IntVal(Option::get($module_id, "objects_icon_anchor_x_" . $versionId));
                $iconAnchorY = IntVal(Option::get($module_id, "objects_icon_anchor_y_" . $versionId));
                $iconLogoX = IntVal(Option::get($module_id, "objects_icon_logo_x_" . $versionId));
                $iconLogoY = IntVal(Option::get($module_id, "objects_icon_logo_y_" . $versionId));
                ?>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_SPRITES')?></td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <? $imageId = COption::GetOptionInt($module_id, "objects_icon_" . $versionId, 0); ?>
                        <?=CFile::InputFile("objects_icon_" . $versionId, 20, $imageId, false, 0, "IMAGE", "", 0, "", "", true, false);?>
                        <? if ($imageId > 0) { ?><input type="hidden" name="objects_icon_<?=$versionId?>_del_id" value="<?=Intval($imageId)?>"><? } ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l">&nbsp;</td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <div class="bx-map-crop-image">
                            <? $imageSrc = $obDataMixer->getImageFilename("objects_icon_" . $versionId); echo ShowImage($imageSrc);?>
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="4" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_SIZE')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeX?>" class="bx-map-tiny-input bx_map_objects_icon_changer_<?=$versionId?>" name="objects_icon_size_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeY?>" class="bx-map-tiny-input bx_map_objects_icon_changer_<?=$versionId?>" name="objects_icon_size_y_<?=$versionId?>">
                    </td>
                    <td rowspan="<?=($versionId == "desktop") ? "3" : "2" ?>">
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) 0 -<?=$iconLogoY?>px" class="bx-map-icon-example" id="bx_map_obj_icon_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconSizeX?>px -<?=$iconLogoY?>px" class="bx-map-icon-example" id="bx_map_obj_icon_1_<?=$versionId?>"></div>

                        <div style="width:<?=$iconLogoX?>px;height:<?=$iconLogoY?>px;background:url(<?=$arImage["icon"]?>)" class="bx-map-icon-example" id="bx_map_cat_icon_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconLogoX?>px;height:<?=$iconLogoY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconLogoX?>px 0" class="bx-map-icon-example" id="bx_map_cat_icon_1_<?=$versionId?>"></div>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_ANCHOR')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorX?>" class="bx-map-tiny-input bx_map_objects_icon_changer_<?=$versionId?>" name="objects_icon_anchor_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorY?>" class="bx-map-tiny-input bx_map_objects_icon_changer_<?=$versionId?>" name="objects_icon_anchor_y_<?=$versionId?>">
                    </td>
                </tr>
                <? if ($versionId == "desktop") { ?>
                    <tr>
                        <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_LOGO')?></td>
                        <td width="140" class="adm-detail-content-cell-r">
                            <input type="text" size="3" maxlength="255" value="<?=$iconLogoX?>" class="bx-map-tiny-input bx_map_objects_icon_changer_<?=$versionId?>" name="objects_icon_logo_x_<?=$versionId?>">
                            &times;
                            <input type="text" size="3" maxlength="255" value="<?=$iconLogoY?>" class="bx-map-tiny-input bx_map_objects_icon_changer_<?=$versionId?>" name="objects_icon_logo_y_<?=$versionId?>">
                        </td>
                    </tr>
                <? } ?>
            </table>


            <table cellspacing="4" cellpadding="0" width="100%">
                <?
                $iconSizeX = IntVal(Option::get($module_id, "events_icon_size_x_" . $versionId));
                $iconSizeY = IntVal(Option::get($module_id, "events_icon_size_y_" . $versionId));
                $iconAnchorX = IntVal(Option::get($module_id, "events_icon_anchor_x_" . $versionId));
                $iconAnchorY = IntVal(Option::get($module_id, "events_icon_anchor_y_" . $versionId));
                $iconLogoX = IntVal(Option::get($module_id, "events_icon_logo_x_" . $versionId));
                $iconLogoY = IntVal(Option::get($module_id, "events_icon_logo_y_" . $versionId));
                ?>
                <tr class="heading">
                    <td colspan="2"><?=Loc::getMessage('BITRIX_MAP_SPRITES_EVENTS') . " (" . $versionName . ")"?></td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_SPRITES')?></td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <? $imageId = COption::GetOptionInt($module_id, "events_icon_" . $versionId, 0); ?>
                        <?=CFile::InputFile("events_icon_" . $versionId, 20, $imageId, false, 0, "IMAGE", "", 0, "", "", true, false);?>
                        <? if ($imageId > 0) { ?><input type="hidden" name="events_icon_<?=$versionId?>_del_id" value="<?=Intval($imageId)?>"><? } ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l">&nbsp;</td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <div class="bx-map-crop-image">
                            <? $imageSrc = $obDataMixer->getImageFilename("events_icon_" . $versionId); echo ShowImage($imageSrc);?>
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="4" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_SIZE')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeX?>" class="bx-map-tiny-input  bx_map_events_icon_changer_<?=$versionId?>" name="events_icon_size_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeY?>" class="bx-map-tiny-input  bx_map_events_icon_changer_<?=$versionId?>" name="events_icon_size_y_<?=$versionId?>">
                    </td>
                    <td rowspan="<?=($versionId == "desktop") ? "3" : "2" ?>">
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) 0 -<?=$iconLogoY?>px" class="bx-map-icon-example" id="bx_map_events_icon_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconSizeX?>px -<?=$iconLogoY?>px" class="bx-map-icon-example" id="bx_map_events_icon_1_<?=$versionId?>"></div>

                        <div style="width:<?=$iconLogoX?>px;height:<?=$iconLogoY?>px;background:url(<?=$arImage["icon"]?>)" class="bx-map-icon-example" id="bx_map_events_cat_icon_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconLogoX?>px;height:<?=$iconLogoY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconLogoX?>px 0" class="bx-map-icon-example" id="bx_map_events_cat_icon_1_<?=$versionId?>"></div>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_ANCHOR')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorX?>" class="bx-map-tiny-input  bx_map_events_icon_changer_<?=$versionId?>" name="events_icon_anchor_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorY?>" class="bx-map-tiny-input  bx_map_events_icon_changer_<?=$versionId?>" name="events_icon_anchor_y_<?=$versionId?>">
                    </td>
                </tr>
                <? if ($versionId == "desktop") { ?>
                    <tr>
                        <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_LOGO')?></td>
                        <td width="140" class="adm-detail-content-cell-r">
                            <input type="text" size="3" maxlength="255" value="<?=$iconLogoX?>" class="bx-map-tiny-input  bx_map_events_icon_changer_<?=$versionId?>" name="events_icon_logo_x_<?=$versionId?>">
                            &times;
                            <input type="text" size="3" maxlength="255" value="<?=$iconLogoY?>" class="bx-map-tiny-input  bx_map_events_icon_changer_<?=$versionId?>" name="events_icon_logo_y_<?=$versionId?>">
                        </td>
                    </tr>
                <? } ?>
            </table>


            <table cellspacing="4" cellpadding="0" width="100%">
                <?
                $iconSizeX = IntVal(Option::get($module_id, "routes_icon_size_x_" . $versionId));
                $iconSizeY = IntVal(Option::get($module_id, "routes_icon_size_y_" . $versionId));
                $iconAnchorX = IntVal(Option::get($module_id, "routes_icon_anchor_x_" . $versionId));
                $iconAnchorY = IntVal(Option::get($module_id, "routes_icon_anchor_y_" . $versionId));
                $iconLogoX = IntVal(Option::get($module_id, "routes_icon_logo_x_" . $versionId));
                $iconLogoY = IntVal(Option::get($module_id, "routes_icon_logo_y_" . $versionId));
                ?>
                <tr class="heading">
                    <td colspan="2"><?=Loc::getMessage('BITRIX_MAP_SPRITES_ROUTES') . " (" . $versionName . ")"?></td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_SPRITES')?></td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <? $imageId = COption::GetOptionInt($module_id, "routes_icon_" . $versionId, 0); ?>
                        <?=CFile::InputFile("routes_icon_" . $versionId, 20, $imageId, false, 0, "IMAGE", "", 0, "", "", true, false);?>
                        <? if ($imageId > 0) { ?><input type="hidden" name="routes_icon_<?=$versionId?>_del_id" value="<?=Intval($imageId)?>"><? } ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l">&nbsp;</td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <div class="bx-map-crop-image">
                            <? $imageSrc = $obDataMixer->getImageFilename("routes_icon_" . $versionId); echo ShowImage($imageSrc);?>
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="4" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_SIZE')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeX?>" class="bx-map-tiny-input  bx_map_routes_icon_changer_<?=$versionId?>" name="routes_icon_size_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeY?>" class="bx-map-tiny-input  bx_map_routes_icon_changer_<?=$versionId?>" name="routes_icon_size_y_<?=$versionId?>">
                    </td>
                    <td rowspan="<?=($versionId == "desktop") ? "3" : "2" ?>">
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) 0 -<?=$iconLogoY?>px" class="bx-map-icon-example" id="bx_map_routes_icon_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconSizeX?>px -<?=$iconLogoY?>px" class="bx-map-icon-example" id="bx_map_routes_icon_1_<?=$versionId?>"></div>

                        <div style="width:<?=$iconLogoX?>px;height:<?=$iconLogoY?>px;background:url(<?=$arImage["icon"]?>)" class="bx-map-icon-example" id="bx_map_routes_cat_icon_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconLogoX?>px;height:<?=$iconLogoY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconLogoX?>px 0" class="bx-map-icon-example" id="bx_map_routes_cat_icon_1_<?=$versionId?>"></div>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_ANCHOR')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorX?>" class="bx-map-tiny-input  bx_map_routes_icon_changer_<?=$versionId?>" name="routes_icon_anchor_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorY?>" class="bx-map-tiny-input  bx_map_routes_icon_changer_<?=$versionId?>" name="routes_icon_anchor_y_<?=$versionId?>">
                    </td>
                </tr>
                <? if ($versionId == "desktop") { ?>
                    <tr>
                        <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_LOGO')?></td>
                        <td width="140" class="adm-detail-content-cell-r">
                            <input type="text" size="3" maxlength="255" value="<?=$iconLogoX?>" class="bx-map-tiny-input  bx_map_routes_icon_changer_<?=$versionId?>" name="routes_icon_logo_x_<?=$versionId?>">
                            &times;
                            <input type="text" size="3" maxlength="255" value="<?=$iconLogoY?>" class="bx-map-tiny-input  bx_map_routes_icon_changer_<?=$versionId?>" name="routes_icon_logo_y_<?=$versionId?>">
                        </td>
                    </tr>
                <? } ?>

                <?
                $iconSizeX = IntVal(Option::get($module_id, "path_def_size_x_" . $versionId));
                $iconSizeY = IntVal(Option::get($module_id, "path_def_size_y_" . $versionId));
                $iconAnchorX = IntVal(Option::get($module_id, "path_def_anchor_x_" . $versionId));
                $iconAnchorY = IntVal(Option::get($module_id, "path_def_anchor_y_" . $versionId));
                $iconOffsetX = IntVal(Option::get($module_id, "path_def_offset_x_" . $versionId));
                $iconOffsetY = IntVal(Option::get($module_id, "path_def_offset_y_" . $versionId));
                ?>
                <tr class="heading">
                    <td colspan="3"><?=Loc::getMessage('BITRIX_MAP_SPRITES_PATH_DEF') . " (" . $versionName . ")"?></td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_SIZE')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeX?>" class="bx-map-tiny-input  bx_map_path_def_changer_<?=$versionId?>" name="path_def_size_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeY?>" class="bx-map-tiny-input  bx_map_path_def_changer_<?=$versionId?>" name="path_def_size_y_<?=$versionId?>">
                    </td>
                    <td rowspan="3">
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconOffsetX?>px -<?=$iconOffsetY?>px" class="bx-map-icon-example" id="bx_map_path_def_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconOffsetX?>px -<?=$iconOffsetY+$iconSizeY?>px" class="bx-map-icon-example" id="bx_map_path_def_1_<?=$versionId?>"></div>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_ANCHOR')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorX?>" class="bx-map-tiny-input  bx_map_path_def_changer_<?=$versionId?>" name="path_def_anchor_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorY?>" class="bx-map-tiny-input  bx_map_path_def_changer_<?=$versionId?>" name="path_def_anchor_y_<?=$versionId?>">
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_OFFSET')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconOffsetX?>" class="bx-map-tiny-input  bx_map_path_def_changer_<?=$versionId?>" name="path_def_offset_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconOffsetY?>" class="bx-map-tiny-input  bx_map_path_def_changer_<?=$versionId?>" name="path_def_offset_y_<?=$versionId?>">
                    </td>
                </tr>

                <?
                $iconSizeX = IntVal(Option::get($module_id, "path_active_size_x_" . $versionId));
                $iconSizeY = IntVal(Option::get($module_id, "path_active_size_y_" . $versionId));
                $iconAnchorX = IntVal(Option::get($module_id, "path_active_anchor_x_" . $versionId));
                $iconAnchorY = IntVal(Option::get($module_id, "path_active_anchor_y_" . $versionId));
                $iconOffsetX = IntVal(Option::get($module_id, "path_active_offset_x_" . $versionId));
                $iconOffsetY = IntVal(Option::get($module_id, "path_active_offset_y_" . $versionId));
                ?>
                <tr class="heading">
                    <td colspan="3"><?=Loc::getMessage('BITRIX_MAP_SPRITES_PATH_ACTIVE') . " (" . $versionName . ")"?></td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_SIZE')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeX?>" class="bx-map-tiny-input bx_map_path_active_changer_<?=$versionId?>" name="path_active_size_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeY?>" class="bx-map-tiny-input bx_map_path_active_changer_<?=$versionId?>" name="path_active_size_y_<?=$versionId?>">
                    </td>
                    <td rowspan="3">
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconOffsetX?>px -<?=$iconOffsetY?>px" class="bx-map-icon-example" id="bx_map_path_active_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconOffsetX?>px -<?=$iconOffsetY+$iconSizeY?>px" class="bx-map-icon-example" id="bx_map_path_active_1_<?=$versionId?>"></div>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_ANCHOR')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorX?>" class="bx-map-tiny-input bx_map_path_active_changer_<?=$versionId?>" name="path_active_anchor_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorY?>" class="bx-map-tiny-input bx_map_path_active_changer_<?=$versionId?>" name="path_active_anchor_y_<?=$versionId?>">
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_OFFSET')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconOffsetX?>" class="bx-map-tiny-input bx_map_path_active_changer_<?=$versionId?>" name="path_active_offset_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconOffsetY?>" class="bx-map-tiny-input bx_map_path_active_changer_<?=$versionId?>" name="path_active_offset_y_<?=$versionId?>">
                    </td>
                </tr>
                <tr class="heading">
                    <td colspan="3"><?=Loc::getMessage('BITRIX_MAP_SPRITES_PATH_STROKE') . " (" . $versionName . ")"?></td>
                </tr>

                <? foreach ($arAllOptions["path_stroke_" . $versionId] as $mxOption) { ?>
                    <tr>
                        <td width="50%" class="adm-detail-content-cell-l"><?=$mxOption[1]?></td>
                        <td width="140" class="adm-detail-content-cell-r">
                            <? if ($mxOption[3][0] == "selectbox") { $val = Option::get($module_id, $mxOption[0]); ?>
                                <select name="<?=$mxOption[0]?>">
                                    <? foreach ($mxOption[3][1] as $value => $name) { ?>
                                        <option value="<?=$value?>"<?=($value == $val) ? " selected=\"selected\"" : "" ?>><?=$name?></option>
                                    <? } ?>
                                </select>
                            <? } else { ?>
                                <input type="text" size="<?=$mxOption[3][1]?>" maxlength="255" value="<?=htmlspecialcharsbx(Option::get($module_id, $mxOption[0]))?>" name="<?=$mxOption[0]?>">
                            <? } ?>
                        </td>
                        <? if ($mxOption[0] == "path_stroke_weight_" . $versionId) { ?>
                            <td rowspan="<?=$versionId == "mobile" ? 3 : 5?>">
                                <?
                                $strokeColor   = htmlspecialcharsbx(Option::get($module_id, 'path_stroke_color_' . $versionId, ''));
                                $strokeOpacity = FloatVal(Option::get($module_id, 'path_stroke_opacity_' . $versionId));
                                if ($versionId == "desktop") {
                                    $strokeColorActive   = htmlspecialcharsbx(Option::get($module_id, 'path_stroke_color_active_' . $versionId, ''));
                                    $strokeOpacityHover = FloatVal(Option::get($module_id, 'path_stroke_opacity_hover_' . $versionId));
                                }
                                $strokeWeight  = IntVal(Option::get($module_id, 'path_stroke_weight_' . $versionId));
                                ?>
                                <div class="line_example_holder" style="background:url(http://static-maps.yandex.ru/1.x/?ll=49.592804,58.590994&z=15&l=map&size=200,200)">
                                    <div style="background-color:<?=$strokeColor?>;height:<?=$strokeWeight?>px;opacity:<?=$strokeOpacity?>" class="line_example" id="line_path_example_<?=$versionId?>"></div>
                                    <? if ($versionId == "desktop") { ?>
                                        <div style="background-color:<?=$strokeColor?>;height:<?=$strokeWeight?>px;opacity:<?=$strokeOpacityHover?>" class="line_example" id="line_path_example_h_<?=$versionId?>"></div>
                                        <div style="background-color:<?=$strokeColorActive?>;height:<?=$strokeWeight?>px;opacity:<?=$strokeOpacityHover?>" class="line_example" id="line_path_example_ah_<?=$versionId?>"></div>
                                    <? } ?>
                                </div>
                            </td>
                        <? } ?>
                    </tr>
                <? } ?>

            </table>

            <? if ($versionId == "mobile") { ?>

            <table cellspacing="4" cellpadding="0" width="100%">
                <?
                $iconSizeX = IntVal(Option::get($module_id, "categories_icon_size_x_" . $versionId));
                $iconSizeY = IntVal(Option::get($module_id, "categories_icon_size_y_" . $versionId));
                $iconAnchorX = IntVal(Option::get($module_id, "categories_icon_anchor_x_" . $versionId));
                $iconAnchorY = IntVal(Option::get($module_id, "categories_icon_anchor_y_" . $versionId));
                $iconScale = IntVal(Option::get($module_id, "categories_icon_scale_" . $versionId));
                ?>
                <tr class="heading">
                    <td colspan="2"><?=Loc::getMessage('BITRIX_MAP_SPRITES_CATEGORIES') . " (" . $versionName . ")"?></td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_SPRITES')?></td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <? $imageId = COption::GetOptionInt($module_id, "categories_icon_" . $versionId, 0); ?>
                        <?=CFile::InputFile("categories_icon_" . $versionId, 20, $imageId, false, 0, "IMAGE", "", 0, "", "", true, false);?>
                        <? if ($imageId > 0) { ?><input type="hidden" name="categories_icon_<?=$versionId?>_del_id" value="<?=Intval($imageId)?>"><? } ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l">&nbsp;</td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <div class="bx-map-crop-image">
                            <? $imageSrc = $obDataMixer->getImageFilename("categories_icon_" . $versionId); echo ShowImage($imageSrc);?>
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="4" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_SIZE')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeX?>" class="bx-map-tiny-input bx_map_categories_icon_changer_<?=$versionId?>" name="categories_icon_size_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeY?>" class="bx-map-tiny-input bx_map_categories_icon_changer_<?=$versionId?>" name="categories_icon_size_y_<?=$versionId?>">
                    </td>
                    <td rowspan="3">
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) 0 -<?=$iconSizeY?>px" class="bx-map-icon-example" id="bx_map_categories_icon_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconSizeX?>px -<?=$iconSizeY?>px" class="bx-map-icon-example" id="bx_map_categories_icon_1_<?=$versionId?>"></div>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_ANCHOR')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorX?>" class="bx-map-tiny-input bx_map_categories_icon_changer_<?=$versionId?>" name="categories_icon_anchor_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorY?>" class="bx-map-tiny-input bx_map_categories_icon_changer_<?=$versionId?>" name="categories_icon_anchor_y_<?=$versionId?>">
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_SCALE')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconScale?>" class="bx-map-tiny-input bx_map_categories_icon_changer_<?=$versionId?>" name="categories_icon_scale_<?=$versionId?>">
                    </td>
                </tr>
            </table>


        <? } ?>
            <table cellspacing="4" cellpadding="0" width="100%">
                <?
                $iconSizeX = IntVal(Option::get($module_id, "direction_icon_size_x_" . $versionId));
                $iconSizeY = IntVal(Option::get($module_id, "direction_icon_size_y_" . $versionId));
                $iconAnchorX = IntVal(Option::get($module_id, "direction_icon_anchor_x_" . $versionId));
                $iconAnchorY = IntVal(Option::get($module_id, "direction_icon_anchor_y_" . $versionId));
                ?>
                <tr class="heading">
                    <td colspan="2"><?=Loc::getMessage('BITRIX_MAP_SPRITES_DIRECTION') . " (" . $versionName . ")"?></td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_SPRITES')?></td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <? $imageId = COption::GetOptionInt($module_id, "direction_icon_" . $versionId, 0); ?>
                        <?=CFile::InputFile("direction_icon_" . $versionId, 20, $imageId, false, 0, "IMAGE", "", 0, "", "", true, false);?>
                        <? if ($imageId > 0) { ?><input type="hidden" name="direction_icon_<?=$versionId?>_del_id" value="<?=Intval($imageId)?>"><? } ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l">&nbsp;</td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <div class="bx-map-crop-image">
                            <? $imageSrc = $obDataMixer->getImageFilename("direction_icon_" . $versionId); echo ShowImage($imageSrc);?>
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="4" cellpadding="0" width="100%">
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_SIZE')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeX?>" class="bx-map-tiny-input bx_map_direction_icon_changer_<?=$versionId?>" name="direction_icon_size_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconSizeY?>" class="bx-map-tiny-input bx_map_direction_icon_changer_<?=$versionId?>" name="direction_icon_size_y_<?=$versionId?>">
                    </td>
                    <td rowspan="2">
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) 0 -<?=$iconSizeY?>px" class="bx-map-icon-example" id="bx_map_direction_icon_0_<?=$versionId?>"></div>
                        <div style="width:<?=$iconSizeX?>px;height:<?=$iconSizeY?>px;background:url(<?=$arImage["icon"]?>) -<?=$iconSizeX?>px -<?=$iconSizeY?>px" class="bx-map-icon-example" id="bx_map_direction_icon_1_<?=$versionId?>"></div>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_ANCHOR')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorX?>" class="bx-map-tiny-input bx_map_direction_icon_changer_<?=$versionId?>" name="direction_icon_anchor_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorY?>" class="bx-map-tiny-input bx_map_direction_icon_changer_<?=$versionId?>" name="direction_icon_anchor_y_<?=$versionId?>">
                    </td>
                </tr>

                <? //if ($versionId != "desktop") { // ���� ��������� ��������� �� �������� ?>
                    <tr class="heading">
                        <td colspan="3"><?=Loc::getMessage('BITRIX_MAP_DIRECTION_OPTIONS') . " (" . $versionName . ")"?></td>
                    </tr>
                    <? foreach ($arAllOptions["direction_opt_" . $versionId] as $mxOption) { ?>
                        <tr>
                            <td width="50%" class="adm-detail-content-cell-l"><?=$mxOption[1]?></td>
                            <td width="140" class="adm-detail-content-cell-r">
                                <? if ($mxOption[3][0] == "selectbox") { $val = Option::get($module_id, $mxOption[0]); ?>
                                    <select name="<?=$mxOption[0]?>">
                                        <? foreach ($mxOption[3][1] as $value => $name) { ?>
                                            <option value="<?=$value?>"<?=($value == $val) ? " selected=\"selected\"" : "" ?>><?=$name?></option>
                                        <? } ?>
                                    </select>
                                <? } else { ?>
                                    <input type="text" size="<?=$mxOption[3][1]?>" maxlength="255" value="<?=htmlspecialcharsbx(Option::get($module_id, $mxOption[0]))?>" name="<?=$mxOption[0]?>">
                                <? } ?>
                            </td>
                            <? if ($mxOption[0] == "dir_stroke_weight_" . $versionId) { ?>
                                <td rowspan="5">
                                    <?
                                    $strokeColor   = htmlspecialcharsbx(Option::get($module_id, 'dir_stroke_color_' . $versionId, ''));
                                    $strokeOpacity = FloatVal(Option::get($module_id, 'dir_stroke_opacity_' . $versionId));
                                    if ($versionId == "desktop") {
                                        $strokeColorActive   = htmlspecialcharsbx(Option::get($module_id, 'dir_stroke_color_active_' . $versionId, ''));
                                        $strokeOpacityHover = FloatVal(Option::get($module_id, 'dir_stroke_opacity_hover_' . $versionId));
                                    }
                                    $strokeWeight  = IntVal(Option::get($module_id, 'dir_stroke_weight_' . $versionId));
                                    ?>
                                    <div class="line_example_holder" style="background:url(http://static-maps.yandex.ru/1.x/?ll=49.592804,58.590994&z=15&l=map&size=200,200)">
                                        <div style="background-color:<?=$strokeColor?>;height:<?=$strokeWeight?>px;opacity:<?=$strokeOpacity?>" class="line_example" id="line_dir_example_<?=$versionId?>"></div>
                                        <? if ($versionId == "desktop") { ?>
                                            <div style="background-color:<?=$strokeColor?>;height:<?=$strokeWeight?>px;opacity:<?=$strokeOpacityHover?>" class="line_example" id="line_dir_example_h_<?=$versionId?>"></div>
                                            <div style="background-color:<?=$strokeColorActive?>;height:<?=$strokeWeight?>px;opacity:<?=$strokeOpacityHover?>" class="line_example" id="line_dir_example_ah_<?=$versionId?>"></div>
                                        <? } ?>
                                    </div>
                                </td>
                            <? } ?>
                        </tr>
                    <? } ?>
                <? //} // ���� ��������� ��������� �� ��������  ?>
            </table>
            <table cellspacing="4" cellpadding="0" width="100%" class="bx-map-edit-table">
                <?__AdmSettingsDrawList($module_id, $arAllOptions["cluster_" . $versionId]);?>
                <?
                $clusterColor = htmlspecialcharsbx(Option::get($module_id, 'cluster_color_' . $versionId, ''));
                $clusterSize1 = intval(Option::get($module_id, 'cluster_size1_' . $versionId, ''));
                $clusterSize2 = intval(Option::get($module_id, 'cluster_size2_' . $versionId, ''));
                $clusterSize3 = intval(Option::get($module_id, 'cluster_size3_' . $versionId, ''));
                $clusterSize4 = intval(Option::get($module_id, 'cluster_size4_' . $versionId, ''));
                $iconAnchorX = intval(Option::get($module_id, 'cluster_anchor_x_' . $versionId, ''));
                $iconAnchorY = intval(Option::get($module_id, 'cluster_anchor_y_' . $versionId, ''));
                ?>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_ICONS_SPRITES_ANCHOR')?></td>
                    <td width="140" class="adm-detail-content-cell-r">
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorX?>" class="bx-map-tiny-input bx_map_direction_icon_changer_<?=$versionId?>" name="cluster_anchor_x_<?=$versionId?>">
                        &times;
                        <input type="text" size="3" maxlength="255" value="<?=$iconAnchorY?>" class="bx-map-tiny-input bx_map_direction_icon_changer_<?=$versionId?>" name="cluster_anchor_y_<?=$versionId?>">
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l"><?=Loc::getMessage('BITRIX_MAP_SPRITES')?></td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <? $imageId = COption::GetOptionInt($module_id, "cluster_icon_" . $versionId, 0); ?>
                        <?=CFile::InputFile("cluster_icon_" . $versionId, 20, $imageId, false, 0, "IMAGE", "", 0, "", "", true, false);?>
                        <? if ($imageId > 0) { ?><input type="hidden" name="cluster_icon_<?=$versionId?>_del_id" value="<?=Intval($imageId)?>"><? } ?>
                    </td>
                </tr>
                <tr>
                    <td width="50%" class="adm-detail-content-cell-l">&nbsp;</td>
                    <td width="50%" class="adm-detail-content-cell-r">
                        <div class="bx-map-crop-image">
                            <? $imageSrc = $obDataMixer->getImageFilename("cluster_icon_" . $versionId); echo ShowImage($imageSrc);?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table cellspacing="4" cellpadding="0" width="100%">
                            <tr>
                                <td width="25%" class="bx-map-adm-center"><?=Loc::getMessage("BITRIX_MAP_CLUSTER_ICON1")?></td>
                                <td width="25%" class="bx-map-adm-center"><?=Loc::getMessage("BITRIX_MAP_CLUSTER_ICON2")?></td>
                                <td width="25%" class="bx-map-adm-center"><?=Loc::getMessage("BITRIX_MAP_CLUSTER_ICON3")?></td>
                                <td width="25%" class="bx-map-adm-center"><?=Loc::getMessage("BITRIX_MAP_CLUSTER_ICON4")?></td>
                            </tr>
                            <tr>
                                <td width="25%" class="bx-map-adm-center">
                                    <div id="cluster_size1_<?=$versionId?>_example" class="bx-map-cluster-example bx-map-cluster-color-<?=$versionId?>" style="line-height:<?=$clusterSize1?>px;width:<?=$clusterSize1?>px;height:<?=$clusterSize1?>px;color:<?=$clusterColor?>;background:url(<?=$imageSrc?>) no-repeat 0 0;">5</div>
                                </td>
                                <td width="25%" class="bx-map-adm-center">
                                    <div id="cluster_size2_<?=$versionId?>_example" class="bx-map-cluster-example bx-map-cluster-color-<?=$versionId?>" style="line-height:<?=$clusterSize2?>px;width:<?=$clusterSize2?>px;height:<?=$clusterSize2?>px;color:<?=$clusterColor?>;background:url(<?=$imageSrc?>) no-repeat -<?=$clusterSize1?>px 0;">15</div>
                                </td>
                                <td width="25%" class="bx-map-adm-center">
                                    <div id="cluster_size3_<?=$versionId?>_example" class="bx-map-cluster-example bx-map-cluster-color-<?=$versionId?>" style="line-height:<?=$clusterSize3?>px;width:<?=$clusterSize3?>px;height:<?=$clusterSize3?>px;color:<?=$clusterColor?>;background:url(<?=$imageSrc?>) no-repeat -<?=($clusterSize1 + $clusterSize2)?>px 0;">125</div>
                                </td>
                                <td width="25%" class="bx-map-adm-center">
                                    <div id="cluster_size4_<?=$versionId?>_example" class="bx-map-cluster-example bx-map-cluster-color-<?=$versionId?>" style="line-height:<?=$clusterSize4?>px;width:<?=$clusterSize4?>px;height:<?=$clusterSize4?>px;color:<?=$clusterColor?>;background:url(<?=$imageSrc?>) no-repeat -<?=($clusterSize1 + $clusterSize2 + $clusterSize3)?>px 0;">1024</div>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%" class="bx-map-adm-center"><?=Loc::getMessage("BITRIX_MAP_CLUSTER_SIZE")?></td>
                                <td width="25%" class="bx-map-adm-center"><?=Loc::getMessage("BITRIX_MAP_CLUSTER_SIZE")?></td>
                                <td width="25%" class="bx-map-adm-center"><?=Loc::getMessage("BITRIX_MAP_CLUSTER_SIZE")?></td>
                                <td width="25%" class="bx-map-adm-center"><?=Loc::getMessage("BITRIX_MAP_CLUSTER_SIZE")?></td>
                            </tr>
                            <tr>
                                <td width="25%" class="bx-map-adm-center">
                                    <input name="cluster_size1_<?=$versionId?>" value="<?=$clusterSize1?>" class="adm-input bx-map-tiny-input" />
                                </td>
                                <td width="25%" class="bx-map-adm-center">
                                    <input name="cluster_size2_<?=$versionId?>" value="<?=$clusterSize2?>" class="adm-input bx-map-tiny-input" />
                                </td>
                                <td width="25%" class="bx-map-adm-center">
                                    <input name="cluster_size3_<?=$versionId?>" value="<?=$clusterSize3?>" class="adm-input bx-map-tiny-input" />
                                </td>
                                <td width="25%" class="bx-map-adm-center">
                                    <input name="cluster_size4_<?=$versionId?>" value="<?=$clusterSize4?>" class="adm-input bx-map-tiny-input" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </td>
            </tr>
        <? } ?>

        <?$tabControl->BeginNextTab();?>
        <tr>
            <td>
                <?
                $arGeocoderSettings = \Bitrix\InteractiveMap\DataMixer::loadGeocoderConditions();

                $arProperties = array();
                if (is_array($arGeocoderSettings))
                {
                    $arIblocks = array_keys($arGeocoderSettings);
                    foreach ($arIblocks as $iblock)
                    {
                        $rsProperties = Bitrix\Iblock\PropertyTable::getList(array(
                            "filter" => array("IBLOCK_ID" => $iblock),
                            "select" => array("ID", "NAME"),
                            "order"  => array("NAME" => "ASC")
                        ));

                        while ($arProperty = $rsProperties->fetch())
                        {
                            $arProperties[$iblock][$arProperty["ID"]] = $arProperty["NAME"];
                        }
                    }
                }
                ?>
                <script language="JavaScript">
                    $(document).ready(function(){
                        $GeocoderConditions.init({
                            'container': '#geocoder-container',
                            'varName': 'geocoder_cond',
                            'iblocks':   <?=CUtil::PhpToJsObject($iblockList);?>,
                            'iblockProperties':   <?=CUtil::PhpToJsObject($arProperties);?>,
                            'data':   <?=CUtil::PhpToJsObject($arGeocoderSettings);?>,
                            'interface': {
                                'selectIb': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_SELECT_ID"))?>',
                                'address': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_ADDRESS"))?>',
                                'longitude': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_LONGITUDE"))?>',
                                'latitude': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_LATITUDE"))?>',
                                'addIb': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_ADD_IB"))?>',
                                'geocoder': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_GEOCODER"))?>',
                                'yandex': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_YANDEX"))?>',
                                'google': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_GOOGLE"))?>',
                                'inCap': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_IN_CAP"))?>',
                                'in': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_IN"))?>',
                                'and': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_AND"))?>',
                                'doGeocoding': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_DO_GEOCODING"))?>',
                                'withHelp': '<?=CUtil::JsEscape(Loc::getMessage("BX_MAP_GC_INT_WITH_HELP"))?>'
                            }
                        });
                    });
                </script>
                <div id="geocoder-container"></div>
            </td>
        </tr>

        <?$tabControl->BeginNextTab();?>

        <?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');?>

        <?$tabControl->Buttons();?>

        <script language="JavaScript">
            function RestoreDefaults()
            {
                if(confirm('<?echo AddSlashes(Loc::getMessage('MAIN_HINT_RESTORE_DEFAULTS_WARNING'))?>'))
                    window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)."&".bitrix_sessid_get();?>";
            }
        </script>
        <input type="submit" name="Update" <?if ($MOD_RIGHT<'W') echo "disabled" ?> value="<?echo Loc::getMessage('MAIN_SAVE')?>">
        <input type="reset" name="reset" value="<?echo Loc::getMessage('MAIN_RESET')?>">
        <input type="hidden" name="Update" value="Y">
        <?=bitrix_sessid_post();?>
        <input type="button" <?if ($MOD_RIGHT<'W') echo "disabled" ?> title="<?echo Loc::getMessage('MAIN_HINT_RESTORE_DEFAULTS')?>" OnClick="RestoreDefaults();" value="<?echo Loc::getMessage('MAIN_RESTORE_DEFAULTS')?>">
        <?$tabControl->End();?>
        </form>
    <?
    }
    else
    {
        CAdminMessage::ShowMessage(array(
            "TYPE" => "ERROR",
            "MESSAGE" => Loc::getMessage("BX_MAP_WANT_IBLOCK_TITLE"),
            "DETAILS" => Loc::getMessage("BX_MAP_WANT_IBLOCK_DESC"),
            "HTML" => true,
        ));
    }
}
?>