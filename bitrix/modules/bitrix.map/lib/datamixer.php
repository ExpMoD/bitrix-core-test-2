<?
namespace Bitrix\InteractiveMap;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Web\Json,
    Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

/**
 * Class DataMixer
 *
 * @package Bitrix\InteractiveMap
 */
class DataMixer
{

    private $jsonOptions = null; //JSON_FORCE_OBJECT;

    private $maxIconFilesize = 2; // Mb

    private $maxIconSize = 0; // px

    private $jsPath = "/bitrix/js/bitrix.map/";

    private $imagesPath = "/bitrix/images/bitrix.map/";

    private $defaultIcons = array(
        "cluster_icon_desktop"   => "cluster.png",
        "objects_icon_desktop"   => "desktop_objects.png",
        "events_icon_desktop"    => "desktop_events.png",
        "routes_icon_desktop"    => "desktop_routes.png",
        "direction_icon_desktop" => "desktop_direction.png",

        "cluster_icon_mobile"    => "cluster.png",
        "objects_icon_mobile"    => "mobile_objects.png",
        "events_icon_mobile"     => "mobile_events.png",
        "routes_icon_mobile"     => "mobile_routes.png",
        "categories_icon_mobile" => "mobile_category.png",
        "direction_icon_mobile"  => "mobile_routes.png",
    );

    private $interfaceStrings = array(
        "main"   => array(
            "collapsePanel",
//            "categoriesTitle",
            "catsTitle",
            "subcatsTitle",
            "objectsTitle",
            "popupTitle",
            "clearField",
            "placeHolder",
            "refreshMarkers",
            "clearCategories",
            "closeList",
            "showList",
            "back",
            "showMarker",
            "route",
            "walking",
            "transit",
            "driving",
            "bicycling",
            "toWalk",
            "toDrive",
            "reverseDirection",
            "createRoute",
            "pointsTitle",
            "currentPosition",
            "from",
            "to",
            "catAbstractName",
            "directionTitle",
            "error",
            "multiObjects",
            "popular",
            "showMap",
            "showMapObject",
            "closeMap",
            "groupCategoryName",
            "showObjects",
            "showCurrentObject"
        ),
        "routes" => array(
            "INVALID_REQUEST",
            "MAX_WAYPOINTS_EXCEEDED",
            "NOT_FOUND",
            "OK",
            "OVER_QUERY_LIMIT",
            "REQUEST_DENIED",
            "UNKNOWN_ERROR",
            "ZERO_RESULTS",
            "wait",
            "blocked",
            "none",
        ),
        "errors" => array(
            "DEVICE_NOT_DEFINED",
            "UNKNOWN_DEFAULT_PATH",
            "PAGETYPE_NOT_DEFINED",
            "AJAX_NOT_DEFINED",
            "ROUTE_NOT_DEFINED",
            "MAP_PARAMETERS_NOT_DEFINED",
            "COMMON_SCRIPT_NOT_LOADED",
            "MAP_SCRIPT_NOT_LOADED",
            "MAP_DENIED",
            "NO_CATS",
            "NO_ITEMS",
            "INDEFINED_ERRORS",
            "ERRORS_COMMON_LOADING",
            "ERRORS_MAP_LOADING",
        )
    );

    private $componentsList = array(
        "map.map",
        "map.routing",
    );

    private $defaultMapBounds = array("lat" => array(90, -90), "lng" => array(180, -180));
    protected $currentMapBounds = array(); //array("lat" => array(90, -90), "lng" => array(180, -180));

    private $defaultPropParams = array(
        "NAME_PROP_CODE" => "name",
        "LATITUDE_PROP_CODE" => "lat",
        "LONGITUDE_PROP_CODE" => "lng",
        "ADDRESS_PROP_CODE" => "address",
        "PHONE_PROP_CODE" => "phone",
        "OPENING_PROP_CODE" => "opening",
        "LINK_PROP_CODE" => "link",
        "DESCRIPTION_PROP_CODE" => "description",
        "EMAIL_PROP_CODE" => "email",
        "PICTURE_PROP_CODE" => "photo",
        "PARENT_PROP_CODE" => "parent",
//        "ICONPOS_PROP_CODE" => "iconpos",
//        "CLOSED_PROP_CODE" => "closed"
    );

    private $arPropertiesIB = array();

    protected $arSessionParams = array();

    protected $paramIblockId = 0;
    protected $paramNoCats = false;
    protected $paramDataType = "objects";
    protected $paramShowElementsCount = true;
    protected $paramParentSection = 0;
    protected $paramIconposPropCode = '';
    protected $paramRouteTypePropCode = '';
    protected $paramLoadItems = true;
    protected $paramConvertFields = false;
    protected $paramMaxDescLength = 100;
    protected $paramDateFormat = '';

    protected $arPropCodes = array();
    protected $arFieldsName = array();

    public function __construct($arParams=array())
    {
        $this->paramIblockId = $arParams["IBLOCK_ID"];
        $this->paramNoCats = "Y" == $arParams["NO_CATS"];
        $this->paramDataType = $arParams["DATA_TYPE"];
        $this->paramShowElementsCount = $arParams["SHOW_ELEMENTS_COUNT"];
        $this->paramParentSection = $arParams["PARENT_SECTION"];
        $this->paramIconposPropCode = $arParams["ICONPOS_PROP_CODE"];
        $this->paramRouteTypePropCode = $arParams["ROUTETYPE_PROP_CODE"];
        $this->paramClosedPropCode = $arParams["CLOSED_PROP_CODE"];
        $this->paramLoadItems = true === $arParams["LOAD_ITEMS"];
        $this->paramConvertFields = true === $arParams["OLD_DATA_MODE"];
        $this->paramMaxDescLength = $arParams["PREVIEW_TRUNCATE_LEN"];
        $this->paramDateFormat = $arParams["ACTIVE_DATE_FORMAT"];
        $this->getPropertiesIB();
    }

    private function checkUrl($url = "")
    {
        if (empty($url))
        {
            return "";
        }

        $parsed_url = parse_url(filter_var($url, FILTER_SANITIZE_URL));

        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : 'http://';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';

        return "$scheme$user$pass$host$port$path$query$fragment";
    }

    public function clearComponentsCache()
    {
        $rsSites = \CSite::GetList($by = "sort", $order = "asc", array());

        while ($arSite = $rsSites->Fetch())
        {
            foreach ($this->componentsList as $componentName)
            {
                BXClearCache(true, "/" . $arSite["ID"] . "/bitrix/" . $componentName);
            }
        }
    }

    public function getDefaultPaths($json = false)
    {
        $return = array(
            "libs"   => $this->jsPath,
            "images" => $this->imagesPath,
        );

        if ($json)
        {
            return Json::encode($return, $this->jsonOptions);
        }
        else
        {
            return $return;
        }
    }

    public function includeMapScripts($type = "desktop")
    {
        if ($type == "mobile" || $type == "mobileapp")
        {
            $GLOBALS["APPLICATION"]->AddHeadString("<script src=\"/bitrix/js/bitrix.map/map_mobile.js\" id=\"bxMapScript\" charset=\"utf-8\"></script>");
        }
        /*else if ($type == "mobileapp")
        {
            $GLOBALS["APPLICATION"]->AddHeadString("<script src=\"/bitrix/js/bitrix.map/map_mobileapp.js\" id=\"bxMapScript\" charset=\"utf-8\"></script>");
        }*/
        else
        {
            $GLOBALS["APPLICATION"]->AddHeadString("<script src=\"/bitrix/js/bitrix.map/map_desktop.js\" id=\"bxMapScript\" charset=\"utf-8\"></script>");
        }
    }

    public function getImageFilename($imageType)
    {
        $imageFilename = "";
        $fileId        = Option::get('bitrix.map', $imageType, 0);

        if ($fileId > 0)
        {
            $imageFilename = \CFile::GetPath($fileId);
        }

        if (empty($imageFilename))
        {
            $imageFilename = $this->imagesPath . $this->defaultIcons[$imageType];
        }

        return $imageFilename;
    }

    private function checkGeocoderSetttings($arSettings, &$errors)
    {
        if (!is_array($arSettings))
        {
            $errors = Loc::getMessage("BX_MAP_ERROR_WRONG_SETTINGS");
            return false;
        }

        $errors = array();
        $count = count($arSettings["iblock"]);

        for ($row = 0; $row < $count; $row++)
        {
            $rowError = array();

            if ($arSettings["iblock"][$row] <= 0)
            {
                $rowError[] = Loc::getMessage("BX_MAP_ERROR_NOT_SET_IB");
            }

            if ($arSettings["address"][$row] <= 0)
            {
                $rowError[] = Loc::getMessage("BX_MAP_ERROR_NOT_SET_ADDRESS");
            }

            if ($arSettings["longitude"][$row] <= 0)
            {
                $rowError[] = Loc::getMessage("BX_MAP_ERROR_NOT_SET_LNG");//
            }

            if ($arSettings["latitude"][$row] <= 0)
            {
                $rowError[] = Loc::getMessage("BX_MAP_ERROR_NOT_SET_LAT");
            }

            if (strlen($arSettings["geocoder"][$row]) == 0)
            {
                $rowError[] = Loc::getMessage("BX_MAP_ERROR_NOT_SET_GEO");
            }

            if (count($rowError) > 0)
            {
                $errors[$row] = Loc::getMessage("BX_MAP_ERROR_ERR_LINE_NUM") . " #" . ($row + 1) . ": " . implode(", ", $rowError);
            }
        }

        return count($errors) == 0;
    }

    private function transformGeocoderCondition($arContionds)
    {
        $arGeocoderSettings = array();

        foreach ($arContionds["iblock"] as $i => $iblockId)
        {
            $arGeocoderSettings[$iblockId] = array(
                "address"   => $arContionds["address"][$i],
                "latitude"  => $arContionds["latitude"][$i],
                "longitude" => $arContionds["longitude"][$i],
                "geocoder"  => $arContionds["geocoder"][$i],
            );
        }

        return $arGeocoderSettings;
    }

    public function loadGeocoderConditions()
    {
        if (!empty($_POST["geocoder_cond"]))
        {
            return self::transformGeocoderCondition($_POST["geocoder_cond"]);
        }
        else
        {
            return unserialize(Option::get("bitrix.map", "geocoder_cond"));
        }
    }

    public function saveGeocoderConditions()
    {
        if (!empty($_POST["geocoder_cond"]))
        {
            $errors = array();

            if (self::checkGeocoderSetttings($_POST["geocoder_cond"], $errors))
            {
                Option::set("bitrix.map", "geocoder_cond", serialize(self::transformGeocoderCondition($_POST["geocoder_cond"])));
            }
            else
            {
                return $errors;
            }
        }
        else
        {
            Option::delete("bitrix.map", array("name" => "geocoder_cond"));
        }

        return true;
    }

    public function saveIcons()
    {
        if (empty($_FILES))
        {
            return true;
        }

        $arErrors = array();

        foreach ($this->defaultIcons as $paramsName => $defaultValue)
        {
            $fileId = 0;

            if (is_array($_FILES[$paramsName]) && $_FILES[$paramsName]["error"] != 4)
            {
                if ($_FILES[$paramsName]["error"] > 0)
                {
                    $arErrors[] = $_FILES[$paramsName]["name"] . ": " . Loc::getMessage("BX_MAP_FILE_SAVE_FAILED_PHP_" . $_FILES[$paramsName]["error"]);
                }
                else
                {
                    $_FILES[$paramsName]["MODULE_ID"] = "bitrix.map";

                    $checkResult = \CFile::CheckImageFile($_FILES[$paramsName], $this->maxIconFilesize * 1024 * 1024, $this->maxIconSize, $this->maxIconSize);
                    if (strlen($checkResult) == 0)
                    {
                        if ($fileId = \CFile::SaveFile($_FILES[$paramsName], "/bitrix.map"))
                        {
                            Option::set('bitrix.map', $paramsName, $fileId);
                        }
                        else
                        {
                            $arErrors[] = $_FILES[$paramsName]["name"] . ": " . Loc::getMessage("BX_MAP_FILE_SAVE_FAILED");
                        }
                    }
                    else
                    {
                        if ($this->maxIconSize > 0)
                        {
                            $arErrors[] = $_FILES[$paramsName]["name"] . ": " . Loc::getMessage("BX_MAP_FILE_CHECK_FAILED", array("#MAX_FILESIZE#" => $this->maxIconFilesize, "#MAX_SIZE#" => $this->maxIconSize));
                        }
                        else
                        {
                            $arErrors[] = $_FILES[$paramsName]["name"] . ": " . Loc::getMessage("BX_MAP_FILE_CHECK_FAILED_ALT", array("#MAX_FILESIZE#" => $this->maxIconFilesize));
                        }
                    }
                }
            }

            if ($_POST[$paramsName . "_del_id"] > 0 && ($fileId > 0 || $_POST[$paramsName . "_del"] == "Y"))
            {
                \CFile::Delete($_POST[$paramsName . "_del_id"]);
                if ($fileId == 0)
                {
                    Option::delete('bitrix.map', array("name" => $paramsName));
                }
            }
        }

        return (count($arErrors) > 0) ? $arErrors : true;
    }

    public function getIconsSettings($version = "desktop", $arSections = array(), $json = false)
    {
        $arResult  = array();
        $returnAll = count($arSections) == 0;

        if (in_array("icon_objects", $arSections) || $returnAll)
        {
            $arResult["icon"]["objects"] = array(
                "url"    => $this->GetImageFilename("objects_icon_" . $version),
                "size"   => array(
                    intval(Option::get('bitrix.map', 'objects_icon_size_x_' . $version)),
                    intval(Option::get('bitrix.map', 'objects_icon_size_y_' . $version))
                ),
                "anchor" => array(
                    intval(Option::get('bitrix.map', 'objects_icon_anchor_x_' . $version)),
                    intval(Option::get('bitrix.map', 'objects_icon_anchor_y_' . $version))
                ),
                "logo"   => array(
                    intval(Option::get('bitrix.map', 'objects_icon_logo_x_' . $version)),
                    intval(Option::get('bitrix.map', 'objects_icon_logo_y_' . $version))
                ),
            );
        }

        if (in_array("icon_events", $arSections) || $returnAll)
        {
            $arResult["icon"]["events"] = array(
                "url"    => $this->GetImageFilename("events_icon_" . $version),
                "size"   => array(
                    intval(Option::get('bitrix.map', 'events_icon_size_x_' . $version)),
                    intval(Option::get('bitrix.map', 'events_icon_size_y_' . $version))
                ),
                "anchor" => array(
                    intval(Option::get('bitrix.map', 'events_icon_anchor_x_' . $version)),
                    intval(Option::get('bitrix.map', 'events_icon_anchor_y_' . $version))
                ),
                "logo"   => array(
                    intval(Option::get('bitrix.map', 'events_icon_logo_x_' . $version)),
                    intval(Option::get('bitrix.map', 'events_icon_logo_y_' . $version))
                ),
            );
        }

        if (in_array("icon_routes", $arSections) || $returnAll)
        {
            $arResult["icon"]["routes"] = array(
                "url"    => $this->GetImageFilename("routes_icon_" . $version),
                "size"   => array(
                    intval(Option::get('bitrix.map', 'routes_icon_size_x_' . $version)),
                    intval(Option::get('bitrix.map', 'routes_icon_size_y_' . $version))
                ),
                "anchor" => array(
                    intval(Option::get('bitrix.map', 'routes_icon_anchor_x_' . $version)),
                    intval(Option::get('bitrix.map', 'routes_icon_anchor_y_' . $version))
                ),
                "logo"   => array(
                    intval(Option::get('bitrix.map', 'routes_icon_logo_x_' . $version)),
                    intval(Option::get('bitrix.map', 'routes_icon_logo_y_' . $version))
                ),
            );
        }

        if (in_array("icon_category", $arSections) || $returnAll)
        {
            $arResult["icon"]["category"] = array(
                "url"    => $this->GetImageFilename("categories_icon_" . $version),
                "size"   => array(
                    intval(Option::get('bitrix.map', 'categories_icon_size_x_' . $version)),
                    intval(Option::get('bitrix.map', 'categories_icon_size_y_' . $version))
                ),
                "anchor" => array(
                    intval(Option::get('bitrix.map', 'categories_icon_anchor_x_' . $version)),
                    intval(Option::get('bitrix.map', 'categories_icon_anchor_y_' . $version))
                ),
                "scale"  => intval(Option::get('bitrix.map', 'categories_icon_scale_' . $version)),
            );
        }

        if (in_array("icon_direction", $arSections) || $returnAll)
        {
            $arResult["icon"]["direction"] = array(
                "url"    => $this->GetImageFilename("direction_icon_" . $version),
                "size"   => array(
                    intval(Option::get('bitrix.map', 'direction_icon_size_x_' . $version)),
                    intval(Option::get('bitrix.map', 'direction_icon_size_y_' . $version))
                ),
                "anchor" => array(
                    intval(Option::get('bitrix.map', 'direction_icon_anchor_x_' . $version)),
                    intval(Option::get('bitrix.map', 'direction_icon_anchor_y_' . $version))
                ),
            );
        }

        if (in_array("path", $arSections) || in_array("icon_routes", $arSections) || $returnAll)
        {
            $arResult["path"] = array(
                "def"                => array(
                    "size"   => array(
                        intval(Option::get('bitrix.map', 'path_def_size_x_' . $version)),
                        intval(Option::get('bitrix.map', 'path_def_size_y_' . $version))
                    ),
                    "anchor" => array(
                        intval(Option::get('bitrix.map', 'path_def_anchor_x_' . $version)),
                        intval(Option::get('bitrix.map', 'path_def_anchor_y_' . $version))
                    ),
                    "offset" => array(
                        intval(Option::get('bitrix.map', 'path_def_offset_x_' . $version)),
                        intval(Option::get('bitrix.map', 'path_def_offset_y_' . $version))
                    ),
                ),
                "active"             => array(
                    "size"   => array(
                        intval(Option::get('bitrix.map', 'path_active_size_x_' . $version)),
                        intval(Option::get('bitrix.map', 'path_active_size_y_' . $version))
                    ),
                    "anchor" => array(
                        intval(Option::get('bitrix.map', 'path_active_anchor_x_' . $version)),
                        intval(Option::get('bitrix.map', 'path_active_anchor_y_' . $version))
                    ),
                    "offset" => array(
                        intval(Option::get('bitrix.map', 'path_active_offset_x_' . $version)),
                        intval(Option::get('bitrix.map', 'path_active_offset_y_' . $version))
                    ),
                ),
                "strokeWeight"       => intval(Option::get('bitrix.map', 'path_stroke_weight_' . $version)),
                "strokeColor"        => Option::get('bitrix.map', 'path_stroke_color_' . $version),
                "strokeOpacity"      => floatval(Option::get('bitrix.map', 'path_stroke_opacity_' . $version)),
            );
            if ($version == "desktop")
            {
                $arResult["path"]["strokeColorActive"]  = Option::get('bitrix.map', 'path_stroke_color_active_' . $version);
                $arResult["path"]["strokeOpacityHover"] = floatval(Option::get('bitrix.map', 'path_stroke_opacity_hover_' . $version));
            }
        }

        if (in_array("directionOptions", $arSections) || in_array("icon_direction", $arSections) || $returnAll)
        {
            $arResult["directionOptions"] = array(
                "strokeColor"        => Option::get('bitrix.map', 'dir_stroke_color_' . $version),
                "strokeOpacity"      => floatval(Option::get('bitrix.map', 'dir_stroke_opacity_' . $version)),
                "strokeWeight"       => intval(Option::get('bitrix.map', 'dir_stroke_weight_' . $version)),
            );
            if ($version == "desktop")
            {
                $arResult["directionOptions"]["strokeColorActive"]  = Option::get('bitrix.map', 'dir_stroke_color_active_' . $version);
                $arResult["directionOptions"]["strokeOpacityHover"] = floatval(Option::get('bitrix.map', 'dir_stroke_opacity_hover_' . $version));
            }
        }

        if (in_array("cluster", $arSections) || $returnAll)
        {
            $arResult["cluster"] = array(
                "gridSize" => intval(Option::get('bitrix.map', 'cluster_grid_size_' . $version)),
                "color"    => Option::get('bitrix.map', 'cluster_color_' . $version),
                "anchor"   => array(
                    intval(Option::get('bitrix.map', 'cluster_anchor_x_' . $version)),
                    intval(Option::get('bitrix.map', 'cluster_anchor_y_' . $version))
                ),
                "icon"     => $this->GetImageFilename("cluster_icon_" . $version),
                "set"      => array(
                    array("size" => intval(Option::get('bitrix.map', 'cluster_size1_' . $version))),
                    array("size" => intval(Option::get('bitrix.map', 'cluster_size2_' . $version))),
                    array("size" => intval(Option::get('bitrix.map', 'cluster_size3_' . $version))),
                    array("size" => intval(Option::get('bitrix.map', 'cluster_size4_' . $version))),
                ),
            );
        }

        if ($json)
        {
            foreach ($arResult as &$arIcons)
            {
                $arIcons = Json::encode($arIcons, $this->jsonOptions);
            }
        }

        return $arResult;
    }

    public function clearIcons()
    {
        foreach ($this->defaultIcons as $paramsName => $defaultvalue)
        {
            $fileId = Option::get('bitrix.map', $paramsName, 0);

            if ($$fileId > 0)
            {
                \CFile::Delete($fileId);
            }
        }
    }

    public function buildInterfacePropertiesList(&$arOptions, $type = "main")
    {
        foreach ($this->interfaceStrings[$type] as $strProperty)
        {
            $arOptions[] = array(
                'int_' . $strProperty,
                Loc::getMessage('BX_MAP_INT_' . strtoupper($strProperty)),
                '',
                array(
                    'text',
                    50
                )
            );
        }
    }

    public function getInterfaceStrings($json = false)
    {
        $arReturn = array();

        $bDiffIntSettings = Option::get('bitrix.map', 'different_int_settings', 'N') == "Y";

        foreach ($this->interfaceStrings as $type => $arStrings)
        {
            $arReturn[$type] = array();

            foreach ($arStrings as $strProperty)
            {
                $arReturn[$type][$strProperty] = Option::get('bitrix.map', 'int_' . $strProperty, '', ($bDiffIntSettings) ? SITE_ID : "");
            }

            if ($json)
            {
                $arReturn[$type] = Json::encode($arReturn[$type], $this->jsonOptions);
            }
        }

        return $arReturn;
    }

    public function getRouteTypes($mapType, $json = false)
    {
        if ($mapType == "google")
        {
            $arReturn[$mapType] = array(
                "driving",
                "walking"
            );

            if (Option::get('bitrix.map', 'route_type_google_transit', 'N') == "Y")
            {
                $arReturn[$mapType][] = "transit";
            }

            if (Option::get('bitrix.map', 'route_type_google_bicycle', 'N') == "Y")
            {
                $arReturn[$mapType][] = "bicycle";
            }
        }
        else
        {
            $arReturn[$mapType] = array("driving");
        }

        if ($json)
        {
            $arReturn = Json::encode($arReturn, $this->jsonOptions);
        }

        return $arReturn;
    }

    public function prepareJsonData(&$arResult)
    {
        $arResult["ERROR"] = "";

        $arResult["JSON_SECTIONS"] = array();
        $arResult["JSON_ELEMENTS"] = array();
        switch ($this->paramDataType)
        {
            case "routes":
                $arResult["JSON_SECTIONS"] = $this->getSectionsToRoutes();
                $arResult["JSON_ELEMENTS"] = $this->getElementsToRoutes($arResult["SECTIONS"], $arResult["ELEMENTS"]);
                break;

            case "events":
                $arResult["JSON_SECTIONS"] = $this->getSectionsToEvents();
                $arResult["JSON_ELEMENTS"] = $this->getElementsToEvents($arResult["ELEMENTS"]);
                break;

            case "objects":
                $arResult["JSON_SECTIONS"] = $this->getSectionsToObjects($arResult["SECTIONS"]);
                $arResult["JSON_ELEMENTS"] = $this->getElementsToObjects($arResult["ELEMENTS"]);
                break;

            default:
                $arResult["ERROR"] = Loc::getMessage("BX_MAP_ERROR_UNKNOWN_TYPE");

        }

        $this->setElementsCount($arResult["JSON_SECTIONS"], $arResult["JSON_ELEMENTS"]);
        
        if($this->paramLoadItems) {
            $this->delEmptySections($arResult["JSON_SECTIONS"]);
        }
        
        $arResult["JSON_SECTIONS"] = Json::encode($arResult["JSON_SECTIONS"], $this->jsonOptions);
        $arResult["JSON_ELEMENTS"] = Json::encode($arResult["JSON_ELEMENTS"], $this->jsonOptions);

        if (empty($arResult["PARAMS"]["BOUNDS"]) || !$this->isMapBounds($arResult["PARAMS"]["BOUNDS"])) {
            if ($this->isMapBounds($this->currentMapBounds)) {
                $arResult["PARAMS"]["BOUNDS"] = $this->currentMapBounds;
            } else {
                $arResult["PARAMS"]["BOUNDS"] = $this->getDefaultMapBounds();
            }
        }
        $arResult["PARAMS"]["BOUNDS"] = Json::encode($arResult["PARAMS"]["BOUNDS"], $this->jsonOptions);
    }

    public function prepareSectionJsonData(&$arResult, $arParams)
    {
        foreach ($arResult["SECTIONS"] as $arSection)
        {
            $arResult["JSON_SECTIONS"]["s" . $arSection["ID"]] = array("name" => $arSection["NAME"]);

            if (strlen($arSection["PICTURE"]["SRC"]) > 0)
            {
                $arResult["JSON_SECTIONS"]["s" . $arSection["ID"]]["icon"] = $arSection["PICTURE"]["SRC"];
            }

            $arResult["JSON_SECTIONS"]["s" . $arSection["ID"]]["pos"] = (int)$arSection[$arParams["ICONPOS_PROP_CODE"]];
        }

        $arResult["JSON_SECTIONS"] = Json::encode($arResult["JSON_SECTIONS"], $this->jsonOptions);
    }

    /**
     * ��������� ������� �������� ��� json (��� "��������")
     * @return array
     */
    public function getSectionsToRoutes()
    {
        $arJsonSections = array();
        $obEnum = new \CUserFieldEnum;
        $arRouteTypeProperty = \CUserTypeEntity::GetList(array("ID" => "ASC"), array(
                    "ENTITY_ID" => "IBLOCK_" . $this->paramIblockId . "_SECTION",
                    "FIELD_NAME" => $this->paramRouteTypePropCode
                ))->Fetch();

        $rsEnum = $obEnum->GetList(array(), array("USER_FIELD_ID" => $arRouteTypeProperty["ID"]));
        while ($arEnum = $rsEnum->GetNext()) {
            $arJsonSections["s" . $arEnum["ID"]] = array(
                "name" => $arEnum["VALUE"],
                "pos" => $arEnum["XML_ID"]
            );
        }
        return $arJsonSections;
    }

    /**
     * ��������� ������� ��������� ��� json (��� "��������")
     * @param array $arSections ������ ��������
     * @param array $arElements ������ ���������
     * @return array
     */
    public function getElementsToRoutes($arSections, $arElements)
    {
        $arJsonElements = array();
        foreach ($arSections as $arElement) {
            $arElement["PREVIEW_TEXT"] = $arElement["DESCRIPTION"];
            $arData = $this->getElementsToJson($arElement);

            $arData["cat"] = "s".$arElement[$this->paramRouteTypePropCode];
            if (!empty($arElement["~LIST_PAGE_URL"])) {
                $arData["url"] = $arElement["~LIST_PAGE_URL"];
            }
            $arData["closed"] = (!empty($arElement[$this->paramClosedPropCode])) ? true : false;
            
            $arJsonElements["e".$arElement["ID"]] = $arData;
        }
        foreach ($arElements as $arElement) {
            $arData = $this->getElementsToJson($arElement);

            if (
                empty($arJsonElements["e" . $arElement["IBLOCK_SECTION_ID"]][$this->arFieldsName["lat"]])
                && empty($arJsonElements["e" . $arElement["IBLOCK_SECTION_ID"]][$this->arFieldsName["lng"]])
            ) {
                $arJsonElements["e" . $arElement["IBLOCK_SECTION_ID"]][$this->arFieldsName["lat"]] = (double) $arData[$this->arFieldsName["lat"]];
                $arJsonElements["e" . $arElement["IBLOCK_SECTION_ID"]][$this->arFieldsName["lng"]] = (double) $arData[$this->arFieldsName["lng"]];
                if (!empty($arData[$this->arFieldsName["photo"]])) {
                    $arJsonElements["e" . $arElement["IBLOCK_SECTION_ID"]][$this->arFieldsName["photo"]] = $arData[$this->arFieldsName["photo"]];
                }
            } else {
                $arJsonElements["e" . $arElement["IBLOCK_SECTION_ID"]]["points"][] = $arData;
            }
        }
        return $arJsonElements;
    }

    /**
     * ��������� ������� �������� ��� json (��� "�������")
     * @return array
     */
    function getSectionsToEvents()
    {
        return array(
            "pass"    => array(
                "name" => Loc::getMessage("BX_MAP_INT_CATS_PASS"),
                "pos"  => 0
            ),
            "today"   => array(
                "name" => Loc::getMessage("BX_MAP_INT_CATS_TODAY"),
                "pos"  => 30
            ),
            "planned" => array(
                "name" => Loc::getMessage("BX_MAP_INT_CATS_PLANNED"),
                "pos"  => 60
            )
        );
    }
    /**
     * ��������� ������� ��������� ��� json (��� "�������")
     * @param array $arElements ������ ���������
     * @return array
     */
    public function getElementsToEvents($arElements)
    {
        $arJsonElements = array();
        $now = time();
        foreach ($arElements as $arElement) {
            $arData = array();
            if (
                !empty($arElement["PROPERTIES"][$this->arPropCodes["lat"]]["VALUE"])
                && !empty($arElement["PROPERTIES"][$this->arPropCodes["lng"]]["VALUE"])
            ) {
                
                $arData = $this->getElementsToJson($arElement);
                
                $arData["cat"] = "today";
                if (strlen($arElement["ACTIVE_TO"]) == 0 || $arElement["ACTIVE_TO"] == $arElement["ACTIVE_FROM"]) {
                    if (MakeTimeStamp($arElement["ACTIVE_FROM"]) < $now) {
                        $arData["cat"] = "pass";
                    }
                } else {
                    if (MakeTimeStamp($arElement["ACTIVE_TO"]) < $now) {
                        $arData["cat"] = "pass";
                    }
                }
                if (MakeTimeStamp($arElement["ACTIVE_FROM"]) > $now) {
                    $arData["cat"] = "planned";
                }
                
                $arJsonElements["e".$arElement["ID"]] = $arData;
            }
        }
        return $arJsonElements;
    }
    /**
     * ��������� ������� �������� ��� json (��� "�������")
     * @return array
     */
    public function getSectionsToObjects($arSections)
    {
        $arJsonSections = array();
        foreach ($arSections as $arSection) {
            $arJsonSections["s" . $arSection["ID"]]["name"] = $arSection["NAME"];

            if ($arSection["IBLOCK_SECTION_ID"] > 0) {
                if ($this->paramParentSection > 0) {
                    if ($arSection["IBLOCK_SECTION_ID"] != $this->paramParentSection) {
                        $arJsonSections["s" . $arSection["ID"]]["cat"] = "s" . $arSection["IBLOCK_SECTION_ID"];
                        $arJsonSections["s" . $arSection["IBLOCK_SECTION_ID"]]["parent"] = true;
                    }
                } else {
                    $arJsonSections["s" . $arSection["ID"]]["cat"] = "s" . $arSection["IBLOCK_SECTION_ID"];
                    $arJsonSections["s" . $arSection["IBLOCK_SECTION_ID"]]["parent"] = true;
                }
            }

            if (strlen($arSection["PICTURE"]["SRC"]) > 0) {
                $arJsonSections["s" . $arSection["ID"]]["icon"] = $arSection["PICTURE"]["SRC"];
            }

            $arJsonSections["s" . $arSection["ID"]]["pos"] = (int) $arSection[$this->paramIconposPropCode];
        }
        return $arJsonSections;
    }

    /*
     * ��������� ������� ��������� ��� json (��� "�������")
     */
    public function getElementsToObjects($arElements)
    {
        $arJsonElements = array();
        $happyParents = array();

        foreach ($arElements as $arElement) {
            if (!empty($arElement["PROPERTIES"][$this->arPropCodes["parent"]]["VALUE"])) {
                $happyParents[] = $arElement["PROPERTIES"][$this->arPropCodes["parent"]]["VALUE"];
            }
        }
        foreach ($arElements as $arElement) {
            $arData = array();
            if (
                in_array($arElement["ID"], $happyParents)
                || (!empty($arElement["PROPERTIES"][$this->arPropCodes["lat"]]["VALUE"])
                && !empty($arElement["PROPERTIES"][$this->arPropCodes["lng"]]["VALUE"]))
            ) {

                $arData = $this->getElementsToJson($arElement);

                if (!$this->paramNoCats || !empty($arElement["PROPERTIES"][$this->arPropCodes["parent"]]["VALUE"])) {
                    if (!empty($arElement["PROPERTIES"][$this->arPropCodes["parent"]]["VALUE"])) {
                        if ($this->paramConvertFields) {
                            $arData["cat"] = "e" . $arElement["PROPERTIES"][$this->arPropCodes["parent"]]["VALUE"];
                        } else {
                            $arData["item"] = "e" . $arElement["PROPERTIES"][$this->arPropCodes["parent"]]["VALUE"];
                        }
                    } else {
                        $arData["cat"] = "s" . $arElement["IBLOCK_SECTION_ID"];
                    }
                }
                $arJsonElements["e".$arElement["ID"]] = $arData;
            }
        }
        return $arJsonElements;
    }

    /*
     * ��������� ������ ��� ���� ����� ������������� ���������
     */
    protected function getElementsToJson($arElement)
    {
        $arData = array();
        if (!empty($arElement["PROPERTIES"][$this->arPropCodes["name"]]["VALUE"])) {
            $arData[$this->arFieldsName["name"]] = $arElement["PROPERTIES"][$this->arPropCodes["name"]]["VALUE"];
        } else {
            $arData[$this->arFieldsName["name"]] = $arElement["NAME"];
        }
        if (!empty($arElement["PROPERTIES"][$this->arPropCodes["address"]]["VALUE"])) {
            $arData[$this->arFieldsName["address"]] = $arElement["PROPERTIES"][$this->arPropCodes["address"]]["VALUE"];
        }
        if ($arElement["PROPERTIES"][$this->arPropCodes["photo"]]["VALUE"] > 0) {
            $arPhoto = \CFile::ResizeImageGet($arElement["PROPERTIES"][$this->arPropCodes["photo"]]["VALUE"], array(
                        "width" => 300,
                        "height" => 300
                            ), BX_RESIZE_IMAGE_PROPORTIONAL);
            $arData[$this->arFieldsName["photo"]] = $arPhoto["src"];
        } elseif (is_array($arElement["PREVIEW_PICTURE"])) {
            $arPhoto = \CFile::ResizeImageGet($arElement["PREVIEW_PICTURE"]["ID"], array(
                        "width" => 300,
                        "height" => 300
                            ), BX_RESIZE_IMAGE_PROPORTIONAL);
            $arData[$this->arFieldsName["photo"]] = $arPhoto["src"];
        }
        if (!empty($arElement["PROPERTIES"][$this->arPropCodes["email"]]["VALUE"])) {
            $arData[$this->arFieldsName["email"]] = $arElement["PROPERTIES"][$this->arPropCodes["email"]]["VALUE"];
        }
        if (!empty($arElement["PROPERTIES"][$this->arPropCodes["link"]]["VALUE"])) {
            $arData[$this->arFieldsName["link"]] = $this->checkUrl($arElement["PROPERTIES"][$this->arPropCodes["link"]]["VALUE"]);
        }
        if (!empty($arElement["~DETAIL_PAGE_URL"])) {
            $arData["url"] = $arElement["~DETAIL_PAGE_URL"];
        }
        if (!empty($arElement["PROPERTIES"][$this->arPropCodes["description"]]["VALUE"]) > 0) {
            $arData[$this->arFieldsName["description"]] = $arElement["PROPERTIES"][$this->arPropCodes["description"]]["VALUE"];
        } elseif (!empty($arElement["PREVIEW_TEXT"])) {
            $arData[$this->arFieldsName["description"]] = $arElement["PREVIEW_TEXT"];
        }
        if (!empty($arElement["PROPERTIES"][$this->arPropCodes["opening"]]["VALUE"])) {
            $arData[$this->arFieldsName["opening"]] = $arElement["PROPERTIES"][$this->arPropCodes["opening"]]["VALUE"];
        }
        if (!empty($arElement["PROPERTIES"][$this->arPropCodes["phone"]]["VALUE"])) {
            $arData[$this->arFieldsName["phone"]] = $arElement["PROPERTIES"][$this->arPropCodes["phone"]]["VALUE"];
        }
        $arBounds = array(
            "lat"=>$arElement["PROPERTIES"][$this->arPropCodes["lat"]]["VALUE"],
            "lng"=>$arElement["PROPERTIES"][$this->arPropCodes["lng"]]["VALUE"]
        );
        $this->setMapBounds($arBounds);
        if (!empty($arBounds["lat"])) {
            $arData[$this->arFieldsName["lat"]] = $arBounds["lat"];
        }
        if (!empty($arBounds["lng"])) {
            $arData[$this->arFieldsName["lng"]] = $arBounds["lng"];
        }
        if (is_array($arElement["DISPLAY_PROPERTIES"])) {
            foreach ($arElement["DISPLAY_PROPERTIES"] as $propCode=>$arProp) {
                if (!in_array($propCode, $this->arPropCodes) && (!empty($arProp["VALUE_ENUM"]) || !empty($arProp["DISPLAY_VALUE"]))) {
                    $arData[$propCode] = $arProp['PROPERTY_TYPE'] == "L" ? $arProp["VALUE_ENUM"]: $arProp["DISPLAY_VALUE"];
                }
            }
        }
        return $arData;
    }

    /**
     * ������� � ��������� ���-�� ��������� � �������
     */
    public function setElementsCount(&$arSections, &$arElements)
    {
        foreach ($arElements as $arElement) {
            if (isset($arSections[$arElement["cat"]])) {
                $arSections[$arElement["cat"]]["count"] ++;
            }
        }
    }

    /**
     * �������� ������ ��������
     */
    public function delEmptySections(&$arSections)
    {
        foreach ($arSections as $key=>$arSection) {
            if (empty($arSection["count"]) && !$arSection["parent"]) {
                unset($arSections[$key]);
            }
        }
    }

    public function getDefaultMapHeight()
    {
        return intval(Option::get('bitrix.map', 'def_map_height'));
    }

    public function getDefaultBarHeight()
    {
        return intval(Option::get('bitrix.map', 'def_bar_height'));
    }

    public function getDefaultPlateHeight()
    {
        return intval(Option::get('bitrix.map', 'def_plate_height'));
    }

    public function getDefaultMapBounds()
    {
        return $this->defaultMapBounds;
    }

    /**
     * @author Eugene Mamaev
     * @param $arElements
     * @param $iblockID
     */
    public function GetIBlockElementProperties(&$arElements)
    {
        if (empty($arElements))
        {
            return;
        }

        if (!\CModule::IncludeModule('iblock'))
        {
            return;
        }

        $iblockID    = intval($this->paramIblockId);
        $rsIBlock    = \CIBlock::GetByID($iblockID);
        $arIBlock    = $rsIBlock->GetNext();
        $arElementID = array_keys($arElements);
        if ($arIBlock['VERSION'] == 2)
        {
            $sql = "SELECT BPE.ID enum_id, BPE.VALUE enum_value, BP.* FROM b_iblock B
                INNER JOIN b_iblock_property BP ON B.ID=BP.IBLOCK_ID
                LEFT JOIN b_iblock_property_enum BPE ON BP.ID=BPE.PROPERTY_ID
                WHERE B.ID = " . $iblockID . " AND BP.ACTIVE='Y'
                ORDER BY BP.SORT asc, BP.ID asc";
            global $DB;
            $arProperties = array();
            $res          = $DB->Query($sql, true);
            while ($arRes = $res->GetNext())
            {
                if (!isset($arProperties[$arRes['ID']])) {
                    $arProperties[$arRes['ID']] = array(
                        'ID' => $arRes['ID'],
                        'CODE' => $arRes['CODE'],
                        'ACTIVE' => $arRes['ACTIVE'],
                        'SORT' => $arRes['SORT'],
                        'DEFAULT_VALUE' => $arRes['DEFAULT_VALUE'],
                        'PROPERTY_TYPE' => $arRes['PROPERTY_TYPE'],
                        'MULTIPLE' => $arRes['MULTIPLE'],
                        'MULTIPLE_CNT' => $arRes['MULTIPLE_CNT'],
                        'LINK_IBLOCK_ID' => $arRes['LINK_IBLOCK_ID'],
                        'IS_REQUIRED' => $arRes['IS_REQUIRED'],
                        'USER_TYPE' => $arRes['USER_TYPE']
                    );
                }
                if ($arRes['PROPERTY_TYPE'] == 'L' && !empty($arRes['enum_id'])) {
                    $arProperties[$arRes['ID']]['VALUE_ENUM'][$arRes['enum_id']] = $arRes['enum_value'];
                }
                
            }
            foreach ($arElements as $arRes)
            {
                if (!empty($arRes["ID"])) {
                    foreach ($arProperties as $pid => $arProperty) {
                        $prop_value = $arRes['PROPERTY_' . $pid];
                        $arElements[$arRes['ID']]['PROPERTIES'][$arProperty['CODE']] = $arProperty;
                        $arElements[$arRes['ID']]['PROPERTIES'][$arProperty['CODE']]['VALUE'] = $prop_value;
                        if (!empty($prop_value)) {
                            $arElements[$arRes['ID']]["DISPLAY_PROPERTIES"][$arProperty['CODE']] = array(
                                "CODE" => $arProperty['CODE'],
                                "DISPLAY_VALUE" => is_array($prop_value) ? implode('/', $prop_value) : $prop_value,
                                'PROPERTY_TYPE' => $arProperty['PROPERTY_TYPE']
                            );
                            if ($arProperty['PROPERTY_TYPE'] == 'L') {
                                
                                $arElements[$arRes['ID']]["DISPLAY_PROPERTIES"][$arProperty['CODE']]['VALUE_ENUM'] = is_array($prop_value)
                                        ? implode(' / ', $prop_value)
                                        : $arProperty['VALUE_ENUM'][$prop_value];
                            }
                        }
                    }
                }
            }
        }
        else
        {
            $sql = "SELECT BP.*, BEP.ID as PROPERTY_VALUE_ID, BEP.IBLOCK_ELEMENT_ID as ELEMENT_ID, BEP.VALUE, BEP.DESCRIPTION, BEPE.VALUE VALUE_ENUM, BEPE.XML_ID VALUE_XML_ID, BEPE.SORT VALUE_SORT
FROM
b_iblock B
INNER JOIN b_iblock_property BP ON B.ID=BP.IBLOCK_ID
LEFT JOIN b_iblock_element_property BEP ON (BP.ID = BEP.IBLOCK_PROPERTY_ID AND BEP.IBLOCK_ELEMENT_ID in (" . implode(',', $arElementID) . "))
LEFT JOIN b_iblock_property_enum BEPE ON (BP.PROPERTY_TYPE = 'L' AND BEPE.ID=BEP.VALUE_ENUM AND BEPE.PROPERTY_ID=BP.ID)
WHERE
B.ID = " . $iblockID . " AND BP.ACTIVE='Y'
ORDER BY
BP.SORT asc, BP.ID asc, BEPE.SORT asc, BEP.ID asc ";
            global $DB;
            $res = $DB->Query($sql, true);
            while ($arRes = $res->GetNext())
            {
                if (!empty($arRes['ELEMENT_ID'])) {
                    $arElements[$arRes['ELEMENT_ID']]['PROPERTIES'][$arRes['CODE']] = array(
                        'ID' => $arRes['ID'],
                        'NAME' => $arRes['NAME'],
                        'CODE' => $arRes['CODE'],
                        'ACTIVE' => $arRes['ACTIVE'],
                        'SORT' => $arRes['SORT'],
                        'DEFAULT_VALUE' => $arRes['DEFAULT_VALUE'],
                        'PROPERTY_TYPE' => $arRes['PROPERTY_TYPE'],
                        'MULTIPLE' => $arRes['MULTIPLE'],
                        'MULTIPLE_CNT' => $arRes['MULTIPLE_CNT'],
                        'LINK_IBLOCK_ID' => $arRes['LINK_IBLOCK_ID'],
                        'IS_REQUIRED' => $arRes['IS_REQUIRED'],
                        'USER_TYPE' => $arRes['USER_TYPE'],
                        'VALUE_ENUM' => $arRes['VALUE_ENUM'],
                        'VALUE' => $arRes['VALUE'],
                        'VALUE_XML_ID' => $arRes['VALUE_XML_ID'],
                        'VALUE_SORT' => $arRes['VALUE_SORT'],
                    );
                    if ((is_array($arRes["VALUE"]) && count($arRes["VALUE"]) > 0) || (!is_array($arRes["VALUE"]) && strlen($arRes["VALUE"]) > 0)) {
                        $arElements[$arRes['ELEMENT_ID']]["DISPLAY_PROPERTIES"][$arRes['CODE']] = \CIBlockFormatProperties::GetDisplayValue($arElements[$arRes['ELEMENT_ID']], $arRes, "news_out");
                    }
                }
            }
        }
    }

    public function setMapBounds(&$arBounds=array())
    {
        if (!empty($arBounds["lat"])) {
            $arBounds["lat"] = (double) str_replace(',', '.', $arBounds["lat"]);
            if (empty($this->currentMapBounds["lat"][0]) || $arBounds["lat"] < $this->currentMapBounds["lat"][0]) {
                $this->currentMapBounds["lat"][0] = $arBounds["lat"];
            }
            if (empty($this->currentMapBounds["lat"][1]) || $arBounds["lat"] > $this->currentMapBounds["lat"][1]) {
                $this->currentMapBounds["lat"][1] = $arBounds["lat"];
            }
        }
        if (!empty($arBounds["lng"])) {
            $arBounds["lng"] = (double) str_replace(',', '.', $arBounds["lng"]);
            if (empty($this->currentMapBounds["lng"][0]) || $arBounds["lng"] < $this->currentMapBounds["lng"][0]) {
                $this->currentMapBounds["lng"][0] = $arBounds["lng"];
            }
            if (empty($this->currentMapBounds["lng"][1]) || $arBounds["lng"] > $this->currentMapBounds["lng"][1]) {
                $this->currentMapBounds["lng"][1] = $arBounds["lng"];
            }
        }
    }
    public function getMapBounds($arParams)
    {
        if (!Loader::includeModule("iblock") || empty($arParams["IBLOCK_ID"]) || empty($arParams["LATITUDE_PROP_CODE"]) || empty($arParams["LONGITUDE_PROP_CODE"])) {
            return $this->getDefaultMapBounds();
        }
        $arBounds = array();
        $arFilterLat = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "!PROPERTY_" . $arParams["LATITUDE_PROP_CODE"] => false);
        $arFilterLng = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "!PROPERTY_" . $arParams["LONGITUDE_PROP_CODE"] => false);

        $arEl = \CIBlockElement::GetList(
            array("PROPERTY_" . $arParams["LATITUDE_PROP_CODE"] => "asc"),
            $arFilterLat,
            false,
            array("nTopCount"=>1),
            array("IBLOCK_ID", "PROPERTY_" . $arParams["LATITUDE_PROP_CODE"])
        )->Fetch();
        $arBounds["lat"][0] = (double)str_replace(",", ".", $arEl["PROPERTY_" . $arParams["LATITUDE_PROP_CODE"] . "_VALUE"]);

        $arEl = \CIBlockElement::GetList(
            array("PROPERTY_" . $arParams["LATITUDE_PROP_CODE"] => "desc"),
            $arFilterLat,
            false,
            array("nTopCount"=>1),
            array("IBLOCK_ID", "PROPERTY_" . $arParams["LATITUDE_PROP_CODE"])
        )->Fetch();
        $arBounds["lat"][1] = (double)str_replace(",", ".", $arEl["PROPERTY_" . $arParams["LATITUDE_PROP_CODE"] . "_VALUE"]);

        $arEl = \CIBlockElement::GetList(
            array("PROPERTY_" . $arParams["LONGITUDE_PROP_CODE"] => "asc"),
            $arFilterLng,
            false,
            array("nTopCount"=>1),
            array("IBLOCK_ID", "PROPERTY_" . $arParams["LONGITUDE_PROP_CODE"])
        )->Fetch();
        $arBounds["lng"][0] = (double)str_replace(",", ".", $arEl["PROPERTY_" . $arParams["LONGITUDE_PROP_CODE"] . "_VALUE"]);

        $arEl = \CIBlockElement::GetList(
            array("PROPERTY_" . $arParams["LONGITUDE_PROP_CODE"] => "desc"),
            $arFilterLng,
            false,
            array("nTopCount"=>1),
            array("IBLOCK_ID", "PROPERTY_" . $arParams["LONGITUDE_PROP_CODE"])
        )->Fetch();
        $arBounds["lng"][1] = (double)str_replace(",", ".", $arEl["PROPERTY_" . $arParams["LONGITUDE_PROP_CODE"] . "_VALUE"]);

        return $arBounds;
    }

    public function isMapBounds($arBounds = array())
    {
        if (is_array($arBounds) && !empty($arBounds['lat'][0]) && !empty($arBounds['lat'][1])
            && !empty($arBounds['lng'][0]) && !empty($arBounds['lng'][1])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * ��������� ������� ��������� ���������
     * @param array $params
     * @return array
     */
    public function getElements(array $params)
    {
        if (!\CModule::IncludeModule('iblock'))
        {
            return;
        }
        $obParser = new \CTextParser;
        $arElements = array();
        $params["select"][] = "PROPERTY_*";
        $rsElement = \CIBlockElement::GetList($params["sort"], $params["filter"], $params["group"], $params["nav"], $params["select"]);
        $rsElement->SetUrlTemplates($params["params"]["DETAIL_URL"], "", $params["params"]["IBLOCK_URL"]);
        while ($obElement = $rsElement->GetNextElement()) {
            $arItem = $obElement->GetFields();
            $arButtons = \CIBlock::GetPanelButtons(
                            $arItem["IBLOCK_ID"], $arItem["ID"], 0, array("SECTION_BUTTONS" => false, "SESSID" => false)
            );
            $arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
            $arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

            if ($this->paramMaxDescLength > 0)
                $arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["PREVIEW_TEXT"], $this->paramMaxDescLength);

            if (strlen($arItem["ACTIVE_FROM"]) > 0)
                $arItem["DISPLAY_ACTIVE_FROM"] = \CIBlockFormatProperties::DateFormat($this->paramDateFormat, MakeTimeStamp($arItem["ACTIVE_FROM"], \CSite::GetDateFormat()));
            else
                $arItem["DISPLAY_ACTIVE_FROM"] = "";

            $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
            $arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();
            
            if (isset($arItem["PREVIEW_PICTURE"])) {
                $arItem["PREVIEW_PICTURE"] = (0 < $arItem["PREVIEW_PICTURE"] ? \CFile::GetFileArray($arItem["PREVIEW_PICTURE"]) : false);
                if ($arItem["PREVIEW_PICTURE"]) {
                    $arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"];
                    if ($arItem["PREVIEW_PICTURE"]["ALT"] == "")
                        $arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["NAME"];
                    $arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"];
                    if ($arItem["PREVIEW_PICTURE"]["TITLE"] == "")
                        $arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["NAME"];
                }
            }
            if (isset($arItem["DETAIL_PICTURE"])) {
                $arItem["DETAIL_PICTURE"] = (0 < $arItem["DETAIL_PICTURE"] ? \CFile::GetFileArray($arItem["DETAIL_PICTURE"]) : false);
                if ($arItem["DETAIL_PICTURE"]) {
                    $arItem["DETAIL_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"];
                    if ($arItem["DETAIL_PICTURE"]["ALT"] == "")
                        $arItem["DETAIL_PICTURE"]["ALT"] = $arItem["NAME"];
                    $arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"];
                    if ($arItem["DETAIL_PICTURE"]["TITLE"] == "")
                        $arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["NAME"];
                }
            }
            $arItem["PROPERTIES"] = array();
            $arItem["DISPLAY_PROPERTIES"] = array();
            $arItem["IBLOCK_SECTION_ID"] = intval($arItem["IBLOCK_SECTION_ID"]);
            $arElements[$arItem["ID"]] = $arItem;
        }
        if (count($arElements)>0) {
            $this->GetIBlockElementProperties($arElements);
        }
        return $arElements;
    }

    /**
     * ��������� ������� �������� ���������
     * @param array $arSort
     * @param array $arFilter
     * @param array $arSelect
     * @return array
     */
    public function getSections($arSort, $arFilter, $arSelect)
    {
        if (!\CModule::IncludeModule('iblock'))
        {
            return;
        }
        $arSections = array();
        $rsSection = \CIBlockSection::GetList($arSort, $arFilter, false /*$this->paramShowElementsCount*/, $arSelect);
        while ($arSection = $rsSection->GetNext()) {
            $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arSection["IBLOCK_ID"], $arSection["ID"]);
            $arSection["IPROPERTY_VALUES"] = $ipropValues->getValues();

            if (isset($arSection["PICTURE"])) {
                $arSection["PICTURE"] = (0 < $arSection["PICTURE"] ? \CFile::GetFileArray($arSection["PICTURE"]) : false);
                if ($arSection["PICTURE"]) {
                    $arSection["PICTURE"]["ALT"] = $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"];
                    if ($arSection["PICTURE"]["ALT"] == "")
                        $arSection["PICTURE"]["ALT"] = $arSection["NAME"];
                    $arSection["PICTURE"]["TITLE"] = $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"];
                    if ($arSection["PICTURE"]["TITLE"] == "")
                        $arSection["PICTURE"]["TITLE"] = $arSection["NAME"];
                }
            }

            $arSections[$arSection["ID"]] = $arSection;
        }
        return $arSections;
    }

    /**
     * ��������� �������� ������� ���������
     * @return array
     */
    public function getPropertiesIB()
    {
        if (count($this->arPropertiesIB) == 0) {
            $rsIB = \CIBlock::GetProperties($this->paramIblockId, array("sort" => "asc"), Array("ACTIVE" => "Y"));
            while ($arIB = $rsIB->Fetch()) {
                $this->arPropertiesIB[$arIB["CODE"]] = $arIB;
            }
        }
        return $this->arPropertiesIB;
    }


    public function getSettingsPropCodes($arParams)
    {
        $arPropCodes = array();
        foreach ($this->defaultPropParams as $key => $jsName) {
            if (!empty($arParams[$key]) && isset($this->arPropertiesIB[$arParams[$key]])) {
                $arPropCodes[$key] = $arParams[$key];
            }
        }
        return $arPropCodes;
    }

    public function setSystemFields($arPropCodes)
    {
        foreach ($arPropCodes as $key => $propCode) {
            $this->arPropCodes[$this->defaultPropParams[$key]] = $propCode;
        }
        foreach ($this->defaultPropParams as $jsName) {
            $this->arFieldsName[$jsName] = $jsName;
        }
        if (!$this->paramConvertFields) {
            foreach ($this->arPropCodes as $key=>$value) {
                $this->arFieldsName[$key] = $value;
            }
        }
    }

    public function getJsonFields($arPropView = array())
    {
        if (!\CModule::IncludeModule('iblock'))
        {
            return array();
        }
        
        $jsonFields = array("standard"=>array());
        if ($this->paramConvertFields) {
            $jsonFields["standard"] = array(
                $this->arFieldsName["name"] => array("name"=>"name", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_name")),
                $this->arFieldsName["address"] => array("name"=>"address", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_address")),
                $this->arFieldsName["lat"] => array("name"=>"lat", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_lat")),
                $this->arFieldsName["lng"] => array("name"=>"lng", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_lng")),
                $this->arFieldsName["link"] => array("name"=>"link", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_link")),
                $this->arFieldsName["phone"] => array("name"=>"phone", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_phone")),
                $this->arFieldsName["opening"] => array("name"=>"opening", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_opening")),
                $this->arFieldsName["description"] => array("name"=>"description", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_description")),
                $this->arFieldsName["email"] => array("name"=>"email", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_email")),
                $this->arFieldsName["photo"] => array("name"=>"photo", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_photo")),
                "url" => array("name"=>"url", "title"=>Loc::getMessage("BX_MAP_FIELDS_SYSTEM_url")),
            );

            foreach ($this->arPropertiesIB as $arIB) {
                if (!in_array($arIB["CODE"], $this->arPropCodes)) {
                    $jsonFields["standard"][$arIB["CODE"]] = array("title" => $arIB["NAME"]);
                }
            }
        } else {
            if (!isset($this->arPropCodes["name"])) {
                $jsonFields["standard"][$this->arFieldsName["name"]] = array(
                    "name" => "name",
                    "title" => Loc::getMessage("BX_MAP_FIELDS_SYSTEM_name")
                );
            }
            if (!isset($this->arPropCodes["photo"])) {
                $jsonFields["standard"][$this->arFieldsName["photo"]] = array(
                    "name" => "photo",
                    "title" => Loc::getMessage("BX_MAP_FIELDS_SYSTEM_photo")
                );
            }
            if (!isset($this->arPropCodes["description"])) {
                $jsonFields["standard"][$this->arFieldsName["description"]] = array(
                    "name" => "description",
                    "title" => Loc::getMessage("BX_MAP_FIELDS_SYSTEM_description")
                );
            }
            $jsonFields["standard"]["url"] = array(
                "name" => "url",
                "title" => Loc::getMessage("BX_MAP_FIELDS_SYSTEM_url")
            );
            
            foreach($this->arPropertiesIB as $arIB) {
                $name = array_keys($this->arPropCodes, $arIB["CODE"]);
                $jsonFields["standard"][$arIB["CODE"]] = in_array($arIB["CODE"], $this->arPropCodes)
                        ? array("name"=>$name[0], "title"=>$arIB["NAME"])
                        : array("title"=>$arIB["NAME"]);
                if (!in_array($arIB["CODE"], $arPropView)) {
                    $jsonFields["standard"][$arIB["CODE"]]["hidden"] = true;
                }

            }
        }
        return $jsonFields;
    }

    /**
     * ��������� ������� ��������� �� �������
     * @param array $arCat
     * @return array
     */
    public function getSectionRequest($arCat = array())
    {
        $arCatsId = array();
        foreach ($arCat as $cat) {
            $id = (int) substr($cat, -(strlen($cat) - 1));
            if ($id > 0) {
                $arCatsId[] = $id;
            }
        }
        return $arCatsId;
    }
}
?>