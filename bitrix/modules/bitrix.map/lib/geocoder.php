<?
namespace Bitrix\InteractiveMap;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Config\Configuration;
use \Bitrix\Main\Text\Encoding;

/**
 * Class Geocoder
 *
 * @package Bitrix\InteractiveMap
 * @version 15.1.0
 */
class Geocoder
{
    /**
     * @var int �������� Google
     */
    const GOOGLE = 1;

    /**
     * @var int �������� ������
     */
    const YANDEX = 2;

    /**
     * @var string ����� ������� �������������� ������ (��� ����������)
     */
    const YANDEX_GEOCODER_URL = "https://geocode-maps.yandex.ru/1.x/";

    /**
     * @var string ����� ������� �������������� Google (��� ����������)
     */
    const GOOGLE_GEOCODER_URL = "http://maps.googleapis.com/maps/api/geocode/json";

    /**
     * @var array ��������� ��������������
     */
    private $settings;

    /**
     * @var int ������� http-����������
     */
    private $httpTimeout = 3000;

    /**
     * @var string ��������� ������
     */
    private $lastError;

    /**
     * @var bool UTF-8 �����
     */
    private $isUtf;

    /**
     * @var int ������ ���������
     */
    private $geocoder = self::GOOGLE;

    /**
     * �������������� ������
     *
     * @param int $geocoder �������� �� ���������
     */
    public function __construct($geocoder = 0)
    {
        $this->settings = unserialize(Option::get("bitrix.map", "geocoder_cond"));
        $this->isUtf = Configuration::getValue("utf_mode");

        if ($geocoder > 0)
        {
            $this->setGeocoder($geocoder);
        }
    }

    private function isPropertyEmpty($values)
    {
        return current(array_pop($values)) == "";
    }

    /**
     * ������ �������� �� ���������
     *
     * @api
     *
     * @param int $geocoder
     *
     * @return bool true � ������ ������
     */
    public function setGeocoder($geocoder)
    {
        if ($geocoder == self::GOOGLE || $geocoder == self::YANDEX)
        {
            $this->geocoder = $geocoder;
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * ���������� ������� OnAfterIBlockElementUpdate � OnAfterIBlockElementAdd
     *
     * @see http://dev.1c-bitrix.ru/api_help/iblock/events/index.php ������� ������ "���������"
     *
     * @param array $arFields ���� �������� ���������
     */
    public static function onIBlockElementActionHandler($arFields)
    {
        if ($arFields["RESULT"] && $arFields["ID"] > 0)
        {
            $Geocoder = new Geocoder();
            if ($Geocoder->isGeocodingNeeded($arFields))
            {
                $Geocoder->geocodeIblockElement($arFields["IBLOCK_ID"], $arFields["ID"]);
            }
        }
    }

    /**
     * ��������� ������������� ��������������
     *
     * @param array $arFields ���� �������� ���������
     *
     * @return bool true � ������ ������������� �������������
     */
    public function isGeocodingNeeded($arFields)
    {
        if (!is_array($this->settings) || count($this->settings) == 0)
        {
            return false;
        }

        if (!array_key_exists($arFields["IBLOCK_ID"], $this->settings))
        {
            return false;
        }

        if ($this->isPropertyEmpty($arFields["PROPERTY_VALUES"][$this->settings[$arFields["IBLOCK_ID"]]["address"]]))
        {
            return false;
        }

        return $this->isPropertyEmpty($arFields["PROPERTY_VALUES"][$this->settings[$arFields["IBLOCK_ID"]]["latitude"]]) || $this->isPropertyEmpty($arFields["PROPERTY_VALUES"][$this->settings[$arFields["IBLOCK_ID"]]["longitude"]]);
    }

    /**
     * ����������� ����� ���������� �������� � ���������� � ��������� �� � �������� ��������
     *
     * @param int $iblockId  ������������� ��������� ��������
     * @param int $elementId ������������� ��������
     */
    public function geocodeIblockElement($iblockId, $elementId)
    {
        /**
         * @tood Use new Bitrix D7 API
         */
        $arAddress = \CIBlockElement::GetProperty($iblockId, $elementId, array(), Array("ID"=> $this->settings[$iblockId]["address"]))->Fetch();
        if (!empty($arAddress["VALUE"]) && ($arCoords = $this->getCoordsFromAddress($arAddress["VALUE"], $this->settings[$iblockId]["geocoder"] == "yandex" ? self::YANDEX : self::GOOGLE)))
        {
            \CIBlockElement::SetPropertyValuesEx($elementId, $iblockId, array(
                $this->settings[$iblockId]["latitude"] => $arCoords["latitude"],
                $this->settings[$iblockId]["longitude"] => $arCoords["longitude"],
            ));
        }
    }

    /**
     * ���������� ���������� ���������� ������
     *
     * @api
     *
     * @uses self::$geocoder ��� �������� �� ���������
     *
     * @param string $address  �����
     * @param int    $geocoder ��������
     *
     * @return array|bool ���������� ������� ��� false � ������ ������
     */
    public function getCoordsFromAddress($address, $geocoder = 0)
    {
        if ($geocoder == 0)
        {
            $geocoder = $this->geocoder;
        }

        if ($geocoder == self::YANDEX)
        {
            $result = $this->yandexGeocode($address, array(
                "results" => 1,
            ));

            if ($result)
            {
                $arCoords = explode(" ", $result["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"]);

                return array(
                    "latitude"  => $arCoords[1],
                    "longitude" => $arCoords[0],
                    "precision" => $result["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["precision"],
                    "geocoder"  => "yandex"
                );
            }

            return false;
        }
        else
        {
            $result = $this->googleGeocode($address);

            if ($result)
            {
                return array(
                    "latitude"  => $result["results"][0]["geometry"]["location"]["lat"],
                    "longitude" => $result["results"][0]["geometry"]["location"]["lng"],
                    "precision" => $result["results"][0]["geometry"]["location_type"],
                    "geocoder"  => "google"
                );
            }

            return false;
        }
    }

    /**
     * @param       $address
     * @param array $params
     *
     * @return array|bool|mixed|null
     */
    private function yandexGeocode($address, array $params = array())
    {
        if ($address == "")
        {
            return false;
        }

        if (!$this->isUtf)
        {
            $address = Encoding::convertEncoding($address, LANG_CHARSET, "utf-8");
        }

        $request = self::YANDEX_GEOCODER_URL . "?geocode=" . urlencode($address);

        $params["format"] = "json";

        foreach ($params as $param => $value)
        {
            if ($value !== "")
            {
                $request .= "&" . $param . "=" . urlencode($value);
            }
        }

        $response = $this->parseResponse(\CHTTP::sGetHeader($request, array(), $this->httpTimeout));

        if (!empty($response["error"]))
        {
            $this->lastError = "Error #" . $response["error"]["status"] . ": " . $response["error"]["message"];
        }

        return $response;
    }

    /**
     * @param       $address
     * @param array $params
     *
     * @return array|bool|mixed|null
     */
    private function googleGeocode($address, array $params = array())
    {
        if ($address == "")
        {
            return false;
        }

        if (!$this->isUtf)
        {
            $address = Encoding::convertEncoding($address, LANG_CHARSET, "utf-8");
        }

        $request = self::GOOGLE_GEOCODER_URL . "?address=" . urlencode($address);

        $params["sensor"] = "false"; // I think your server does not have GPS/GLONASS sensor :)

        foreach ($params as $param => $value)
        {
            if ($value !== "")
            {
                $request .= "&" . $param . "=" . urlencode($value);
            }
        }

        $response = $this->parseResponse(\CHTTP::sGetHeader($request, array(), $this->httpTimeout));

        if ($response["status"] != "OK")
        {
            $this->lastError = "Error: " . $response["error"];
        }

        return $response;
    }

    /**
     * @param $json
     *
     * @return array|bool|mixed|null
     */
    private function parseResponse($json)
    {
        if (!$this->isUtf)
        {
            $json = Encoding::convertEncodingToCurrent($json);
        }

        return \CUtil::JsObjectToPhp($json);
    }
}
?>