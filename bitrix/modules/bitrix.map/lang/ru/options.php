<?

$MESS ['BITRIX_MAP_SCRIPTS_LOAD'] = "Загрузка скриптов";
$MESS ['BITRIX_MAP_RESPONSE_TIME'] = "Максимальное время ожидания загрузки пула скриптов, мс";
$MESS ['BITRIX_MAP_LOAD_TIME'] = "Минимальное время ожидания загрузки контента, мс";
$MESS ['BITRIX_MAP_WITHOUT_MAP'] = "Режим &laquo;Без карты&raquo;";
$MESS ['BITRIX_MAP_WITHOUT_MAP_DESCR'] = "Режим &laquo;Без карты&raquo; запускается при сбое загрузки скрипта карты. Это означает, что компонент продолжает осуществлять навигацию, не используя слой карты, отображая только текстовую и графическую информацию.";

$MESS ['MAIN_TAB_SET'] = "Настройка";
$MESS ['MAIN_TAB_TITLE_SET'] = "Настройка параметров модуля";
$MESS ['MAIN_TAB_INTERFACE'] = "Интерфейс";
$MESS ['MAIN_TAB_TITLE_INTERFACE'] = "Настройка параметров интерфейса";
$MESS ['MAIN_TAB_GEOCODER'] = "Геокодирование";
$MESS ['MAIN_TAB_TITLE_GEOCODER'] = "Настройка параметров геокодирования";
$MESS ['MAIN_TAB_RIGHTS'] = "Доступ";
$MESS ['MAIN_TAB_TITLE_RIGHTS'] = "Уровень доступа к модулю";
$MESS ['BITRIX_MAP_INT_MAIN'] = "Основной интерфейс";
$MESS ['BITRIX_MAP_INT_ROUTES'] = "Прокладка маршрутов";
$MESS ['BITRIX_MAP_INT_ERRORS'] = "Сообщения об ошибках";
$MESS ['BITRIX_MAP_DIFFERENT_INT_SETTINGS'] = "Различные настройки для каждого сайта";
$MESS ['BITRIX_MAP_SITE_ID_SETTINGS'] = "Настройки для сайта";

$MESS ['BITRIX_MAP_DEF_SETTINGS'] = "Параметры по умолчанию";
$MESS ['BITRIX_MAP_HEIGHT'] = "Высота карты, пикселей";
$MESS ['BITRIX_BAR_HEIGHT'] = "Высота верхнего тулбара, пикселей";
$MESS ['BITRIX_PLATE_HEIGHT'] = "Высота верхней области детальной информации, пикселей";
$MESS ['BITRIX_FEW_OBJECTS_HEIGHT'] = "Высота области информации о количестве объектов по одному адресу, пикселей";

$MESS ['BITRIX_MAP_ANIMATION'] = "Настройки анимации";
$MESS ['BITRIX_MAP_ANIMATION_TIME'] = "Время анимации контейнера, мс";
$MESS ['BITRIX_MAP_ITEM_TIME'] = "Время плавного вертикального движения, мс";
$MESS ['BITRIX_BAR_LIST_TIME'] = "Время плавного горизонтального движения, мс";

$MESS ['BITRIX_MAP_DESKTOP_ONLY'] = "Используется только в десктоп-версии";
$MESS ['BITRIX_MAP_MOBILE_ONLY'] = "Используется только в мобильной версии";

$MESS ['MAIN_TAB_ICONS'] = "Значки и цвета";
$MESS ['MAIN_TAB_TITLE_ICONS'] = "Настройка цветов и значков маркеров и кластеров";

$MESS ['BITRIX_MAP_ROUTE_TYPE_YANDEX'] = "Прокладка маршрута Яндекс.Карт";
$MESS ['BITRIX_MAP_ROUTE_TYPE_GOOGLE'] = "Прокладка маршрута Google Maps";
$MESS ['BITRIX_MAP_ROUTE_TYPE_DRIVING'] = "На автомобиле";
$MESS ['BITRIX_MAP_ROUTE_TYPE_WALKING'] = "Пешком";
$MESS ['BITRIX_MAP_ROUTE_TYPE_TRANSIT'] = "На общественном транспорте";
$MESS ['BITRIX_MAP_ROUTE_TYPE_BYCICLE'] = "На велосипеде";

$MESS ['BITRIX_MAP_V_DESKTOP'] = "Десктоп";
$MESS ['BITRIX_MAP_V_MOBILE'] = "Мобильная";
$MESS ['BITRIX_MAP_VERSION'] = "Версия";
$MESS ['BITRIX_MAP_CLUSTER'] = "Настройки иконок кластера";
$MESS ['BITRIX_MAP_CLUSTER_GRID_SIZE'] = "Шаг сетки";
$MESS ['BITRIX_MAP_CLUSTER_COLOR'] = "Цвет цифр";

$MESS ['BITRIX_MAP_CLUSTER_SIZE'] = "Размер";
$MESS ['BITRIX_MAP_CLUSTER_ICON1'] = "0&ndash;10 маркеров";
$MESS ['BITRIX_MAP_CLUSTER_ICON2'] = "10&ndash;100 маркеров";
$MESS ['BITRIX_MAP_CLUSTER_ICON3'] = "100&ndash;1000 маркеров";
$MESS ['BITRIX_MAP_CLUSTER_ICON4'] = "свыше 1000 маркеров";
$MESS ['BITRIX_MAP_FILES_SAVE_ERROR'] = "Не удалось сохранить файлы";
$MESS ['BITRIX_MAP_GEOCODER_SAVE_ERROR'] = "Не удалось сохранить настройки геокодера";

$MESS ['BITRIX_MAP_DIRECTION_OPTIONS'] = "Линия прокладки маршрута";
$MESS ['BITRIX_MAP_DIRECTION_OPTIONS_EXAMPLE'] = "Пример";
$MESS ['BITRIX_MAP_STROKE_COLOR'] = "Цвет линии";
$MESS ['BITRIX_MAP_STROKE_COLOR_ACTIVE'] = "Цвет активной линии";
$MESS ['BITRIX_MAP_STROKE_OPACITY'] = "Прозрачность линии";
$MESS ['BITRIX_MAP_STROKE_OPACITY_HOVER'] = "Прозрачность линии при наведении";
$MESS ['BITRIX_MAP_STROKE_WEIGHT'] = "Толщина линии";

$MESS ['BITRIX_MAP_SPRITES'] = "Файл";
$MESS ['BITRIX_MAP_SPRITES_OBJECTS'] = "Маркеры объектов";
$MESS ['BITRIX_MAP_SPRITES_EVENTS'] = "Маркеры событий";
$MESS ['BITRIX_MAP_SPRITES_ROUTES'] = "Маркеры маршрутов";
$MESS ['BITRIX_MAP_ICONS_SPRITES'] = "Файл";
$MESS ['BITRIX_MAP_ICONS_SPRITES_SIZE'] = "Размер иконки";
$MESS ['BITRIX_MAP_ICONS_SPRITES_ANCHOR'] = "Точка привязки";
$MESS ['BITRIX_MAP_ICONS_SPRITES_OFFSET'] = "Смещение";
$MESS ['BITRIX_MAP_ICONS_SPRITES_LOGO'] = "Размер иконки категории";
$MESS ['BITRIX_MAP_SPRITES_CATEGORIES'] = "Маркеры категорий";
$MESS ['BITRIX_MAP_ICONS_SPRITES_SCALE'] = "Масштаб";
$MESS ['BITRIX_MAP_SPRITES_DIRECTION'] = "Маркеры прокладки маршрута";
$MESS ['BITRIX_MAP_SPRITES_PATH_DEF'] = "Маркеры-узлы маршрутов";
$MESS ['BITRIX_MAP_SPRITES_PATH_ACTIVE'] = "Активные маркеры-узлы маршрутов";
$MESS ['BITRIX_MAP_SPRITES_PATH_STROKE'] = "Линии маршрутов";

$MESS ['BX_MAP_WANT_IBLOCK_TITLE'] = "Модуль инфоблоков не установлен";
$MESS ['BX_MAP_WANT_IBLOCK_DESC'] = "Для функционирования модуля \"Интерактивная карта\" необходим установленный модуль \"Информационные блоки\". Установите его из <a href=\"/bitrix/admin/module_admin.php?lang=ru\">списка модулей</a> и вернитесь к настройкам карты.";

$MESS ['BX_MAP_NO_OPTION'] = "- не выбрано -";

$MESS ['BX_MAP_GC_INT_SELECT_ID'] = "инфоблоке&hellip;";
$MESS ['BX_MAP_GC_INT_ADDRESS'] = "адрес из&hellip;";
$MESS ['BX_MAP_GC_INT_LONGITUDE'] = "долготу&hellip;";
$MESS ['BX_MAP_GC_INT_LATITUDE'] = "широту&hellip;";
$MESS ['BX_MAP_GC_INT_ADD_IB'] = "Добавить инфоблок";
$MESS ['BX_MAP_GC_INT_GEOCODER'] = "геокодера&hellip;";
$MESS ['BX_MAP_GC_INT_GEOCODER'] = "геокодера&hellip;";
$MESS ['BX_MAP_GC_INT_YANDEX'] = "Яндекс";
$MESS ['BX_MAP_GC_INT_GOOGLE'] = "Google";
$MESS ['BX_MAP_GC_INT_IN_CAP'] = "В";
$MESS ['BX_MAP_GC_INT_IN'] = "в";
$MESS ['BX_MAP_GC_INT_AND'] = "и";
$MESS ['BX_MAP_GC_INT_DO_GEOCODING'] = "геокодировать";
$MESS ['BX_MAP_GC_INT_WITH_HELP'] = "с помощью";

$MESS ['MAIN_HINT_RESTORE_DEFAULTS_WARNING'] = "Внимание! Все настройки будут перезаписаны значениями по умолчанию. Продолжить?";
?>