<?
$MESS["MMAP_INSTALL_ERROR_SITE_NOT_FOUND"] = "Выбранный сайт не найден";
$MESS["MMAP_INSTALL_ERROR_IBLOCK_NO_PERMISSTION"] = "Недостаточно прав для работы с модулем инфоблоков";

$MESS["MMAP_INSTALL_IBLOCK_TYPE_LANG_RU"] = "Карта";
$MESS["MMAP_INSTALL_IBLOCK_TYPE_LANG_EN"] = "Map";
$MESS["MMAP_INSTALL_IBLOCK_TYPE_LANG_DEFAULT"] = "Map";
$MESS["MMAP_INSTALL_IBLOCK_SECTION_NAME_LANG_RU"] = "Категория";
$MESS["MMAP_INSTALL_IBLOCK_SECTION_NAME_LANG_EN"] = "Category";
$MESS["MMAP_INSTALL_IBLOCK_SECTION_NAME_LANG_DEFAULT"] = "Category";

$MESS["MMAP_INSTALL_DEMO_SIMPLE"] = "Установить демо-данные";
$MESS["MMAP_INSTALL_SITE_ID"] = "Cайт для установки демо данных";
$MESS["MMAP_INSTALL_FILE_REWRITE"] = "Перезаписывать файлы";
$MESS["MMAP_INSTALL_PUBLIC_DIR"] = "Папка установки";
$MESS["MMAP_INSTALL_WAIT"] = "Устанавливаем...";

$MESS["MMAP_UF_ICON_POS_text"] = "Позиция иконки";
$MESS["MMAP_UF_CLOSED_text"] = "Замкнутый маршрут";
$MESS["MMAP_UF_ROUTE_TYPE_text"] = "Тип маршрута";
$MESS["MMAP_UF_ROUTE_TYPE_1"] = "Пешеходные";
$MESS["MMAP_UF_ROUTE_TYPE_2"] = "Транспортом";
$MESS["MMAP_UF_ROUTE_TYPE_3"] = "По воде";
?>