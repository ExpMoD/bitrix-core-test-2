<?
$MESS["MMAP_INSTALL_NAME"] = "Интерактивная карта";
$MESS["MMAP_INSTALL_DESCRIPTION"] = "Модуль для работы с картами";
$MESS["MMAP_INSTALL_TITLE"] = "Установка модуля";
$MESS["MMAP_UNINSTALL_TITLE"] = "Удаление модуля";
$MESS["SPER_PARTNER"] = "1С-Битрикс";
$MESS["PARTNER_URI"] = "http://www.1c-bitrix.ru";
$MESS["MMAP_INSTALL_NOTIFY"] = "<b>Модуль карт:</b> Обратите внимание, для корректного отображения не рекомендуем выводить на карте описание объектов длиной более 100 символов.";

$MESS["MOD_UNINST_CLEAR_DEMO"] = "Удалить демонстрационные данные";
$MESS["MOD_UNINST_DEMO"] = "Вы также можете удалить демонстрационные данные, добавленные при установке модуля";
?>