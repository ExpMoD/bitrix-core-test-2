<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

$arResult = array();

if (Bitrix\Main\Loader::includeModule("iblock"))
{
    if ($_GET["iblock"] > 0)
    {
        $arResult["properties"] = array();
        $rsProperties = Bitrix\Iblock\PropertyTable::getList(array(
            "filter" => array("IBLOCK_ID" => $_GET["iblock"], "PROPERTY_TYPE" => array("S", "N")),
            "select" => array("ID", "NAME"),
            "order"  => array("NAME" => "ASC")
        ));

        $isUtf = Bitrix\Main\Config\Configuration::getValue("utf_mode");

        while ($arProperty = $rsProperties->fetch())
        {
            $arResult["properties"][$arProperty["ID"]] = $isUtf ? $arProperty["NAME"] : Bitrix\Main\Text\Encoding::convertEncoding($arProperty["NAME"], LANG_CHARSET, "UTF-8");
        }
    }
    else
    {
        $arResult["error"] = "�� ������ ��������";
    }
}
else
{
    $arResult["error"] = "������ ���������� �� ����������";
}


echo json_encode($arResult);
?>