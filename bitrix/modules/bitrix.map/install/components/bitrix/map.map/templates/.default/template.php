<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if (strlen($arResult["ERROR"]) > 0) { ?>
    <? ShowError($arResult["ERROR"]); ?>
<? } else { ?>
    <script>
    $GeoMapp.init({
        defaultPath: <?=$arResult["PARAMS"]["DEFAULT_PATHS"]?>,
        mapType: '<?=$arParams["MAP_TYPE"]?>',
        pageType: '<?=$arParams["DATA_TYPE"]?>',
        loadTime: <?=$arResult["PARAMS"]["LOAD_TIME"]?>,
        responseTime: <?=$arResult["PARAMS"]["RESPONSE_TIME"]?>,
        universalMarker: <?=$arParams["UNIVERSAL_MARKER"] == "Y" ? "true" : "false"?>,
        noCatIcons: <?=$arParams["NO_CAT_ICONS"] == "Y" ? "true" : "false"?>,
        noCats: <?=$arParams["NO_CATS"] == "Y" ? "true" : "false"?>,
        replaceRules: <?=$arParams["REPLACE_RULES"] != "N" ? "true" : "false"?>,
        height: <?=intval($arParams["MAP_HEIGHT"])?>,
        narrowWidth: <?=intval($arParams["MAP_NARROW_WIDTH"])?>,
        animationTime: <?=$arResult["PARAMS"]["ANIMATION_TIME"]?>,
        interfaceText: <?=$arResult["INTERFACE"]["main"]?>,
        routeMessages: <?=$arResult["INTERFACE"]["routes"]?>,
        parseMessages: <?=$arResult["INTERFACE"]["errors"]?>,
        routeType: <?=$arResult["PARAMS"]["ROUTE_TYPES"]?>,
        <? foreach ($arResult["PARAMS"]["ICONS"] as $strVarName => $strData) { ?>
            <?=$strVarName?>: <?=$strData?>,
        <? } ?>
        cats: <?=$arResult["JSON_SECTIONS"]?>,
        items: <?=$arResult["JSON_ELEMENTS"]?>
    });
    </script>

    <div id="bxMapContainer" class="map-wrapper"></div>
<? } ?>
