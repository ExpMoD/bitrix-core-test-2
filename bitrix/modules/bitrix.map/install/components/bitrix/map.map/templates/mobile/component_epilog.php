<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
Bitrix\Main\Loader::includeModule("bitrix.map");

Bitrix\InteractiveMap\DataMixer::IncludeMapScripts("mobile");
$APPLICATION->AddHeadScript("/bitrix/js/bitrix.map/app.js");
?>