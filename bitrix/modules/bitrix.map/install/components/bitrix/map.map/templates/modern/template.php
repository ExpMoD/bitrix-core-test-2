<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if (strlen($arResult["ERROR"]) > 0) { ?>
    <? ShowError($arResult["ERROR"]); ?>
<? } else { ?>
    <script>
    $GeoMapp.init({
        device: {desktop: 'desktop_map_v3.js'},
        mapBounds: <?=$arResult["PARAMS"]["BOUNDS"]?>,
        ajax: '<?=$arParams["AJAX_PATH"]?>',
        <? if (!empty($arParams["OVERLAY_TYPE"])) { ?>overlayType: <?=$arParams["OVERLAY_TYPE"]?>,<? } ?>
        defaultPath: <?=$arResult["PARAMS"]["DEFAULT_PATHS"]?>,
        mapType: '<?=$arParams["MAP_TYPE"]?>',
        pageType: '<?=$arParams["DATA_TYPE"]?>',
        loadTime: <?=$arResult["PARAMS"]["LOAD_TIME"]?>,
        responseTime: <?=$arResult["PARAMS"]["RESPONSE_TIME"]?>,
        universalMarker: <?=$arParams["UNIVERSAL_MARKER"] == "Y" ? "true" : "false"?>,
        noCatIcons: <?=$arParams["NO_CAT_ICONS"] == "Y" ? "true" : "false"?>,
        noCats: <?=$arParams["FULLSCREEN_SLIDE"] != "Y" ? ($arParams["NO_CATS"] == "Y" ? "true" : "false") : "false"?>,
        replaceRules: <?=$arParams["REPLACE_RULES"] != "N" ? "true" : "false"?>,
        height: <?=intval($arParams["MAP_HEIGHT"])?>,
        narrowWidth: <?=intval($arParams["MAP_NARROW_WIDTH"])?>,
        animationTime: <?=$arResult["PARAMS"]["ANIMATION_TIME"]?>,
        interfaceText: <?=$arResult["INTERFACE"]["main"]?>,
        routeMessages: <?=$arResult["INTERFACE"]["routes"]?>,
        parseMessages: <?=$arResult["INTERFACE"]["errors"]?>,
        <? foreach ($arResult["PARAMS"]["ICONS"] as $strVarName => $strData) { ?>
            <?=$strVarName?>: <?=$strData?>,
        <? } ?>
        <? if(!empty($arResult["JSON_FIELDS"])): ?>
        fields: <?=$arResult["JSON_FIELDS"]?>,
        <? endif; ?>
        cats: <?=$arResult["JSON_SECTIONS"]?>,
        <? if ($arParams["LOAD_ITEMS"]) { ?>
        items: <?=$arResult["JSON_ELEMENTS"]?>,
        <? } ?>
        routeType: <?=$arResult["PARAMS"]["ROUTE_TYPES"]?>,
        title: '<?= (!empty($arParams["TITLE_MAP"]) ? $arParams["TITLE_MAP"] : $arResult['NAME']) ?>',
        <? if (!empty($arParams["QUERY"])) { ?>
        query: '<?= $arParams["QUERY"] ?>',
        <? } ?>
        itemCustomView: <?=$arParams["CUSTOM_VIEW"] ? "true" : "false"?>
    });
    </script>

    <div id="bxMapContainer" class="bxmap-wrapper"></div>
<? } ?>
