<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "REPLACE_RULES" => array(
        "NAME" => GetMessage("T_MAP_DESC_REPLACE_RULES"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "Y",
    ),
    "MAP_HEIGHT" => array(
        "NAME" => GetMessage("T_MAP_DESC_MAP_HEIGHT"),
        "TYPE" => "STRING",
        "DEFAULT" => COption::GetOptionInt("bitrix.map", "def_map_height"),
    ),
    "MAP_NARROW_WIDTH" => array(
        "NAME" => GetMessage("T_MAP_DESC_MAP_NARROW_WIDTH"),
        "TYPE" => "STRING",
        "DEFAULT" => "900",
    ),
);

if ($arCurrentValues["DATA_TYPE"] == "objects")
{
    $arTemplateParameters["NO_CATS"] = array(
        "NAME" => GetMessage("T_MAP_DESC_MAP_NO_CATS"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
    );
}
?>
