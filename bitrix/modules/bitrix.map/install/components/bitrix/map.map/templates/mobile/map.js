'use strict';

var $GeoMapp = {
    data: {
        mapType: 'yandex',
        barHeight: 0,
        plateHeight: 70,
        itemTime: 300,
        listTime: 600,
        loadTime: 500,
        responseTime: 1000,
        listenMainScriptLoading: false,
        listenMapScriptLoading: false,
        rebuildContainer: false,
        withoutMap: false,
        interfaceText: {
            collapsePanel: '&#x2190; Collapse',
            categoriesTitle: 'Categories',
            clearField: 'Clear',
            placeHolder: 'Search phrase, i.e. Arbat',
            refreshMarkers: 'Refresh markers',
            clearCategories: 'Cancel',
            closeList: 'Close',
            showList: 'List',
            back: 'Back',
            showMarker: 'Show on map',
            route: 'Route',
            walking: 'On foot',
            transit: 'Transport',
            driving: 'Car',
            bicycling: 'Bicycle',
            toWalk: 'Walk',
            toDrive: 'Drive',
            reverseDirection: 'Change direction',
            createRoute: 'Get',
            pointsTitle: 'Route objects',
            currentPosition: 'My location',
            from: 'From',
            to: 'To',
            catAbstractName: 'Group'
        },
        routeMessages: {
            INVALID_REQUEST: 'Invalid query',
            MAX_WAYPOINTS_EXCEEDED: 'Too many intermediate points',
            NOT_FOUND: 'Part of the coordinate set incorrectly or impossible to recognize the address',
            OK: 'Request complete',
            OVER_QUERY_LIMIT: 'Requests number limit exceeded',
            REQUEST_DENIED: 'Service is not available on this page',
            UNKNOWN_ERROR: 'Error of unknown nature',
            ZERO_RESULTS: 'Unable to build the route',
            blocked: 'Positioning function is disabled. Enter the address manually.',
            none: 'Positioning function is not supported. Enter the address manually.',
            wait: 'Waiting for response...'
        },
        parseMessages: {
            NO_CONTAINER: 'No container for map',
            NO_SCRIPT_ID: 'No script id',
            COMMON_SCRIPT_NOT_LOADED: 'Unable to load components scripts',
            MAP_SCRIPT_NOT_LOADED: 'Map script load fails',
            MAP_DENIED: 'Cant use map for routing',
            PAGETYPE_NOT_DEFINED: 'Page type is not defined',
            NO_CATS: 'No one category',
            NO_ITEMS: 'No one object',
            INDEFINED_ERRORS: 'Undefined error',
            ERRORS_COMMON_LOADING: 'Undefined error while loading main scripts',
            ERRORS_MAP_LOADING: 'Undefined error while loading map scripts'
        },
        icon: {
            category: {
                url: 'category.png',
                size: [70, 60],
                anchor: [35, 35],
                scale: 2
            },
            objects: {
                url: 'objects.png',
                size: [30, 40],
                anchor: [15, 37]
            },
            events: {
                url: 'events.png',
                size: [30, 30],
                anchor: [15, 15]
            },
            routes: {
                url: 'routes.png',
                size: [30, 40],
                anchor: [15, 37]
            },
            direction: {
                url: 'routes.png',
                size: [30, 40],
                anchor: [15, 37]
            }
        },
        path: {
            def: {
                size: [20, 20],
                anchor: [10, 10],
                offset: [60, 0]
            },
            active: {
                size: [20, 20],
                anchor: [10, 10],
                offset: [80, 0]
            },
            strokeWeight: 4,
            strokeColor: '#5c46aa',
            strokeColorActive: '#ec473b',
            strokeOpacity: .7,
            strokeOpacityHover: 1
        },
        directionOptions: {
            strokeColor: '#481fd9',
            strokeOpacity: .7,
            strokeWeight: 3
        },
        cluster: {
            gridSize: 32,
            color: '#fff',
            set: [
                {icon: '1.png', size: 50},
                {icon: '2.png', size: 60},
                {icon: '3.png', size: 74},
                {icon: '4.png', size: 90}
            ]
        },
        routeType: {
            google: ['walking', 'driving', 'transit'],
            yandex: ['driving']
        },
        libs: [
            'common.js',
            'iscroll.js'
        ],
        mapScript: {
            google: {
                main: 'http://maps.googleapis.com/maps/api/js?sensor=false&language=ru',
                cluster: 'markerclusterer.js'
            },
            yandex: {
                main: 'http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru'
            }
        }
    },

    alternate: {
        div: 'alt-block',
        ol: 'alt-ordered-list',
        ul: 'alt-unordered-list',
        li: 'alt-list-item',
        dl: 'alt-definition-list',
        dt: 'alt-definition-title',
        dd: 'alt-definition-description',
        span: 'alt-inline'
    },

    root: $(document.documentElement),
    head: $(document.head),
    offset: 0,
    clearance: 0,
    options: {},
    params: {
        list: 'show',
        item: 'none'
    },

    scriptSet: {},
    cats: {},
    counts: {},
    sets: {},
    icon: {},
    items: {},
    activeItems: {},
    markers: {},
    activeMarkers: {},
    routes: {},
    directions: {},

    init: function (options) {
        var _class = this,
            script,
            listener = $.proxy(this.errorListener, this);

        document.write(this.createElement('*div', {
            id: 'bxMapFakeElement'
        }).get(0).outerHTML);

        if (!this.head.length) {
            this.head = $('head');
        }

        if (!options.cats) {
            options.cats = {};
        }

        if (!options.defaultPath) {
            script = $('#bxMapScript');

            if (script.length) {
                var componentPath = (script.attr('src').replace(/js\/[^\/]+$/, '') + '/').replace(/\/+$/, '/');

                options.defaultPath = {
                    libs: componentPath.replace(/[^\/]+\/$/, '') + 'lib/',
                    images: componentPath + 'images/'
                }
            }
        }

        for (var i in options) {
            switch (i) {
                case 'items':
                    this.data.entities = options.items;
                    break;
                case 'icon':
                    extendObject(this.data.icon[options.pageType], options.icon[options.pageType] || options.icon);
                    break;
                case 'libs':
                    this.data.libs = extendArray(this.data.libs, options.libs);
                    break;
                default:
                    if (this.data[i] && (this.data[i] instanceof Object || this.data[i] instanceof Array)) {
                        extendObject(this.data[i], options[i]);
                    } else {
                        this.data[i] = options[i];
                    }

                    break;
            }
        }

        $(function () {
            var _wrapper = _class.getWrapper()

            if (_wrapper && _wrapper.length) {
                _wrapper.css({
                    top: _class.data.barHeight + 'px'
                });
            }
        });

        if (!options.defaultPath && !script.length) {
            $(function () {
                _class.showError('NO_SCRIPT_ID');
            });

            return;
        }

        this.snapError(listener, 'listenMainScriptLoading', 'common', 'ERRORS_COMMON_LOADING');
        this.createScriptQueue(
                this.data.libs,
                'common',
                'COMMON_SCRIPT_NOT_LOADED'
            ).then(function (error) {
                if (!error) {
                    try {
                        _class.snapError(listener);

                        if (!_class.data.pageType) {
                            $Switch.init();
                            $(document).trigger('tab:create');
                        } else {
                            _class.preCreatingPage();
                        }
                    } catch (z) {
                        _class.showError('COMMON_SCRIPT_NOT_LOADED');
                    }
                } else {
                    _class.showError(error);
                }
            });

        function extendObject (defObject, userObject) {
            if (userObject !== undefined && userObject !== null && userObject !== '') {
                if (userObject instanceof Object || userObject instanceof Array) {
                    for (var i in userObject) {
                        if (userObject[i] !== undefined && userObject[i] !== null && userObject[i] !== '') {
                            if (userObject[i] instanceof Object || userObject[i] instanceof Array) {
                                if (!defObject[i]) {
                                    defObject[i] = new userObject[i].constructor();
                                }

                                extendObject(defObject[i], userObject[i]);
                            } else {
                                defObject[i] = userObject[i];
                            }
                        }
                    }
                } else {
                    defObject = userObject;
                }
            } else {

            }

            return defObject;
        }

        function extendArray (base, elements) {
            var intersect = [],
                adding = [];

            for (var i = 0, count = 0; i < elements.length; i++, count = 0) {
                for (var j = 0; j < base.length; j++) {
                    if (elements[i] == base[j] || elements[i].toString() == base[j].toString()) {
                        intersect.push(base.splice(j, 1));
                        count++;
                        break;
                    }
                }

                if (!count) {
                    adding.push(elements[i]);
                }
            }

            return base.concat(adding);
        }
    },

    preCreatingPage: function () {
        var _class = this;

        this.maxPageWidth = this.root.width();
        this.maxPageHeight = this.root.height();
        this.minHeight = this.clearance;
        this.maxHeight = this.maxPageHeight - this.data.plateHeight - this.data.barHeight;

        $('<style>').attr('type', 'text/css').appendTo($('head'));
        this.link = document.styleSheets[document.styleSheets.length - 1];

        if (!this.link.cssRules) {
            this.link.cssRules = this.link.rules;
        }

        this.data.icon[this.data.pageType].url = this.setFullPath(this.data.icon[this.data.pageType].url, 'images');

        if (!this.data.icon[this.data.pageType].anchor) {
            this.data.icon[this.data.pageType].anchor = [
                this.data.icon[this.data.pageType].size[0] / 2,
                this.data.icon[this.data.pageType].size[1]
            ];
        }

        if (this.data.pageType == 'category') {
            $(function () {
                _class.createCategoryData(_class.data.pageType);
            });
        } else {
            if (this.data.withoutMap) {
                create(true);
            } else {
                var listener = $.proxy(this.errorListener, this);

                this.snapError(listener, 'listenMapScriptLoading', 'maps', 'ERRORS_MAP_LOADING');

                try {
                    $DiffMap.init(this.data.mapType, this.data.pageType);
                    $(document).on('map:done', function (e, status) {
                        create(!status);
                        _class.snapError(listener);
                    });

                    this.createScriptQueue(this.data.mapScript[this.data.mapType], 'maps', 'MAP_SCRIPT_NOT_LOADED').then(function (error) {
                        if (!error) {
                            $DiffMap.mapInitialize(true);
                        } else {
                            if (_class.data.listenMapScriptLoading) {
                                _class.showError(error);
                            } else {
                                create(true);
                            }

                            _class.snapError(listener);
                        }
                    });
                } catch (z) {
                    _class.showError('COMMON_SCRIPT_NOT_LOADED');
                }
            }
        }

        function create (status) {
            _class.data.withoutMap = status;
            $(function () {
                _class.creatingPage();
            });
        }
    },

    creatingPage: function () {
        var _class = this,
            _wrapper = this.getWrapper();

        if (!_wrapper || !_wrapper.length) {
            if (this.data.rebuildContainer) {
                _wrapper = this.insertWrapper();
            } else {
                this.showError('NO_CONTAINER');
                return;
            }
        }

        $(document)
            .on('set:view', function (e) {
                if (_class.listLayer) {
                    if (_class.params.list == 'hide') {
                        _class.listLayer.removeClass('active');

                        if (!_class.data.withoutMap) {
                            $DiffMap.createMap({
                                container: _class.mapContainer.get(0),
                                scroll: true
                            });

                            if (_class.data.pageType == 'objects' || _class.data.pageType == 'events') {
                                if (!_class.markersComplete) {
                                    _class.showMarkers();
                                    _class.markersComplete = true;
                                }
                            }
                        } else {
                            _class.params.item = 'show';
                        }
                    } else {
                        _class.mapLayer.removeClass('active');
                    }
                }

                if (_class.itemLayer) {
                    if (_class.params.item == 'none') {
                        _class.itemLayer.removeClass('active hide');
                    } else {
                        if (!_class.data.withoutMap) {
                            if (_class.data.pageType == 'objects' || _class.data.pageType == 'events') {
                                var markerName = _class.data.pageType.slice(0, -1) + 'Marker';

                                if (!_class.itemComplete && _class.options.item) {
                                    $(document).trigger(markerName + ':click', [_class.options.item]);
                                    _class.itemComplete = true;
                                }
                            } else if (_class.data.pageType == 'routes') {
                                if (!_class.routeInitComplete && _class.options.item) {
                                    $(document).trigger('route:show', [_class.options.item]);
                                    _class.routeInitComplete = true;
                                }

                                if (!_class.routeComplete && !_class.data.withoutMap) {
                                    _class.placeRoute();
                                    _class.routeComplete = true;
                                }
                            } else if (_class.data.pageType == 'direction') {
                                _class.addDirection();
                            }
                        }

                        _class.itemLayer.addClass('active');

                        if (_class.params.item == 'show') {
                            _class.itemLayer.removeClass('hide');
                        } else {
                            _class.itemLayer.addClass('hide');
                        }
                    }
                }
            })
            .on('app:title', function (e, title) {
                if (window.app) {
                    app.setPageTitle(title);
                }
            })
            .on('app:button', function (e, type) {
                if (window.app) {
                    app.removeButtons();

                    if (type) {
                        app.addButtons({
                            button: {
                                type: type
                            }
                        });
                    }
                }
            });

        $Switch.init();
        this.createItemData(this.data.pageType);
    },

    createCategoryData: function (pageType) {
        var _wrapper = this.getWrapper();

        if (!_wrapper || !_wrapper.length) {
            this.showError('NO_CONTAINER');
            return;
        }

        var _catlist = this.createElement('*ul', 'catlist'),
            scale = this.data.icon[pageType].scale || 1,
            catLink = this.data.catLink || '',
            count = 0;

        for (var i in this.data.cats) {
            var rule = '';

            if (this.data.cats[i].icon) {
                rule += 'background-image:url(' + this.data.cats[i].icon + ');';
            }

            if (this.data.cats[i].pos) {
                rule += 'background-position:-' + this.data.cats[i].pos / scale + 'px 0;';
            }

            if (rule) {
                $Common.createRules('.catlist .item.' + i + ' > a:before', rule, this.link);
            }

            this.createElement('*li', 'item ' + i).append(
                this.createElement('a', {
                    'href': $Common.insertParam(catLink, {cat: i})
                }).html(this.data.cats[i].name)
            ).appendTo(_catlist);
            count ++;
        }

        if (count) {
            _catlist.appendTo(
                this.createElement('*div', 'catcontainer', {
                    'data-container': 'category'
                }).appendTo(_wrapper)
            );
            $Common.createRules('.catlist .item > a:before', 'background-image:url(' + this.data.icon[pageType].url + ');background-size:auto ' + this.data.icon[pageType].size[1] / scale + 'px;background-repeat:no-repeat;', this.link);
        } else {
            this.showError('NO_CATS');
        }
    },

    createItemData: function (pageType) {
        var _class = this,
            search = location.search.slice(1).split('&'),
            header = this.createElement('*div', 'header content', {
                'data-action': 'detail'
            }),
            content = this.createElement('*div', 'content', {
                'data-container': 'content'
            }),
            dl = this.createElement('*dl', 'item ' + pageType).appendTo(header),
            startY,
            currentY;

        if (!this.data.withoutMap) {
            if (pageType == 'objects' || pageType == 'events') {
                if (this.data.directionLink) {
                    dl.append(
                        this.createElement('*dt', 'route', {
                            'data-action': 'route'
                        }).append(
                                this.createElement('a', {
                                    'href': $Common.insertParam(this.data.directionLink)
                                }).html(this.data.interfaceText.route)
                            )
                    );
                }
            } else {
                var directionPath = pageType == 'direction' ? this.data.directionOptions : null;

                this.routeIcon = $DiffMap.createRouteIconSet(this.data.icon[pageType], this.data.path, directionPath);
            }
        }

        dl.append(
            this.createElement('*dd', 'data', {
                'data-container': 'info'
            })
        );
        this.listLayer = this.getWrapper().find('[data\-item~="page"]');

        if (!this.listLayer.length) {
            this.listLayer = this.createElement('*div', 'page-list', {
                'data-item': 'page left'
            }).appendTo(this.getWrapper());
        }

        this.listLayer.css({
            minHeight: this.maxPageHeight - this.data.barHeight + 'px'
        });

        this.mapLayer = this.createElement('*div', 'page-map', {
            'data-item': 'page right',
            'data-type': 'map'
        }).appendTo(this.getWrapper());

        this.mapContainer = this.createElement('*div', 'page-map-container', {
            'data-container': 'map'
        }).appendTo(this.mapLayer);

        for (var i in search) {
            var j = search[i].split(/\s*=\s*/);

            if (j[1]) {
                if (j[0] == 'cat') {
                    for (var k = 0, precat = j[1].split(/\s*,\s*/); k < precat.length; k++) {
                        if (!this.data.cats[precat[k]]) {
                            precat.splice(k, 1);
                        }
                    }

                    if (precat.length) {
                        this.options.cat = precat;
                    }
                } else if (j[0] == 'item') {
                    if (this.data.entities[j[1]]) {
                        this.options.item = j[1];
                    }
                } else {
                    this.options[j[0]] = j[1];
                }
            }
        }

        this.itemLayer = this.createElement('*div', 'page-item', {
            'data-container': 'item'
        }).css({
                minHeight: this.maxPageHeight - this.data.barHeight + 'px'
            }).append(header, content).appendTo(this.mapLayer);

        if (pageType == 'direction') {
            if (this.data.withoutMap) {
                this.showError('MAP_DENIED');
                return;
            } else {
                this.params.list = 'show';
                this.createDirectionData();
            }
        } else {
            if (this.options.item) {
                var catid = this.getCatId(this.data.entities[this.options.item].cat),
                    count = 0;

                if (this.options.cat) {
                    for (var i in this.options.cat) {
                        if (this.options.cat == catid) {
                            count++;
                            break;
                        }
                    }

                    if (!count) {
                        this.options.cat.push(catid);
                    }
                } else {
                    if (this.data.pageType == 'events') {
                        this.options.cat = [catid];
                    }
                }
            }

            if (this.options.cat) {
                this.activeCatID = this.options.cat[0];
            }

            if (this.options.item) {
                this.params.list = 'hide';

                if (this.options.type && !this.data.withoutMap) {
                    this.params.item = 'hide';
                } else {
                    this.params.item = 'show';
                }
            } else if (this.options.type && !this.data.withoutMap) {
                this.params.list = 'hide';
            }

            if (!this.createMapData(pageType)) {
                this.showError('NO_ITEMS');
                return;
            }
        }

        this.complete = true;
        this.defineButtonType();
        this.manageList();
        $(document).trigger('tab:create', [this.activeCatID]);

        if (this.params.list == 'hide') {
            $(document).trigger('set:view');
        }

        this.listLayer.on($Params.altEvents.transitionEnd, $.proxy(this.listTransitionEnd, this));
        this.setProperties('list');

        if (!this.data.withoutMap) {
            this.itemLayer
                .on($Params.altEvents.transitionEnd, $.proxy(this.itemTransitionEnd, this))
                .on($Params.altEvents.start, '[data\-action~="detail"]', _start)
                .css({
                    top: this.maxHeight + 'px'
                });

            this.setProperties('item');
            this.manageItem();
        } else {
            this.root.addClass('without-map');
        }

        $(document)
            .on($Params.altEvents.start, '.control[data\-action~="list"], .control[data\-action~="geo"], .control[data\-action~="route"]', function (e) {
                e.preventDefault();
                e.stopPropagation();

                _class.buttonReaction();
            });

        function _start (e) {
            var _self = $(e.target);

            if (_self.closest('[data\-action~="route"]').length) {
                location = _self.attr('href');
            } else {
                e.preventDefault();
                e.stopPropagation();

                startY = ($Params.hasTouch ? e.originalEvent.touches[0] : e.originalEvent).pageY;
                _class.itemLayer.css($Params.transitionDuration, '');

                $(document).on($Params.altEvents.move, _move);
                $(document).on($Params.altEvents.end, _end);
            }
        }

        function _move (e) {
            e.preventDefault();
            e.stopPropagation();

            currentY = ($Params.hasTouch ? e.originalEvent.touches[0] : e.originalEvent).pageY;

            if (isFinite(currentY - startY)) {
                if ($Params.hasTouch) {
                    _class.resize('item', currentY - startY);
                } else {
                    if (currentY <= 0 || currentY >= _class.maxPageHeight) {
                        if ($Params.hasTransitionEnd) {
                            _class.itemLayer.css($Params.transitionDuration, _class.data.itemTime / 1000 + 's');
                        }
                        _class.resize('item', 0);

                        $(document).off($Params.altEvents.move, _move);
                        $(document).off($Params.altEvents.end, _end);
                    } else {
                        _class.resize('item', currentY - startY);
                    }
                }
            }
        }

        function _end (e) {
            $(document).off($Params.altEvents.move, _move);
            $(document).off($Params.altEvents.end, _end);

            if (!currentY && !isFinite(currentY - startY)) {
                return;
            }

            if (currentY - startY < (_class.minHeight - _class.maxHeight) * Math.pow(-1, !!_class.offset * 1) / 3) {
                _class.manageItem('show');
            } else {
                _class.manageItem('hide');
            }
            currentY = NaN;
        }
    },

    createMapData: function (pageType) {
        var _class = this,
            _navs = {},
            countCats = 0,
            countItems = 0;

        if (!this.data.withoutMap) {
            if (pageType == 'objects' || pageType == 'events') {
                $DiffMap.setClusterOptions(this.data.cluster);
            }
        }

        for (var i in this.data.entities) {
            if (!this.data.entities[i].cat || !this.data.entities[i].name || !this.data.entities[i].lat || !this.data.entities[i].lng) {
                continue;
            }

            var catid = this.getCatId(this.data.entities[i].cat);

            if (!this.sets[catid]) {
                if (!this.data.cats[catid]) {
                    countCats++;
                    this.data.cats[catid] = {
                        name: this.data.interfaceText.catAbstractName + ' #' + countCats,
                        pos: 0
                    };
                }

                createCat(catid);
                this.items[catid] = {};

                if (!this.data.withoutMap) {
                    if (pageType == 'objects' || pageType == 'events') {
                        this.icon[catid] = $DiffMap.createIconSet(this.data.cats[catid], this.data.icon[pageType], catid, this.data.noIcons);
                    }
                }
            }

            var _item = this.createElement('*li', 'item next', {
                'data-action': 'item',
                'data-id': i,
                'data-catid': catid
            }).append(
                    setContent(this.data.entities[i], i, catid)
                ).appendTo(
                    this.sets[catid]
                );

            this.items[catid][i] = [this.data.entities[i].name, _item.data({
                lat: this.data.entities[i].lat,
                lng: this.data.entities[i].lng
            })];
            this.counts[catid]++;
            countItems++;

            if (pageType == 'objects' || pageType == 'events') {
                if (!this.data.withoutMap) {
                    this.markers[i] = $DiffMap.createMarker(this.data.entities[i], i, catid, this.icon[catid], 'default').marker;
                }
            } else if (pageType == 'routes') {
                var points = this.data.entities[i].points;

                if (this.data.entities[i].pops) {
                    if (!this.data.cats.pops) {
                        this.data.cats.pops = {
                            name: '����������',
                            pos: 0
                        };
                    }

                    if (!this.sets.pops) {
                        createCat('pops');
                    }

                    this.counts.pops++;
                    this.sets.pops.append(_item.clone(true));
                }

                if (!this.data.withoutMap) {
                    var status = 'active',
                        geo = $DiffMap.createMarker(this.data.entities[i], i, catid, this.routeIcon, status);

                    this.routes[i] = {
                        id: i,
                        markers: [geo.marker],
                        coords: [geo.point],
                        bounds: {
                            lat: [this.data.entities[i].lat],
                            lng: [this.data.entities[i].lng]
                        },
                        polyLine: null
                    };
                }

                if (points && points.length) {
                    var routeInfo = this.createElement('*dl', 'routeinfo').append(
                            this.createElement('*dt', 'head').html(this.data.interfaceText.pointsTitle + ':')
                        ),
                        items = this.createElement('*ul', 'items').appendTo(
                            this.createElement('*dd', 'body').appendTo(routeInfo)
                        )

                    for (var j = 0, point; j < points.length; j++) {
                        if (!this.data.withoutMap) {
                            var markerID = i + '_' + j;
                            this.routes[i].bounds.lat.push(points[j].lat);
                            this.routes[i].bounds.lng.push(points[j].lng);

                            if (points[j].description) {
                                var subgeo = $DiffMap.createMarker(points[j], markerID, catid, this.routeIcon, status);

                                this.routes[i].coords.push(subgeo.point);
                                this.routes[i].markers.push(subgeo.marker);
                            } else {
                                this.routes[i].coords.push($DiffMap.createPoint(points[j]));
                            }
                        }

                        if (points[j].description) {
                            point = this.createElement('*dl', 'items-point').appendTo(
                                this.createElement('*li', 'item').appendTo(items)
                            );

                            if (points[j].photo) {
                                point.append(
                                    this.createElement('*dt', 'image').append(
                                        this.createElement('img', {
                                            'src': points[j].photo
                                        })
                                    )
                                );
                            }

                            point.append(
                                this.createElement('*dd', 'data').append(
                                    this.createElement('*div', 'description').html(points[j].description),
                                    this.createElement('*div', 'address').html(points[j].address)
                                )
                            );
                        }
                    }

                    if (!this.data.withoutMap) {
                        if (this.data.entities[i].closed && points.length > 1) {
                            if (points.length > 1) {
                                this.routes[i].coords.push(this.routes[i].coords[0]);
                            }
                        } else {
                            if (this.routes[i].markers.length == 1) {
                                this.routes[i].markers.push($DiffMap.createMarker(points[points.length - 1], i + '_' + (points.length - 1), catid, this.routeIcon, status).marker);
                            }

                            this.routes[i].end = true;
                        }

                        this.routes[i].polyLine = $DiffMap.createPolyLine(i, this.routes[i].coords, this.routeIcon._pathActive);
                    }

                    this.data.entities[i].routeInfo = routeInfo.clone(true);
                }
            }
        }

        if (!countItems) {
            return countItems;
        }

        if (pageType == 'objects') {
            var _container = $('[data\-container~="items"]');

            for (var i in this.data.cats) {
                if (this.counts[i]) {
                    if (_container.length) {
                        this.cats[i] = this.sets[i].appendTo(_container);
                    } else {
                        this.cats[i] = this.sets[i].appendTo(this.listLayer);
                    }
                }
            }
        } else if (pageType == 'events' || pageType == 'routes') {
            var _container = $('[data\-container~="switch"]');

            if (!_container.length) {
                _container = this.createElement('*div', 'switch', {
                    'data-container': 'switch'
                }).appendTo(this.listLayer);
            }

            var _navContainer = this.createElement('*ul', 'navigation narrow').prependTo(_container);

            for (var i in this.data.cats) {
                if (this.counts[i]) {
                    this.cats[i] = this.sets[i].appendTo(
                        this.createElement('*div', 'body', {
                            'data-target': 'switch',
                            'data-id': i
                        }).appendTo(_container)
                    );
                    _navs[i].prepend(
                        this.createElement('*span', 'navigation-count').html(this.counts[i])
                    ).appendTo(_navContainer);
                }
            }

            if (_navs.pops) {
                _navs.pops.prependTo(_navContainer);
            }
        }

        $(document)
            .on('switch:done', function (e, id) {
                if (_class.data.pageType != 'objects') {
                    _class.activeNewCatID = id;
                }
            })
            .on('click', '[data\-container~="items"] [data\-action~="item"]', function (e) {
                e.preventDefault();
                e.stopPropagation();

                var id = $(this).data('id');

                _class.manageList('hide');
                if (id != _class.activeID) {
                    $(document).trigger(pageType.slice(0, -1) + 'Item:click', [id]);
                }
            })
            .on('objectItem:click', function (e, id) {
                $(document).trigger('objectMarker:click', [id]);
            })
            .on('objectMarker:click', function (e, id) {
                if (id) {
                    if (_class.resetActiveMarker(_class.activeID) != id) {
                        _class.setActiveMarker(id);
                    }
                }
            })
            .on('eventItem:click', function (e, id) {
                $(document).trigger('eventMarker:click', [id]);
            })
            .on('eventMarker:click', function (e, id) {
                if (id) {
                    var catid = _class.getCatId(_class.data.entities[id].cat);

                    if (catid != _class.activeCatID) {
                        _class.clearMarkers();
                        _class.activeCatID = catid;

                        if (_class.markersComplete) {
                            _class.showMarkers();
                        }

                        _class.setActiveMarker(id);
                    } else {
                        if (_class.resetActiveMarker(_class.activeID) != id) {
                            _class.setActiveMarker(id);
                        }
                    }
                }
            })
            .on('routeItem:click', function (e, id) {
                $(document).trigger('route:show', [id]);
            })
            .on('route:show', function (e, id) {
                if (id) {
                    if (_class.activeID) {
                        if (_class.activeID != id) {
                            _class.removeRoute(_class.activeID);
                            _class.showRoute(id);

                            if (!_class.data.withoutMap) {
                                delete _class.routeComplete;
                            }
                        }
                    } else {
                        _class.showRoute(id);
                    }
                }
            });

        return countItems;

        function createCat (catid) {
            _class.counts[catid] = 0;
            _class.sets[catid] = _class.createElement('*ul', 'items map-items', {
                'data-container': 'items',
                'data-id': catid
            });

            if (pageType == 'routes' || pageType == 'events') {
                _navs[catid] = _class.createElement('*li', 'navigation-item', {
                    'data-action': 'switch',
                    'data-id': catid
                }).append(
                        _class.createElement('*span', 'navigation-title').html(_class.data.cats[catid].name)
                    )
            }
        }

        function setContent (data, id, catid) {
            var content = _class.createElement('*dl', 'item-short-info'),
                info = _class.createElement('*dd', 'data', {
                    'data-container': 'info'
                }),
                infoObject = [];

            if (data.photo) {
                content.append(
                    _class.createElement('*dt', 'image').append(
                        _class.createElement('img', {
                            'src': data.photo
                        })
                    )
                );
            }

            if (data.name) {
                info.append(
                    _class.createElement('*div', 'description').html(data.name)
                );
            }

            if (data.opening && data.opening.length) {
                if (typeof data.opening == 'string') {
                    infoObject.push(
                        _class.createElement('*div', 'more').html(data.opening)
                    );
                } else {
                    for (var i in data.opening) {
                        infoObject.push(
                            _class.createElement('*div', 'more').html(data.opening[i])
                        );
                    }
                }
            }

            if (data.address) {
                var address = _class.createElement('*div', 'address').html(data.address);

                switch (pageType) {
                    case 'objects':
                        infoObject.unshift(address);
                        break;
                    case 'events':
                    case 'routes':
                        infoObject.push(address);
                        break;
                    case 'direction':

                        break;
                }
            }

            for (var i in infoObject) {
                info.append(infoObject[i]);
            }

            data.info = info.clone(true);

            return content.append(info);
        }
    },

    showMarkers: function (cat) {
        var cats = cat || this.activeCatID || this.options.cat;

        if (!cats) {
            if (this.data.pageType == 'objects') {
                cats = $Common.toIDArray(this.cats);
            }
        } else {
            cats = $Common.toIDArray(cats);
        }

        if (cats) {
            for (var catid in cats) {
                for (var i in this.items[cats[catid]]) {
                    this.activeMarkers[i] = this.markers[i];
                }
            }

            $DiffMap.updateMarkerCluster();
        }
    },

    updateMarkers: function () {
        for (var catid in this.cats) {
            for (var i in this.items[catid]) {
                if (!this.items[catid][i][1].hasClass('hidden')) {
                    this.activeMarkers[i] = this.markers[i];
                } else {
                    $DiffMap.removeMarker(this.markers[i], catid);
                    this.resetActiveMarker(i);
                    delete this.activeMarkers[i];
                }
            }
        }

        $DiffMap.updateMarkerCluster();
    },

    clearMarkers: function () {
        for (var i in this.activeMarkers) {
            var catid = this.getCatId(this.data.entities[i].cat);

            $DiffMap.removeMarker(this.markers[i], catid);
            delete this.activeMarkers[i];
        }

        this.resetActiveMarker(this.activeID);
    },

    setActiveMarker: function (id) {
        if (id) {
            var data = this.data.entities[id],
                catid = this.getCatId(data.cat);

            if (this.params.item == 'none') {
                this.params.item = 'hide';
            }

            this.activeID = id;
            this.active = this.items[catid][id][1];
            this.active.addClass('active');

            if (!this.data.withoutMap) {
                $DiffMap.setMarkerView(this.markers[id], this.icon[catid], catid, 'active');
            }

            this.showItem(id, data);
        }

        return id;
    },

    resetActiveMarker: function (id) {
        if (id) {
            var catid = this.getCatId(this.data.entities[id].cat),
                activeID;

            if (this.activeID == id) {
                this.active.removeClass('active');
                this.params.item = 'none';
                this.showItem(this.activeID);
                activeID = this.activeID;
                delete this.activeID;
                delete this.active;
            }

            if (!this.data.withoutMap) {
                $DiffMap.setMarkerView(this.markers[id], this.icon[catid], catid, 'default');
            }

            return activeID;
        }

        return id;
    },

    placeRoute: function () {
        if (this.activeID) {
            $DiffMap.showRoute(this.routes[this.activeID], this.routeIcon, 'active');
            $DiffMap.fitMap(this.routes[this.activeID].bounds);
        }
    },

    showRoute: function (id) {
        if (id) {
            var data = this.data.entities[id],
                catid = this.getCatId(data.cat);

            if (this.params.item == 'none') {
                this.params.item = 'hide';
            }

            this.activeID = id;
            this.active = this.listLayer.find('[data-catid="' + catid + '"]').filter('[data-id="' + id + '"]');
            this.active.addClass('active');
            this.showItem(id, data);
        }

        return id;
    },

    removeRoute: function (id) {
        if (id) {
            var activeID;

            if (this.activeID == id) {
                this.active.removeClass('active');
                this.params.item = 'none';
                this.showItem(this.activeID);
                activeID = this.activeID;
                delete this.activeID;
                delete this.active;
            }

            if (!this.data.withoutMap) {
                $DiffMap.removeRoute(this.routes[id]);
            }

            return activeID;
        }

        return id;
    },

    showItem: function (id, data) {
        var _class = this;

        if (data) {
            var _link = this.itemLayer.find('[data\-action~="route"] a'),
                _content = this.itemLayer.find('[data\-container~="content"]').empty();

            if (_link.length) {
                _link.attr('href', $Common.insertParam(_link.attr('href'), {item: id}));
            }

            this.itemLayer
                .addClass('active hide')
                .find('[data\-container~="info"]').replaceWith(data.info);

            $.when(getContent()).then(function () {
                _content.append(data.detail);

                if (data.routeInfo) {
                    _content.append(data.routeInfo);
                }
            });
        } else {
            this.itemLayer.removeClass('active hide');
        }

        function getContent () {
            var defer = $.Deferred(),
                start = new Date(),
                dataType = 'html';

            if (data.detail === undefined) {
                var detail = _class.createElement('*div', 'info-container');

                if (data.description) {
                    detail.append(
                        _class.createElement('*div', 'info').html(data.description)
                    );
                }

                if (data.phone || data.link) {
                    var ul = _class.createElement('*ul', 'group icon').appendTo(detail);

                    if (data.phone && data.phone.replace(/^\s*/, '').replace(/\s*$/, '')) {
                        var phones = data.phone.split(',');

                        for (var i = 0; i < phones.length; i++) {
                            ul.append(
                                _class.createElement('*li', 'item').append(
                                    _class.createElement('a', 'block phone', {
                                        'href': 'tel://' + phones[i]
                                    }).html(phones[i])
                                )
                            );
                        }
                    }

                    if (data.link) {
                        ul.append(
                            _class.createElement('*li', 'item').append(
                                _class.createElement('a', 'block url', {
                                    'href': data.link
                                }).html(data.link)
                            )
                        );
                    }
                }

                data.detail = detail.clone(true);

                if (_class.data.ajax !== undefined) {
                    $.ajax({
                        url: $Common.insertParam(_class.data.ajax, {
                            item: id,
                            type: _class.data.pageType
                        }),
                        type: 'POST',
                        dataType: dataType,
                        beforeSend: function () {
                            _class.itemLayer.addClass('loading');
                        },
                        success: function (additional) {
                            if (data.detail) {
                                data.detail.append(additional);
                            } else {
                                data.detail = additional;
                            }
                            returnResult();
                        },
                        error: function (e) {
                            _class.itemLayer.removeClass('loading');
                        }
                    });
                }
            } else {
                returnResult();
            }

            function returnResult () {
                var delta = new Date() - start;

                if (delta < _class.data.loadTime) {
                    setTimeout(function () {
                        _class.itemLayer.removeClass('loading');
                        defer.resolve();
                    }, _class.data.loadTime - delta);
                } else {
                    defer.resolve();
                }
            }

            return defer.promise();
        }
    },

    listTransitionEnd: function (e) {
        e.stopPropagation();

        this.defineButtonType();
        $(document).trigger('set:view');
    },

    itemTransitionEnd: function (e) {
        e.stopPropagation();

        this.itemLayer.css($Params.transitionDuration, '');
        this.itemLayer.toggleClass('hide', this.offset == 0);
        this.defineButtonType();
    },

    setProperties: function (side) {
        var item = this[side + 'Layer'],
            time = this.data[side + 'Time'],
            dir;

        if (side == 'item') {
            dir = 'top';
        } else if (side == 'list') {
            dir = 'left';
        }

        if ($Params.hasTransitionEnd) {
            if ($Params.hasTransform) {
                item
                    .css($Params.transitionProperty, $Params.cssVendor + 'transform')
                    .css($Params.transformOrigin, '0 0');
            } else {
                item.css($Params.transitionProperty, dir);
            }

            item
                .css($Params.transitionTimingFunction, 'cubic-bezier(.33, .66, .66, 1)')
                .css($Params.transitionDuration, time / 1000 + 's');
        }
    },

    resize: function (side, delta) {
        var item = this[side + 'Layer'],
            func = this[side + 'TransitionEnd'],
            time = this.data[side + 'Time'],
            pos = {},
            dir;

        if (side == 'item') {
            pos.X = 0;
            pos.Y = delta + this.offset;
            dir = {
                top: pos.Y + this.maxHeight
            };
        } else if (side == 'list') {
            pos.X = delta;
            pos.Y = 0;
            dir = {
                left: pos.X
            };
        }

        if ($Params.hasTransform) {
            item.css($Params.transform, $Params.getTranslate(pos.X + 'px', pos.Y + 'px'));
        } else {
            item.animate(dir, {
                duration: time,
                complete: function() {
                    func();
                }
            });
        }
    },

    defineButtonType: function (clear) {
        var buttonType;

        if (!this.withoutRightButton) {
            if (!clear) {
                if (this.data.withoutMap) {
                    if (this.params.list != 'show') {
                        buttonType = 'list';
                    }
                } else {
                    if (this.data.pageType == 'direction') {
                        //type = 'route';
                    } else {
                        if (this.params.list != 'show') {
                            buttonType = this.params.item != 'show' ? 'list' : 'geo';
                        } else {
                            if (this.data.pageType == 'objects' || this.data.pageType == 'events') {
                                buttonType = 'geo';
                            } if (this.data.pageType == 'routes') {
                                if (this.activeID) {
                                    buttonType = 'geo';
                                } else {
                                    buttonType = '';
                                }
                            }
                        }
                    }
                }
            }

            $(document).trigger('app:button', [buttonType]);
        }
    },

    buttonReaction: function () {
        if (this.params.list == 'show') {
            this.manageList('hide');
        } else {
            if (this.data.withoutMap) {
                this.manageList('show');
            } else {
                if (this.params.item == 'show') {
                    this.manageItem('hide');
                } else {
                    this.manageList('show');
                }
            }
        }
    },

    manageItem: function (action) {
        if (action) {
            this.params.item = action;
        }

        if (this.complete) {
            if ($Params.hasTransitionEnd) {
                this.itemLayer.css($Params.transitionDuration, this.data.itemTime / 1000 + 's');
            }
        }

        this.offset = this.params.item == 'show' ? this.minHeight - this.maxHeight : 0;
        this.resize('item', 0);
    },

    manageList: function (action) {
        var delta = 0;

        if (action) {
            this.params.list = action;
        }

        if (this.params.list == 'hide') {
            delta = - (this.maxPageWidth + 25);
            this.mapLayer.addClass('active');

            if (this.data.pageType == 'events') {
                if (this.activeNewCatID && this.activeCatID && this.activeCatID != this.activeNewCatID) {
                    if (!this.data.withoutMap) {
                        this.clearMarkers();
                        delete this.markersComplete;
                    }

                    this.activeCatID = this.activeNewCatID;
                }
            }
        } else {
            this.listLayer.addClass('active');
        }

        this.resize('list', delta);
    },

    checkRoutes: function () {
        var empty = !this.routeLayer.pointUser.val(),
            same = this.routeLayer.pointUser.val() == this.routeLayer.pointUser.get(0).defaultValue,
            direction = this.routeLayer.direction.is(':checked') == this.routeLayer.direction.get(0).defaultChecked,
            travel = this.routeLayer.activeTravel.is(':checked') == this.routeLayer.activeTravel.get(0).defaultChecked;

        if (empty) {
            this.routeLayer.routeButton.attr('disabled', true);
        } else  {
            if (same) {
                if (!this.withoutRightButton) {
                    if (!direction || !travel) {
                        this.routeLayer.routeButton.removeAttr('disabled');
                    } else {
                        this.routeLayer.routeButton.attr('disabled', true);
                    }
                } else {
                    this.routeLayer.routeButton.removeAttr('disabled');
                }
            } else {
                this.routeLayer.routeButton.removeAttr('disabled');
            }
        }
    },

    reversePoint: function () {
        var flag = this.routeLayer.direction.is(':checked'),
            field = flag ? 'from' : 'to';

        if (flag) {
            this.routeLayer.pointUser.parent().prependTo(this.routeLayer.pointContainer);
        } else {
            this.routeLayer.pointObject.parent().prependTo(this.routeLayer.pointContainer);
        }

        this.routeLayer.pointUser.attr('placeholder', this.data.interfaceText[field]);
    },

    createDirectionData: function () {
        var _class = this,
            _container = this.createElement('form', 'reverse', {
                'action': '',
                'data-custom': 'true'
            }).appendTo(this.listLayer),
            _navigation = this.createElement('*ul', 'navigation icon', {
                'data-container': 'label'
            }).appendTo(_container),
            _content = this.createElement('*div', 'header content').appendTo(_container),
            _fieldset = this.createElement('fieldset', 'group fieldset', {
                'data-group': 'point'
            }).appendTo(_content);

        for (var i in this.data.routeType[this.data.mapType]) {
            var text = this.data.routeType[this.data.mapType][i] == 'walking' ? 'toWalk' : 'toDrive';

            this.createElement('*li', 'navigation-item', {
                'data-action': 'label'
            }).append(
                    this.createElement('*span', 'navigation-title ' + this.data.routeType[this.data.mapType][i]).append(
                        this.createElement('input', {
                            'type': 'radio',
                            'name': 'travel',
                            'value': this.data.routeType[this.data.mapType][i],
                            'data-text': this.data.interfaceText[text]
                        }),
                        this.data.interfaceText[this.data.routeType[this.data.mapType][i]]
                    )
                ).appendTo(_navigation);
        }

        this.routeLayer = {
            target: this.itemLayer.find('[data\-container~="content"]'),
            travels: _navigation.find('input[type="radio"]'),
            pointContainer: _fieldset,
            pointUser: this.createElement('input', {
                'type': 'text',
                'name': 'coords',
                'required': 'required'
            }).appendTo(
                    this.createElement('*span', 'item user', {
                        'data-type': 'coords user'
                    }).appendTo(_fieldset)
                ),
            pointObject: this.createElement('*span', 'data').appendTo(
                this.createElement('*span', 'item object', {
                    'data-type': 'coords object'
                }).appendTo(_fieldset)
            ),
            direction: this.createElement('input', {
                'type': 'checkbox',
                'name': 'direction',
                'value': '1',
                'checked': 'checked'
            }).prependTo(
                    this.createElement('*span', 'change', {
                        'data-action': 'reverse'
                    }).append(this.data.interfaceText.reverseDirection).appendTo(_fieldset)
                ),
            routeButton: this.createElement('button', 'button', {
                'type': 'submit',
                'data-action': 'create',
                'disabled': 'disabled'
            }).append(this.data.interfaceText.createRoute).appendTo(_content),
            messageContainer: this.createElement('*div', 'content', {
                'data-container': 'message'
            }).appendTo(_content)
        };

        this.createDirection(this.options.item);

        this.routeLayer.pointUser
            .on('input', $.proxy(this.checkRoutes, this))
            .on('blur', function (e) {
                var _self = $(this);

                if (_self.val() && !new RegExp(_self.val()).test(_class.data.interfaceText.currentPosition)) {
                    _class.directions[_class.activeID].location.custom = _self.val();
                } else {
                    if (!_self.val()) {
                        if (_class.directions[_class.activeID].location.user) {
                            _self.val(_class.data.interfaceText.currentPosition);
                        }

                        _class.directions[_class.activeID].location.custom = null;
                    }
                }

                _class.checkRoutes();
            })
            .on('focus', function (e) {
                var _self = $(this);

                if (new RegExp(_self.val()).test(_class.data.interfaceText.currentPosition)) {
                    _self.val('');
                }

                _class.checkRoutes();
            });

        _container
            .on($Params.altEvents.start, '[data\-action~="label"]', function (e) {
                var _self = $(this);

                if (!_self.hasClass('active')) {
                    _class.routeLayer.activeTravel = _self.find('input[type="radio"]').trigger('click');
                    _self.addClass('active').siblings().removeClass('active');
                }

                _class.checkRoutes();
            })
            .on($Params.altEvents.start, '[data\-action~="reverse"]', function (e) {
                _class.routeLayer.direction.trigger('click');
                _class.reversePoint();
                _class.checkRoutes();
            })
            .on('submit', function (e) {
                e.preventDefault();

                var wait = _class.setMessage('wait', 'wait'),
                    start = new Date(),
                    delta = 1000,
                    points = [
                        _class.directions[_class.activeID].location.custom || _class.directions[_class.activeID].location.user,
                        _class.directions[_class.activeID].location.object
                    ];

                _class.directions[_class.activeID].direction = _class.routeLayer.direction.is(':checked');

                if (!_class.directions[_class.activeID].direction) {
                    points.reverse();
                }

                _class.directions[_class.activeID].options.origin = points[0];
                _class.directions[_class.activeID].options.destination = points[1];
                _class.directions[_class.activeID].options.travelMode = _class.routeLayer.activeTravel.val().toUpperCase();

                $DiffMap.getRoute(_class.directions[_class.activeID].options, _class.routeIcon, {
                    suppressMarkers: true,
                    polylineOptions: _class.data.directionOptions
                }, function (result, error) {
                    var current = new Date() - start;

                    if (current > delta) {
                        current = delta;
                    }

                    setTimeout(function () {
                        if (result) {
                            var text = _class.routeLayer.activeTravel.val() == 'walking' ? 'toWalk' : 'toDrive',
                                distance = 0,
                                dl = _class.createElement('*dl', 'routedetail').append(
                                    _class.createElement('*dt', 'head').append(
                                        _class.data.interfaceText[text],
                                        _class.createElement('*span', 'bolder').html(result.route.humanDuration),
                                        ' (' + result.route.humanLength + ')'
                                    )
                                ),
                                ul = _class.createElement('*ul', 'direction-points').append(
                                        _class.createElement('*li', 'point start').html(result.origin.text)
                                    ).appendTo(
                                        _class.createElement('*dd', 'body').appendTo(dl)
                                    );

                            for (var i in result.route.segments) {
                                var action = result.route.segments[i].action ? ' action ' + result.route.segments[i].action : '';

                                distance += result.route.segments[i].length;
                                ul.append(
                                    _class.createElement('*li', 'point' + action).append(
                                        _class.createElement('*span', 'order').html(i * 1 + 1),
                                        _class.createElement('*div', 'text').html(result.route.segments[i].text),
                                        _class.createElement('*div', 'distance').append(
                                            _class.createElement('*div', 'distanceline').width(distance * 100 / result.route.length + '%').append(
                                                _class.createElement('*span', 'distancesegment').width(result.route.segments[i].length * 100 / distance + '%'),
                                                _class.createElement('*span', 'distancetext').html(result.route.segments[i].humanLength)
                                            )
                                        )
                                    )
                                )
                            }

                            ul.append(
                                _class.createElement('*li', 'point end').html(result.destination.text)
                            );

                            if (result.route.warnings && result.route.warnings.length) {
                                dl.append(
                                    _class.createElement('*dd', 'note').html(result.route.warnings)
                                );
                            }

                            dl.append(
                                _class.createElement('*dd', 'copyright').html(result.route.copyrights)
                            );

                            _class.directions[_class.activeID].route = result.route;
                            $DiffMap.setBounds(result.route.bounds);

                            _class.routeLayer.pointUser.get(0).defaultValue = _class.directions[_class.activeID].value = _class.routeLayer.pointUser.val();
                            _class.routeLayer.direction.get(0).defaultChecked = _class.directions[_class.activeID].direction;

                            _class.directions[_class.activeID].data = {
                                info: _class.createElement('*dd', 'data', {
                                    'data-container': 'info'
                                }).append(
                                        _class.createElement('*div', 'points').append(
                                            _class.createElement('*span', 'title').html(_class.data.interfaceText.from),
                                            result.origin.text
                                        ),
                                        _class.createElement('*div', 'points').append(
                                            _class.createElement('*span', 'title').html(_class.data.interfaceText.to),
                                            result.destination.text
                                        )
                                    ),
                                detail: dl
                            };

                            _class.setMessage('OK', 'success', wait);
                            _class.showItem(null, _class.directions[_class.activeID].data);
                            _class.manageItem('hide');
                            _class.manageList('hide');
                        } else {
                            var count = 0,
                                prev;

                            if (error) {
                                if ($Common.getType(error) == 'Object') {
                                    for (var i in error) {
                                        if (error[i]) {
                                            if (count) {
                                                _class.setMessage(error[i], 'fail');
                                            } else {
                                                _class.setMessage(error[i], 'fail', wait);
                                            }

                                            count++;
                                        }
                                    }

                                    if (!count) {
                                        _class.setMessage('UNKNOWN_ERROR', 'fail', wait);
                                    }
                                } else {
                                    _class.setMessage(error, 'fail', wait);
                                }
                            }
                        }

                    }, delta - current);
                });
            });
    },

    createDirection: function (id) {
        var _class = this;

        if (!id || !this.data.entities[id]) {
            for (var i in this.data.entities) {
                id = i;
                break;
            }
        }

        if (id) {
            getLocation().then(function (geoLocateResult) {
                _class.activeID = id;

                if (!_class.directions[id]) {
                    _class.directions[id] = {
                        options: {
                            origin: null,
                            destination: null,
                            travelMode: null
                        },
                        location: {
                            custom: null,
                            user: null,
                            object: $DiffMap.createPoint(_class.data.entities[id])
                        }
                    };

                    _class.routeLayer.activeTravel = _class.routeLayer.travels.eq(0);
                } else {
                    _class.routeLayer.direction.get(0).defaultChecked = _class.directions[id].direction;
                    _class.routeLayer.activeTravel = _class.routeLayer.travels.filter('[value="' + _class.directions[id].options.travelMode.toLowerCase() + '"]');
                }

                if (geoLocateResult.point) {
                    _class.routeLayer.pointUser.val(_class.data.interfaceText.currentPosition);
                    _class.directions[id].location.user = $DiffMap.createPoint(geoLocateResult.point);
                } else {
                    _class.routeLayer.pointUser.val('');
                    _class.setMessage(geoLocateResult.message, geoLocateResult.status);
                }

                _class.routeLayer.activeTravel.get(0).defaultChecked = true;
                _class.routeLayer.activeTravel.closest('[data\-action~="label"]').addClass('active');
                _class.routeLayer.pointObject.html(_class.data.entities[id].name);
                _class.checkRoutes();
                _class.reversePoint();
            });
        }

        function getLocation () {
            var defer = $.Deferred();

            if (_class.data.geolocation) {
                success(_class.data.geolocation);
            } else {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        success,
                        function () {
                            defer.resolve({
                                status: 'fail',
                                message: 'blocked'
                            });
                        },
                        {
                            timeout: 5000
                        }
                    );
                } else {
                    defer.resolve({
                        status: 'fail',
                        message: 'none'
                    });
                }
            }

            function success (point) {
                if (point.coords) {
                    point = {
                        lat: point.coords.latitude,
                        lng: point.coords.longitude
                    };
                }

                defer.resolve({
                    point: point
                });
            }

            return defer.promise();
        }
    },

    addDirection: function () {
        $DiffMap.addDirection(this.directions[this.activeID].route);
    },

    removeDirection: function () {
        $DiffMap.removeDirection(this.directions[this.activeID].route);
    },

    createElement: function (tag) {
        var set = [],
            name = tag.split('*');

        if (name[1]) {
            set.push(this.alternate[name[1]]);
            tag = 'bxmap';
        }

        var element = $('<' + tag + '>');

        for (var i = 1; i < arguments.length; i++) {
            if (typeof arguments[i] == 'string') {
                set = set.concat(arguments[i].split(/\s+/));
            } else {
                element.attr(arguments[i]);
            }
        }

        if (set.length) {
            element.addClass(set.join(' '));
        }

        return element;
    },

    addEvent: function (element, name, handler) {
        try {
            element.addEventListener(name, handler, false);
        } catch (z) {
            element.attachEvent('on' + name, handler);
        }

        return this;
    },

    removeEvent: function (element, name, handler) {
        try {
            element.removeEventListener(name, handler, false);
        } catch (z) {
            element.detachEvent('on' + name, handler);
        }

        return this;
    },

    getWrapper: function () {
        if (!this.wrapper || !this.wrapper.length) {
            this.wrapper = $('#bxMapContainer').addClass(this.data.mapType).css({
                top: this.data.barHeight + 'px'
            });

            if (this.wrapper && this.wrapper.length && $('#bxMapFakeElement').length) {
                $('#bxMapFakeElement').remove();
            }
        }

        return this.wrapper;
    },

    insertWrapper: function (status) {
        var _wrapper = this.getWrapper();

        if (!_wrapper || !_wrapper.length) {
            this.wrapper = this.createElement('*div', 'map-wrapper', {
                id: 'bxMapContainer'
            }).replaceAll($('#bxMapFakeElement'));
        }

        this.wrapper.css({
            top: this.data.barHeight + 'px'
        });

        return this.wrapper;
    },

    showError: function (message) {
        this.insertWrapper().addClass('error-container').empty().append(
            this.createElement('*div', 'error-message', {
                'data-container': 'message'
            })
        );

        this.defineButtonType(true);
        this.setMessage(this.data.parseMessages[message] || message, 'fail');
        $(document).trigger('show:error');
    },

    setMessage: function (message, type, _old) {
        var _class = this,
            _container = this.routeLayer ? this.routeLayer.messageContainer : this.getWrapper().find('[data\-container~="message"]'),
            _list = _container.find('.group.status');

        if (message && type) {
            var _new = _class.createElement('*li', 'item').append(
                _class.createElement('*span', 'block ' + type).html(this.data.routeMessages[message] || message)
            );

            if (!_list.length) {
                _list = _class.createElement('*ul', 'group status icon').appendTo(_container);
            }

            if (_old) {
                _old.animate({
                    opacity: 0
                }, 200, function () {
                    _old.remove();
                    _new.appendTo(_list);
                    return _new;
                });
            } else {
                _new.appendTo(_list);
                return _new;
            }
        } else {
            var defer = $.Deferred();

            _list.animate({
                opacity: 0
            }, 200, function () {
                _list.remove();
                defer.resolve(null);
            });

            return defer.promise();
        }
    },

    errorListener: function (e) {
        try {
            e.preventDefault();
        } catch (z) {
            e.returnValue = false;
        }

        if (this.data[this.errorFields.type]) {
            $(document).trigger('check:script', [this.errorFields.name, this.errorFields.message]);
        }
    },

    snapError: function (listener, type, name, message) {
        var _class = this;

        if (arguments.length > 1) {
            this.errorFields = {
                type: type,
                name: name,
                message: message
            };

            if (listener) {
                this.addEvent(window, 'error', listener);
            }
        } else {
            this.removeEvent(window, 'error', listener);
        }
    },

    setFullPath: function (path, type) {
        if (!/^(\/|http)/.test(path)) {
            path = (this.data.defaultPath[type] + '/').replace(/\/+$/, '/') + path;
        }

        return path;
    },

    getCatId: function (catid) {
        if (this.data.entities[catid]) {
            return this.getCatId(this.data.entities[catid].cat);
        } else {
            return catid;
        }
    },

    getScript: function (src, id, name, errorName) {
        var _class = this,
            script = document.createElement('script');

        script.type = 'text/javascript';
        script.src = src;
        script.async = false;
        script.charset = 'utf-8';
        script.onload = script.onreadystatechange = process;
        script.onerror = error;
        this.head.get(0).insertBefore(script, this.head.get(0).lastChild);

        function process (e) {
            e = e || event;

            if (e.type === 'load' || (/loaded|complete/.test(script.readyState) && (!document.documentMode || document.documentMode < 9))) {
                script.onload = script.onreadystatechange = script.onerror = null;
                _class.checkScriptQueue(id, name, errorName, true);
            }
        }

        function error (e) {
            script.onload = script.onreadystatechange = script.onerror = null;
            _class.checkScriptQueue(id, name, errorName);
        }

        return src;
    },

    checkScriptQueue: function (id, name, errorName, load) {
        this.scriptSet[id].count --;

        if (!this.scriptSet[id].stop) {
            if (!load) {
                this.scriptSet[id].stop = true;
                $(document).trigger('check:script', [id, errorName]);
            } else {
                delete this.scriptSet[id].set[name];
            }

            if (!this.scriptSet[id].count) {
                $(document).trigger('check:script', [id, null]);
            }
        } else {
            delete this.scriptSet[id].set[name];
        }
    },

    createScriptQueue: function (data, id, errorName) {
        var _class = this,
            defer = $.Deferred(),
            name;

        this.scriptSet[id] = {
            id: id,
            count: 0,
            set: {}
        };
        this.scriptSet[id].timing = setTimeout($.proxy(checkScript, this.scriptSet[id]), this.data.responseTime);

        $(document).one('check:script', function (e, id, error) {
            clearTimeout(_class.scriptSet[id].timing);
            defer.resolve(error);
        });

        for (var i in data) {
            if (data instanceof Array) {
                name = 'set_' + i;
            } else {
                name = i;

                if (id == 'maps' && name == 'main') {
                    data[i] += '&callback=$DiffMap.mapInitialize';
                }
            }

            this.scriptSet[id].count ++;
            this.scriptSet[id].set[name] = this.getScript(this.setFullPath(data[i], 'libs'), id, name, errorName);
        }

        function checkScript () {
            $(document).trigger('check:script', [this.id, !!this.stop && !this.count]);
        }

        return defer.promise();
    }
};

var $Switch = {
    init: function () {
        var _class = this;

        $(document)
            .on('tab:create', function (e, id) {
                if (_class.complete) {
                    return;
                }

                _class._container = $('[data\-container~="switch"]');

                if (!_class._container.length) {
                    return;
                }

                _class._actions = _class._container.find('[data\-action~="switch"]');
                if (!_class._actions.length) {
                    return;
                }

                _class._targets = _class._container.find('[data\-target~="switch"]');
                _class._targets.find('.field-wrapper-container').attr('disabled', true);
                _class.count();

                if (_class._container.data('type') == 'filter') {
                    _class._items = _class._targets.find('[data-type]');
                }

                _class.complete = true;
                $(document).trigger('tab:switch', [id]);
            })
            .on($Params.altEvents.start, '[data\-action~="switch"]', function (e) {
                var _self = $(this),
                    id = _self.data('id');

                if (!_self.is(_class.active)) {
                    $(document).trigger('tab:switch', [id]);
                    history.replaceState({}, _self.text(), location.href.replace(/#.*$/, '') + '#' + id);
                }
            })
            .on('tab:switch', function (e, id) {
                var prev;

                if (_class.active) {
                    prev = _class.active.data('id');
                    _class.active.removeClass('active');
                }

                if (id !== undefined) {
                    _class.active = _class._actions.filter('[data-id="' + id + '"]');
                } else {
                    if (location.hash) {
                        id = location.hash.replace('#', '');
                        _class.active = _class._actions.filter('[data-id="' + id + '"]');
                    }

                    if (!id) {
                        _class.active = _class._actions.eq(0);
                        id = _class.active.data('id');
                    }
                }

                _class.active.addClass('active');

                if (!_class._items) {
                    if (id != 0) {
                        if (prev) {
                            switchTarget(_class._targets.filter('[data-id="' + prev + '"]'), true);
                        }

                        switchTarget(_class._targets.filter('[data-id="' + id + '"]'));
                    }
                } else {
                    if (id != 0) {
                        var count = 0,
                            visible = 0;

                        _class._items.each(function (index) {
                            var _item = $(this),
                                type = _item.data('type');

                            if (id == type) {
                                _item.removeClass('none first last');
                                visible = index;
                                count++;

                                if (count == 0) {
                                    _item.addClass('first');
                                }
                            } else {
                                _item.addClass('none');
                            }
                        });

                        _class._items.eq(visible).addClass('last');
                    } else {
                        _class._items.removeClass('none first last');
                    }
                }

                $(document).trigger('switch:done', [id]);

                function switchTarget (item, disabled) {
                    if (disabled) {
                        item.removeClass('active');
                        item.find('.field-wrapper-container').attr('disabled', true);
                    } else {
                        item.addClass('active');
                        item.find('.field-wrapper-container').removeAttr('disabled');
                    }
                }
            });
    },

    count: function (_elems) {
        var _class = this;

        (_elems || this._actions).each(function () {
            var _self = $(this),
                _count = _self.find('.navigation-count'),
                _target = _class._targets.filter('[data-id="' + _self.data('id') + '"]'),
                amount = _target.find('[data\-action~="item"]').length;

            if (!amount) {
                amount = _target.find('.item').length;
            }

            if (amount) {
                if (_count.length) {
                    _count.html(amount);
                } else {
                    _self.prepend(
                        $GeoMapp.createElement('*span', 'navigation-count').html(amount)
                    );
                }
            }
        });
    }
};