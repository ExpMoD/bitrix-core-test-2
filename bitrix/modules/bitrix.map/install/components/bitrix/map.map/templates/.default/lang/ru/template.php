<?
$MESS["BX_MAP_INT_COLLAPSE_PANEL"] = "&larr; Свернуть";
$MESS["BX_MAP_INT_CAT_TITLE"] = "Категории";
$MESS["BX_MAP_INT_CLEAR_FIELD"] = "Очистить поле";
$MESS["BX_MAP_INT_SEARCH_PLACEHOLDER"] = "Поиск. Например, Кремль";
$MESS["BX_MAP_INT_REFRESH_MARKERS"] = "Обновить маркеры";
$MESS["BX_MAP_INT_CLEAR_CATS"] = "Отменить выбор";
$MESS["BX_MAP_INT_CLOSE_LIST"] = "Закрыть";
$MESS["BX_MAP_INT_SHOW_LIST"] = "Списком";
$MESS["BX_MAP_INT_BACK"] = "Вернуться";
$MESS["BX_MAP_INT_SHOW_MARKER"] = "Показать на карте";
$MESS["BX_MAP_INT_ROUTE"] = "Маршрут";
$MESS["BX_MAP_INT_WALKING"] = "Пешком";
$MESS["BX_MAP_INT_TRANSIT"] = "Транспорт";
$MESS["BX_MAP_INT_DRIVING"] = "Авто";
$MESS["BX_MAP_INT_BYCICLE"] = "Велосипед";
$MESS["BX_MAP_INT_REVERSE"] = "Сменить направление";
$MESS["BX_MAP_INT_CREATE_ROUTE"] = "Проложить";
$MESS["BX_MAP_INT_CATS_PASS"] = "Прошли";
$MESS["BX_MAP_INT_CATS_TODAY"] = "Сегодня";
$MESS["BX_MAP_INT_CATS_PLANNED"] = "Запланированы";
$MESS["BX_MAP_INT_TO_WALK"] = "Идти";
$MESS["BX_MAP_INT_TO_DRIVE"] = "Ехать";
$MESS["BX_MAP_INT_POINTS_TITLE"] = "Объекты маршрута";
$MESS["BX_MAP_INT_CUR_POS"] = "Мое текущее положение";
$MESS["BX_MAP_INT_FROM"] = "Откуда";
$MESS["BX_MAP_INT_TO"] = "Куда";
$MESS["BX_MAP_INT_CAT"] = "Группа";

$MESS["BX_MAP_RMES_WAIT"] = "Ожидается ответ на запрос...";
$MESS["BX_MAP_RMES_INVALID_REQUEST"] = "Неверный запрос";
$MESS["BX_MAP_RMES_MAX_WAYPOINTS_EXCEEDED"] = "Слишком много промежуточных точек";
$MESS["BX_MAP_RMES_NOT_FOUND"] = "Часть координат неверно задана или невозможно распознать адрес";
$MESS["BX_MAP_RMES_OK"] = "Запрос выполнен";
$MESS["BX_MAP_RMES_OVER_QUERY_LIMIT"] = "Превышен лимит на количество запросов";
$MESS["BX_MAP_RMES_REQUEST_DENIED"] = "Сервис на этой странице недоступен";
$MESS["BX_MAP_RMES_UNKNOWN_ERROR"] = "Ошибка неизвестной природы";
$MESS["BX_MAP_RMES_ZERO_RESULTS"] = "Невозможно проложить маршрут";
$MESS["BX_MAP_RMES_BLOCKED"] = "Функция определения местоположения заблокирована. Введите адрес вручную.";
$MESS["BX_MAP_RMES_NONE"] = "Функция определения не поддерживается. Введите адрес вручную.";

$MESS["BX_MAP_NO_CONTAINER"] = "Отсутствует контейнер для карты";
$MESS["BX_MAP_NO_SCRIPT_ID"] = "У подключенного скрипта компонента не указан идентификатор";
$MESS["BX_MAP_COMMON_SCRIPT_NOT_LOADED"] = "Не удалось загрузить основные скрипты";
$MESS["BX_MAP_MAP_SCRIPT_NOT_LOADED"] = "Не удалось загрузить скрипты карты";
$MESS["BX_MAP_MAP_DENIED"] = "Запрет на использование карты для прокладки маршрута";
$MESS["BX_MAP_PAGETYPE_NOT_DEFINED"] = "Не указан тип страницы";
$MESS["BX_MAP_NO_CATS"] = "Нет ни одной категории";
$MESS["BX_MAP_NO_ITEMS"] = "Нет ни одного объекта";
$MESS["BX_MAP_INDEFINED_ERRORS"] = "Неопределённая ошибка";
$MESS["BX_MAP_ERRORS_COMMON_LOADING"] = "Неопределённая ошибка во время загрузки основных скриптов";
$MESS["BX_MAP_ERRORS_MAP_LOADING"] = "Неопределённая ошибка во время загрузки скриптов карты";

$MESS["BX_MAP_ERROR_NO_SECTIONS"] = "В исходных данных отсутствуют категории";
$MESS["BX_MAP_ERROR_NO_ELEMENTS"] = "В исходных данных отсутствуют элементы или нет ни одного элемента с координатами";
?>