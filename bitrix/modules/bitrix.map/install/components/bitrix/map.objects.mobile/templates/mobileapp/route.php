<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$APPLICATION->IncludeComponent("bitrix:map.routing", "mobileapp_modern", array(
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "OBJECT_ID" => $_REQUEST["item"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "MAP_TYPE" => $arParams["MAP_TYPE"],
        "GEOLOC_LATITUDE" => $arParams["GEOLOC_LATITUDE"],
        "GEOLOC_LONGITUDE" => $arParams["GEOLOC_LONGITUDE"],
        "NAME_PROP_CODE" => $arParams["NAME_PROP_CODE"],
        "LATITUDE_PROP_CODE" => $arParams["LATITUDE_PROP_CODE"],
        "LONGITUDE_PROP_CODE" => $arParams["LONGITUDE_PROP_CODE"],
        "ADDRESS_PROP_CODE" => $arParams["ADDRESS_PROP_CODE"],
		"PHONE_PROP_CODE" => $arParams["PHONE_PROP_CODE"],
		"OPENING_PROP_CODE" => $arParams["OPENING_PROP_CODE"],
		"LINK_PROP_CODE" => $arParams["LINK_PROP_CODE"],
		"ICONPOS_PROP_CODE" => $arParams["ICONPOS_PROP_CODE"],
        "PARENT_PROP_CODE" => $arParams["PARENT_PROP_CODE"],
        "BAR_HEIGHT" => $arParams["BAR_HEIGHT"],
        "PLATE_HEIGHT" => $arParams["PLATE_HEIGHT"],
        "DATA_TYPE" => $arParams["DATA_TYPE"],
        "OLD_DATA_MODE" => $arParams["OLD_DATA_MODE"]
    ),
    $component
);?>