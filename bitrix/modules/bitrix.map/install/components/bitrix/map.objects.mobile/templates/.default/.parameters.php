<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "AJAX_PATH" => array(
        "NAME" => GetMessage("T_MAP_DESC_AJAX_PATH"),
        "TYPE" => "STRING",
        "DEFAULT" => "/bitrix/components/bitrix/map.map/ajax.php",
    ),
    "BAR_HEIGHT" => array(
        "NAME" => GetMessage("T_MAP_DESC_BAR_HEIGHT"),
        "TYPE" => "STRING",
        "DEFAULT" => "0",
    ),
    "PLATE_HEIGHT" => array(
        "NAME" => GetMessage("T_MAP_DESC_PLATE_HEIGHT"),
        "TYPE" => "STRING",
        "DEFAULT" => "65",
    ),
    "GEOLOC_LATITUDE" => array(
        "NAME" => GetMessage("T_MAP_DESC_GEOLOC_LATITUDE"),
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
    "GEOLOC_LONGITUDE" => array(
        "NAME" => GetMessage("T_MAP_DESC_GEOLOC_LONGITUDE"),
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
    "LOAD_ITEMS" => array(
        "NAME" => GetMessage("T_MAP_DESC_MAP_LOAD_ITEMS"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "Y"
    )
);

?>