<?
$MESS["T_MAP_DESC_AJAX_PATH"] = "Адрес для AJAX";
$MESS["T_MAP_DESC_PLATE_HEIGHT"] = "Высота верхней области";
$MESS["T_MAP_DESC_DIRECTION_LINK"] = "Ссылка на страницу с прокладкой маршрута";
$MESS["T_MAP_DESC_GEOLOC_LATITUDE"] = "Широта по умолчанию";
$MESS["T_MAP_DESC_GEOLOC_LONGITUDE"] = "Долгота по умолчанию";
$MESS["T_MAP_DESC_FEW_OBJECTS_HEIGHT"] = "Высота области информации о кол-ве объектов";
?>