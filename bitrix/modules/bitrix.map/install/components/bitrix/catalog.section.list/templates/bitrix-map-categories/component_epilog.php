<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (Bitrix\Main\Loader::includeModule("bitrix.map"))
{
    Bitrix\InteractiveMap\DataMixer::IncludeMapScripts("mobile");
    $APPLICATION->SetAdditionalCss('/bitrix/components/bitrix/map.map/templates/mobile/style.css');
}
?>