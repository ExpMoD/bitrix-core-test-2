<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "AJAX_PATH" => array(
        "NAME" => GetMessage("T_MAP_DESC_AJAX_PATH"),
        "TYPE" => "STRING",
        "DEFAULT" => "/bitrix/components/bitrix/map.map/ajax.php",
    ),
    "BAR_HEIGHT" => array(
        "NAME" => GetMessage("T_MAP_DESC_BAR_HEIGHT"),
        "TYPE" => "STRING",
        "DEFAULT" => "0",
    ),
    "PLATE_HEIGHT" => array(
        "NAME" => GetMessage("T_MAP_DESC_PLATE_HEIGHT"),
        "TYPE" => "STRING",
        "DEFAULT" => "44",
    ),
);
?>
