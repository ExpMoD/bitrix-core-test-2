<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (Bitrix\Main\Loader::includeModule("mobileapp"))
{
    $obDataMixer = new \Bitrix\InteractiveMap\DataMixer();

    $arParams["BAR_HEIGHT"] = IntVal($arParams["BAR_HEIGHT"]);
    if (empty($arParams["BAR_HEIGHT"]))
    {
        $arParams["BAR_HEIGHT"] = $obDataMixer->GetDefaultBarHeight();
    }
    if ($arParams["BAR_HEIGHT"] < 0)
    {
        $arParams["BAR_HEIGHT"] = 0;
    }

    $arParams["PLATE_HEIGHT"] = IntVal($arParams["PLATE_HEIGHT"]);
    if (empty($arParams["PLATE_HEIGHT"]))
    {
        $arParams["PLATE_HEIGHT"] = $obDataMixer->GetDefaultPlateHeight();
    }
    if ($arParams["PLATE_HEIGHT"] < 0)
    {
        $arParams["PLATE_HEIGHT"] = 0;
    }

    $arParams["GEOLOC_LATITUDE"]  = FloatVal($arParams["GEOLOC_LATITUDE"]);
    $arParams["GEOLOC_LONGITUDE"] = FloatVal($arParams["GEOLOC_LONGITUDE"]);

    $arResult["PARAMS"]["ICONS"] = $obDataMixer->GetIconsSettings("mobile", array(
        "icon_direction",
    ), true);

    if (!empty($arParams["AJAX_PATH"]))
    {
        $passedParams = array("LATITUDE_PROP_CODE", "LONGITUDE_PROP_CODE", "ADDRESS_PROP_CODE", "PHONE_PROP_CODE", "OPENING_PROP_CODE", "LINK_PROP_CODE");

        foreach ($passedParams as $param)
        {
            $query[] = "prop[" . strtolower(str_replace("_PROP_CODE", "", $param)) . "]=" . $arParams[$param];
        }

        $uri = parse_url($arParams["AJAX_PATH"]);
        $arParams["AJAX_PATH"] = $arParams["AJAX_PATH"] . (empty($uri["query"]) ? "?" : "&") . implode("&", $query);

    }
}
else
{
    ShowError(GetMessage("MOBILEAPP_MODULE_NOT_INSTALLED"));
    return;
}
?>