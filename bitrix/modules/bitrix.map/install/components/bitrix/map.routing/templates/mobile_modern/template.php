<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if (strlen($arResult["ERROR"]) > 0) { ?>
    <? ShowError($arResult["ERROR"]); ?>
<? } else { ?>
    <script>
        $GeoMapp.init({
            device: {mobile: 'mobile_map_v3.js'},
            defaultPath: <?=$arResult["PARAMS"]["DEFAULT_PATHS"]?>,
            mapBounds: <?=$arResult["PARAMS"]["BOUNDS"]?>,
            mapType: '<?=$arParams["MAP_TYPE"]?>',
            pageType: 'direction',
            ajax: '<?=$arParams["AJAX_PATH"]?>',
            loadTime: <?=$arResult["PARAMS"]["LOAD_TIME"]?>,
            responseTime: <?=$arResult["PARAMS"]["RESPONSE_TIME"]?>,
            barHeight: <?=$arParams["BAR_HEIGHT"]?>,
            plateHeight: <?=$arParams["PLATE_HEIGHT"]?>,
            <? foreach ($arResult["PARAMS"]["ICONS"] as $strVarName => $strData) { ?>
            <?=$strVarName?>: <?=$strData?>,
            <? } ?>
            <? if (!empty($arParams["GEOLOC_LATITUDE"]) && !empty($arParams["GEOLOC_LONGITUDE"])) { ?>
            geolocation: {
                lat: <?=$arParams["GEOLOC_LATITUDE"]?>,
                lng: <?=$arParams["GEOLOC_LONGITUDE"]?>
                },
            <? } ?>
            interfaceText: <?=$arResult["INTERFACE"]["main"]?>,
            routeMessages: <?=$arResult["INTERFACE"]["routes"]?>,
            parseMessages: <?=$arResult["INTERFACE"]["errors"]?>,
            cats: <?=$arResult["JSON_SECTIONS"]?>,
            <? if ($arParams["LOAD_ITEMS"]) { ?>
            items: <?=$arResult["JSON_ELEMENTS"]?>,
            <? } ?>
            routeType: <?=$arResult["PARAMS"]["ROUTE_TYPES"]?>
        });
    </script>

    <div id="bxMapContainer" class="bxmap-wrapper"></div>
<? } ?>