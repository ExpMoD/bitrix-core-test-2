<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
    $arParams["IBLOCK_TYPE"] = "news";
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);
if (substr($arParams["OBJECT_ID"], 0, 1) == "e")
{
    $arParams["OBJECT_ID"] = substr($arParams["OBJECT_ID"], 1);
    $arParams["~OBJECT_ID"] = substr($arParams["~OBJECT_ID"], 1);
}
$arParams["OBJECT_ID"] = intval($arParams["~OBJECT_ID"]);
if($arParams["OBJECT_ID"] > 0 && $arParams["OBJECT_ID"]."" != $arParams["~OBJECT_ID"])
{
    ShowError(GetMessage("T_OBJECT_NF"));
    @define("ERROR_404", "Y");
    if($arParams["SET_STATUS_404"]==="Y")
        CHTTP::SetStatus("404 Not Found");
    return;
}

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["INCLUDE_IBLOCK_INTO_CHAIN"] = $arParams["INCLUDE_IBLOCK_INTO_CHAIN"]!="N";
$arParams["LOAD_ITEMS"] = empty($arParams["LOAD_ITEMS"]) || $arParams["LOAD_ITEMS"] == "Y" || $arParams["NO_CATS"] == "Y";

$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
if(!is_array($arParams["GROUP_PERMISSIONS"]))
    $arParams["GROUP_PERMISSIONS"] = array(1);

if ($arParams["MAP_TYPE"] != "yandex")
{
    $arParams["MAP_TYPE"] = "google";
}

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
    $arUserGroupArray = $USER->GetUserGroupArray();
    foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
    {
        if(in_array($PERM, $arUserGroupArray))
        {
            $bUSER_HAVE_ACCESS = true;
            break;
        }
    }
}

if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arrFilter)))
{
    if(!CModule::IncludeModule("bitrix.map"))
    {
        $this->AbortResultCache();
        ShowError(GetMessage("MAP_MODULE_NOT_INSTALLED"));
        return;
    }
    elseif(!CModule::IncludeModule("iblock"))
    {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $arFilter = array(
        "IBLOCK_LID" => SITE_ID,
        "IBLOCK_ACTIVE" => "Y",
        "ACTIVE" => "Y",
        "CHECK_PERMISSIONS" => "Y",
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    );
    if(intval($arParams["IBLOCK_ID"]) > 0)
        $arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];

    $arSelect = array(
        "ID",
        "NAME",
        "IBLOCK_ID",
        "IBLOCK_SECTION_ID",
        "DETAIL_TEXT",
        "DETAIL_TEXT_TYPE",
        "PREVIEW_TEXT",
        "PREVIEW_TEXT_TYPE",
        "DETAIL_PICTURE",
        "ACTIVE_FROM",
        "LIST_PAGE_URL",
        "DETAIL_PAGE_URL",
        "PROPERTY_*"
    );

    $arFilter["ID"] = $arParams["OBJECT_ID"];

    $rsElement = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    $rsElement->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
    if($obElement = $rsElement->GetNextElement())
    {
        $arElement = $obElement->GetFields();
        
        $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arResult["IBLOCK_ID"], $arResult["ID"]);
        $arElement["IPROPERTY_VALUES"] = $ipropValues->getValues();

        $arElement["PROPERTIES"] = $obElement->GetProperties();

        $arResult["SECTIONS"] = array();
        if($arElement["IBLOCK_SECTION_ID"] > 0)
        {
            $rsPath = CIBlockSection::GetById($arElement["IBLOCK_SECTION_ID"]);
            if($arPath=$rsPath->GetNext())
            {
                $arResult["SECTIONS"][] = $arPath;
            }
        }

        $arResult["ELEMENTS"][] = $arElement;

        $obDataMixer = new \Bitrix\InteractiveMap\DataMixer($arParams);
        $obDataMixer->setSystemFields($obDataMixer->getSettingsPropCodes($arParams));
        $obDataMixer->PrepareJsonData($arResult);
        $arResult["INTERFACE"] = $obDataMixer->GetInterfaceStrings(true);

        $arResult["PARAMS"]["LOAD_TIME"] = IntVal(\Bitrix\Main\Config\Option::get('bitrix.map', 'load_time', 500));
        $arResult["PARAMS"]["RESPONSE_TIME"] = IntVal(\Bitrix\Main\Config\Option::get('bitrix.map', 'response_time', 1000));

        $arResult["PARAMS"]["ROUTE_TYPES"] = $obDataMixer->GetRouteTypes($arParams["MAP_TYPE"], true);
        $arResult["PARAMS"]["DEFAULT_PATHS"] = $obDataMixer->GetDefaultPaths(true);

        $this->SetResultCacheKeys(array(
            "ID",
            "IBLOCK_ID",
            "NAME",
            "IBLOCK_SECTION_ID",
            "IBLOCK",
            "SECTION",
            "PROPERTIES",
            "IPROPERTY_VALUES",
        ));
        $this->IncludeComponentTemplate();
    }
    else
    {
        $this->AbortResultCache();
        ShowError(GetMessage("T_OBJECT_NF"));
        @define("ERROR_404", "Y");
        if($arParams["SET_STATUS_404"]==="Y")
            CHTTP::SetStatus("404 Not Found");
    }
}

if(isset($arResult["ID"]))
{
    $arTitleOptions = null;
    if(CModule::IncludeModule("iblock"))
    {
        CIBlockElement::CounterInc($arResult["ID"]);

        if($USER->IsAuthorized())
        {
            if(
                $APPLICATION->GetShowIncludeAreas()
                || $arParams["SET_TITLE"]
                || isset($arResult[$arParams["BROWSER_TITLE"]])
            )
            {
                $arReturnUrl = array(
                    "add_element" => CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "DETAIL_PAGE_URL"),
                    "delete_element" => (
                        empty($arResult["SECTION_URL"])?
                            $arResult["LIST_PAGE_URL"]:
                            $arResult["SECTION_URL"]
                        ),
                );

                $arButtons = CIBlock::GetPanelButtons(
                    $arResult["IBLOCK_ID"],
                    $arResult["ID"],
                    $arResult["IBLOCK_SECTION_ID"],
                    Array(
                        "RETURN_URL" => $arReturnUrl,
                        "SECTION_BUTTONS" => false,
                    )
                );

                if($APPLICATION->GetShowIncludeAreas())
                    $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

                if($arParams["SET_TITLE"] || isset($arResult[$arParams["BROWSER_TITLE"]]))
                {
                    $arTitleOptions = array(
                        'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_element"]["ACTION"],
                        'PUBLIC_EDIT_LINK' => $arButtons["edit"]["edit_element"]["ACTION"],
                        'COMPONENT_NAME' => $this->GetName(),
                    );
                }
            }
        }
    }

    $this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);

    if($arParams["SET_TITLE"])
    {
        if ($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != "")
            $APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"], $arTitleOptions);
        else
            $APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
    }

    $browserTitle = \Bitrix\Main\Type\Collection::firstNotEmpty(
        $arResult["PROPERTIES"], array($arParams["BROWSER_TITLE"], "VALUE")
        ,$arResult, $arParams["BROWSER_TITLE"]
        ,$arResult["IPROPERTY_VALUES"], "ELEMENT_META_TITLE"
    );
    if (is_array($browserTitle))
        $APPLICATION->SetPageProperty("title", implode(" ", $browserTitle), $arTitleOptions);
    elseif ($browserTitle != "")
        $APPLICATION->SetPageProperty("title", $browserTitle, $arTitleOptions);

    $metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
        $arResult["PROPERTIES"], array($arParams["META_KEYWORDS"], "VALUE")
        ,$arResult["IPROPERTY_VALUES"], "ELEMENT_META_KEYWORDS"
    );
    if (is_array($metaKeywords))
        $APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords), $arTitleOptions);
    elseif ($metaKeywords != "")
        $APPLICATION->SetPageProperty("keywords", $metaKeywords, $arTitleOptions);

    $metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
        $arResult["PROPERTIES"], array($arParams["META_DESCRIPTION"], "VALUE")
        ,$arResult["IPROPERTY_VALUES"], "ELEMENT_META_DESCRIPTION"
    );
    if (is_array($metaDescription))
        $APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
    elseif ($metaDescription != "")
        $APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);

    if($arParams["INCLUDE_IBLOCK_INTO_CHAIN"] && isset($arResult["IBLOCK"]["NAME"]))
    {
        $APPLICATION->AddChainItem($arResult["IBLOCK"]["NAME"], $arResult["~LIST_PAGE_URL"]);
    }

    if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
    {
        foreach($arResult["SECTION"]["PATH"] as $arPath)
        {
            $APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
        }
    }

    return $arResult["ID"];
}
else
{
    return 0;
}
?>