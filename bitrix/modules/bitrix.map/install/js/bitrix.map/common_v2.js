/*!
 * GeoMapp v2.0
 * Author: Eugene Petrov petroveg@gmail.com
 * Company: Bitrix http://1c-bitrix.ru/
 * Date: 2014-12-28
 */

var $GeoMapp=function(p,A,v){var r=function(){function x(a){if(a&&k.getType(a,"object"))for(var c=Object.keys(a),b=c.length;b--;)g[c[b]]=a[c[b]]}function b(a){g.getWrapper().trigger("map:loaded");w.mapInitialize(a).then(function(){g.getWrapper().trigger("map:init");g.get("mapScript")[g.get("mapType")].loaded=!0;f()})}function f(){g.get("pageType")&&(g.get("cluster").icon&&(g.get("cluster").icon=k.setFullPath(g.get("cluster").icon,"images")),Object.keys(g.get("cluster").set).forEach(function(a){g.get("cluster").set[a].icon&&
(g.get("cluster").set[a].icon=k.setFullPath(g.get("cluster").set[a].icon,"images"))}),Object.keys(g.get("icon")).forEach(function(a){g.get("icon")[a].url=k.setFullPath(g.get("icon")[a].url,"images")}),q.getIcon("anchor")||(q.getIcon().anchor=[q.getIcon("size")[0]/2,q.getIcon("size")[1]]));q.__start&&(k.getType(q.__start,"function")&&q.__start.call(g),delete q.__start);g.getWrapper().on("click keydown keyup keypress input propertychange scroll focus blur change reset submit objectMarker:hover eventMarker:hover routeMarker:hover objectMarker:out eventMarker:out routeMarker:out objectMarker:click eventMarker:click routeMarker:click objectItem:click eventItem:click routeItem:click objectItem:click eventItem:click routeItem:click route:hover route:out route:click route:show direction:show set:direction check:value check:cat set:view cluster:detected cluster:done select:item set:scroll module:ready module:complete map:init map:complete map:loaded balloon:build",
function(a){a.stopPropagation()});delete g.init;delete g.__mapScriptLoaded__}function d(){var a=$.Deferred();g.get("mapScript")[g.get("mapType")].loaded?a.resolve():(Object.keys(g.get("mapScript")).forEach(function(a){g.get("mapScript")[a].main=k.insertParam(g.get("mapScript")[a].main,{callback:"$GeoMapp.__mapScriptLoaded__"})}),k.loadScript({address:g.get("mapScript")[g.get("mapType")],id:"maps",errorName:"MAP_SCRIPT_NOT_LOADED",listener:"listenMapScriptLoading",listenerErrorName:"ERRORS_MAP_LOADING",
timeout:!0}).then(function(c){a.resolve(c)}));return a.promise()}var h=$(document.documentElement),m,e={require:{defaultPath:"UNKNOWN_DEFAULT_PATH",device:"DEVICE_NOT_DEFINED"}},n={},l,a,c,t={},g={__mapScriptLoaded__:b,init:function(a){function h(a){c=setTimeout(function(){g.showError(a)},0)}if(!l){l=!0;$.extend(n,a);a=Object.keys(e.require);for(var m=a.length;m--;)if(n[a[m]])"device"==a[m]?p.deviceType=Object.keys(n.device)[0]:p[a[m]]=n[a[m]];else{h(e.require[a[m]]);return}document.write(k.createElement("*div",
{id:"bxMapFakeElement"}).get(0).outerHTML);k.loadScript({address:[n.device[g.get("deviceType")]],id:"common",errorName:"COMMON_SCRIPT_NOT_LOADED",listener:"listenMainScriptLoading",listenerErrorName:"ERRORS_COMMON_LOADING"}).then(function(a){function B(){for(var a=[e.require],b=Object.keys(e.dependence),m=Object.keys(e.independence),d=b.length;d--;)b[d]in n&&a.push(e.dependence[b[d]]);for(d=m.length;d--;)m[d]in n||a.push(e.independence[m[d]]);for(d=a.length;d--;)Object.keys(a[d]).forEach(function(c){c in
n?(p[c]=n[c],delete t[a[d][c]]):t[a[d][c]]=!0});Object.keys(t).length?h(t):(clearTimeout(c),k.extend(n),g.get("routeType")||(p.routeType={}),g.get("routeType")[g.get("mapType")]||(g.get("routeType")[g.get("mapType")]=["driving"]));return Object.keys(t).length}a?g.showError(a):B()||k.loadScript({address:g.get("libs"),id:"lib",errorName:"COMMON_SCRIPT_NOT_LOADED",listener:"listenMainScriptLoading",listenerErrorName:"ERRORS_COMMON_LOADING"}).then(function(a){a?g.showError(a):$(function(){g.getWrapper().one("module:complete",
function(){q.moduleComplete=!0}).one("map:complete",function(){q.mapComplete=!0});g.get("withoutMap")?f():(w.init({wrapper:g.getWrapper(),status:status,mapType:g.get("mapType"),pageType:g.get("pageType")}),d().then(function(a){a?g.get("device","mobile")?(p.withoutMap=!0,f()):g.showError(a):b(!0)}))})})})}},install:function(c,b,h){q.__start=h;b&&k.getType(b,"function")&&(b=b.call(this,n,k,w,C,q),b.options&&k.extend(b.options),b.requires&&$.extend(e,b.requires));x(c);a=!0;g.getWrapper().trigger("module:ready",
["GeoMapp"]);return this},extend:function(c){if(a)x(c);else g.getWrapper().one("module:ready",function(){x(c)});return this},complete:function(a){if(a)if(q.moduleComplete)a.call(this);else this.getWrapper().one("module:complete",function(){a.call(g)})},mapComplete:function(a){function c(){a.call(g,g.getMap())}if(a)if(q.mapComplete)c();else this.getWrapper().one("map:complete",c)},showError:function(a){$(function(){var c=g.getWrapper(),b=c.data("errorContainer"),h=g.get("barHeight")||n.barHeight,m=
k.createElement("*ul","bxmap-group bxmap-status bxmap-icon",{"data-target":"message"});b||(b=k.createElement("*div","bxmap-error-message",{"data-container":"message"}).appendTo(k.createElement("*div","bxmap-error-container",{"data-container":"list"}).appendTo(c)),c.data({errorContainer:b}));m.appendTo(b);c.addClass("bxmap-wrapper-show bxmap-error-wrapper").removeClass("bxmap-loading");h&&c.css({top:h+"px"});q.slideMode||c.addClass("bxmap-visible");if(a)if(k.getType(a,"string"))c=[a];else if(k.getType(a,
"array"))c=a;else if(k.getType(a,"object"))c=Object.keys(a);else return;else m.empty(),c=Object.keys(t);for(b=0;b<c.length;b++)a&&!a[c[b]]&&c[b]in t?t[c[b]].remove():t[c[b]]=k.createElement("*li","bxmap-item").append(k.createElement("*span","bxmap-block bxmap-fail").html(g.get("parseMessages")[c[b]]||g.get("routeMessages")[c[b]]||c[b])).appendTo(m)});return this},getMap:function(){return w.Map},hide:function(){h.removeClass("bxmap-root-overflow");this.getWrapper().addClass("bxmap-none");return this},
show:function(){h.addClass("bxmap-root-overflow");this.getWrapper().removeClass("bxmap-none");return this},trackItem:function(a){this.getWrapper().on("select:item",function(c,b){c.stopPropagation();a&&k.getType(a,"function")&&a.call(g,b)})},getRequestURL:function(a,c){return k.insertParam(a,c)},getRemoteData:function(a){var c=$.Deferred(),b={type:this.get("pageType"),fields:a.fields};a.cat?b.cat=a.cat:a.item?b.item=a.item:c.resolve();$.ajax({url:this.getRequestURL(g.get("ajax"),b),type:a.type||"get",
dataType:a.dataType||"json"}).done(function(a){c.resolve(a)}).fail(function(){c.resolve()});return c.promise()},getWrapper:function(a){m&&m.length||(m=$("#bxMapContainer"))&&m.length&&(m.addClass("bxmap-"+(n.mapType||g.get("mapType"))),$("#bxMapFakeElement").remove());return m},get:function(a,c){if(a)switch(a){case "cats":case "items":if(c){if(p[a]&&p[a][c])return p[a][c]}else return p[a];default:if(void 0!==p[a]){if(c){if(k.getType(c,"array")){for(var b=Object.keys(c),g=b.length;g--;)if(p[a]===c[b[g]])return!0;
return!1}return p[a]===c}return p[a]}}else return Object.keys(p)},set:function(a,c,b){if(a)switch(a){case "cats":case "items":if(c)if(b){if(k.getType(b,"object"))return p[a][c]=b,p[a][c]}else{delete p[a][c];break}else{if(null===c&&b)return p[a]=b,p[a];if(p[a])break;else return p[a]={},p[a]}case "noCats":case "animationTime":case "verticalTime":case "horizontalTime":case "geolocation":case "mapBounds":if(void 0!==c)return p[a]=c,p[a];delete p[a]}}};return g}(),q=function(){var k={noCatsName:"standard",
defaultStatus:"default",activeStatus:"active",icons:{direction:{},route:{},cat:{}},cats:{},counts:{},sets:{},items:{},routes:{},markers:{},visibleCats:{},visibleItems:{},visibleRoutes:{},visibleMarkers:{},activeIDs:{},getCatID:function(b,f){if(b){if(r.get("cats")){if(r.get("cats",b))return b;f=f||r.get("items");return f[b]?this.getCatID(f[b].item||f[b].cat,f):b}return this.noCatsName}},getCatChain:function(b,f){f.push(b);return r.get("cats",b).cat?this.getCatChain(r.get("cats",b).cat,f):f.reverse()},
catIcon:function(b,f){b||(b=this.noCatsName);f&&(this.icons.cat[b]=f);return this.icons.cat[b]},directionIcon:function(b){return b?this.icons.direction=b:this.icons.direction},routeIcon:function(b){return b?this.icons.route=b:this.icons.route},getCounts:function(b){return this.counts[b]||0},setCounts:function(b,f){return this.counts[b]=f&&0<f?f:0},changeCounts:function(b,f){return this.setCounts(b,(this.getCounts(b)||0)+f?1:-1)},getActiveID:function(b){return b?b==this.activeID:this.activeID},setActiveID:function(b){if(b)return this.activeID=
b;delete this.activeID},getActiveItem:function(b){return this.activeItem},setActiveItem:function(b,f){this.activeItem&&(Object.keys(this.getActiveIDs()).length?this.getActiveIDs(f)||this.resetActiveIDs():this.activeItem.removeClass("bxmap-active"));if(b){if(this.getItems(b,f))return this.activeItem=this.getItems(b,f).item.addClass("bxmap-active")}else if(f){if(this.getItems(f))return this.activeItem=this.getItems(f).item.addClass("bxmap-active")}else delete this.activeItem},getActiveIDs:function(b){if(arguments.length){if(b)return this.activeIDs[b]}else return this.activeIDs},
setActiveIDs:function(b,f){if(b){if(f)return this.getItems(b).item.addClass("bxmap-active"),this.activeIDs[b]=f;this.resetActiveIDs(b)}else this.resetActiveIDs()},resetActiveIDs:function(b){function f(b){k.getItems(b)&&k.getItems(b).item&&k.getItems(b).item.removeClass("bxmap-active");delete k.activeIDs[b]}if(b)f(b);else{b=Object.keys(this.activeIDs);for(var d=b.length;d--;)f(b[d])}},getCats:function(b){return b?this.cats[b]:this.cats},setCats:function(b,f){if(f)return this.cats[b]=f;delete this.cats[b];
return Object.keys(this.cats).length},getActiveCats:function(b){return b?this.visibleCats[b]:this.visibleCats},setActiveCats:function(b,f){b&&(b in this.visibleCats?(this.getCats(b).removeClass("bxmap-active"),this.getItemSets(b).addClass("bxmap-none"),delete this.visibleCats[b]):this.getCats(b)&&f&&(this.getCats(b).addClass("bxmap-active"),this.getItemSets(b).removeClass("bxmap-none"),this.visibleCats[b]=f))},getItems:function(b,f){if(b){if(f)return this.items[b]?this.items[b][f]:this.items[f];if(!this.items[b]){var d=
this.getCatID(b);if(this.items[d])return this.items[d][b]}return this.items[b]}return this.items},setItems:function(b,f,d){return r.get("noCats")?f?d?this.items[f]=d:this.items[f]={}:this.items={}:b?(this.items[b]||(this.items[b]={}),f?d?this.items[b][f]=d:this.items[b][f]={}:this.items[b]={}):this.items={}},getVisibleItems:function(b,f){return b?f?this.visibleItems[b]?this.visibleItems[b][f]:this.visibleItems[f]:this.visibleItems[b]?this.visibleItems[b]:this.visibleItems[this.getCatID(b)]:this.visibleItems},
setVisibleItems:function(b,f){if(b){var d=this.getItems(b);if(d){if(r.get("noCats")){if(f)return this.visibleItems[b]=d;delete this.visibleItems[b];return this.visibleItems[b]}f&&this.visibleItems[b]||(this.visibleItems[b]={});return f?f in d?this.visibleItems[b][f]=d[f]:this.visibleItems[b]=d:this.visibleItems[b]}}else return this.visibleItems={}},getItemSets:function(b){return b?this.sets[b]:this.sets},setItemSets:function(b,f){if(f)return this.sets[b]=this.createItemSet(b,f);delete this.sets[b]},
getMarker:function(b){return b?this.markers[b]:this.markers},setMarker:function(b,f){var d=w.createMarker(b,f);this.markers[b]=d.marker;return d},getVisibleMarker:function(b){return b?this.visibleMarkers[b]:this.visibleMarkers},setVisibleMarker:function(b,f){b?f?this.getMarker(b)&&(this.visibleMarkers[b]=this.getMarker(b)):delete this.visibleMarkers[b]:this.visibleMarkers={}},getActiveMarker:function(){return this.activeMarker},setActiveMarker:function(b){if(b)return this.activeMarker=this.markers[b];
delete this.activeMarker},getIcon:function(b){if(r.get("icon")&&r.get("pageType"))return b?r.get("icon")[r.get("pageType")][b]:r.get("icon")[r.get("pageType")]},getRouteType:function(b){if(r.get("routeType")&&r.get("pageType"))return b?r.get("routeType")[r.get("mapType")][b]:r.get("routeType")[r.get("mapType")]},getRoute:function(b,f){if(b){if(f)return this.routes[b]?this.routes[b][f]:this.routes[f];if(!this.routes[b]){var d=this.getCatID(b);if(this.routes[d])return this.routes[d][b]}return this.routes[b]}return this.routes},
setRoute:function(b,f,d){if(r.get("noCats"))if(b){if(f)return this.routes[b]=f;delete this.routes[b]}else this.routes={};else if(b)if(this.getRoute(b)||(this.routes[b]={}),f){if(d)return this.routes[b][f]=d;delete this.routes[b][f]}else this.routes[b]={};else this.routes={}},getVisibleRoutes:function(b){return b?this.visibleRoutes[b]:this.visibleRoutes},setVisibleRoutes:function(b,f){f?this.visibleRoutes[b]=this.getRoute(b):b?delete this.visibleRoutes[b]:this.visibleRoutes={}},isActiveRoute:function(b){return b?
b==this.activeRoute:this.activeRoute},getActiveRoute:function(){if(this.activeRoute)return this.getRoute(this.activeRoute)},setActiveRoute:function(b){if(b)return this.activeRoute=b,this.getRoute(b);delete this.activeRoute},getActiveDirection:function(){return this.activeDirection},setActiveDirection:function(b){if(b)return this.activeDirection={options:{origin:null,destination:null,travelMode:null},location:{custom:null,user:null,object:w.createPoint(r.get("items",b))}};delete this.activeDirection}};
return k}(),w=function(){function p(){var a=l.clusterIconOptions||{},c=l.markersdata;this.extend(p,google.maps.OverlayView);this.markers_=[];this.clusters_=[];this.ready_=!1;this.template_=a.template;this.gridSize_=a.gridSize/2||32;this.minClusterSize_=a.minimumClusterSize||2;this.maxZoom_=a.maxZoom||null;this.set_=a.set;this.zoomOnClick_=a.zoomOnClick?a.zoomOnClick:!0;this.averageCenter_=a.averageCenter?a.averageCenter:!1;this.setMap(l.Map);this.prevZoom_=l.Map.getZoom();c&&(c.length||Object.keys(c).length)&&
this.addMarkers(c,!1)}function b(a){this.markerClusterer_=a;this.minClusterSize_=a.getMinClusterSize();this.averageCenter_=a.isAverageCenter();this.markers_=[];this.clusterIcon_=new f(this,a.set_.length)}function f(a){a.getMarkerClusterer().extend(f,google.maps.OverlayView);this.cluster_=a;this.setMap(l.Map)}var d,h,m,e,n,l={bounds:{lat:[],lng:[]},markersdata:[],defaultZoom:12,scaleList:["house","street","district","locality"],createCollection:function(){"google"==d?this.collection=new google.maps.MVCArray:
"yandex"==d&&(this.collection=new ymaps.GeoObjectCollection({},{}))},init:function(a){m=a.wrapper;d=a.mapType;h=a.pageType;this.createCollection.prototype={constructor:l.createCollection,add:function(a){"google"==d?this.collection.push(a):"yandex"==d&&this.collection.add(a)},getItems:function(){if("google"==d)return this.collection.getArray();if("yandex"==d){var a=[];this.collection.each(function(b){a.push(b)});return a}},remove:function(){"google"==d?this.collection.forEach(function(a){l.removeMarker(a,
a.catid)}):"yandex"==d&&this.collection.each(function(a){l.removeMarker(a,a.catid)})}}},getWrapper:function(){return $wrapper},getContainer:function(){return e},mapInitialize:function(a){var c=$.Deferred();"google"==d?window.google&&google.maps&&(a||c.resolve()):"yandex"==d&&window.ymaps&&ymaps.ready&&ymaps.ready(function(){c.resolve()});return c.promise()},createMap:function(a){function c(){m.trigger("map:complete");b.resolve(l.Map)}var b=$.Deferred();this.Map&&b.resolve(this.Map);e=a.container;
var g={};a=this.getBounds(a.bounds);"google"==d?($.extend(g,{mapTypeId:google.maps.MapTypeId.ROADMAP,mapTypeControl:!1,draggable:!0,zoom:this.defaultZoom,center:a.center}),$.extend(g,{zoomControl:!1,panControl:!1,streetViewControl:!1}),this.Map=new google.maps.Map(e.get(0),g),a.bounds&&l.Map.fitBounds(a.bounds),google.maps.event.addListener(this.Map,"idle",function(){n=l.Map.mapTypes[l.Map.getMapTypeId()].maxZoom;l.cluster&&l.cluster.redraw()}),google.maps.event.addListener(l.Map,"zoom_changed",function(){n=
l.Map.mapTypes[l.Map.getMapTypeId()].maxZoom;l.cluster&&l.cluster.react()}),n<l.Map.getZoom()&&this.changeZoom(n),google.maps.event.addListenerOnce(this.Map,"idle",c)):"yandex"==d&&($.extend(g,{controls:[]}),a.bounds?$.extend(g,{bounds:a.bounds}):a.center&&$.extend(g,{center:a.center,zoom:this.defaultZoom}),this.Map=new ymaps.Map(e.get(0),g),this.Map.zoomRange.get().done(function(a){n=a[a.length-1];l.Map.options.set("maxZoom",n);l.Map.setZoom(Math.round(Math.floor(l.Map.getZoom())));c()}));return b.promise()},
refreshMap:function(a){if("google"==d){var c=this.Map.getCenter();google.maps.event.trigger(this.Map,"resize");a?this.Map.fitBounds(this.getBounds().bounds):this.Map.setCenter(c)}else"yandex"==d&&(this.Map.container.fitToViewport(),a&&this.Map.setBounds(this.getBounds().bounds))},destroyMap:function(){"google"==d?this.Map.container.empty():"yandex"==d&&this.Map.destroy();delete this.Map},createPoint:function(a){switch(d){case "google":return new google.maps.LatLng(a.lat,a.lng);case "yandex":return[a.lat,
a.lng]}},getBounds:function(a){a=a||r.get("mapBounds");try{a.lat.sort();a.lng.sort();if(!a.lat.length||!a.lng.length)return null;if(a.lat[0]==a.lat[a.lat.length-1]||a.lng[0]==a.lat[a.lng.length-1])return{center:this.createPoint({lat:a.lat[0],lng:a.lng[0]})};var c,b=[a.lat[0],a.lng[0]],g=[a.lat[a.lat.length-1],a.lng[a.lng.length-1]];switch(d){case "google":c=new google.maps.LatLngBounds;c.extend(new google.maps.LatLng(b[0],b[1]));c.extend(new google.maps.LatLng(g[0],g[1]));break;case "yandex":c=[b,
g]}return{bounds:c,center:this.createPoint({lat:(a.lat[0]+a.lat[a.lat.length-1])/2,lng:(a.lng[0]+a.lng[a.lng.length-1])/2})}}catch(h){return null}},setBounds:function(a){if(a=this.getBounds(a))this.bounds=a},fitMap:function(a){if(a=this.getBounds(a))a.bounds?"google"==d?this.Map.fitBounds(a.bounds):"yandex"==d&&this.Map.setBounds(a.bounds):a.center&&("google"==d?(this.Map.setCenter(this.createPoint(a.center)),this.Map.setZoom(this.defaultZoom)):"yandex"==d&&this.Map.setCenter(this.createPoint(a.center),
this.defaultZoom));return this.Map},centerMap:function(a){var c=a.getMap()||this.Map,b;"google"==d?b=a.getPosition():"yandex"==d&&(b=a.geometry.getCoordinates());c.setCenter(b);return c},changeZoom:function(a){k.getType(a,"number")||(a=Math.round(this.Map.getZoom())-Math.pow(-1,1*!!a));"google"==d?a<=n&&this.Map.setZoom(a):"yandex"==d&&this.Map.setZoom(a,{checkZoomRange:!0})},panToMarker:function(a){this.Map.panTo(this.getCoords(a))},getCoords:function(a){if("google"==d)return a.getPosition();if("yandex"==
d)return a.geometry.getCoordinates()},createMarker:function(a,c){var b=this.createPoint(c.data),g,e=h.slice(0,-1)+"Marker";this.bounds.lat&&this.bounds.lng&&(this.bounds.lat.push(c.data.lat),this.bounds.lng.push(c.data.lng));"google"==d?(g=new google.maps.Marker({icon:c.icon["_"+c.status],position:b,title:c.data.name}),a&&("routes"==h&&(google.maps.event.addListener(g,"mouseover",function(){m.trigger(e+":hover",[a])}),google.maps.event.addListener(g,"mouseout",function(){m.trigger(e+":out",[a])})),
google.maps.event.addListener(g,"click",function(){m.trigger(e+":click",[a])}))):"yandex"==d&&(g=new ymaps.Placemark(b,{hintContent:c.data.name},{preset:c.catid+"#"+c.status}),a&&("routes"==h&&(g.events.add("mouseenter",function(c){m.trigger(e+":hover",[a])}),g.events.add("mouseleave",function(c){m.trigger(e+":out",[a])})),g.events.add("click",function(c){m.trigger(e+":click",[a])})));a&&(g.id=a);c.catid&&(g.catid=c.catid);return{marker:g,point:b}},createPolyLine:function(a,c,b,g){var h;"google"==
d?(h=new google.maps.Polyline(k.clone(b,{path:c})),g&&(google.maps.event.addListener(h,"click",function(){m.trigger("route:click",[a])}),google.maps.event.addListener(h,"mouseover",function(){m.trigger("route:hover",[a])}),google.maps.event.addListener(h,"mouseout",function(){m.trigger("route:out",[a])}))):"yandex"==d&&(h=new ymaps.Polyline(c,b),g&&(h.events.add("click",function(c){m.trigger("route:click",[a])}),h.events.add("mouseenter",function(c){m.trigger("route:hover",[a])}),h.events.add("mouseleave",
function(c){m.trigger("route:out",[a])})));return h},showMarker:function(a){var c=a.getMap()||this.Map;"google"==d?a.setMap(c):"yandex"==d&&c.geoObjects.add(a);return a},removeMarker:function(a){var c=a.getMap()||this.Map;"google"==d?a.setMap(null):"yandex"==d&&c.geoObjects.remove(a);return a},setMarkerView:function(a,c,b,g){"google"==d?(a.setIcon(c["_"+g]),g==q.activeStatus?a.setZIndex(1E3):a.setZIndex(0)):"yandex"==d&&a.options.set("preset",b+"#"+g);return a},showRoute:function(a,c,b){var g=(a.polyLine||
a.markers[0]).getMap()||this.Map;b=b||q.defaultStatus;this.setRouteView(a,c,b);"google"==d?(a.polyLine&&a.polyLine.setMap(g),Object.keys(a.markers).forEach(function(c){a.markers[c].setMap(g)})):"yandex"==d&&(a.polyLine&&g.geoObjects.add(a.polyLine),Object.keys(a.markers).forEach(function(c){g.geoObjects.add(a.markers[c])}));return a},removeRoute:function(a){var c=(a.polyLine||a.markers[0]).getMap()||this.Map;"google"==d?(a.polyLine&&a.polyLine.setMap(null),Object.keys(a.markers).forEach(function(c){a.markers[c].setMap(null)})):
"yandex"==d&&(a.polyLine&&c.geoObjects.remove(a.polyLine),Object.keys(a.markers).forEach(function(b){c.geoObjects.remove(a.markers[b])}));return a},setPathView:function(a,c,b,g){"google"==d?a.setOptions(c["_path"+b.slice(0,1).toUpperCase()+b.slice(1)]):"yandex"==d&&a.options.set("preset",g+"path#"+b)},setRouteView:function(a,c,b){var g=0,h=a.markers.length;a.polyLine&&this.setPathView(a.polyLine,c,b,"route");a.status=b;b==q.activeStatus&&(g++,this.setMarkerView(a.markers[0],c,"route","start"),a.end&&
(h--,this.setMarkerView(a.markers[h],c,"route","end")));for(;g<h;g++)this.setMarkerView(a.markers[g],c,"route",b);return a},createIconSet:function(a,c,b,g){var h={},m=c.url,e=c.logo?c.logo[1]:0;a&&a.icon&&(m=k.setFullPath(a.icon,"images"));if(!a||g&&!a.icon)a={pos:0};a.pos=isFinite(a.pos)?parseInt(a.pos):0;"google"==d?(h._default={url:m,size:new google.maps.Size(c.size[0],c.size[1]),anchor:new google.maps.Point(c.anchor[0],c.anchor[1]),origin:new google.maps.Point(a.pos,e)},h._active=k.clone(h._default,
{origin:new google.maps.Point(a.pos,e+c.size[1])})):"yandex"==d&&(h._default={iconLayout:q.defaultStatus+"#image",iconImageHref:m,iconImageSize:[c.size[0],c.size[1]],iconImageOffset:[-c.anchor[0],-c.anchor[1]],iconImageClipRect:[[a.pos,e],[a.pos+c.size[0],e+c.size[1]]],hideIconOnBalloonOpen:!1},h._active=k.clone(h._default,{iconImageClipRect:[[a.pos,e+c.size[1]],[a.pos+c.size[0],e+2*c.size[1]]]}),ymaps.option.presetStorage.add(b+"#"+q.defaultStatus,h._default),ymaps.option.presetStorage.add(b+"#"+
q.activeStatus,h._active));return h},createRouteIconSet:function(a,c,b){var g={},h=a.logo?a.logo[1]:0;"google"==d?(g._start={url:a.url,size:new google.maps.Size(a.size[0],a.size[1]),anchor:new google.maps.Point(a.anchor[0],a.anchor[1]),origin:new google.maps.Point(0,h)},g._end=k.clone(g._start,{origin:new google.maps.Point(a.size[0],h)}),g._default={url:a.url,size:new google.maps.Size(c.def.size[0],c.def.size[1]),anchor:new google.maps.Point(c.def.anchor[0],c.def.anchor[1]),origin:new google.maps.Point(c.def.offset[0],
c.def.offset[1])},g._hover=k.clone(g._default,{origin:new google.maps.Point(c.def.offset[0],c.def.offset[1]+c.def.size[1])}),g._pathDefault={strokeColor:c.strokeColor,strokeOpacity:c.strokeOpacity,strokeWeight:c.strokeWeight},void 0!==c.active&&(g._active={url:a.url,size:new google.maps.Size(c.active.size[0],c.active.size[1]),anchor:new google.maps.Point(c.active.anchor[0],c.active.anchor[1]),origin:new google.maps.Point(c.active.offset[0],c.active.offset[1])},g._super=k.clone(g._active,{origin:new google.maps.Point(c.active.offset[0],
c.active.offset[1]+c.active.size[1])})),void 0!==c.strokeOpacityHover&&(g._pathHover=k.clone(g._pathDefault,{strokeOpacity:c.strokeOpacityHover})),void 0!==c.strokeColorActive&&(g._pathActive=k.clone(g._pathHover,{strokeColor:c.strokeColorActive}))):"yandex"==d&&(g._start={iconLayout:q.defaultStatus+"#image",iconImageHref:a.url,iconImageSize:[a.size[0],a.size[1]],iconImageOffset:[-a.anchor[0],-a.anchor[1]],iconImageClipRect:[[0,h],[a.size[0],h+a.size[1]]],hideIconOnBalloonOpen:!1},g._end=k.clone(g._start,
{iconImageClipRect:[[a.size[0],h],[2*a.size[0],h+a.size[1]]]}),g._default=k.clone(g._start,{iconImageSize:[c.def.size[0],c.def.size[1]],iconImageClipRect:[[c.def.offset[0],c.def.offset[1]],[c.def.offset[0]+c.def.size[0],c.def.offset[1]+c.def.size[1]]],iconImageOffset:[-c.def.anchor[0],-c.def.anchor[1]]}),g._hover=k.clone(g._default,{iconImageClipRect:[[c.def.offset[0],c.def.offset[1]+c.def.size[1]],[c.def.offset[0]+c.def.size[0],c.def.offset[1]+2*c.def.size[1]]]}),g._pathDefault={strokeColor:c.strokeColor,
strokeOpacity:c.strokeOpacity,strokeWidth:c.strokeWeight},ymaps.option.presetStorage.add(b+"#start",g._start),ymaps.option.presetStorage.add(b+"#end",g._end),ymaps.option.presetStorage.add(b+"#"+q.defaultStatus,g._default),ymaps.option.presetStorage.add(b+"#hover",g._hover),ymaps.option.presetStorage.add(b+"path#"+q.defaultStatus,g._pathDefault),ymaps.option.presetStorage.add(b+"#invisible",k.clone(g._start,{iconImageClipRect:[[0,0],[0,0]],iconImageSize:[0,0]})),void 0!==c.active&&(g._active=k.clone(g._default,
{iconImageClipRect:[[c.active.offset[0],c.active.offset[1]],[c.active.offset[0]+c.active.size[0],c.active.offset[1]+c.active.size[1]]],iconImageOffset:[-c.active.anchor[0],-c.active.anchor[1]]}),g._super=k.clone(g._active,{iconImageClipRect:[[c.active.offset[0],c.active.offset[1]+c.active.size[1]],[c.active.offset[0]+c.active.size[0],c.active.offset[1]+2*c.active.size[1]]]}),ymaps.option.presetStorage.add(b+"#"+q.activeStatus,g._active),ymaps.option.presetStorage.add(b+"#super",g._super)),void 0!==
c.strokeOpacityHover&&(g._pathHover=k.clone(g._pathDefault,{strokeOpacity:c.strokeOpacityHover}),ymaps.option.presetStorage.add(b+"path#hover",g._pathHover)),void 0!==c.strokeColorActive&&(g._pathActive=k.clone(g._pathHover,{strokeColor:c.strokeColorActive}),ymaps.option.presetStorage.add(b+"path#"+q.activeStatus,g._pathActive)));return g},setClusterOptions:function(a,c){var b=[],g=[],h=a.anchor||[0,0],b=[],g=[];k.createRule(".bxmap-cluster",["background-image:url("+a.icon+");","color:"+a.color+";"].join(""));
for(var e=0,n=0,f=0;e<a.set.length;e++){var n=a.set[e].size,p=["width:"+n+"px;","height:"+n+"px;","line-height:"+n+"px;"],y=[f,0],z=[f,n],f=[f,2*n];a.set[e].color&&p.push("color:"+a.set[e].color+";");a.set[e].icon&&(p.push("background-image:url("+a.set[e].icon+");"),y[0]=0,z[0]=0,f[0]=0);p.push("background-position:-"+y[0]+"px -"+y[1]+"px;");k.createRule(".bxmap-cluster.bxmap-cluster-count-"+(e+1),p.join(""));k.createRule(".bxmap-cluster.bxmap-cluster-group.bxmap-cluster-count-"+(e+1),"background-position:-"+
z[0]+"px -"+z[1]+"px;");k.createRule(".bxmap-cluster.bxmap-cluster-group.bxmap-active.bxmap-cluster-count-"+(e+1),"background-position:-"+f[0]+"px -"+f[1]+"px;");b.push({size:[n,n],offset:[-n/2-h[0],-n/2-h[1]]});f=n;e&&g.push(Math.pow(10,e))}"google"==d?(this.clusterIconOptions={averageCenter:!0,template:c,gridSize:a.gridSize,set:b},m.on("mouseenter",'[data-item~="cluster"]',function(){$(this).css({zIndex:1})}).on("mouseleave",'[data-item~="cluster"]',function(){$(this).css({zIndex:"auto"})})):"yandex"==
d&&(h=ymaps.templateLayoutFactory.createClass(c.get(0).outerHTML,{build:function(){var a=this.getData().properties.get("geoObjects"),c=Math.ceil(Math.log(a.length)/Math.log(10));this.constructor.superclass.build.call(this);this.container_=$(this.getParentElement()).find(".bxmap-cluster").html(a.length).addClass("bxmap-cluster-count-"+c);this.target=this.getData().geoObject;if(this.objects="function"==typeof this.target.getGeoObjects&&this.target.getGeoObjects())this.bounds=this.target.getBounds(),
(this.group=this.bounds[0][0]==this.bounds[1][0]&&this.bounds[0][1]==this.bounds[1][1])&&this.container_.addClass("bxmap-cluster-group");this.target==l.activeCluster?this.container_.addClass("bxmap-active"):this.container_.removeClass("bxmap-active");this.target.events.add("marker:"+q.defaultStatus,this.unsetActive,this);this.target.events.add("marker:"+q.activeStatus,this.setActive,this);this.target.events.add("click",this.onclick,this)},onclick:function(a){a.preventDefault();this.objects&&(this.group?
this.isActive()?(l.setClusterView(),m.trigger("cluster:detected")):(l.setClusterView(this.target),m.trigger("cluster:detected",[this.objects,this.bounds[0]])):l.Map.setBounds(this.bounds))},isActive:function(){return this.container_.hasClass("bxmap-active")},setActive:function(){this.container_.addClass("bxmap-active")},unsetActive:function(){this.container_.removeClass("bxmap-active")},clear:function(){this.target.events.remove("marker:"+q.defaultStatus,this.unsetActive,this);this.target.events.remove("marker:"+
q.activeStatus,this.setActive,this);this.target.events.remove("click",this.onclick,this);this.constructor.superclass.clear.call(this)}}),this.cluster=new ymaps.Clusterer({clusterIcons:b,clusterNumbers:g,clusterIconContentLayout:h,clusterOpenBalloonOnClick:!1,clusterDisableClickZoom:!0,gridSize:a.gridSize}))},setClusterView:function(a){"google"==d?(this.activeCluster&&(this.activeCluster.unsetActive(),delete this.activeCluster),a&&setTimeout($.proxy(function(){this.activeCluster=a;a.setActive()},this),
0)):"yandex"==d&&(this.activeCluster&&this.activeCluster.events.fire("marker:"+q.defaultStatus),a&&setTimeout($.proxy(function(){this.activeCluster=a;a.events.fire("marker:"+q.activeStatus)},this),0))},updateMarkerCluster:function(a){"google"==d?(this.cluster&&this.cluster.removeMarkers(this.markersdata),this.markersdata=k.values(a),this.cluster=new p):"yandex"==d&&(this.cluster.getMap()||this.Map.geoObjects.add(this.cluster),this.cluster.removeAll(),(this.markersdata=k.values(a))&&this.markersdata.length?
(this.cluster.add(this.markersdata),a=this.cluster.getClusters(),a.length?(l.clusterLength?m.trigger("cluster:done",[a.length],l.clusterLength):m.trigger("cluster:done"),l.clusterLength=a.length):m.trigger("cluster:done")):l.clusterLength&&(delete l.clusterLength,m.trigger("cluster:done")))},getMarkerInCluster:function(a,c){function b(a){n?c.noZoom||n.equal?e.resolve(n.cluster,n.cluster[m](),n.bounds):a(n.bounds):e.resolve(null)}function g(){e.resolve(null)}function h(c){if(c)for(var b=c.length;b--;){var g=
c[b][m]();if(1<g.length)for(var e=g.length;e--;)if(g[e].id in a)return{cluster:c[b],bounds:c[b].getBounds()}}return null}var e=$.Deferred(),m,n;if(k.getType(a,"string")){var f=a;a={};a[f]=!0}if("google"==d){m="getMarkers";if(n=h(l.cluster.clusters_))n.equal=n.bounds.getNorthEast().equals(n.bounds.getSouthWest());b(function(a){google.maps.event.addListenerOnce(l.Map,"zoom_changed",g);l.Map.fitBounds(a)})}else if("yandex"==d)if(c.marker&&c.marker.getMap())e.resolve(null);else{m="getGeoObjects";if(n=
h(this.cluster.getClusters()))n.equal=n.bounds[0][0]==n.bounds[1][0]&&n.bounds[0][1]==n.bounds[1][1];b(function(a){l.Map.events.once("boundschange",g);l.Map.setBounds(a)})}return e.promise()},getGeoPoint:function(a,c){var b=$.Deferred(),g={},h=0;if(k.getType(a,"string")){if(c&&c.point){var e=c.point.address.split(/\s*,\s*/).slice(0,2);e.push(a);a=e.join(", ")}g.address=a}else g.location=a;"google"==d?(new google.maps.Geocoder).geocode(g,function(a,g){if("OK"==g){if(c.getLocation)for(var e=0;e<a.length;e++)if(0<=
a[e].types.indexOf("locality")){h=e;break}console.log(a);b.resolve({location:a[h].geometry.location,address:a[h].formatted_address})}else b.resolve(g)}):"yandex"==d&&(g=0,c&&c.getLocation&&(g=3),ymaps.geocode(a,{kind:l.scaleList[g]}).then(function(a){b.resolve({location:a.geoObjects.get(0).geometry.getCoordinates(),address:a.geoObjects.get(0).properties.get("text")})},function(a){b.resolve(status)}));return b.promise()},getRoute:function(a,c,b,g){function h(c,b){function g(){l.scaleList[++b]?h(c,
b):(k[c]="NOT_FOUND",e())}ymaps.geocode(a[c],{kind:l.scaleList[b]}).then(function(a){try{var b=a.geoObjects.get(0).properties.get("text"),h=a.geoObjects.get(0).geometry.getCoordinates();b&&h?(f[c]={text:b,coords:{lat:h[0],lng:h[1]}},e()):g()}catch(m){g()}},function(a){a[c]=a;e()})}function e(){f.origin&&f.destination&&ymaps.route([l.createPoint(f.origin.coords),l.createPoint(f.destination.coords)],{mapStateAutoApply:!0}).then(function(a){var b=a.getPaths(),h=a.getWayPoints(),e=["start","end"],d="start"==
g?1:0;a={main:a,duration:a.getTime(),length:a.getLength(),humanDuration:a.getHumanTime(),humanLength:a.getHumanLength(),copyrights:"\u041a\u0430\u0440\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u0434\u0430\u043d\u043d\u044b\u0435 \u00a9 "+(new Date).getFullYear()+" Yandex",points:[],segments:[],bounds:{lat:[f.origin.coords.lat],lng:[f.origin.coords.lng]},startMarker:h.get(0),endMarker:h.get(h.getLength()-1),polyLine:b};g?e[d]="invisible":e=["invisible","invisible"];
a.startMarker.properties.set("iconContent","");a.endMarker.properties.set("iconContent","");l.setPathView(a.polyLine,c,q.defaultStatus,"direction");l.setMarkerView(a.startMarker,c,"direction",e[0]);l.setMarkerView(a.endMarker,c,"direction",e[1]);for(e=0;e<b.getLength();e++)for(var d=b.get(e).getSegments(),h=0,t;h<d.length;h++)t=d[h].getCoordinates(),h<d.length-1?(a.bounds.lat.push(t[t.length-1][0]),a.bounds.lng.push(t[t.length-1][1]),a.points.push(l.createPoint({lat:t[t.length-1][0],lng:t[t.length-
1][1]}))):(a.bounds.lat.push(f.destination.coords.lat),a.bounds.lng.push(f.destination.coords.lng)),t=d[h].getHumanAction(),t=t.slice(0,1).toUpperCase()+t.slice(1),d[h].getStreet()&&(t+=", "+d[h].getStreet()),a.segments.push({length:d[h].getLength(),time:d[h].getTime(),action:m(d[h].getAction()),humanLength:d[h].getHumanLength(),humanTime:d[h].getHumanTime(),text:t});f.route=a;n.resolve(f,k)},function(a){k.route=a.message;n.resolve(null,k)})}function m(a){if(!a)return"";var c=[];/\bright\b/.test(a)?
c.unshift("right"):/(\bleft\b|\bback\b|\benter\b)/.test(a)&&c.unshift("left");c.length?/\bslight\b/.test(a)?c.unshift("slight"):/(\bhard\b|\bsharp\b)/.test(a)?c.unshift("hard"):/(\buturn\b|\bback\b)/.test(a)?c.unshift("back"):/(\bramp\b|\bexit\b)/.test(a)?c.unshift("exit"):/\bfork\b/.test(a)?c.unshift("fork"):/\bkeep\b/.test(a)?c.unshift("keep"):/\bmerge\b/.test(a)?c.unshift("merge"):/\broundabout\b/.test(a)&&(/\bleave\b/.test(a)?c.unshift("exit","roundabout"):c.unshift("enter","roundabout")):(/\bexit\b/.test(a)?
c.unshift("exit"):/\bmerge\b/.test(a)?c.unshift("merge"):/\broundabout\b/.test(a)?c=a.split(/[^\w+]/):/\bferry\b/.test(a)&&(c.unshift("ferry"),/\btrain\b/.test(a)?c.unshift("train"):c.unshift("board")),c.length||/(\bstraight\b|\bnone\b)/.test(a)&&c.unshift("straight"));return c.join("-")}var n=$.Deferred();if("google"==d)(new google.maps.DirectionsService).route(a,function(a,h){if(h==google.maps.DirectionsStatus.OK){var e=a.routes[0],d=e.legs[0],f=d.steps,k={origin:{text:d.start_address,coords:d.start_location},
destination:{text:d.end_address,coords:d.end_location}},e={main:a,duration:d.duration.value,length:d.distance.value,humanDuration:d.duration.text,humanLength:d.distance.text,warnings:e.warnings,copyrights:e.copyrights,points:[],segments:[],bounds:{lat:[k.origin.coords.lat()],lng:[k.origin.coords.lng()]}},d={start:"origin",end:"destination"};g&&(e[g+"Marker"]=l.createMarker(g,{data:{lat:k[d[g]].coords.lat(),lng:k[d[g]].coords.lng(),name:k[d[g]].text},catid:null,icon:c,status:g}).marker);e.directionDisplay=
new google.maps.DirectionsRenderer(b);e.directionDisplay.setDirections(a);for(var d=0,p;d<f.length;d++)p=f[d].lat_lngs,d<f.length-1?(e.bounds.lat.push(p[p.length-1].lat()),e.bounds.lng.push(p[p.length-1].lng()),e.points.push(l.createPoint(p[p.length-1]))):(e.bounds.lat.push(k.destination.coords.lat()),e.bounds.lng.push(k.destination.coords.lng())),e.segments.push({length:f[d].distance.value,time:f[d].duration.value,action:m(f[d].maneuver),humanLength:f[d].distance.text,humanTime:f[d].duration.text,
text:f[d].instructions.replace(/<(\w+)(\s[^>]+)*>/g,'<$1 class="instructions">')});k.route=e;n.resolve(k,null)}else n.resolve(null,h)});else if("yandex"==d){var f={},k={};h("origin",0);h("destination",0)}return n.promise()},addDirection:function(a){a&&("google"==d?(a.directionDisplay.setMap(this.Map),a.startMarker&&a.startMarker.setMap(this.Map),a.endMarker&&a.endMarker.setMap(this.Map)):"yandex"==d&&this.Map.geoObjects.add(a.main))},removeDirection:function(a){a&&("google"==d?(a.directionDisplay.setMap(null),
a.startMarker&&a.startMarker.setMap(null),a.endMarker&&a.endMarker.setMap(null)):"yandex"==d&&this.Map.geoObjects.remove(a.main))}};p.prototype={constructor:p,extend:function(a,c){return function(a){for(var c in a.prototype)this.prototype[c]=a.prototype[c];return this}.apply(a,[c])},onAdd:function(){this.setReady_(!0)},draw:function(){},react:function(){var a=l.Map.minZoom||0,a=Math.min(Math.max(l.Map.getZoom(),a),n);this.prevZoom_!=a&&(this.prevZoom_=a,this.resetViewport())},fitMapToMarkers:function(){for(var a=
this.getMarkers(),c=new google.maps.LatLngBounds,b=a.length;b--;)c.extend(a[b].getPosition());l.Map.fitBounds(c)},isZoomOnClick:function(){return this.zoomOnClick_},isAverageCenter:function(){return this.averageCenter_},getMarkers:function(){return this.markers_},getTotalMarkers:function(){return this.markers_.length},setMaxZoom:function(a){this.maxZoom_=a},getMaxZoom:function(){return this.maxZoom_},calculator_:function(a,c){for(var b=0,h=a.length,e=h;0!==e;)e=parseInt(e/10,10),b++;b=Math.min(b,
c);return{text:h,index:b}},setCalculator:function(a){this.calculator_=a},getCalculator:function(){return this.calculator_},addMarkers:function(a,c){if(a.length)for(var b=a.length;b--;)this.pushMarkerTo_(a[b]);else{var h=Object.keys(a);if(h.length)for(b=h.length;b--;)this.pushMarkerTo_(a[h[b]])}c||this.redraw()},pushMarkerTo_:function(a){a.isAdded=!1;if(a.draggable){var c=this;google.maps.event.addListener(a,"dragend",function(){a.isAdded=!1;c.repaint()})}this.markers_.push(a)},addMarker:function(a,
c){this.pushMarkerTo_(a);c||this.redraw()},removeMarker_:function(a){var c=this.markers_.indexOf(a);if(0>c)return!1;a.setMap(null);this.markers_.splice(c,1);return!0},removeMarker:function(a,c){var b=this.removeMarker_(a);return!c&&b?(this.resetViewport(),this.redraw(),!0):!1},removeMarkers:function(a,c){for(var b=!1,h=a.length;h--;)var e=this.removeMarker_(a[h]),b=b||e;if(!c&&b)return this.resetViewport(),this.redraw(),!0},setReady_:function(a){this.ready_||(this.ready_=a,this.createClusters_(),
m.trigger("cluster:done",[this.clusters_.length]))},getTotalClusters:function(){return this.clusters_.length},getMap:function(){return l.Map},setMap:function(a){l.Map=a},getGridSize:function(){return this.gridSize_},setGridSize:function(a){this.gridSize_=a},getMinClusterSize:function(){return this.minClusterSize_},setMinClusterSize:function(a){this.minClusterSize_=a},getExtendedBounds:function(a){var c=this.getProjection(),b=new google.maps.LatLng(a.getNorthEast().lat(),a.getNorthEast().lng()),h=
new google.maps.LatLng(a.getSouthWest().lat(),a.getSouthWest().lng()),b=c.fromLatLngToDivPixel(b),h=c.fromLatLngToDivPixel(h);b.x+=this.gridSize_;b.y-=this.gridSize_;h.x-=this.gridSize_;h.y+=this.gridSize_;a.extend(c.fromDivPixelToLatLng(b));a.extend(c.fromDivPixelToLatLng(h));return a},isMarkerInBounds_:function(a,c){return c.contains(a.getPosition())},clearMarkers:function(){this.resetViewport(!0);this.markers_=[]},resetViewport:function(a){for(var c=this.clusters_.length;c--;)this.clusters_[c].remove();
for(c=this.markers_.length;c--;)this.markers_[c].isAdded=!1,a&&this.markers_[c].setMap(null);this.clusters_=[]},repaint:function(){var a=this.clusters_.slice();this.clusters_.length=0;this.resetViewport();this.redraw();setTimeout(function(){for(var c=a.length;c--;)a[c].remove()},0)},redraw:function(){this.createClusters_()},distanceBetweenPoints_:function(a,c){if(!a||!c)return 0;var b=(c.lat()-a.lat())*Math.PI/180,h=(c.lng()-a.lng())*Math.PI/180,b=Math.sin(b/2)*Math.sin(b/2)+Math.cos(a.lat()*Math.PI/
180)*Math.cos(c.lat()*Math.PI/180)*Math.sin(h/2)*Math.sin(h/2);return 12742*Math.atan2(Math.sqrt(b),Math.sqrt(1-b))},addToClosestCluster_:function(a){var c=4E4,h=null;a.getPosition();for(var e=this.clusters_.length,m;e--;)if(m=this.clusters_[e].getCenter())m=this.distanceBetweenPoints_(m,a.getPosition()),m<c&&(c=m,h=this.clusters_[e]);h&&h.isMarkerInClusterBounds(a)?h.addMarker(a):(c=new b(this),c.addMarker(a),this.clusters_.push(c))},createClusters_:function(){if(this.ready_)for(var a=new google.maps.LatLngBounds(l.Map.getBounds().getSouthWest(),
l.Map.getBounds().getNorthEast()),a=this.getExtendedBounds(a),c=this.markers_.length;c--;)!this.markers_[c].isAdded&&this.isMarkerInBounds_(this.markers_[c],a)&&this.addToClosestCluster_(this.markers_[c])}};b.prototype={constructor:b,isMarkerAlreadyAdded:function(a){return 0<=this.markers_.indexOf(a)},addMarker:function(a){if(this.isMarkerAlreadyAdded(a))return!1;if(!this.center_)this.center_=a.getPosition(),this.calculateBounds_();else if(this.averageCenter_){var c=this.markers_.length+1,b=(this.center_.lat()*
(c-1)+a.getPosition().lat())/c,c=(this.center_.lng()*(c-1)+a.getPosition().lng())/c;this.center_=new google.maps.LatLng(b,c);this.calculateBounds_()}a.isAdded=!0;this.markers_.push(a);b=this.markers_.length;b<this.minClusterSize_&&a.getMap()!=l.Map&&a.setMap(l.Map);if(b==this.minClusterSize_)for(c=0;c<b;c++)this.markers_[c].setMap(null);b>=this.minClusterSize_&&a.setMap(null);this.updateIcon();return!0},getMarkerClusterer:function(){return this.markerClusterer_},getBounds:function(){for(var a=new google.maps.LatLngBounds(this.center_,
this.center_),c=this.getMarkers(),b=c.length;b--;)a.extend(c[b].getPosition());return a},getSize:function(){return this.markers_.length},getMarkers:function(){return this.markers_},getCenter:function(){return this.center_},calculateBounds_:function(){this.bounds_=this.markerClusterer_.getExtendedBounds(new google.maps.LatLngBounds(this.center_,this.center_))},isMarkerInClusterBounds:function(a){return this.bounds_.contains(a.getPosition())},updateIcon:function(){var a=l.Map.getZoom(),c=this.markerClusterer_.getMaxZoom();
if(c&&a>c)for(a=this.markers_.length;a--;)this.markers_[a].setMap(l.Map);else this.markers_.length<this.minClusterSize_?this.clusterIcon_.hide():(this.clusterIcon_.setCenter(this.center_),this.clusterIcon_.setSums(this.markerClusterer_.getCalculator()(this.markers_,this.markerClusterer_.set_.length)),this.clusterIcon_.show())},setActive:function(){this.clusterIcon_.template_.addClass("bxmap-active")},unsetActive:function(){this.clusterIcon_.template_&&this.clusterIcon_.template_.removeClass("bxmap-active")},
select:function(){this.clusterIcon_.triggerClusterClick()},remove:function(){this.clusterIcon_.template_&&this.clusterIcon_.template_.removeClass("bxmap-active");this.clusterIcon_.remove();this.markers_.length=0;delete this.markers_}};f.prototype={constructor:f,triggerClusterClick:function(){var a=this.cluster_.getMarkerClusterer();google.maps.event.trigger(a,"clusterclick",this.cluster_);a.isZoomOnClick()&&(a=this.cluster_.getBounds(),a.getNorthEast().equals(a.getSouthWest())?this.isActive()?(l.setClusterView(),
m.trigger("cluster:detected")):(l.setClusterView(this.cluster_),m.trigger("cluster:detected",[this.cluster_.getMarkers(),a])):l.Map.fitBounds(a))},isActive:function(){return this.template_.hasClass("bxmap-active")},onAdd:function(){var a=this;this.template_=this.cluster_.getMarkerClusterer().template_.clone(!0);if(this.visible_){var c=this.cluster_.getBounds();this.createCSS(this.getPosFromLatLng_(this.center_));this.template_.css({color:this.color_?this.color_:"#fff"}).html(this.sums_.text);c.getNorthEast().equals(c.getSouthWest())&&
this.template_.addClass("bxmap-cluster-group")}$(this.getPanes().overlayMouseTarget).append(this.template_);google.maps.event.addDomListener(this.template_.get(0),"click",function(){a.triggerClusterClick()})},getPosFromLatLng_:function(a){a=this.getProjection().fromLatLngToDivPixel(a);var c=this.cluster_.markerClusterer_.set_[this.sums_.index-1];a.x+=c.offset[0];a.y+=c.offset[1];return a},draw:function(){if(this.visible_){var a=this.getPosFromLatLng_(this.center_);this.template_.css({top:a.y,left:a.x})}},
hide:function(){this.template_&&this.template_.length&&this.template_.addClass("none");this.visible_=!1},show:function(){this.template_&&this.template_.length&&(this.createCSS(this.getPosFromLatLng_(this.center_)),this.template_.removeClass("none"));this.visible_=!0},remove:function(){this.setMap(null)},onRemove:function(){this.template_&&this.template_.length&&(this.hide(),this.template_.remove(),this.template_=null)},setSums:function(a){this.sums_=a;this.text_=a.text;this.index_=a.index;this.template_&&
this.template_.length&&this.template_.html(a.text)},setCenter:function(a){this.center_=a},createCSS:function(a){this.template_.get(0).className=this.template_.get(0).className.replace(/\s*\bbxmap-cluster-count-\d/,"");this.template_.addClass("bxmap-cluster-count-"+this.sums_.index).css({top:a.y,left:a.x})}};return l}(),k=function(){var q=document.head||document.getElementsByTagName("head")[0],b=document.createElement("style"),f;q.appendChild(b);f=document.styleSheets[document.styleSheets.length-1];
var d={getType:function(b,m){var e,d="Element;Attribute;Text Node;CDATA Section;Entity Reference;Entity;Processing Instruction;Comment;Document;Document Type;Document Fragment;Notation".split(";");try{var f=/^.+?\s+([^(\s]+)/i.exec(b.constructor.toString().replace(/[\r\n]/g,"").replace(/^\[/,"").replace(/\]$/,""));if(b.nodeType)if(1==b.nodeType)if("Function"==f[1])e="Element:"+b.tagName.slice(0,1).toUpperCase()+b.tagName.slice(1).toLowerCase();else{var a=/html(.+)element/i.exec(f[f.length-1]);e="Element:"+
("Image"==a[1]?"Img":a[1])}else e=d[b.nodeType-1];else e=f[1]}catch(c){e=b}if(m){e&&e.toLowerCase&&(e=e.toLowerCase());if("string"==typeof m)return e==m.toLowerCase();if("object"==typeof m){for(d=m.length;d--;)if(e==m[d].toLowerCase())return!0;return!1}}else return e},getLang:function(){return document.documentElement.getAttribute("lang")||"ru"},getMonth:function(b,m){var e=this.getLang(),d=v.monthesName[e][b];m&&"ru"==e&&Object.keys(v.monthesRule).forEach(function(b){d=d.replace(new RegExp(v.monthesRule[b][0]+
"$"),v.monthesRule[b][1])});return d},getDay:function(b,d){var e=v[d&&1==d?"shortDaysName":"fullDaysName"][this.getLang()][b];if(d&&2==d)switch(b){case 2:case 4:case 5:Object.keys(v.dayRule).forEach(function(b){e=e.replace(new RegExp(v.dayRule[b][0]+"$"),v.dayRule[b][1])}),e=e.toLowerCase()}return e},clone:function(b,d){var e=new b.constructor;Object.keys(b).forEach(function(d){e[d]=b[d]});Object.keys(d).forEach(function(b){e[b]=d[b]});return e},insertParam:function(b,d,e){if(void 0===b)return b||
location;b=b.split("#");var f=b[0].split("?"),l=f[0];if(this.getType(d,"object")){var a=[];f[1]?(Object.keys(d).forEach(function(a){var b=a.replace(/([\[\]])/g,"\\$1"),h=(new RegExp("\\b"+b+"\\b","i")).exec(f[1]);if(h){var e=new RegExp("&?\\b"+b+"\\b=[^=&]+","ig"),b=new RegExp("(\\b"+b+"\\b=)[^=&]+","ig");void 0===d[a]||""===d[a]||null===d[a]?f[1]=f[1].replace(e,""):1<h.length?(f[1]=f[1].replace(e,""),f[1]=f[1]+"&"+a+"="+d[a]):f[1]=f[1].replace(b,"$1"+d[a])}else void 0!==d[a]&&""!==d[a]&&null!==d[a]&&
(f[1]+="&"+a+"="+d[a])}),f[1]=f[1].replace(/^&/,""),f[1].length&&(l+="?"+f[1])):(Object.keys(d).forEach(function(b){void 0!==d[b]&&""!==d[b]&&null!==d[b]&&a.push(b+"="+d[b])}),a.length&&(l+="?"+a.join("&")))}else e=e||d;e&&b[1]&&(l+="#"+b[1]);return l},createElement:function(b){var d=[],e=b.split("*");e[1]&&(d.push(A[e[1]]),b="bxmap");for(var e=$("<"+b+">"),f=1;f<arguments.length;f++)"string"==typeof arguments[f]?d=d.concat(arguments[f].split(/\s+/)):e.attr(arguments[f]);d.length&&e.addClass(d.join(" "));
return e},createRule:function(b,d,e){e||0==e||(e=f.cssRules.length);f.insertRule(b+"{"+d+"}",e)},deleteRule:function(b){function d(b){f.removeRule(b)}if(b)if(isFinite(b))b=parseInt(b),b<f.cssRules.length&&d(parseInt(b));else for(var e=0;e<f.cssRules.length;e++)this.clearText(b)==this.clearText(f.cssRules[e].selectorText)&&(d(e),e--);else $.each(f.cssRules,function(b){d(b)})},clearText:function(b){return b.replace(/[\s'"]/g,"")},setFullPath:function(b,d){/^(\/|http)/.test(b)||(b=(r.get("defaultPath")[d]+
"/").replace(/\/+$/,"/")+b);return b},loadScript:function(b){function f(a,c){"lib"==b.id&&/\biscroll\b/.test(c)&&window.IScroll&&k.resolve();u.count++;u.set[a]=e(d.setFullPath(c,"libs"),b.id,a,b.errorName)}function e(a,b,e,d){var h=document.createElement("script");h.src=a;h.async=!0;h.charset="utf-8";h.onload=h.onreadystatechange=function(a){a=a||event;if("load"===a.type||/loaded|complete/.test(h.readyState)&&(!document.documentMode||9>document.documentMode))h.onload=h.onreadystatechange=h.onerror=
null,c(b,e,d,!0)};h.onerror=function(a){h.onload=h.onreadystatechange=h.onerror=null;c(b,e,d)};q.insertBefore(h,q.lastChild);return a}function n(c,e){u.done||(u.done=!0,a(u.listener),clearTimeout(u.timing),b.timeout?(r=p.loadTime-(new Date-g),0>r?k.resolve(e):setTimeout(function(){k.resolve(e)},r)):k.resolve(e))}function l(a){try{a.preventDefault()}catch(c){a.returnValue=!1}n(b.id,b.listenerErrorName)}function a(a,b){if(b)$(window).on("error",a);else $(window).off("error",a)}function c(a,b,c,h){u.count--;
u.stop?delete u.set[b]:(h?delete u.set[b]:(u.stop=!0,n(a,c)),u.count||n(a,null))}var k=$.Deferred(),g=new Date,r,u={id:b.id,count:0,set:{}};p[b.listener]&&(u.listener=l,a(u.listener,!0));u.timing=setTimeout($.proxy(function(){n(this.id,!!this.stop&&!this.count)},u),p.responseTime);this.getType(b.address,["array","object"])?Object.keys(b.address).forEach(function(a){f("set_"+a,b.address[a])}):this.getType(b.address,"string")?f("single",b.address):k.resolve();return k.promise()},extend:function(b,f){function e(e){if(p[e])switch(e){case "icon":case "path":case "directionOptions":Object.keys(b[e]).forEach(function(f){d.extendObject(p[e][f],
b[e][f])});break;case "libs":p.libs=p.libs.concat(b.libs);break;default:k.getType(p[e],["object","array"])?d.extendObject(p[e],b[e]):p[e]=b[e]}else p[e]=b[e]}(f||Object.keys(b)).forEach(function(d){void 0!==b[d]&&e(d);delete b[d]})},extendObject:function(b,d){void 0!==d&&null!==d&&""!==d&&(k.getType(d,["object","array"])?Object.keys(d).forEach(function(e){void 0!==d[e]&&null!==d[e]&&""!==d[e]&&(k.getType(d[e],["object","array"])?(b[e]||(b[e]=new d[e].constructor),k.extendObject(b[e],d[e])):b[e]=d[e])}):
b=d);return b},extendArray:function(b,d){for(var e=[],f=[],l=0,a=0;l<d.length;l++,a=0){for(var c=0;c<b.length;c++)if(d[l]==b[c]||d[l].toString()==b[c].toString()){e.push(b.splice(c,1));a++;break}a||f.push(d[l])}return b.concat(f)},values:function(b){if(this.getType(b,"array"))return b;if(this.getType(b,"string")){if(b=b.split(/\s*;\s*/),b.length)return b}else if(this.getType(b,"object")){for(var d=[],e=Object.keys(b),f=0;f<e.length;f++)d.push(b[e[f]]);return d}return[]},delay:function(b){function d(){e.resolve(b)}
var e=$.Deferred();!b||0>=b?d():setTimeout(d,b);return e.promise()},pushState:function(b){this.state("push",b)},replaceState:function(b){this.state("replace",b)},state:function(b,d){try{history[b+"State"](d.state||d.query,d.title||document.title,this.insertParam(d.href||location.href,d.query,!d.removeHash))}catch(e){}},getQuery:function(b){var d={},e;b=b||{};b.remove=this.makeObject(b.remove);b.add=this.makeObject(b.add);e=b.url?b.url.replace(/#.+$/,"").replace(/^[^\?]+/,"").replace("?",""):location.search.replace("?",
"");if(!e)return b.add;for(var f=0,l=e.split("&");f<l.length;f++)if(e=l[f].split("="),!b.remove||b.remove&&!b.remove[e[0]])b.add&&b.add[e[0]]?(d[e[0]]=b.add[e[0]],delete b.add[e[0]]):d[e[0]]=decodeURIComponent(e[1]);if(b.add)for(l=Object.keys(b.add),f=l.length;f--;)d[l[f]]=b.add[l[f]];return d},makeObject:function(b){if(b)switch(this.getType(b)){case "String":if(b=b.split(/\W+/),!b.length)return null;case "Array":for(var d=0,e={};d<b.length;d++)e[b[d]]=!0;return e;case "Object":return b;default:return null}else return null},
getArray:function(b){return b?b.replace(/\s/g,"").split(","):[]},normalizeValue:function(b,d){switch(d){case "phone":b=b.replace(/\s+/g,"");var e=/(.*)(\(\d+\))(.+)/.exec(b);e&&(b=(e[1]?e[1]:"+7")+" "+e[2]+" "+e[3]);if((e=/(.*?)(\d+)$/.exec(b))&&2<e[2].length&&8>e[2].length){var f=[e[2].slice(-4,-2),e[2].slice(-2)];4<e[2].length&&f.unshift(e[2].slice(0,-4));b=e[1]+f.join("-")}return b;case "url":return b.replace(/^(https?:\/\/)?(www\.)?/,"");default:return b}}};return d}(),C=function(){function k(b){if(""===
d)return b;b=b.charAt(0).toUpperCase()+b.substr(1);return d+b}var b=document.documentElement.style,f={"":"transitionend",webkit:"webkitTransitionEnd",Moz:"transitionend",O:"otransitionend",ms:"MSTransitionEnd"},d=function(){for(var d=["t","webkitT","MozT","msT","OT"],f=0;f<d.length;f++)if(d[f]+"ransform"in b)return d[f].slice(0,d[f].length-1);return!1}(),h=/hp-tablet/gi.test(navigator.appVersion),m="ontouchstart"in window&&!h;return{isAndroid:/android/gi.test(navigator.appVersion),isIDevice:/iphone|ipad/gi.test(navigator.appVersion),
isTouchPad:h,cssVendor:d?"-"+d.toLowerCase()+"-":"",has3d:k("perspective")in b,hasTouch:"ontouchstart"in window&&!h,hasTransform:!1!==d,hasTransitionEnd:k("transition")in b,transform:k("transform"),transitionProperty:k("transitionProperty"),transitionDuration:k("transitionDuration"),transformOrigin:k("transformOrigin"),transitionTimingFunction:k("transitionTimingFunction"),transitionDelay:k("transitionDelay"),altEvents:{resize:"onorientationchange"in window?"orientationchange":"resize",start:m?"touchstart":
"mousedown",move:m?"touchmove":"mousemove",end:m?"touchend":"mouseup",cancel:m?"touchcancel":"mouseup",transitionEnd:!1!==d?f[d]:!1},getTranslate:function(b,d){return this.has3d?"translate3d("+b+", "+d+", 0)":"translate("+b+", "+d+")"}}}();return r}({
	mapType: 'yandex',
	loadTime: 500,
	responseTime: 10000,
	interfaceText: {
		error: 'Ошибка',
		collapsePanel: 'Свернуть',
		categoriesTitle: 'Категории',
		objectsTitle: 'Объекты',
		clearField: 'Очистить поле',
		placeHolder: 'Поиск. Например, Арбат',
		refreshMarkers: 'Обновить маркеры',
		clearCategories: 'Отменить выбор',
		closeList: 'Закрыть',
		showList: 'Список',
		back: 'Вернуться',
		showMarker: 'Показать на карте',
		route: 'Маршрут',
		walking: 'Пешком',
		transit: 'Транспорт',
		driving: 'Авто',
		bicycling: 'Велосипед',
		toWalk: 'Идти',
		toDrive: 'Ехать',
		reverseDirection: 'Сменить направление',
		createRoute: 'Проложить',
		pointsTitle: 'Объекты маршрута',
		currentPosition: 'Мое текущее положение',
		from: 'Откуда',
		to: 'Куда',
		catAbstractName: 'Группа',
		directionTitle: 'Настройка маршрута',
		multiObjects: 'Объекты по адресу',
		popular: 'Популярные',
		showMap: 'Открыть карту',
		showMapObject: 'Показать на карте',
		closeMap: 'Закрыть карту',
		groupCategoryName: 'Общая категория',
		showObjects: 'Показать все объекты по адресу',
		showCurrentObject: 'Вернуться к выбранному объекту',
		increaseZoom: 'Увеличить масштаб',
		decreaseZoom: 'Уменьшить масштаб',
		showFullScreen: 'Развернуть карту на полный экран',
		choiceMarker: 'Выбрать маркер на карте',
		choiceText: 'Переключить выбор'
	},
	routeMessages: {
		INVALID_REQUEST: 'Неверный запрос',
		MAX_WAYPOINTS_EXCEEDED: 'Слишком много промежуточных точек',
		NOT_FOUND: 'Часть координат неверно задана или невозможно распознать адрес',
		OK: 'Маршрут успешно построен',
		OVER_QUERY_LIMIT: 'Превышен лимит на количество запросов',
		REQUEST_DENIED: 'Сервис на этой странице недоступен',
		UNKNOWN_ERROR: 'Ошибка неизвестной природы',
		ZERO_RESULTS: 'Невозможно проложить маршрут',
		wait: 'Ожидается ответ на запрос...',
		blocked: 'Функция определения местоположения заблокирована. Введите адрес вручную.',
		none: 'Функция определения не поддерживается. Введите адрес вручную.'
	},
	parseMessages: {
		DEVICE_NOT_DEFINED: 'Не указан тип устройства',
		UNKNOWN_DEFAULT_PATH: 'Не указаны пути к файлам модуля',
		PAGETYPE_NOT_DEFINED: 'Не указан тип страницы',
		AJAX_NOT_DEFINED: 'Не указан путь для загрузки данных',
		MAP_PARAMETERS_NOT_DEFINED: 'Не заданы параметры карты',
		COMMON_SCRIPT_NOT_LOADED: 'Не удалось загрузить основные скрипты',
		MAP_SCRIPT_NOT_LOADED: 'Не удалось загрузить скрипты карты',
		MAP_DENIED: 'Запрет на использование карты для прокладки маршрута',
		NO_CATS: 'Нет ни одной категории',
		NO_ITEMS: 'Нет ни одного объекта',
		INDEFINED_ERRORS: 'Неопределённая ошибка',
		ERRORS_COMMON_LOADING: 'Неопределённая ошибка во время загрузки основных скриптов',
		ERRORS_MAP_LOADING: 'Неопределённая ошибка во время загрузки скриптов карты'
	},
	cluster: {
		gridSize: 32,
		anchor: [0, 0],
		icon: 'cluster.png',
		color: '#fff',
		set: [
			{size: 50},
			{size: 60},
			{size: 74},
			{size: 90}
		]
	},
	libs: [
		'iscroll.js'
	],
	mapScript: {
		google: {
			main: '//maps.googleapis.com/maps/api/js?sensor=true&language=ru'
		},
		yandex: {
			main: '//api-maps.yandex.ru/2.1/?lang=ru'
		}
	}
},
{
	div: 'alt-block',
	ol: 'alt-ordered-list',
	ul: 'alt-unordered-list',
	li: 'alt-list-item',
	dl: 'alt-definition-list',
	dt: 'alt-definition-title',
	dd: 'alt-definition-description',
	span: 'alt-inline',
	ispan: 'alt-inline-block'
},
{
	monthesName: {
		ru: [
			'Январь',
			'Февраль',
			'Март',
			'Апрель',
			'Май',
			'Июнь',
			'Июль',
			'Август',
			'Сентябрь',
			'Октябрь',
			'Ноябрь',
			'Декабрь'
		],
		en: [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		],
		fr: [
			'Janvier',
			'Février',
			'Mars',
			'Avril',
			'Mai',
			'Juin',
			'Juillet',
			'Août',
			'Septembre',
			'Octobre',
			'Novembre',
			'Décembre'
		]
	},
	fullDaysName: {
		ru: [
			'Понедельник',
			'Вторник',
			'Среда',
			'Четверг',
			'Пятница',
			'Суббота',
			'Воскресенье'
		],
		en: [
			'Monday',
			'Tuesday',
			'Wednesday',
			'Thursday',
			'Friday',
			'Saturday',
			'Sunday'
		],
		fr: [
			'Lundi',
			'Mardi',
			'Mercredi',
			'Jeudi',
			'Vendredi',
			'Samedi',
			'Dimanche'
		]
	},
	shortDaysName: {
		ru: [
			'Пн',
			'Вт',
			'Ср',
			'Чт',
			'Пт',
			'Сб',
			'Вс'
		],
		en: [
			'Mon',
			'Tue',
			'Wed',
			'Thu',
			'Fri',
			'Sat',
			'Sun'
		],
		fr: [
			'Lun',
			'Mar',
			'Mer',
			'Jeu',
			'Ven',
			'Sam',
			'Dim'
		]
	},
	monthesRule: [
		['(ь|й)', 'я'],
		['т', 'та']
	],
	dayRule: [
		['а', 'у']
	]
});