<?
if(!check_bitrix_sessid() || !CModule::IncludeModule("iblock"))
    return;

global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/step.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install/step.php"));

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/wizard_util.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/wizard.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/install/wizard_sol/utils.php");

Class CMapModuleInstall
{
    private $PublicDir = '#SITE_DIR#map/';

    private $ModuleIblockType = 'bx_map';
    private $ModuleIblockObjectsCode = 'bx_map_objects';
    private $ModuleIblockEventsCode = 'bx_map_events';
    private $ModuleIblockRoutesCode = 'bx_map_routes';
    private $SITE_ID = false;
    private $REWRITE = true;
    private $IO = false;
    private $DOCUMENT_ROOT = false;

    private $arResult = array(
        "SETTINGS" => array(),
        "INSTALLATION" => array(
            "SITE" => array(),
        ),
        "ERRORS" => array(),
    );

    public function __construct($arParams)
    {
        $this->SITE_ID = $arParams["SITE_ID"];
        $this->REWRITE = $arParams["REWRITE"];
        $this->PublicDir = $arParams["PUBLIC_DIR"];

        //NULL CACHE
        BXClearCache(True, '/'.$this->SITE_ID.'/map/');
        BXClearCache(True, '/'.SITE_ID.'/map/');

        //Lang List
        $l = CLanguage::GetList($by="sort", $order="asc");
        while($r = $l->Fetch())
            $this->arResult["SETTINGS"]["LANG"][] = $r;

        //Sites List
        $oSites = CSite::GetList(($b = ""), ($o = ""), Array("ACTIVE" => "Y"));
        while ($site = $oSites->Fetch())
            $this->arResult["SETTINGS"]["SITE"][$site["LID"]] = Array(
                "LANGUAGE_ID" => $site["LANGUAGE_ID"],
                "ABS_DOC_ROOT" => $site["ABS_DOC_ROOT"],
                "DIR" => $site["DIR"],
                "SITE_ID" => $site["LID"],
                "SERVER_NAME" =>$site["SERVER_NAME"],
                "NAME" => $site["NAME"],
            );

        if(array_key_exists($this->SITE_ID, $this->arResult["SETTINGS"]["SITE"]))
            $this->PublicDir = str_replace("//", "/", $this->arResult["SETTINGS"]["SITE"][$this->SITE_ID]["DIR"] . $this->PublicDir);

        $site = CFileMan::__CheckSite($this->SITE_ID);
        $this->DOCUMENT_ROOT = CSite::GetSiteDocRoot($site);
        $this->IO = CBXVirtualIo::GetInstance();

        //SetDefault
        $this->arResult["INSTALLATION"]["IBLOCK_TYPE_INSTALL"] = true;
        $this->arResult["INSTALLATION"]["OBJECTS_IBLOCK_INSTALL"] = true;
        $this->arResult["INSTALLATION"]["EVENTS_IBLOCK_INSTALL"]  = true;
        $this->arResult["INSTALLATION"]["ROUTES_IBLOCK_INSTALL"]  = true;

        $this->CheckParams();
    }

    public function CheckPrevInstallation()
    {
        if(!$this->HaveError())
        {
            $this->CheckIblockType();
            $this->CheckIblock();
        }
    }

    public function HaveError()
    {
        return !empty($this->arResult["ERRORS"]);
    }

    public function GetError()
    {
        return $this->arResult["ERRORS"];
    }

    private function CheckParams()
    {
        global $APPLICATION;

        if(empty($this->arResult["SETTINGS"]["SITE"]) || !in_array($this->SITE_ID, array_keys($this->arResult["SETTINGS"]["SITE"])))
            $this->arResult["ERRORS"][] = GetMessage("MMAP_INSTALL_ERROR_SITE_NOT_FOUND");
        if($APPLICATION->GetGroupRight("iblock") < "W")
            $this->arResult["ERRORS"][] = GetMessage("MMAP_INSTALL_ERROR_IBLOCK_NO_PERMISSTION");
    }

    private function CheckIblockType()
    {
        if($arIblockType = CIBlockType::GetByID($this->ModuleIblockType)->Fetch()) //IBType already exists
        {
            $this->arResult["INSTALLATION"]["IBLOCK_TYPE_INSTALL"] = false;
            $this->arResult["INSTALLATION"]["IBLOCK_TYPE_ID"] = $this->ModuleIblockType;
        }
    }

    private function CheckIblock()
    {
        if($arIblock = CIblock::GetList(array(), array("CODE" => $this->ModuleIblockObjectsCode))->Fetch())
        {
            $this->arResult["INSTALLATION"]["OBJECTS_IBLOCK_INSTALL"] = false;
            $this->arResult["INSTALLATION"]["OBJECTS_IBLOCK_ID"] = $arIblock["ID"];
        }

        if($arIblock = CIblock::GetList(array(), array("CODE" => $this->ModuleIblockEventsCode))->Fetch())
        {
            $this->arResult["INSTALLATION"]["EVENTS_IBLOCK_INSTALL"] = false;
            $this->arResult["INSTALLATION"]["EVENTS_IBLOCK_ID"] = $arIblock["ID"];
        }

        if($arIblock = CIblock::GetList(array(), array("CODE" => $this->ModuleIblockRoutesCode))->Fetch())
        {
            $this->arResult["INSTALLATION"]["ROUTES_IBLOCK_INSTALL"] = false;
            $this->arResult["INSTALLATION"]["ROUTES_IBLOCK_ID"] = $arIblock["ID"];
        }
    }

    public function Install()
    {
        if(!$this->HaveError())
            $this->InstallIblockType();
        if(!$this->HaveError())
            $this->InstallIblock();
        if(!$this->HaveError())
            $this->CopyPublucFiles();
    }

    private function InstallIblockType()
    {
        if($this->arResult["INSTALLATION"]["IBLOCK_TYPE_INSTALL"])
        {
            $arFields = array(
                "ID" => $this->ModuleIblockType,
                "SECTIONS" => "Y",
                "IN_RSS" => "N",
                "SORT" => 100,
                "LANG" => array()
            );

            foreach($this->arResult["SETTINGS"]["LANG"] as $Lang)
            {
                $arFields["LANG"][$Lang["LANGUAGE_ID"]] = array(
                    "NAME" => strlen(GetMessage("MMAP_INSTALL_IBLOCK_TYPE_LANG_".ToUpper($Lang["LANGUAGE_ID"])))==0
                            ?GetMessage("MMAP_INSTALL_IBLOCK_TYPE_LANG_DEFAULT")
                            :GetMessage("MMAP_INSTALL_IBLOCK_TYPE_LANG_".ToUpper($Lang["LANGUAGE_ID"])),
                    "SECTION_NAME" => strlen(GetMessage("MMAP_INSTALL_IBLOCK_SECTION_NAME_LANG_".ToUpper($Lang["LANGUAGE_ID"])))==0
                            ?GetMessage("MMAP_INSTALL_IBLOCK_SECTION_NAME_LANG_DEFAULT")
                            :GetMessage("MMAP_INSTALL_IBLOCK_SECTION_NAME_LANG_".ToUpper($Lang["LANGUAGE_ID"])),
                );
            }

            $IBT = new CIBlockType();
            if(!$IblockTypeId = $IBT->Add($arFields))
                $this->arResult["ERRORS"][] = $IBT->LAST_ERROR;

            $this->arResult["INSTALLATION"]["IBLOCK_TYPE_ID"] = $IblockTypeId;
        }
    }

    private function InstallIblock()
    {
        if ($this->arResult["INSTALLATION"]["OBJECTS_IBLOCK_INSTALL"])
        {
            $this->arResult["INSTALLATION"]["OBJECTS_IBLOCK_ID"] = WizardServices::ImportIBlockFromXML($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/bitrix.map/install/xml/objects.xml", $this->ModuleIblockObjectsCode, $this->ModuleIblockType, $this->SITE_ID);
			
			$dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => 'IBLOCK_'.$this->arResult["INSTALLATION"]["OBJECTS_IBLOCK_ID"].'_SECTION', "FIELD_NAME" => 'UF_ICON_POS'));
			if ($arProp = $dbRes->Fetch()) {
				$propID = $arProp['ID'];
				$arProp["EDIT_FORM_LABEL"] = array('ru' => GetMessage('MMAP_UF_ICON_POS_text'), 'en' => 'Icon position');
				$arProp["LIST_COLUMN_LABEL"] = array('ru' => GetMessage('MMAP_UF_ICON_POS_text'), 'en' => 'Icon position');
				$arProp["LIST_FILTER_LABEL"] = array('ru' => GetMessage('MMAP_UF_ICON_POS_text'), 'en' => 'Icon position');
				unset ($arProp['ID']);
				$userType = new CUserTypeEntity();
				$success = (bool)$userType->Update($propID, $arProp);
			}
        }
        if ($this->arResult["INSTALLATION"]["EVENTS_IBLOCK_INSTALL"])
        {
            $this->arResult["INSTALLATION"]["EVENTS_IBLOCK_ID"] = WizardServices::ImportIBlockFromXML($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/bitrix.map/install/xml/events.xml", $this->ModuleIblockEventsCode, $this->ModuleIblockType, $this->SITE_ID);
        }
        if ($this->arResult["INSTALLATION"]["ROUTES_IBLOCK_INSTALL"])
        {
            $this->arResult["INSTALLATION"]["ROUTES_IBLOCK_ID"] = WizardServices::ImportIBlockFromXML($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/bitrix.map/install/xml/routes.xml", $this->ModuleIblockRoutesCode, $this->ModuleIblockType, $this->SITE_ID);
			
			$dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => 'IBLOCK_'.$this->arResult["INSTALLATION"]["ROUTES_IBLOCK_ID"].'_SECTION', "FIELD_NAME" => 'UF_CLOSED'));
			if ($arProp = $dbRes->Fetch()) {
				$propID = $arProp['ID'];
				$arProp["EDIT_FORM_LABEL"] = array('ru' => GetMessage('MMAP_UF_CLOSED_text'), 'en' => 'Closed route');
				$arProp["LIST_COLUMN_LABEL"] = array('ru' => GetMessage('MMAP_UF_CLOSED_text'), 'en' => 'Closed route');
				$arProp["LIST_FILTER_LABEL"] = array('ru' => GetMessage('MMAP_UF_CLOSED_text'), 'en' => 'Closed route');
				unset ($arProp['ID']);
				$userType = new CUserTypeEntity();
				$success = (bool)$userType->Update($propID, $arProp);
			}
			$propID = 0;
			$dbRes = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => 'IBLOCK_'.$this->arResult["INSTALLATION"]["ROUTES_IBLOCK_ID"].'_SECTION', "FIELD_NAME" => 'UF_ROUTE_TYPE'));
			if ($arProp = $dbRes->Fetch()) {
				$propID = $arProp['ID'];
				$arProp["EDIT_FORM_LABEL"] = array('ru' => GetMessage('MMAP_UF_ROUTE_TYPE_text'), 'en' => 'Route type');
				$arProp["LIST_COLUMN_LABEL"] = array('ru' => GetMessage('MMAP_UF_ROUTE_TYPE_text'), 'en' => 'Route type');
				$arProp["LIST_FILTER_LABEL"] = array('ru' => GetMessage('MMAP_UF_ROUTE_TYPE_text'), 'en' => 'Route type');
				unset ($arProp['ID']);
				$userType = new CUserTypeEntity();
				$success = (bool)$userType->Update($propID, $arProp);
			}
			if ($propID>0) {
				$obEnum = new CUserFieldEnum;
				$LIST = array(
					'n0' => array(
						'XML_ID' => '0',
						'VALUE' => GetMessage('MMAP_UF_ROUTE_TYPE_1'),
						'SORT' => '10',
						'DEF' => 'Y',
					),
					'n1' => array(
						'XML_ID' => '30',
						'VALUE' => GetMessage('MMAP_UF_ROUTE_TYPE_2'),
						'SORT' => '20',
						'DEF' => 'N',
					),
					'n2' => array(
						'XML_ID' => '60',
						'VALUE' => GetMessage('MMAP_UF_ROUTE_TYPE_3'),
						'SORT' => '30',
						'DEF' => 'N',
					),
				);
				$success = $obEnum->SetEnumValues($propID, $LIST);
				if ($success) {
					$arTypes = array();
					$rsTypes = CUserFieldEnum::GetList(array(),array('USER_FIELD_ID'=>$propID));
					while ($arType = $rsTypes->GetNext())
						$arTypes[$arType['XML_ID']] = $arType['ID'];
					$rsSection = CIBlockSection::GetList(array(),array('IBLOCK_ID'=>$this->arResult["INSTALLATION"]["ROUTES_IBLOCK_ID"]),false,array('ID','NAME','CODE','SORT','UF_*'));
					while ($arSection = $rsSection->GetNext()) {
						$arFields = array(
							'NAME' => $arSection['NAME'],
							'CODE' => $arSection['CODE'],
							'SORT' => $arSection['SORT'],
							'UF_CLOSED' => $arSection['UF_CLOSED'],
							'UF_ROUTE_TYPE' => $arTypes[$arSection['CODE']],
						);
						$sec = new CIBlockSection();
						$sec->Update($arSection['ID'],$arFields);
					}
				}
			}
        }
    }

    private function CopyPublucFiles()
    {
        $target = $this->DOCUMENT_ROOT.$this->PublicDir;
        $source = $_SERVER['DOCUMENT_ROOT']."/bitrix/modules/bitrix.map/install/public/lang/ru/";

        CopyDirFiles($source, $target, $this->REWRITE, true);
        if(file_exists($target.'index.php'))
        {
            CWizardUtil::ReplaceMacros($target.'index.php',        array("IBLOCK_ID" => $this->arResult["INSTALLATION"]["OBJECTS_IBLOCK_ID"]));
            CWizardUtil::ReplaceMacros($target.'routes/index.php', array("IBLOCK_ID" => $this->arResult["INSTALLATION"]["ROUTES_IBLOCK_ID"]));
            CWizardUtil::ReplaceMacros($target.'events/index.php', array("IBLOCK_ID" => $this->arResult["INSTALLATION"]["EVENTS_IBLOCK_ID"]));
        }
    }
}

if(strlen($_REQUEST["map_install"]) > 0)
{
    if($_REQUEST["demo"])
    {
        $CMapInstaller = new CMapModuleInstall(
            array(
                "SITE_ID" => $_REQUEST["site_id"],
                "REWRITE" => $_REQUEST["file_rewrite"] == "Y",
                "PUBLIC_DIR" => $_REQUEST["public_dir"],
            )
        );
        $CMapInstaller->CheckPrevInstallation();
        if(!$CMapInstaller->HaveError())
            $CMapInstaller->Install();

        if($CMapInstaller->HaveError())
        {
            echo CAdminMessage::ShowMessage(Array("TYPE"=>"ERROR", "MESSAGE" =>GetMessage("MOD_INST_ERR"), "DETAILS"=> join('<br/>',$CMapInstaller->GetError()), "HTML"=>true));
        }
        else
        {
            LocalRedirect('/bitrix/admin/partner_modules.php?step=2&lang='.LANGUAGE_ID.'&id=bitrix.map&install=Y&'.bitrix_sessid_get());
        }
    }
    else
    {
        LocalRedirect('/bitrix/admin/partner_modules.php?step=2&lang='.LANGUAGE_ID.'&id=bitrix.map&install=Y&'.bitrix_sessid_get());
    }
}

global $obModule;

//Sites List
$arSite = array();
$oSites = CSite::GetList(($b = ""), ($o = ""), Array("ACTIVE" => "Y"));
while ($site = $oSites->Fetch())
    $arSite[] = Array(
        "LANGUAGE_ID" => $site["LANGUAGE_ID"],
        "ABS_DOC_ROOT" => $site["ABS_DOC_ROOT"],
        "DIR" => $site["DIR"],
        "SITE_ID" => $site["LID"],
        "SERVER_NAME" =>$site["SERVER_NAME"],
        "NAME" => $site["NAME"],
    );
?>
<script language="JavaScript">
    <!--
    CJSMapStep1 = {
        DemoInstallType: function()
        {
            if(BX('demo').checked)
            {
                BX('map_file_rewrite_block').style.display =
                    BX('map_public_dir_block').style.display =
                        BX('map_site_id_block').style.display = '';
            }
            else
            {
                BX('map_file_rewrite_block').style.display =
                    BX('map_public_dir_block').style.display =
                        BX('map_site_id_block').style.display = 'none';
            }
        },
        DemoInstallStart: function()
        {
            var installBtn = BX('map_btn');
            installBtn.disabled = true;
            installBtn.value = '<?=GetMessage("MMAP_INSTALL_WAIT")?>';
        }
    }
    //-->
</script>
<form action="<?=$APPLICATION->GetCurPage()?>" name="form1" onsubmit="CJSMapStep1.DemoInstallStart();">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="lang" value="<?echo LANG?>">
    <input type="hidden" name="id" value="bitrix.map">
    <input type="hidden" name="install" value="Y">
    <input type="hidden" name="map_install" value="Y">
    <input type="hidden" name="step" value="1">

    <table class="list-table">
        <tbody>
            <tr class="head">
                <td colspan="2">
                    <input type="checkbox" onclick="CJSMapStep1.DemoInstallType()" value="Y" id="demo" name="demo"><label for="demo"> <?=GetMessage("MMAP_INSTALL_DEMO_SIMPLE")?></label>
                </td>
            </tr>
            <tr id="map_site_id_block" style="display: none;">
                <td width="10%" style="white-space:nowrap;"><span class="required">*</span> <?=GetMessage("MMAP_INSTALL_SITE_ID")?>:</td>
                <td width="90%">
                    <select name="site_id">
                        <?foreach($arSite as $Site):?>
                            <option value="<?=$Site["SITE_ID"]?>"><?=$Site["NAME"]?></option>
                        <?endforeach;?>
                    </select>
                </td>
            </tr>
            <tr id="map_public_dir_block" style="display: none;">
                <td width="10%" style="white-space:nowrap;"><span class="required">*</span> <?=GetMessage("MMAP_INSTALL_PUBLIC_DIR")?>:</td>
                <td width="90%">
                    <input name="public_dir" value="map/" />
                </td>
            </tr>
            <tr id="map_file_rewrite_block" style="display: none;">
                <td width="10%" style="white-space:nowrap;"><?=GetMessage("MMAP_INSTALL_FILE_REWRITE")?>:</td>
                <td width="90%"><input type="checkbox" value="Y" name="file_rewrite" checked></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="<?=GetMessage("MOD_INSTALL")?>" name="map_btn" id="map_btn"></td>
            </tr>
        </tbody>
    </table>
</form>