<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
    return;

global $USER_FIELD_MANAGER;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>(isset($arCurrentValues["IBLOCK_ID"])?$arCurrentValues["IBLOCK_ID"]:$arCurrentValues["ID"])));
while ($arr=$rsProp->Fetch())
{
    $arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
    if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S")))
    {
        $arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
    }
}

$arComponentParameters = array(
    "GROUPS" => array(
        "MAP_SETTINGS" => array(
            "NAME" => GetMessage("T_MAP_SECTION_MAP_SETTINGS"),
            "SORT" => 1000
        ),
        "MAP_EL_PROPERTIES" => array(
            "NAME" => GetMessage("T_MAP_SECTION_MAP_EL_PROPERTIES"),
            "SORT" => 1100
        ),
        "MAP_EL_GEOLOC" => array(
            "NAME" => GetMessage("T_MAP_SECTION_MAP_EL_GEOLOC"),
            "SORT" => 1200
        ),
    ),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_MAP_DESC_LIST_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "DEFAULT" => "map",
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_MAP_DESC_LIST_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => '={$_REQUEST["ID"]}',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),
        "DATA_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_MAP_DESC_DATA_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => array(
                "objects" => GetMessage("T_MAP_DESC_DATA_TYPE_OBJECTS"),
                "events" => GetMessage("T_MAP_DESC_DATA_TYPE_EVENTS"),
                "routes" => GetMessage("T_MAP_DESC_DATA_TYPE_ROUTES"),
            ),
            "DEFAULT" => "objects",
            "REFRESH" => "Y"
        ),
        "OBJECT_ID" => Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_MAP_DESC_LIST_OBJECT_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => '={$_REQUEST["item"]}',
        ),
        "SET_TITLE" => Array(),
        "SET_STATUS_404" => Array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("CP_BNL_SET_STATUS_404"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "INCLUDE_IBLOCK_INTO_CHAIN" => Array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("T_MAP_DESC_INCLUDE_IBLOCK_INTO_CHAIN"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
        "CACHE_GROUPS" => array(
            "PARENT" => "CACHE_SETTINGS",
            "NAME" => GetMessage("CP_BNL_CACHE_GROUPS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),

        "MAP_TYPE" => array(
            "PARENT" => "MAP_SETTINGS",
            "NAME" => GetMessage("T_MAP_DESC_MAP_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => array(
                "google" => GetMessage("T_MAP_DESC_MAP_TYPE_GOOLGE"),
                "yandex" => GetMessage("T_MAP_DESC_MAP_TYPE_YANDEX"),
            ),
            "DEFAULT" => "google",
        ),
        "LATITUDE_PROP_CODE" => array(
            "PARENT" => "MAP_EL_PROPERTIES",
            "NAME" => GetMessage("T_MAP_DESC_LATITUDE_PROP_CODE"),
            "TYPE" => "LIST",
            "DEFAULT" => "LAT",
            "VALUES" => $arProperty_LNS,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "LONGITUDE_PROP_CODE" => array(
            "PARENT" => "MAP_EL_PROPERTIES",
            "NAME" => GetMessage("T_MAP_DESC_LONGITUDE_PROP_CODE"),
            "TYPE" => "LIST",
            "DEFAULT" => "LNG",
            "VALUES" => $arProperty_LNS,
            "ADDITIONAL_VALUES" => "Y",
        ),
        "GEOLOC_LATITUDE" => Array(
            "PARENT" => "MAP_EL_GEOLOC",
            "NAME" => GetMessage("T_MAP_DESC_LIST_GEOLOC_LATITUDE"),
            "TYPE" => "STRING",
            "DEFAULT" => '',
        ),
        "GEOLOC_LONGITUDE" => Array(
            "PARENT" => "MAP_EL_GEOLOC",
            "NAME" => GetMessage("T_MAP_DESC_LIST_GEOLOC_LONGITUDE"),
            "TYPE" => "STRING",
            "DEFAULT" => '',
        ),
    ),
);
?>