<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
Bitrix\Main\Loader::includeModule("bitrix.map");

Bitrix\InteractiveMap\DataMixer::IncludeMapScripts("mobileapp");
$APPLICATION->SetAdditionalCss('/bitrix/components/bitrix/map.map/templates/mobileapp/style.css');
?>