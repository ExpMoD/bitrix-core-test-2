<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<? if (strlen($arResult["ERROR"]) > 0) { ?>
    <? ShowError($arResult["ERROR"]); ?>
<? } else { ?>
    <script>
        $GeoMapp.init({
            defaultPath: <?=$arResult["PARAMS"]["DEFAULT_PATHS"]?>,
            mapType: '<?=$arParams["MAP_TYPE"]?>',
            pageType: 'direction',
            loadTime: <?=$arResult["PARAMS"]["LOAD_TIME"]?>,
            responseTime: <?=$arResult["PARAMS"]["RESPONSE_TIME"]?>,
            barHeight: 0,
            plateHeight: <?=$arParams["PLATE_HEIGHT"]?>,
            routeType: <?=$arResult["PARAMS"]["ROUTE_TYPES"]?>,
            <? foreach ($arResult["PARAMS"]["ICONS"] as $strVarName => $strData) { ?>
            <?=$strVarName?>: <?=$strData?>,
            <? } ?>
            <? if (!empty($arParams["GEOLOC_LATITUDE"]) && !empty($arParams["GEOLOC_LONGITUDE"])) { ?>
            geolocation: {
                lat: <?=$arParams["GEOLOC_LATITUDE"]?>,
                lng: <?=$arParams["GEOLOC_LONGITUDE"]?>
                },
            <? } ?>
            interfaceText: <?=$arResult["INTERFACE"]["main"]?>,
            routeMessages: <?=$arResult["INTERFACE"]["routes"]?>,
            parseMessages: <?=$arResult["INTERFACE"]["errors"]?>,
            cats: <?=$arResult["JSON_SECTIONS"]?>,
            items: <?=$arResult["JSON_ELEMENTS"]?>
        });
    </script>

    <div id="bxMapContainer" class="map-wrapper"></div>
<? } ?>