<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("T_MAP_DESC_LIST"),
    "DESCRIPTION" => GetMessage("T_MAP_DESC_LIST_DESC"),
    "ICON" => "/images/routing.gif",
    "SORT" => 30,
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "ID" => "map",
            "NAME" => GetMessage("T_MAP_DESC_NEWS"),
            "SORT" => 10,
            "CHILD" => array(
                "ID" => "map_cmpx",
            ),
        ),
    ),
);

?>