<?
$MESS["T_MAP_DESC_LIST_ID"] = "Код информационного блока";
$MESS["T_MAP_DESC_LIST_TYPE"] = "Тип информационного блока (используется только для проверки)";
$MESS["T_MAP_DESC_INCLUDE_IBLOCK_INTO_CHAIN"] = "Включать инфоблок в цепочку навигации";
$MESS["T_MAP_DESC_ADD_SECTIONS_CHAIN"] = "Включать раздел в цепочку навигации";
$MESS["CP_BNL_SET_STATUS_404"] = "Устанавливать статус 404, если не найдены элемент или раздел";
$MESS["CP_BNL_CACHE_GROUPS"] = "Учитывать права доступа";

$MESS["T_MAP_SECTION_MAP_SETTINGS"] = "Настройка карты";
$MESS["T_MAP_SECTION_MAP_EL_PROPERTIES"] = "Свойства элементов карты";
$MESS["T_MAP_SECTION_MAP_EL_GEOLOC"] = "Всегда прокладывать маршрут от точки";

$MESS["T_MAP_DESC_MAP_TYPE"] = "Тип карты";
$MESS["T_MAP_DESC_MAP_TYPE_YANDEX"] = "Яндекс.Карты";
$MESS["T_MAP_DESC_MAP_TYPE_GOOLGE"] = "Google Maps";
$MESS["T_MAP_DESC_LATITUDE_PROP_CODE"] = "Широта";
$MESS["T_MAP_DESC_LONGITUDE_PROP_CODE"] = "Долгота";
$MESS["T_MAP_DESC_LIST_OBJECT_ID"] = "ID объекта";

$MESS["T_MAP_DESC_LIST_GEOLOC_LATITUDE"] = "Широта";
$MESS["T_MAP_DESC_LIST_GEOLOC_LONGITUDE"] = "Долгота";

$MESS["T_MAP_DESC_DATA_TYPE"] = "Представление данных";
$MESS["T_MAP_DESC_DATA_TYPE_OBJECTS"] = "Объекты";
$MESS["T_MAP_DESC_DATA_TYPE_EVENTS"] = "События";
$MESS["T_MAP_DESC_DATA_TYPE_ROUTES"] = "Маршруты";
?>