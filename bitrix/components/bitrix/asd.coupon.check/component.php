<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('catalog')) {
	ShowError(GetMessage('ASD_CMP_NOT_SERVICE'));
	return;
}

if ($arParams['NOT_CAPTCHA'] == 'Y') {
	$arParams['USE_CAPTCHA'] = 'N';
} else {
	$arParams['USE_CAPTCHA'] = !$USER->IsAuthorized() ? 'Y' : 'N';
}

if (!isset($arParams['NOT_SHOW_RESTR']) || !is_array($arParams['NOT_SHOW_RESTR'])) {
	$arParams['NOT_SHOW_RESTR'] = array();
}

$arResult = array();

if (strlen(trim($_REQUEST['COUPON']))) {
	$arResult['STR_ERROR'] = '';
	$arResult['RESULT'] = '';
	if ($arParams['USE_CAPTCHA'] == 'Y') {
		include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/captcha.php');
		$captcha_code = $_POST['captcha_sid'];
		$captcha_word = $_POST['captcha_word'];
		$cpt = new CCaptcha();
		$captchaPass = COption::GetOptionString('main', 'captcha_password', '');
		if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0) {
			if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass)) {
				$arResult['STR_ERROR'] = GetMessage('ASD_CMP_CAPTCHA_WRONG');
			}
		} else {
			$arResult['STR_ERROR'] = GetMessage('ASD_CMP_CAPTHCA_EMPTY');
		}
	}

	if (!strlen($arResult['STR_ERROR'])) {
		if ($arCoupon = CCatalogDiscountCoupon::GetList(array(), array('COUPON' => trim($_REQUEST['COUPON'])))->Fetch()) {
			$arResult['RESULT'] = 'OK';
			$arResult['COUPON'] = $arCoupon;
			$arResult['DISCOUNT'] = CCatalogDiscount::GetByID($arCoupon['DISCOUNT_ID']);
			if ($arResult['DISCOUNT']['SITE_ID'] == SITE_ID) {
				if ($arResult['DISCOUNT']['ACTIVE'] == 'Y') {
					if (strlen($arResult['DISCOUNT']['ACTIVE_FROM']) && MakeTimeStamp($arResult['DISCOUNT']['ACTIVE_FROM']) > time()) {
						$arResult['DISCOUNT']['ACTIVE'] = 'N';
					}
				}
				if ($arResult['DISCOUNT']['ACTIVE'] == 'Y') {
					if (strlen($arResult['DISCOUNT']['ACTIVE_TO']) && MakeTimeStamp($arResult['DISCOUNT']['ACTIVE_TO']) < time()) {
						$arResult['DISCOUNT']['ACTIVE'] = 'N';
					}
				}
				$arResult['DISCOUNT']['ACTIVE_FROM'] = str_replace('00:00:00', '', $arResult['DISCOUNT']['ACTIVE_FROM']);
				$arResult['DISCOUNT']['ACTIVE_TO'] = str_replace('00:00:00', '', $arResult['DISCOUNT']['ACTIVE_TO']);
				$arResult['DISCOUNT']['VALUE'] = round($arResult['DISCOUNT']['VALUE'], 2);
				$arResult['DISCOUNT']['USER_GROUPS'] = array();
				$arResult['DISCOUNT']['SECTIONS'] = array();
				$arResult['DISCOUNT']['PRODUCTS'] = array();
				if ($arResult['DISCOUNT']['VALUE_TYPE'] == 'F') {
					$arResult['DISCOUNT']['VALUE_FORMATED'] = FormatCurrency($arResult['DISCOUNT']['VALUE'], $arResult['DISCOUNT']['CURRENCY']);
				}
				if (!in_array('USER_GROUPS', $arParams['NOT_SHOW_RESTR'])) {
					$arUsersGroups = array();
					$rsGroups = CCatalogDiscount::GetDiscountGroupsList(array(), array('DISCOUNT_ID' => $arResult['DISCOUNT']['ID']));
					while ($arGroups = $rsGroups->Fetch()) {
						if (empty($arUsersGroups)) {
							$rsGroup = CGroup::GetList($by = 'sort', $order = 'asc');
							while ($arGroup = $rsGroup->GetNext()) {
								$arUsersGroups[$arGroup['ID']] = $arGroup;
							}
						}
						$arGroups['GROUP'] = $arUsersGroups[$arGroups['ID']];
						$arResult['DISCOUNT']['USER_GROUPS'][] = $arGroups['GROUP'];
					}
				}

				if (!in_array('SECTIONS', $arParams['NOT_SHOW_RESTR'])) {
					$arSections = array();
					$rsSects = CCatalogDiscount::GetDiscountSectionsList(array(), array('DISCOUNT_ID' => $arResult['DISCOUNT']['ID']));
					while ($arSects = $rsSects->Fetch()) {
						$arSections[] = $arSects['SECTION_ID'];
					}
					if (!empty($arSections)) {
						$rsSects = CIBlockSection::GetList(array(), array('ID' => $arSections));
						while ($arSects = $rsSects->GetNext()) {
							$arResult['DISCOUNT']['SECTIONS'][] = $arSects;
						}
					}
				}

				if (!in_array('PRODUCTS', $arParams['NOT_SHOW_RESTR'])) {
					$arProducts = array();
					$rsProducts = CCatalogDiscount::GetDiscountProductsList(array(), array('DISCOUNT_ID' => $arResult['DISCOUNT']['ID']));
					while ($arProduct = $rsProducts->Fetch()) {
						$arProducts[] = $arProduct['PRODUCT_ID'];
					}
					if (!empty($arProducts)) {
						$rsProduct = CIBlockElement::GetList(array(), array('ID' => $arProducts));
						while ($arProduct = $rsProduct->GetNext()) {
							$arResult['DISCOUNT']['PRODUCTS'][] = $arProduct;
						}
					}
				}
			} else {
				$arResult['STR_ERROR'] = GetMessage('ASD_NOT_FOUND');
			}
		} else {
			$arResult['STR_ERROR'] = GetMessage('ASD_NOT_FOUND');
		}
	}
}

if (empty($arResult['COUPON'])) {
	$arResult['COUPON'] = array('COUPON' => htmlspecialchars(trim($_REQUEST['COUPON'])));
}

if ($arParams['USE_CAPTCHA'] == 'Y') {
	$arResult['capCode'] = htmlspecialchars($APPLICATION->CaptchaGetCode());
}

$this->IncludeComponentTemplate();