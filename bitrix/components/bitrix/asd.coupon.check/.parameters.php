<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"NOT_CAPTCHA" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("ASD_CMP_PARAM_NOT_CAPTCHA"),
			"TYPE" => "CHECKBOX",
		),
		"NOT_SHOW_RESTR" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("ASD_CMP_PARAM_NOT_SHOW_RESTR"),
			"TYPE" => "LIST",
			"VALUES" => array(
								"" => GetMessage("ASD_CMP_PARAM_NOT_SHOW_RESTR_NO"),
								"USER_GROUPS" => GetMessage("ASD_CMP_PARAM_NOT_SHOW_RESTR_USER_GROUPS"),
								"SECTIONS" => GetMessage("ASD_CMP_PARAM_NOT_SHOW_RESTR_SECTIONS"),
								"PRODUCTS" => GetMessage("ASD_CMP_PARAM_NOT_SHOW_RESTR_PRODUCTS"),
							),
			"MULTIPLE" => "Y",
		),
	),
);
?>