<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<p><?= GetMessage('ASD_TPL_NOTE')?></p>
<form method="post" action="<?=POST_FORM_ACTION_URI?>">
	<input type="text" name="COUPON" size="30" value="<?= $arResult['COUPON']['COUPON']?>" />

	<?if($arParams['USE_CAPTCHA'] == 'Y'):?>
	<br/>
	<input type="hidden" name="captcha_sid" value="<?=$arResult['capCode']?>" />
	<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult['capCode']?>" width="180" height="40" alt="CAPTCHA" />
	<br/><?= GetMessage('ASD_TPL_CAPTCHA')?>:<br/>
	<input type="text" name="captcha_word" size="10" maxlength="50" value="" /><br/>
	<?endif;?>

	<input type="submit" value="<?= GetMessage('ASD_TPL_SUBMIT')?>" />
</form>

<?if ($arResult['RESULT'] == 'OK'):?>
<br/>
<table class="data-table" width="400">
	<tr>
		<td colspan="2"><b><?= $arResult['DISCOUNT']['NAME']?></b></td>
	</tr>
	<?if (trim($arResult['COUPON']['DESCRIPTION']) != ''):?>
	<tr>
		<td><?= GetMessage('ASD_TPL_COUPON_DESCRIPTION')?></td>
		<td><?= $arResult['COUPON']['DESCRIPTION']?></td>
	</tr>
	<?endif;?>
	<?if (trim($arResult['DISCOUNT']['NOTES']) != ''):?>
	<tr>
		<td><?= GetMessage('ASD_TPL_DISCOUNT_DESCRIPTION')?></td>
		<td><?= $arResult['DISCOUNT']['NOTES']?></td>
	</tr>
	<?endif;?>
	<tr>
		<td><b><?= GetMessage('ASD_TPL_DISCOUNT_VAL')?></b></td>
		<td><b>
			<?
			if ($arResult['DISCOUNT']['VALUE_TYPE'] == 'P')
				echo $arResult['DISCOUNT']['VALUE'].'%';
			else
				echo $arResult['DISCOUNT']['VALUE_FORMATED'];
			?>
		</b></td>
	</tr>
	<tr>
		<td width="50%"><?= GetMessage('ASD_TPL_DOSCUNT_ACTIVE')?></td>
		<td width="50%">
			<?$act = $arResult['COUPON']['ACTIVE']=='N'||$arResult['DISCOUNT']['ACTIVE']=='N' ? 'N' : 'Y';?>
			<font color="<?= $act=='Y' ? 'green' : 'red'?>"><?= GetMessage('ASD_TPL_DOSCUNT_ACTIVE_'.$act)?></font>
		</td>
	</tr>
	<?if ($arResult['DISCOUNT']['ACTIVE_FROM']!='' || $arResult['DISCOUNT']['ACTIVE_TO']!=''):?>
	<tr>
		<td><?= GetMessage('ASD_TPL_DISCOUNT_DATES')?></td>
		<td>
			<?
			if ($arResult['DISCOUNT']['ACTIVE_FROM']!='' && $arResult['DISCOUNT']['ACTIVE_TO']!='')
			{
				echo GetMessage('ASD_TPL_FROM') . ' ' . $arResult['DISCOUNT']['ACTIVE_FROM'] . ' ';
				echo GetMessage('ASD_TPL_TO') . ' ' . $arResult['DISCOUNT']['ACTIVE_TO'];
			}
			elseif ($arResult['DISCOUNT']['ACTIVE_FROM']!='')
				echo GetMessage('ASD_TPL_FROM') . ' ' . $arResult['DISCOUNT']['ACTIVE_FROM'];
			elseif ($arResult['DISCOUNT']['ACTIVE_TO']!='')
				echo GetMessage('ASD_TPL_TO') . ' ' . $arResult['DISCOUNT']['ACTIVE_TO'];
			?>
		</td>
	</tr>
	<?endif;?>
	<?if ($arResult['COUPON']['DATE_APPLY'] != ''):?>
	<tr>
		<td><?= GetMessage('ASD_TPL_COUPON_DATE_APPLY')?></td>
		<td><?= $arResult['COUPON']['DATE_APPLY']?></td>
	</tr>
	<?endif;?>
	<tr>
		<td><?= GetMessage('ASD_TPL_COUPON_ONE_TIME')?></td>
		<td><?= GetMessage('ASD_TPL_COUPON_ONE_TIME_'.$arResult['COUPON']['ONE_TIME'])?></td>
	</tr>
<?if (!empty($arResult['DISCOUNT']['USER_GROUPS']) || !empty($arResult['DISCOUNT']['SECTIONS']) || !empty($arResult['DISCOUNT']['PRODUCTS'])):?>
	<tr>
		<td colspan="2"><font color="red"><?= GetMessage('ASD_TPL_DISCOUNT_RESTR')?></font></td>
	</tr>
	<?if (!empty($arResult['DISCOUNT']['USER_GROUPS'])):?>
	<tr>
		<td><?= GetMessage('ASD_TPL_DISCOUNT_RESTR_GROUP')?></td>
		<td>
			<ul>
			<?
			foreach ($arResult['DISCOUNT']['USER_GROUPS'] as $arGroup)
				echo '<li>'.$arGroup['NAME'].'</li>';
			?>
			</ul>
		</td>
	</tr>
	<?endif?>
	<?if (!empty($arResult['DISCOUNT']['SECTIONS'])):?>
	<tr>
		<td><?= GetMessage('ASD_TPL_DISCOUNT_RESTR_SECTS')?></td>
		<td>
			<ul>
			<?
			foreach ($arResult['DISCOUNT']['SECTIONS'] as $arSect)
				echo '<li>'.$arSect['NAME'].'</li>';
			?>
			</ul>
		</td>
	</tr>
	<?endif?>
	<?if (!empty($arResult['DISCOUNT']['PRODUCTS'])):?>
	<tr>
		<td><?= GetMessage('ASD_TPL_DISCOUNT_RESTR_PRODUCTS')?></td>
		<td>
			<ul>
			<?
			foreach ($arResult['DISCOUNT']['PRODUCTS'] as $arProduct)
				echo '<li>'.$arProduct['NAME'].'</li>';
			?>
			</ul>
		</td>
	</tr>
	<?endif?>
<?endif?>
</table>

<?elseif ($arResult['STR_ERROR'] != ''): ShowError($arResult['STR_ERROR']); endif;?>