<?
$MESS ['ASD_TPL_NOTE'] = "Введите код купона, чтобы получить информацию по скидке";
$MESS ['ASD_TPL_SUBMIT'] = "Проверить";
$MESS ['ASD_TPL_CAPTCHA'] = "Введите символы с картинки";
$MESS ['ASD_TPL_DOSCUNT_ACTIVE'] = "Активность";
$MESS ['ASD_TPL_DOSCUNT_ACTIVE_Y'] = "Да";
$MESS ['ASD_TPL_DOSCUNT_ACTIVE_N'] = "Нет";
$MESS ['ASD_TPL_DISCOUNT_DATES'] = "Период активности";
$MESS ['ASD_TPL_FROM'] = "с";
$MESS ['ASD_TPL_TO'] = "по";
$MESS ['ASD_TPL_COUPON_DATE_APPLY'] = "Дата применения купона";
$MESS ['ASD_TPL_COUPON_ONE_TIME'] = "Купон одноразовый";
$MESS ['ASD_TPL_COUPON_ONE_TIME_Y'] = "Да";
$MESS ['ASD_TPL_COUPON_ONE_TIME_N'] = "Нет";
$MESS ['ASD_TPL_COUPON_DESCRIPTION'] = "Комментарий к купону";
$MESS ['ASD_TPL_DISCOUNT_DESCRIPTION'] = "Описание скидки";
$MESS ['ASD_TPL_DISCOUNT_VAL'] = "Величина скидки";
$MESS ['ASD_TPL_DISCOUNT_RESTR'] = "Скидкой можно пользоваться, если:";
$MESS ['ASD_TPL_DISCOUNT_RESTR_GROUP'] = "Состоите в одной из групп";
$MESS ['ASD_TPL_DISCOUNT_RESTR_SECTS'] = "Товар из секций";
$MESS ['ASD_TPL_DISCOUNT_RESTR_PRODUCTS'] = "Товар один из перечисленных";
?>