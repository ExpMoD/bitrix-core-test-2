<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
Bitrix\Main\Loader::includeModule("bitrix.map");
Bitrix\InteractiveMap\DataMixer::IncludeMapScripts("desktop");

$dir = str_replace('\\', '/', dirname(__FILE__));
$APPLICATION->AddHeadString("<!--[if lte IE 8]><link href=\"" . substr($dir, strrpos($dir, "/bitrix/templates/")) . "/ie.css\" rel=\"stylesheet\"><![endif]-->");
?>