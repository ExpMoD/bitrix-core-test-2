//'use strict';

var $GeoMapp = {
    data: {
        mapType: 'yandex',
        height: 550,
        replaceRules: true,
        loadTime: 500,
        responseTime: 1000,
        narrowWidth: 900,
        listenMainScriptLoading: false,
        listenMapScriptLoading: false,
        rebuildContainer: false,
        interfaceText: {
            collapsePanel: '&#x2190; Collapse',
            categoriesTitle: 'Categories',
            clearField: 'Clear',
            placeHolder: 'Search phrase, i.e. Arbat',
            refreshMarkers: 'Refresh markers',
            clearCategories: 'Cancel',
            closeList: 'Close',
            showList: 'List',
            back: 'Back',
            showMarker: 'Show on map',
            route: 'Route',
            walking: 'On foot',
            transit: 'Transport',
            driving: 'Car',
            bicycling: 'Bicycle',
            toWalk: 'Walk',
            toDrive: 'Drive',
            reverseDirection: 'Change direction',
            createRoute: 'Get',
            pointsTitle: 'Route objects',
            currentPosition: 'My location',
            from: 'From',
            to: 'To',
            catAbstractName: 'Group'
        },
        routeMessages: {
            wait: 'Waiting for response...',
            INVALID_REQUEST: 'Invalid query',
            MAX_WAYPOINTS_EXCEEDED: 'Too many intermediate points',
            NOT_FOUND: 'Part of the coordinate set incorrectly or impossible to recognize the address',
            OK: 'Request complete',
            OVER_QUERY_LIMIT: 'Requests number limit exceeded',
            REQUEST_DENIED: 'Service is not available on this page',
            UNKNOWN_ERROR: 'Error of unknown nature',
            ZERO_RESULTS: 'Unable to build the route',
            blocked: 'Positioning function is disabled. Enter the address manually.',
            none: 'Positioning function is not supported. Enter the address manually.'
        },
        parseMessages: {
            NO_CONTAINER: 'No container for map',
            NO_SCRIPT_ID: 'No script id',
            COMMON_SCRIPT_NOT_LOADED: 'Unable to load components scripts',
            MAP_SCRIPT_NOT_LOADED: 'Map script load fails',
            MAP_DENIED: 'Cant use map for routing',
            PAGETYPE_NOT_DEFINED: 'Page type is not defined',
            NO_CATS: 'No one category',
            NO_ITEMS: 'No one object',
            INDEFINED_ERRORS: 'Undefined error',
            ERRORS_COMMON_LOADING: 'Undefined error while loading main scripts',
            ERRORS_MAP_LOADING: 'Undefined error while loading map scripts'
        },
        icon: {
            objects: {
                url: 'objects.png',
                size: [30, 40],
                anchor: [15, 37],
                logo: [30, 30]
            },
            events: {
                url: 'events.png',
                size: [30, 30],
                anchor: [15, 15],
                logo: [30, 30]
            },
            routes: {
                url: 'routes.png',
                size: [30, 40],
                anchor: [15, 37],
                logo: [30, 30]
            }
        },
        path: {
            def: {
                size: [20, 20],
                anchor: [10, 10],
                offset: [60, 30]
            },
            active: {
                size: [20, 20],
                anchor: [10, 10],
                offset: [80, 30]
            },
            strokeWeight: 4,
            strokeColor: '#4f84b0',
            strokeColorActive: '#ec473b',
            strokeOpacity: .7,
            strokeOpacityHover: 1
        },
        directionOptions: {
            strokeColor: '#481fd9',
            strokeOpacity: .7,
            strokeWeight: 3
        },
        cluster: {
            gridSize: 32,
            color: '#fff',
            set: [
                {icon: '1.png', size: 50},
                {icon: '2.png', size: 60},
                {icon: '3.png', size: 74},
                {icon: '4.png', size: 90}
            ]
        },
        libs: [
            'common.js',
            'iscroll.js'
        ],
        mapScript: {
            google: {
                main: 'http://maps.googleapis.com/maps/api/js?sensor=false&language=ru',
                cluster: 'markerclusterer.js'
            },
            yandex: {
                main: 'http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru'
            }
        }
    },

    alternate: {
        div: 'alt-block',
        ol: 'alt-ordered-list',
        ul: 'alt-unordered-list',
        li: 'alt-list-item',
        dl: 'alt-definition-list',
        dt: 'alt-definition-title',
        dd: 'alt-definition-description',
        span: 'alt-inline'
    },

    root: $(document.documentElement),
    head: $(document.head),
    scriptSet: {},
    panel: {},
    cats: {},
    counts: {},
    sets: {},
    icon: {},
    items: {},
    activeItems: {},
    markers: {},
    activeMarkers: {},
    routes: {},
    activeRoutes: {},

    init: function (options) {
        var _class = this,
            listener = $.proxy(this.errorListener, this);

        document.write(this.createElement('*div', {
            id: 'bxMapFakeElement'
        }).get(0).outerHTML);

        if (!this.head.length) {
            this.head = $('head');
        }

        if (!options.pageType || !this.data.icon[options.pageType]) {
            $(function () {
                _class.showError('PAGETYPE_NOT_DEFINED');
            });

            return;
        }

        if (!options.defaultPath) {
            var script = $('#bxMapScript');

            if (!script.length) {
                $(function () {
                    _class.showError('NO_SCRIPT_ID');
                });

                return;
            } else {
                var componentPath = (script.attr('src').replace(/js\/[^\/]+$/, '') + '/').replace(/\/+$/, '/');

                options.defaultPath = {
                    libs: componentPath.replace(/[^\/]+\/$/, '') + 'lib/',
                    images: componentPath + 'images/'
                }
            }
        }

        for (var i in options) {
            switch (i) {
                case 'items':
                    this.data.entities = options.items;
                    break;
                case 'icon':
                    extendObject(this.data.icon[options.pageType], options.icon[options.pageType] || options.icon);
                    break;
                case 'libs':
                    this.data.libs = extendArray(this.data.libs, options.libs);
                    break;
                default:
                    if (this.data[i] && (this.data[i] instanceof Object || this.data[i] instanceof Array)) {
                        extendObject(this.data[i], options[i]);
                    } else {
                        this.data[i] = options[i];
                    }

                    break;
            }
        }

        $(function () {
            _class.getWrapper(true);
        });

        if (!this.data.cats) {
            this.data.cats = {};
            this.data.noIcons = true;
        } else {
            var count = 0;

            for (var i in this.data.cats) {
                count ++;
                break;
            }

            if (!count) {
                this.data.noIcons = true;
            }
        }

        this.snapError(listener, 'listenMainScriptLoading', 'common', 'ERRORS_COMMON_LOADING');
        this.createScriptQueue(
                this.data.libs,
                'common',
                'COMMON_SCRIPT_NOT_LOADED'
            ).then(function (error) {
                if (!error) {
                    try {
                        _class.snapError(null, 'listenMapScriptLoading', 'maps', 'ERRORS_MAP_LOADING');
                        $DiffMap.init(_class.data.mapType, _class.data.pageType);
                        $(document).on('map:done', function (e, status) {
                            _class.snapError(listener);

                            if (status) {
                                var _wrapper = _class.getWrapper();

                                if (!_wrapper || !_wrapper.length) {
                                    if (_class.data.rebuildContainer) {
                                        _wrapper = _class.insertWrapper(true);
                                    } else {
                                        _class.showError('NO_CONTAINER');
                                        return;
                                    }
                                }

                                _class.createWrapper();
                                _class.creatingPage();
                            } else {
                                _class.showError('MAP_SCRIPT_NOT_LOADED');
                            }
                        });

                        _class.createScriptQueue(
                                _class.data.mapScript[_class.data.mapType],
                                'maps',
                                'MAP_SCRIPT_NOT_LOADED'
                            ).then(function (error) {
                                if (!error) {
                                    $DiffMap.mapInitialize(true);
                                } else {
                                    _class.showError(error, listener);
                                }
                            });
                    } catch (z) {
                        _class.showError('COMMON_SCRIPT_NOT_LOADED', listener);
                    }
                } else {
                    _class.showError(error, listener);
                }
            });

        function extendObject (defObject, userObject) {
            if (userObject !== undefined && userObject !== null && userObject !== '') {
                if (userObject instanceof Object || userObject instanceof Array) {
                    for (var i in userObject) {
                        if (userObject[i] !== undefined && userObject[i] !== null && userObject[i] !== '') {
                            if (userObject[i] instanceof Object || userObject[i] instanceof Array) {
                                if (!defObject[i]) {
                                    defObject[i] = new userObject[i].constructor();
                                }

                                extendObject(defObject[i], userObject[i]);
                            } else {
                                defObject[i] = userObject[i];
                            }
                        }
                    }
                } else {
                    defObject = userObject;
                }
            } else {

            }

            return defObject;
        }

        function extendArray (base, elements) {
            var intersect = [],
                adding = [];

            for (var i = 0, count = 0; i < elements.length; i++, count = 0) {
                for (var j = 0; j < base.length; j++) {
                    if (elements[i] == base[j] || elements[i].toString() == base[j].toString()) {
                        intersect.push(base.splice(j, 1));
                        count++;
                        break;
                    }
                }

                if (!count) {
                    adding.push(elements[i]);
                }
            }

            return base.concat(adding);
        }
    },

    createWrapper: function () {
        var _class = this;

        $('<style>').attr('type', 'text/css').appendTo($('head'));
        this.link = document.styleSheets[document.styleSheets.length - 1];

        if (!this.link.cssRules) {
            this.link.cssRules = this.link.rules;
        }

        this.data.icon[this.data.pageType].url = this.setFullPath(this.data.icon[this.data.pageType].url, 'images');

        if (!this.data.icon[this.data.pageType].anchor) {
            this.data.icon[this.data.pageType].anchor = [
                this.data.icon[this.data.pageType].size[0] / 2,
                this.data.icon[this.data.pageType].size[1]
            ];
        }

        if ($Params.hasTouch) {
            $(document.documentElement).addClass('touchscreen');
        }

        this.container = this.createElement('*div', 'map-container', {
            'data-container': 'list'
        }).append(
                this.createElement('*div', 'map-canvas'),
                this.createElement('*div', 'map-section map-filter', {
                    'data-container': 'collapse'
                }).append(
                        createTitle ({
                            show: this.data.interfaceText.categoriesTitle,
                            hide: this.data.interfaceText.collapsePanel
                        })
                    ).append(
                        this.createElement('*dd', 'map-section-body', {
                            'data-target': 'collapse',
                            'data-container': 'select'
                        }).append(
                                this.createElement('form', 'map-filter-form', {
                                    'data-container': 'refresh'
                                }).append(
                                        this.createElement('input', 'map-filter-input', {
                                            type: 'text',
                                            placeholder: this.data.interfaceText.placeHolder,
                                            'data-action': 'select'
                                        }),
                                        this.createElement('*div', 'map-filter-field').append(
                                            this.createElement('button', 'map-filter-clear', {
                                                type: 'button',
                                                title: this.data.interfaceText.clearField,
                                                'data-action': 'clear'
                                            })
                                                .html(this.data.interfaceText.clearField),
                                            this.createElement('button', 'map-filter-refresh', {
                                                type: 'submit',
                                                title: this.data.interfaceText.refreshMarkers,
                                                'data-action': 'refresh'
                                            })
                                                .html(this.data.interfaceText.refreshMarkers)
                                        )
                                    ),
                                this.createElement('*div', 'map-scroll-wrapper', {
                                    'data-container': 'scroll',
                                    'data-target': 'select'
                                }).append(
                                        this.createElement('*div', 'map-scroll-container', {
                                            'data-target': 'scroll'
                                        }).append(
                                                this.createElement('*div', 'map-scroll', {
                                                    'data-info': 'scroll'
                                                })
                                            )
                                    ),
                                this.createElement('*div', 'map-list-clear', {
                                    'data-container': 'clear'
                                }).append(
                                        this.createElement('button', 'map-clear-button', {
                                            type: 'button',
                                            'data-action': 'clear'
                                        })
                                            .html(this.data.interfaceText.clearCategories)
                                    )
                            )
                    ),
                this.createElement('*dl', 'map-section map-list', {
                    'data-container': 'collapse'
                }).append(
                        createTitle ({
                            show: this.data.interfaceText.showList,
                            hide: this.data.interfaceText.closeList
                        })
                    ).append(
                        this.createElement('*dd', 'map-section-body', {
                            'data-target': 'collapse'
                        }).append(
                                this.createElement('*div', 'map-scroll-wrapper', {
                                    'data-container': 'scroll variable'
                                }).append(
                                        this.createElement('*div', 'map-scroll-container', {
                                            'data-target': 'scroll'
                                        }).append(
                                                this.createElement('*div', 'map-scroll', {
                                                    'data-info': 'scroll',
                                                    'data-target': 'list'
                                                })
                                            )
                                    )
                            )
                    )
            );

        this.form = this.container.find('[data\-container~="refresh"]');
        this.input = this.form.find('[data\-action~="select"]');
        this.input
            .bind('input propertychange', function (e) {
                setTimeout(function () {
                    _class.form.trigger('check:value', [_class.input.val()]);
                }, 0);
            })
            .bind('keydown', function (e) {
                /*@cc_on
                 @if (@_jscript_version < 10)
                 if (e.keyCode == 8 || e.keyCode == 46) {
                 setTimeout(function () {
                 _class.form.trigger('check:value', [_class.input.val()]);
                 }, 0);
                 }
                 @end
                 @*/
                if (e.keyCode == 27) {
                    setTimeout(function () {
                        _class.input.val('');
                        _class.form.trigger('check:value');

                        switch (_class.data.pageType) {
                            case 'objects':
                            case 'events':
                                _class.showMarkers(_class.panel.left.find('[data\-action~="list"].active'));
                                break;
                            case 'routes':
                                _class.showRoutes(_class.panel.left.find('[data\-action~="list"].active'));
                                break;
                        }
                    }, 0);
                }
            });

        function createTitle (options) {
            var element = _class.createElement('*dt', 'map-section-head').append(
                createSubTitle ('hide'),
                createSubTitle ('show')
            );

            function createSubTitle (type) {
                var element = _class.createElement('*span', 'map-section-title map-' + type, {
                    'data-action': 'collapse',
                    'data-type': type
                })
                    .html(options[type]);

                return element;
            }

            return element;
        }
    },

    creatingPage: function () {
        var _class = this;

        $(document)
            .on('submit', '[data\-container~="refresh"]', function (e) {
                var _self = $(this),
                    _refresh = _self.find('[data\-action~="refresh"]');

                e.preventDefault();

                if (_refresh.hasClass('active')) {
                    _class.input.get(0).defaultValue = _class.input.val();
                    setTimeout(function () {
                        _refresh.removeClass('active');
                    }, 0);

                    switch (_class.data.pageType) {
                        case 'objects':
                        case 'events':
                            _class.updateMarkers();
                            break;
                        case 'routes':
                            _class.updateRoutes();
                            break;
                    }
                }
            })
            .on('mousedown touchstart', '[data\-container~="refresh"] [data-action="clear"]', function (e) {
                setTimeout(function () {
                    _class.input.val('').trigger('focus');
                    _class.form.trigger('check:value');

                    switch (_class.data.pageType) {
                        case 'objects':
                        case 'events':
                            _class.showMarkers(_class.panel.left.find('[data\-action~="list"].active'));
                            break;
                        case 'routes':
                            _class.showRoutes(_class.panel.left.find('[data\-action~="list"].active'));
                            break;
                    }
                }, 0);
            })
            .on('check:value', function (e, value) {
                var _refresh = _class.form.find('[data\-action~="refresh"]'),
                    _clear = _class.form.find('[data\-action~="clear"]');

                value = value || '';
                _class.activeItems = {};

                if (value.length <= 1) {
                    _clear.toggleClass('active', value.length);
                    _refresh.removeClass('active');
                    reset();
                } else {
                    var r = new RegExp(value, 'i'),
                        groupPlus = {},
                        groupMinus = {},
                        globalCount = 0;

                    for (var i in _class.cats) {
                        var count = 0,
                            flag = _class.cats[i].hasClass('active');

                        for (var j in _class.items[i]) {
                            if (_class.items[i][j][1].data('include')) {
                                _class.items[i][j][1].removeClass('include');
                            }

                            if (r.test(_class.items[i][j][0])) {
                                var _parent = _class.items[i][j][1].data('parent');

                                _class.items[i][j][1].removeClass('none');
                                count++;

                                if (flag) {
                                    globalCount++;
                                }

                                if (!_class.activeItems[i]) {
                                    _class.activeItems[i] = {};
                                }
                                _class.activeItems[i][j] = _class.items[i][j];

                                if (_parent) {
                                    groupPlus[_parent.data('id')] = _parent;
                                }
                            } else {
                                _class.items[i][j][1].addClass('none');
                            }
                        }

                        if (count) {
                            _class.cats[i].removeClass('none');
                        } else {
                            _class.cats[i].addClass('none');
                        }

                        _class.cats[i].find('.map-category-count').html('(' + count + ')');
                    }

                    for (var i in groupPlus) {
                        groupPlus[i].addClass('include');
                    }

                    if (globalCount) {
                        _refresh.addClass('active');
                    } else {
                        _refresh.removeClass('active');
                    }

                    _clear.addClass('active');
                }

                function reset() {
                    for (var i in _class.sets) {
                        for (var j in _class.items[i]) {
                            _class.items[i][j][1].removeClass('none');
                        }

                        _class.cats[i].removeClass('none').find('.map-category-count').html('(' + _class.cats[i].data('count') + ')');
                    }
                }

                _class.scrolls.trigger('set:rule');
            })
            .on('mousedown', '[data\-action~="scroll"]', function (e) {
                var _self = $(e.target).closest('[data\-container~="scroll"]'),
                    scroller = _self.data('scroller'),
                    box = _self.data('float'),
                    rule = _self.data('rule'),
                    H = scroller.height(),
                    h = rule.height(),
                    D = _self.data('info').height(),
                    coords = {
                        H: box.height(),
                        T: box.position().top,
                        Y: e.clientY
                    };

                function _up (e) {
                    $(document).unbind('mouseup', _up).unbind('mousemove', _move);
                }

                function _move (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    try {
                        getSelection().removeAllRanges();
                    } catch (z) {}

                    coords.delta = e.clientY - coords.Y;
                    _self.data('coords', coords).trigger('set:scroll');
                }

                $(document).on('mouseup', _up).on('mousemove', _move);
            })
            .on('mousedown', '.map-rule', function (e) {
                var _self = $(e.target),
                    _container = _self.closest('[data\-container~="scroll"]'),
                    box = _container.data('float');

                if (!_self.is(box)) {
                    var t = box.position().top,
                        p = e.clientY - e.target.getBoundingClientRect().top - t;

                    _container.data('coords').T = t;
                    _container.data('coords').delta = _container.data('coords').H * p / Math.abs(p);
                    _container.trigger('set:scroll', [true]);
                }
            })
            .on('set:rule', function (e) {
                var _self = $(e.target);

                if ($Params.hasTouch) {
                    if (!_self.data('iscroll')) {
                        _self.data('iscroll', new iScroll(e.target, {
                            snap: true,
                            momentum: false,
                            hScroll: false,
                            hideScrollbar: false,
                            onScrollEnd: function () {

                            }
                        }));
                    } else {
                        _self.data('iscroll').refresh();
                    }
                } else if (_class.data.replaceRules) {
                    var scroller = _self.data('scroller'),
                        H = scroller.height();

                    if (H) {
                        var box = _self.data('float'),
                            h = _self.data('rule').height(),
                            D = _self.data('info').height();

                        if (H / D < 1) {
                            var d = Math.max(Math.round(h * H / D), _self.data('boxHeight'));

                            if (!_class.mac) {
                                _self.addClass('scrolling');
                            }

                            _self.data('coords', {
                                H: d
                            });

                            box.animate({
                                height: _self.data('coords').H + 'px',
                                top: Math.round((h - d) * scroller.scrollTop() / (D - H)) + 'px'
                            }, 400);
                            scroller.bind('scroll', function (e) {
                                box.css({
                                    top: Math.round((h - d) * scroller.scrollTop() / (D - H)) + 'px'
                                })
                            });
                        } else {
                            _self.removeClass('scrolling');
                        }
                    }
                }
            })
            .on('set:scroll', function (e, animation) {
                var _self = $(e.target),
                    scroller = _self.data('scroller'),
                    box = _self.data('float'),
                    rule = _self.data('rule'),
                    H = scroller.height(),
                    h = rule.height(),
                    D = _self.data('info').height(),
                    coords = _self.data('coords'),
                    t = coords.T + coords.delta,
                    T = Math.round(t * D / H);

                if (t > h - coords.H) {
                    t = h - coords.H;
                    T = D - H;
                } else if (t < 0) {
                    t = T = 0;
                }
                if (animation) {
                    rule.addClass('active');
                    box.animate({
                        top: t + 'px'
                    }, 400, function () {
                        rule.removeClass('active');
                    });
                    scroller.animate({
                        scrollTop: T
                    }, 400, function () {

                    });
                } else {
                    box.css({
                        top: t + 'px'
                    });
                    scroller.scrollTop(T);
                }
            })
            .on('click', '[data\-action~="collapse"]', function (e) {
                var _self = $(this),
                    _container = _self.closest('[data\-container~="collapse"]'),
                    options = {};

                options[_container.is(_class.panel.left) ? 'left' : 'right'] = _self.data('type') == 'show' ? 'show' : 'collapse';
                _class.setPanels(options);
            })
            .on('click', '[data\-action~="list"]', function (e) {
                var _self = $(this);

                _class.setActive(_self);
            })
            .on('click', '[data\-container~="clear"] [data\-action~="clear"]', function (e) {
                var _self = $(this),
                    _container = _self.closest('[data\-container~="list"]');

                _class.setActive(_container.find('[data\-action~="list"].active'));
            })
            .on('click', '[data\-action~="geo"]', function (e) {
                var _item = $(this).closest('[data-item]'),
                    id = _item.data('id');

                if (!_item.hasClass('active')) {
                    if (_class.data.pageType == 'routes') {
                        $(document).trigger('routeMarker:click', [id]);
                    } else {
                        $(document).trigger('objectMarker:click', [id]);
                    }

                    _class.setPanels({
                        right: 'collapse'
                    });
                }
            })
            .on('objectMarker:click eventMarker:click', function (e, id) {
                var data = _class.data.entities[id],
                    catid = _class.getCatId(data.cat);

                if (_class.activeID) {
                    if (_class.activeID == id) {
                        _class.resetActiveMarker(true);
                        //_class.Balloon.close();
                        return;
                    } else {
                        _class.resetActiveMarker();
                    }
                }

                _class.setActiveMarker(id, catid, data);
            })
            .on('routeMarker:click', function (e, id) {
                var ids = id.split(/_/),
                    routeID = ids[0],
                    pointID = ids[1],
                    status = 'start',
                    data = _class.data.entities[routeID],
                    catid = _class.getCatId(data.cat);

                if (_class.routes[routeID].status != 'active') {
                    _class.setActiveRoute(routeID);
                } else {
                    if (_class.activeID) {
                        _class.resetActiveRouteMarker(_class.getStatus(_class.activeID));
                    }
                }

                if (pointID) {
                    data = data.points[pointID];
                }

                _class.setActiveRouteMarker(id, catid, data, _class.getStatus(id, true));
            })
            .on('routeMarker:hover', function (e, id) {
                var routeID = id.split(/_/)[0];

                if (_class.routes[routeID] != _class.activeRoute) {
                    $DiffMap.setRouteView(_class.routes[routeID], _class.routeIcon, 'hover');
                }
            })
            .on('routeMarker:out', function (e, id) {
                var routeID = id.split(/_/)[0];

                if (_class.routes[routeID] != _class.activeRoute) {
                    $DiffMap.setRouteView(_class.routes[routeID], _class.routeIcon, 'default');
                }
            })
            .on('route:click', function (e, id) {
                _class.setActiveRoute(id);
            })
            .on('route:hover', function (e, id) {
                if (_class.routes[id] != _class.activeRoute) {
                    $DiffMap.setRouteView(_class.routes[id], _class.routeIcon, 'hover');
                }
            })
            .on('route:out', function (e, id) {
                if (_class.routes[id] != _class.activeRoute) {
                    $DiffMap.setRouteView(_class.routes[id], _class.routeIcon, 'default');
                }
            })
            .on('popup:closed', function (e) {
                if (_class.active) {
                    if (_class.data.pageType == 'routes') {
                        _class.resetActiveRouteMarker(_class.getStatus(_class.activeID));
                    } else {
                        _class.resetActiveMarker(true);
                    }
                }
            });

        this.createMapData();
    },

    createMapData: function () {
        var _class = this,
            countCats = 0,
            countItems = 0;

        if (this.data.cats) {
            var _list = this.createElement('*ul', 'map-category-list', {
                'data-category': 'container'
            });

            if (!this.data.noIcons) {
                $Common.createRules('.map-category-item:before', 'background-image:url(' + this.data.icon[this.data.pageType].url + ');', this.link);
                $Common.createRules('.map-container .map-category-item:before', 'width:' + this.data.icon[this.data.pageType].logo[0] + 'px;height:' + this.data.icon[this.data.pageType].logo[1] + 'px;left:-' + (this.data.icon[this.data.pageType].logo[0] + 2) + 'px;margin-top:-' + this.data.icon[this.data.pageType].logo[1] / 2 + 'px;', this.link);
                $Common.createRules('.map-container .map-list .map-category-item > .map-category-item-wrapper', 'padding-left:' + (this.data.icon[this.data.pageType].logo[0] + 10) + 'px;', this.link);
                $Common.createRules('.map-container .map-filter .map-category-list', 'padding-left:' + this.data.icon[this.data.pageType].logo[0] + 'px;', this.link);
                $Common.createRules('.map-container .map-subcategory-list', 'margin-left:' + (this.data.icon[this.data.pageType].logo[0] + 10) + 'px;', this.link);
                $Common.createRules('.map-container .map-list-clear', 'left:' + this.data.icon[this.data.pageType].logo[0] + 'px;', this.link);
            }

            switch (this.data.pageType) {
                case 'objects':
                    break;
                case 'events':
                    break;
                case 'routes':
                    this.routeIcon = $DiffMap.createRouteIconSet(this.data.icon[this.data.pageType], this.data.path);
                    break;
            }

            for (var i in this.data.entities) {
                var parentId = this.data.entities[i].cat,
                    catid = this.getCatId(parentId);

                if (!this.sets[catid]) {
                    var position = '';

                    if (!this.data.cats[catid]) {
                        countCats++;
                        this.data.cats[catid] = {
                            name: this.data.interfaceText.catAbstractName + ' #' + countCats,
                            pos: 0
                        };
                    }

                    if (!this.data.noIcons) {
                        var rule = '';

                        if (this.data.cats[catid].icon) {
                            rule += 'background-image:url(' + this.data.cats[catid].icon + ');';
                        }

                        if (this.data.cats[catid].pos) {
                            rule += 'background-position:-' + this.data.cats[catid].pos + 'px 0;';
                        } else {
                            this.data.cats[catid].pos = 0;
                        }

                        if (rule) {
                            $Common.createRules('.' + catid + '.map-category-item:before,.' + catid + ' .map-category-item:before', rule, this.link);
                        }
                    } else {
                        this.container.addClass('no-image');
                    }

                    this.counts[catid] = 0;
                    this.items[catid] = {};
                    this.sets[catid] = this.createElement('*ul', 'map-category-list ' + catid + ' none', {
                        'data-id': catid,
                        'data-set': 'list'
                    });

                    switch (this.data.pageType) {
                        case 'objects':
                        case 'events':
                            this.icon[catid] = $DiffMap.createIconSet(this.data.cats[catid], this.data.icon[this.data.pageType], catid, this.data.noIcons);
                            break;
                        case 'routes':
                            break;
                    }
                }

                switch (this.data.pageType) {
                    case 'objects':
                    case 'events':
                        createObjects(this.data.entities[i], i, catid, parentId, this.sets[catid]);
                        break;
                    case 'routes':
                        createRoutes(this.data.entities[i], i, catid, this.sets[catid]);
                        break;
                }
            }

            if (!countItems) {
                this.showError('NO_ITEMS');
                return;
            }

            for (var i in this.data.cats) {
                if (this.counts[i]) {
                    this.cats[i] = this.createElement('*li', 'map-category-item ' + i, {
                        'data-id': i,
                        'data-action': 'list',
                        'data-count': this.counts[i]
                    }).append(
                            this.createElement('*span', 'map-category-name').append(
                                    this.createElement('*span', 'map-category-title').html(this.data.cats[i].name)
                                ).append(
                                    this.createElement('*span', 'map-category-count').html('(' + this.counts[i] + ')')
                                )
                        ).appendTo(_list);
                }
            }

            switch (this.data.pageType) {
                case 'objects':
                case 'events':
                    $DiffMap.setClusterOptions(this.data.cluster);
                    break;
                case 'routes':
                    break;
            }
        }

        var delta = new Date() - this.start;

        if (delta >= this.data.loadTime) {
            this.insertData(_list);
        } else {
            setTimeout(function () {
                _class.insertData(_list);
            }, this.data.loadTime - delta);
        }

        function createObjects(data, id, catid, parentId, parent) {
            var _item = setContent(
                _class.createElement('*li', 'map-category-item', {
                    'data-item': 'list',
                    'data-catid': catid,
                    'data-id': id
                }),
                data
            );

            if (catid != parentId) {
                var _parent = parent.find('[data-id="' + parentId + '"]'),
                    _list = parent.find('[data-container="' + parentId + '"]');

                if (!_list.length) {
                    _parent.data('include', 1);
                    _list = _class.createElement('*ul', 'map-subcategory-list', {
                        'data-container': parentId
                    }).appendTo(_parent);
                }

                _item.data('parent', _parent);
                _list.append(_item);
                _parent.data('include', _parent.data('include') * 1 + 1);
            } else {
                parent.append(_item);
            }

            if (data.name) {
                if (data.lat && data.lng) {
                    _item.data({
                        lat: data.lat,
                        lng: data.lng
                    });
                    _class.markers[id] = $DiffMap.createMarker(data, id, catid, _class.icon[catid], 'default').marker;
                }

                _class.items[catid][id] = [data.name, _item];
                _class.counts[catid]++;
                countItems++;
            }
        }

        function createRoutes(data, id, catid, parent) {
            var _item = setContent(
                    _class.createElement('*li', 'map-category-item', {
                        'data-item': 'list',
                        'data-catid': catid,
                        'data-id': id
                    }),
                    data
                ),
                geo,
                status = 'default';

            _class.items[catid][id] = [data.name, _item];
            _class.routes[id] = {
                markers: [],
                coords: [],
                polyLine: null,
                status: status,
                id: id
            };
            _class.counts[catid]++;
            countItems++;

            if (data.lat && data.lng) {
                _item.data({
                    lat: data.lat,
                    lng: data.lng
                });
                geo = $DiffMap.createMarker(data, id, catid, _class.routeIcon, status);
                _class.markers[id] = geo.marker;
                _class.routes[id].markers.push(geo.marker);
                _class.routes[id].coords.push(geo.point);
            }

            parent.append(_item);

            if (data.points.length) {
                var _sublist = _class.createElement('*ul', 'map-subcategory-list', {
                    'data-container': parentId
                }).appendTo(_item);

                for (var i = 0, markerID, _subitem; i < data.points.length; i++) {
                    markerID = id + '_' + i;

                    if (data.points[i].description) {
                        _subitem = setContent(
                            _class.createElement('*li', 'map-category-item', {
                                'data-': 'list',
                                'data-catid': catid,
                                'data-id': markerID
                            }),
                            data.points[i]
                        ).appendTo(_sublist);
                        geo = $DiffMap.createMarker(data.points[i], markerID, catid, _class.routeIcon, status);
                        _class.markers[markerID] = geo.marker;
                        _class.routes[id].markers.push(geo.marker);
                        _class.routes[id].coords.push(geo.point);
                        _class.items[catid][markerID] = [data.points[i].name, _subitem.data({
                            lat: data.points[i].lat,
                            lng: data.points[i].lng
                        })];
                    } else {
                        _class.routes[id].coords.push($DiffMap.createPoint(data.points[i]));
                    }
                }

                if (data.closed) {
                    _class.routes[id].coords.push(_class.routes[id].coords[0]);
                } else {
                    _class.routes[id].end = true;
                }

                _class.routes[id].polyLine = $DiffMap.createPolyLine(id, _class.routes[id].coords, _class.routeIcon._pathDefault, true);
            }
        }

        function setContent(_item, data) {
            var content = _class.createElement('*div', 'map-category-item-wrapper');

            if (data.lat) {
                content.append(
                    _class.createElement('*div', 'map-item-geo', {
                        'data-action': 'geo',
                        'title': _class.data.interfaceText.showMarker
                    })
                );
            }

            if (_class.data.pageType == 'events' && data.opening) {
                content.append(
                    _class.createElement('*div', 'map-item-opening').html(data.opening)
                );
            }

            var dl = _class.createElement('*dl', 'map-item-info').appendTo(content),
                dd = _class.createElement('*dd', 'map-item-detail'),
                count = 0;

            if (data.name) {
                var name = _class.createElement('*dt', 'map-item-name').appendTo(dl);

                if (data.url) {
                    name.append(
                        _class.createElement('a', 'map-item-url', {
                            href: data.url
                        }).html(data.name)
                    )
                } else {
                    name.html(data.name);
                }
            }

            if (data.address) {
                dd.append(
                    _class.createElement('*div', 'map-item-address').html(data.address)
                );
                count ++;
            }

            if (data.description) {
                dd.append(
                    _class.createElement('*div', 'map-item-description').html(data.description)
                );
                count ++;
            }

            if ((_class.data.pageType != 'events' || _class.data.pageType == 'objects') && data.opening) {
                dd.append(
                    _class.createElement('*div', 'map-item-opening').html(data.opening)
                );
                count ++;
            }

            if (count) {
                dl.append(dd);
            }

            if (data.phone || data.link) {
                var info = _class.createElement('*div', 'map-item-contacts').appendTo(content);

                if (data.phone) {
                    var phone = _class.createElement('*ul', 'map-item-phone-set').appendTo(info);

                    for (var i = 0, phones = data.phone.split(/\s*,\s*/); i < phones.length; i++) {
                        phone.append(
                            _class.createElement('*li', 'map-item-phone-number').html(phones[i])
                        );
                    }
                } else {
                    info.addClass('single');
                }

                if (data.link) {
                    info.append(
                        _class.createElement('a', 'map-item-link', {
                            href: data.link
                        }).html(data.link.replace(/^(http\w?:\/\/)?(www\.)?/, ''))
                    );
                }
            }

            _item.append(content);
            data.content = content.clone(true);
            return _item;
        }
    },

    insertData: function (_list) {
        var _class = this,
            _wrapper = this.getWrapper();

        if (_wrapper.hasClass('loading')) {
            _wrapper.removeClass('loading');
            this.loading.remove();
        }

        this.container.appendTo(_wrapper);

        if (this.data.height) {
            this.container.height(this.data.height);
        }

        this.scrolls = $('[data\-container~="scroll"]').each(function () {
            var _self = $(this),
                _container = _self.closest('[data\-container~="collapse"]');

            _self
                .data('scroller', _self.find('[data\-target~="scroll"]'))
                .data('info', _self.data('scroller').find('[data\-info~="scroll"]'));

            if (_container.hasClass('map-filter')) {
                _class.panel.left = _container;
                _self.data('info').append(_list);
            } else if (_container.hasClass('map-list')) {
                _class.panel.right = _container;
                for (var i in _class.cats) {
                    _self.data('info').append(_class.sets[i]);
                }
            }

            if (_class.data.replaceRules) {
                if (!$Params.hasTouch) {
                    _self
                        .data(
                            'rule',
                            _class.createElement('*div', 'map-rule').appendTo(_self)
                        )
                        .data(
                            'float',
                            _class.createElement('*div', 'map-rule-float', {
                                'data-action': 'scroll'
                            }).appendTo(_self.data('rule'))
                        )
                        .data(
                            'boxHeight',
                            parseInt(_self.data('float').css('min-height'))
                        );
                }

                var width = _self.width();

                if (!_class.mac && width && width <= _self.data('info').width()) {
                    _class.mac = true;
                }

                _self.trigger('set:rule');
            }
        });

        var _children = _list.children();

        if (_children.length > 1) {
            this.step =  Math.abs(_children.eq(0).position().top - _children.eq(1).position().top);
        }

        this.buttonHeight = parseInt(this.panel.left.find('[data\-container~="clear"]').find('[data\-action~="clear"]').css('height'));

        this.Balloon = $Balloon.init(
            $DiffMap.createMap({
                container: this.container.find('.map-canvas').get(0),
                scroll: true,
                mouse: true,
                zoom: true,
                balloon: true
            }),
            this.createElement('*div', 'map-popup map-' + this.data.mapType).append(
                this.createElement('*span', 'map-popup-close'),
                this.createElement('*div', 'map-popup-image'),
                this.createElement('*div', 'map-popup-container').append(
                    this.createElement('*div', 'map-popup-inner')
                )
            ),
            this.data.mapType,
            this.data.pageType,
            this.createElement('*div', 'map-sizer map-popup')
        );
    },

    createElement: function (tag) {
        var set = [],
            name = tag.split('*');

        if (name[1]) {
            set.push(this.alternate[name[1]]);
            tag = 'bxmap';
        }

        var element = $('<' + tag + '>');

        for (var i = 1; i < arguments.length; i++) {
            if (typeof arguments[i] == 'string') {
                set = set.concat(arguments[i].split(/\s+/));
            } else {
                element.attr(arguments[i]);
            }
        }

        if (set.length) {
            element.addClass(set.join(' '));
        }

        return element;
    },

    addEvent: function (element, name, handler) {
        try {
            element.addEventListener(name, handler, false);
        } catch (z) {
            element.attachEvent('on' + name, handler);
        }

        return this;
    },

    removeEvent: function (element, name, handler) {
        try {
            element.removeEventListener(name, handler, false);
        } catch (z) {
            element.detachEvent('on' + name, handler);
        }

        return this;
    },

    getScript: function (src, id, name, errorName) {
        var _class = this,
            script = document.createElement('script');

        script.src = src;
        script.async = false;
        script.charset = 'utf-8';
        script.onload = script.onreadystatechange = process;
        script.onerror = error;
        _class.head.get(0).insertBefore(script, _class.head.get(0).lastChild);

        function process (e) {
            e = e || event;

            if (e.type === 'load' || (/loaded|complete/.test(script.readyState) && (!document.documentMode || document.documentMode < 9))) {
                script.onload = script.onreadystatechange = script.onerror = null;
                _class.checkScriptQueue(id, name, errorName, true);
            }
        }

        function error (e) {
            script.onload = script.onreadystatechange = script.onerror = null;
            _class.checkScriptQueue(id, name, errorName);
        }

        return src;
    },

    checkScriptQueue: function (id, name, errorName, load) {
        this.scriptSet[id].count --;

        if (!this.scriptSet[id].stop) {
            if (!load) {
                this.scriptSet[id].stop = true;
                $(document).trigger('check:script', [id, errorName]);
            } else {
                delete this.scriptSet[id].set[name];
            }

            if (!this.scriptSet[id].count) {
                $(document).trigger('check:script', [id, null]);
            }
        } else {
            delete this.scriptSet[id].set[name];
        }
    },

    createScriptQueue: function (data, id, errorName) {
        var _class = this,
            defer = $.Deferred(),
            name;

        this.scriptSet[id] = {
            id: id,
            count: 0,
            set: {}
        };

        this.scriptSet[id].timing = setTimeout(
            $.proxy(checkScript, this.scriptSet[id]),
            this.data.responseTime
        );

        $(document).one('check:script', function (e, id, error) {
            clearTimeout(_class.scriptSet[id].timing);
            defer.resolve(error);
        });

        for (var i in data) {
            if (data instanceof Array) {
                name = 'set_' + i;
            } else {
                name = i;

                if (id == 'maps' && name == 'main') {
                    data[i] += '&callback=$DiffMap.mapInitialize';
                }
            }

            this.scriptSet[id].count ++;
            this.scriptSet[id].set[name] = this.getScript(this.setFullPath(data[i], 'libs'), id, name, errorName);
        }

        function checkScript () {
            $(document).trigger('check:script', [this.id, !!this.stop && !this.count]);
        }

        return defer.promise();
    },

    getWrapper: function (status) {
        if (!this.wrapper || !this.wrapper.length) {
            this.wrapper = $('#bxMapContainer').addClass(this.data.mapType);

            if (this.wrapper && this.wrapper.length && $('#bxMapFakeElement').length) {
                $('#bxMapFakeElement').remove();
            }
        }

        if (status) {
            this.setWrapper();
        }

        return this.wrapper;
    },

    setWrapper: function () {
        var _class = this,
            _wrapper = this.getWrapper().css({
                height: this.data.height + 'px'
            }).addClass('loading');

        $(window).on('resize', setWidth);
        this.loading = this.createElement('div', 'loading-informer').appendTo(_wrapper);
        this.start = new Date();
        setWidth();

        function setWidth () {
            if (_wrapper.width() < _class.data.narrowWidth) {
                _wrapper.addClass('narrow');
            } else {
                _wrapper.removeClass('narrow');
            }
        }
    },

    insertWrapper: function (status) {
        var _wrapper = this.getWrapper();

        if (!_wrapper || !_wrapper.length) {
            this.wrapper = this.createElement('*div', 'map-wrapper', {
                id: 'bxMapContainer'
            }).replaceAll($('#bxMapFakeElement'));
        }

        if (status) {
            this.setWrapper();
        }

        return this.wrapper;
    },

    showError: function (message, listener) {
        var _class = this,
            _wrapper = this.insertWrapper();

        if (this.start) {
            var delta = new Date() - this.start;

            if (delta >= this.data.loadTime) {
                collapseWrapper();
            } else {
                setTimeout(function () {
                    collapseWrapper();
                }, this.data.loadTime - delta);
            }
        } else {
            insertError();
        }

        if (listener) {
            this.snapError(listener);
        }

        function collapseWrapper () {
            _wrapper.empty();

            if (_wrapper.hasClass('loading')) {
                _wrapper
                    .removeClass('loading')
                    .animate({
                        height: _wrapper.css('min-height')
                    }, 500, function () {
                        _class.loading.remove();
                        insertError();
                    });
            } else {
                _wrapper.css({
                    height: _wrapper.css('min-height')
                });
                insertError();
            }
        }

        function insertError () {
            _wrapper
                .addClass('error-container')
                .append(
                    _class.createElement('*div', 'error-message', {
                        'data-container': 'message'
                    })
                );
            _class.setMessage(_class.data.parseMessages[message] || message, 'fail');
        }
    },

    errorListener: function (e) {
        try {
            e.preventDefault();
        } catch (z) {
            e.returnValue = false;
        }

        if (this.data[this.errorFields.type]) {
            $(document).trigger('check:script', [this.errorFields.name, this.errorFields.message]);
        }
    },

    snapError: function (listener, type, name, message) {
        var _class = this;

        if (arguments.length > 1) {
            this.errorFields = {
                type: type,
                name: name,
                message: message
            };

            if (listener) {
                this.addEvent(window, 'error', listener);
            }
        } else {
            this.removeEvent(window, 'error', listener);
        }
    },

    setMessage: function (message, type, _old) {
        var _class = this,
            _container = this.getWrapper().find('[data\-container~="message"]'),
            _list = _container.find('[data\-target~="message"]');

        if (message && type) {
            var _new = this.createElement('*li', 'group-item').append(
                this.createElement('*span', 'block ' + type).html(this.data.routeMessages[message] || message)
            );

            if (!_list.length) {
                _list = this.createElement('*ul', 'group status', {
                    'data-target': 'message'
                }).appendTo(_container);
            }

            if (_old) {
                _old.animate({
                    opacity: 0
                }, 200, function () {
                    _old.remove();
                    _new.appendTo(_list);
                    return _new;
                });
            } else {
                _new.appendTo(_list);
                return _new;
            }
        } else {
            var defer = $.Deferred();

            _list.animate({
                opacity: 0
            }, 200, function () {
                _list.remove();
                defer.resolve(null);
            });

            return defer.promise();
        }
    },

    getCatId: function (catid) {
        if (this.data.entities[catid]) {
            return this.getCatId(this.data.entities[catid].cat);
        } else {
            return catid;
        }
    },

    setFullPath: function (path, type) {
        if (!/^(\/|http)/.test(path)) {
            path = (this.data.defaultPath[type] + '/').replace(/\/+$/, '/') + path;
        }

        return path;
    },

    getStatus: function (id, active) {
        var ids = id.split(/_/),
            routeID = ids[0],
            pointID = ids[1],
            route = this.routes[routeID],
            status = 'default';

        if (route == this.activeRoute) {
            if (!pointID) {
                status = 'start';
            } else {
                if (route.end && route.markers[route.markers.length - 1] == this.markers[id]) {
                    status = 'end';
                } else if (active) {
                    status = 'super';
                } else {
                    status = 'active';
                }
            }
        }

        return status;
    },

    setActiveRoute: function (id) {
        if (this.activeRoute) {
            $DiffMap.setRouteView(this.activeRoute, this.routeIcon, 'default');
            this.resetActiveRouteMarker();

            if (this.routes[id] == this.activeRoute) {
                delete this.activeRoute;
                return;
            }
        }

        this.activeRoute = this.routes[id];
        $DiffMap.setRouteView(this.routes[id], this.routeIcon, 'active');
    },

    setActiveRouteMarker: function (id, catid, data, status) {
        this.active = this.items[catid][id][1];
        this.activeID = id;
        this.active.addClass('active');
        this.Balloon.show(this.markers[id], data);
        $DiffMap.setMarkerView(this.markers[id], this.routeIcon, 'route', status);
    },

    resetActiveRouteMarker: function (status) {
        status = status || 'default';

        if (this.activeID) {
            $DiffMap.setMarkerView(this.markers[this.activeID], this.routeIcon, 'route', status);
            this.active.removeClass('active');
            delete this.active;
            delete this.activeID;
            this.Balloon.close();
        }
    },

    setActiveMarker: function (id, catid, data) {
        this.active = this.items[catid][id][1];
        this.activeID = id;
        this.active.addClass('active');
        this.Balloon.show(this.markers[id], data);
        $DiffMap.setMarkerView(this.markers[id], this.icon[catid], catid, 'active');
    },

    resetActiveMarker: function (deletion) {
        if (this.active) {
            var catid = this.active.data('catid');

            this.active.removeClass('active');
            $DiffMap.setMarkerView(this.markers[this.activeID], this.icon[catid], catid, 'default');

            if (deletion) {
                delete this.active;
                delete this.activeID;
                this.Balloon.close();
            }
        }
    },

    showMarkers: function (elems) {
        var _class = this;

        elems.each(function () {
            var _self = $(this),
                id = _self.data('id'),
                _items = _class.items[id];

            if (_self.hasClass('active')) {
                if (_class.activeItems[id]) {
                    _items = _class.activeItems[id];
                }

                for (var i in _items) {
                    if (_class.markers[i]) {
                        _class.activeMarkers[i] = _class.markers[i];
                    }
                }
            } else {
                for (var i in _items) {
                    if (_class.markers[i]) {
                        $DiffMap.removeMarker(_class.markers[i], id);
                        delete _class.activeMarkers[i];

                        if (i == _class.activeID) {
                            _class.resetActiveMarker(true);
                        }
                    }
                }
            }

            $DiffMap.updateMarkerCluster();
        });
    },

    updateMarkers: function () {
        var _class = this;

        _class.activeMarkers = {};

        for (var i in _class.markers) {
            $DiffMap.removeMarker(_class.markers[i]);
        }

        for (var id in _class.activeItems) {
            if (_class.cats[id].hasClass('active')) {
                for (var i in _class.activeItems[id]) {
                    if (_class.markers[i]) {
                        _class.activeMarkers[i] = _class.markers[i];
                    }
                }
            }
        }

        if (_class.active) {
            _class.Balloon.close();
        }

        $DiffMap.updateMarkerCluster();
    },

    showRoutes: function (elems) {
        var _class = this;

        elems.each(function () {
            var _self = $(this),
                id = _self.data('id'),
                _items = _class.items[id];

            if (_self.hasClass('active')) {
                if (_class.activeItems[id]) {
                    _items = _class.activeItems[id];
                }

                for (var i in _items) {
                    if (_class.routes[i]) {
                        $DiffMap.showRoute(_class.routes[i], _class.routeIcon);
                        _class.activeRoutes[i] = _class.routes[i];
                    }
                }
            } else {
                for (var i in _items) {
                    if (_class.routes[i]) {
                        $DiffMap.removeRoute(_class.routes[i]);
                        delete _class.activeRoutes[i];

                        if (_class.routes[i] == _class.activeRoute) {
                            _class.Balloon.close();
                            delete _class.activeRoute;
                        }
                    }
                }
            }
        });
    },

    updateRoutes: function () {
        var _class = this;

        for (var i in _class.activeRoutes) {
            $DiffMap.removeRoute(_class.routes[i]);
        }

        _class.activeRoutes = {};

        for (var id in _class.activeItems) {
            if (_class.cats[id].hasClass('active')) {
                for (var i in _class.activeItems[id]) {
                    if (_class.routes[i]) {
                        _class.activeRoutes[i] = _class.routes[i];
                        $DiffMap.showRoute(_class.routes[i], _class.routeIcon);
                    }
                }
            }
        }

        if (_class.active) {
            _class.Balloon.close();
        }
    },

    setActive: function (elems) {
        var _class = this;

        if (elems.length) {
            var _container = elems.eq(0).closest('[data\-container~="list"]'),
                _target = _container.find('[data\-target~="list"]'),
                _scroll = _class.panel.left.find('[data\-container~="scroll"]'),
                _buttonblock = _class.panel.left.find('[data\-container~="clear"]'),
                count = _container.data('count') || 0;

            elems.each(function () {
                var _self = $(this),
                    id = _self.data('id'),
                    _list = _target.find('[data\-set~="list"][data\-id~="' + id + '"]');

                if (_self.hasClass('active')) {
                    _self.removeClass('active');
                    _list.addClass('none');
                    count--;
                } else {
                    _self.addClass('active');
                    _list.removeClass('none');
                    count++;
                }

                switch (_class.data.pageType) {
                    case 'objects':
                    case 'events':
                        _class.showMarkers(_self);
                        break;
                    case 'routes':
                        _class.showRoutes(_self);
                        break;
                }
            });

            if (count == 0) {
                _scroll
                    .removeData('few')
                    .animate({
                        bottom: 0 + 'px'
                    }, 400, function () {
                        if (_class.data.replaceRules) {
                            $(this).trigger('set:rule');
                        }
                    });

                _buttonblock.animate({
                    height: 0 + 'px'
                }, 400);

                this.setPanels({
                    right: 'invisible'
                });

                $DiffMap.fitMap();
            } else {
                if (!_scroll.data('few')) {
                    if (_class.step){
                        _scroll
                            .data('few', true)
                            .animate({
                                bottom: _class.step + 1 + 'px'
                            }, 400, function () {
                                if (_class.data.replaceRules) {
                                    $(this).trigger('set:rule');
                                }
                            });
                    }

                    _buttonblock.animate({
                        height: _class.buttonHeight + 'px'
                    }, 400);
                }

                if (!_class.panel.right.hasClass('show')) {
                    this.setPanels({
                        right: 'collapse'
                    });
                } else {
                    _target.closest('[data\-container~="scroll"]').trigger('set:rule');
                }
            }

            _container.data('count', count);
        }
    },

    setPanels: function (options) {
        var _scroll = {
            left: this.panel.left.find('[data\-container~="scroll"]'),
            right: this.panel.right.find('[data\-container~="scroll"]')
        };

        if (!options) {
            options = {
                left: 'show',
                right: ''
            }
        }

        if (options.left == 'lock' || options.right == 'show') {
            this.panel.left.removeClass('collapse').addClass('lock');
            this.panel.right.removeClass('collapse').addClass('show');

            if (this.data.replaceRules) {
                _scroll.left.trigger('set:rule');
                _scroll.right.trigger('set:rule');
            }
        } else {
            if (options.left) {
                this.panel.right.addClass('collapse');

                if (options.left == 'collapse') {
                    this.panel.left.removeClass('lock').addClass('collapse');
                } else if (options.left == 'show') {
                    this.panel.left.removeClass('collapse lock');

                    if (this.data.replaceRules) {
                        _scroll.left.trigger('set:rule');
                    }
                }
            }
            if (options.right) {
                this.panel.left.removeClass('lock');

                if (options.right == 'collapse') {
                    this.panel.right.addClass('show collapse');
                } else if (options.right == 'invisible') {
                    this.panel.right.removeClass('show').addClass('collapse');
                }
            }
        }

        return this;
    }
};

var $Balloon = {
    init: function (map, template, mapType, pageType, sizer) {
        function googleBalloon() {
            this.extend(googleBalloon, google.maps.OverlayView);
            this.buildDom_();
        }

        function yandexBalloon() {}

        if (mapType == 'google') {
            googleBalloon.prototype.extend = function (a, b) {
                return (function (object) {
                    for (var property in object.prototype) {
                        this.prototype[property] = object.prototype[property];
                    }
                    return this;
                }).apply(a, [b]);
            };

            googleBalloon.prototype.buildDom_ = function () {
                var _class = this;

                this.container_ = template;
                this.sizer_ = sizer;
                this.image_ = this.container_.find('.map-popup-image');
                this.content_ = this.container_.find('.map-popup-container');

                google.maps.event.addDomListener(this.container_.find('.map-popup-close').get(0), 'click', function () {
                    _class.close();
                    google.maps.event.trigger(_class, 'closeclick');
                });
            };

            googleBalloon.prototype.onAdd = function () {
                var panes = this.getPanes();
                if (panes) {
                    this.container_.appendTo(panes.floatPane);
                }
            };

            googleBalloon.prototype.reset = function () {
                this.container_.reset();
            };

            googleBalloon.prototype.draw = function () {
                var projection = this.getProjection();

                if (!projection) {
                    return;
                }

                var latLng = this.get('position');

                if (!latLng) {
                    this.close();
                    return;
                }

                var pos = projection.fromLatLngToDivPixel(latLng),
                    width = this.container_.get(0).clientWidth,
                    height = this.container_.get(0).clientHeight;

                if (!width) {
                    return;
                }

                this.container_.css({
                    top: pos.y - height - (this.delta || 0) + 'px',
                    left: pos.x - width / 2 + 'px'
                });
            };

            googleBalloon.prototype.onRemove = function () {
                if (this.container_.parent().length) {
                    this.container_.remove();
                }
            };

            googleBalloon.prototype.isOpen = function () {
                return this.isOpen_;
            };

            googleBalloon.prototype.show = function (marker, data) {
                var _class = this;

                this.setContent(data);
                this.delta = marker.getIcon().anchor.y;

                setTimeout(function () {
                    _class.updateContent_();
                    _class.setMap(map);
                    _class.set('anchor', marker);
                    _class.bindTo('anchorPoint', marker);
                    _class.bindTo('position', marker);
                    _class.redraw_();
                    _class.isOpen_ = true;
                    _class.container_.addClass('active');

                    setTimeout(function () {
                        _class.panToView();
                    }, 0);
                }, 0);
            };

            googleBalloon.prototype.setContent = function (data) {
                var _class = this;

                _class.set('content', data.content);
                _class.image_.empty();

                if (data.photo) {
                    var _img = $('<img src="' + data.photo + '">');

                    if (!_img.get(0).complete) {
                        _class.container_.addClass('temporary');
                        _img.bind('load', function () {
                            var h = _class.image_.get(0).clientWidth * this.height / this.width;

                            _class.container_
                                .css({
                                    top: parseInt(_class.container_.css('top')) - h + 'px'
                                })
                                .removeClass('temporary');
                            $(this).appendTo(_class.image_);
                            setTimeout(function () {
                                _class.panToView();
                            }, 0);
                        });
                    } else {
                        _img.appendTo(_class.image_);
                    }
                }
            };

            googleBalloon.prototype.close = function () {
                this.container_.removeClass('active');
                this.isOpen_ = false;
                $(document).trigger('popup:closed');
            };

            googleBalloon.prototype.setPosition = function (position) {
                if (position) {
                    this.set('position', position);
                }
            };

            googleBalloon.prototype.getPosition = function () {
                return this.get('position');
            };

            googleBalloon.prototype.position_changed = function () {
                this.draw();
            };

            googleBalloon.prototype.panToView = function () {
                var projection = this.getProjection();

                if (!projection || !this.container_.length) {
                    return;
                }

                var map = this.get('map'),
                    latLng = this.getPosition(),
                    centerPos = projection.fromLatLngToContainerPixel(map.getCenter()),
                    pos = projection.fromLatLngToContainerPixel(latLng),
                    spaceTop = centerPos.y - this.container_.height(),
                    spaceBottom = map.getDiv().offsetHeight - centerPos.y,
                    deltaY = 0;

                if (spaceTop < 0) {
                    spaceTop *= -1;
                    deltaY = (spaceTop + spaceBottom) / 2;
                }

                pos.y -= deltaY;
                latLng = projection.fromContainerPixelToLatLng(pos);

                if (map.getCenter() != latLng) {
                    map.panTo(latLng);
                }
            };

            googleBalloon.prototype.getContent = function () {
                return this.get('content');
            };

            googleBalloon.prototype.updateContent_ = function () {
                this.content_.empty().append(this.getContent());
                google.maps.event.trigger(this, 'domready');
                this.redraw_();
            };

            googleBalloon.prototype.redraw_ = function () {
                this.figureOutSize_();
                this.draw();
            };

            googleBalloon.prototype.figureOutSize_ = function () {
                var map = this.get('map');

                if (!map) {
                    return;
                }

                var mapDiv = map.getDiv(),
                    mapHeight = mapDiv.offsetHeight,
                    height = this.get('minHeight') || 0,
                    maxHeight = this.get('maxHeight') || 0;

                maxHeight = Math.min(mapHeight, maxHeight);

                var content = this.get('content');

                if (content) {
                    var contentSize = this.getElementSize_(content, maxHeight);

                    if (height < contentSize.height) {
                        height = contentSize.height;
                    }
                }

                if (maxHeight) {
                    height = Math.min(height, maxHeight);
                }

                if (height > mapHeight) {
                    height = mapHeight;
                }

                this.content_.height(height);
            };

            googleBalloon.prototype.getElementSize_ = function (element, opt_maxHeight) {
                var sizer = this.sizer_.clone(true);

                if (typeof element == 'string') {
                    sizer.html(element);
                } else {
                    sizer.append(element.clone(true));
                }

                $(document.body).append(sizer);
                var size = new google.maps.Size(sizer.width(), sizer.height());

                sizer.remove();
                return size;
            };

            return new googleBalloon();
        } else if (mapType == 'yandex') {
            var delta = 0;

            template.find('.map-popup-image').html('$[myHeaderContent]');
            template.find('.map-popup-container').html('$[myBodyContent]');

            yandexBalloon.prototype.layout = ymaps.templateLayoutFactory.createClass(
                template.get(0).outerHTML,
                {
                    build: function () {
                        var _class = this;

                        this.constructor.superclass.build.call(this);
                        this._element = $('.map-popup').addClass('active');
                        this._image = this._element.find('.map-popup-image img');
                        this._element.find('.map-popup-close').on('click', $.proxy(this.onCloseClick, this));

                        if (this._image.length && !this._image.get(0).complete) {
                            this._element.addClass('temporary');
                            this._image.on('load', function () {
                                _class.applyElementOffset();
                                _class._element.removeClass('temporary');
                            });
                        } else {
                            this.applyElementOffset();
                        }
                    },

                    clear: function () {
                        this._element.find('.map-popup-close').off('click');
                        this.constructor.superclass.clear.call(this);
                    },

                    onSublayoutSizeChange: function () {
                        this.constructor.superclass.onSublayoutSizeChange.apply(this, arguments);

                        if (this._isElement()) {
                            this.applyElementOffset();
                        }
                    },

                    applyElementOffset: function () {
                        this._element.css({
                            left: - (this._element.get(0).clientWidth / 2) + 'px',
                            top: - (this._element.get(0).clientHeight) + delta + 'px'
                        });
                        this.events.fire('boundschange');
                    },

                    onCloseClick: function (e) {
                        this.events.fire('userclose');
                        $(document).trigger('popup:closed');
                    },

                    getClientBounds: function () {
                        if (!this._isElement()) {
                            return this.constructor.superclass.getClientBounds.call(this);
                        }

                        var position = this._element.position();

                        return [
                            [position.left, position.top], [
                                position.left + this._element.get(0).clientWidth,
                                position.top + this._element.get(0).clientHeight
                            ]
                        ];
                    },

                    _isElement: function () {
                        return this._element && this._element.get(0);
                    }
                }
            );

            yandexBalloon.prototype.show = function (marker, data) {
                delta = ymaps.option.presetStorage.get(marker.options.get('preset')).iconOffset[1];
                map.balloon.open(marker.geometry.getCoordinates(), {
                        myHeaderContent: data.photo ? '<img src="' + data.photo + '">' : '',
                        myBodyContent: data.content.clone(true).get(0)
                    },
                    {
                        shadow: false,
                        layout: this.layout
                    }
                );
            };

            yandexBalloon.prototype.close = function () {
                map.balloon.close();
            };

            return new yandexBalloon();
        }
    }
};