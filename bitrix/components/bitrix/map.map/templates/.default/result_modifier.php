<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$obDataMixer = new \Bitrix\InteractiveMap\DataMixer();

$arParams["MAP_HEIGHT"] = IntVal($arParams["MAP_HEIGHT"]);

if (empty($arParams["MAP_HEIGHT"]))
{
    $arParams["MAP_HEIGHT"] = $obDataMixer->GetDefaultMapHeight();
}

if ($arParams["MAP_HEIGHT"] < 270)
{
    $arParams["MAP_HEIGHT"] = 270;
}
if ($arParams["MAP_HEIGHT"] > 2000)
{
    $arParams["MAP_HEIGHT"] = 2000;
}

$arParams["MAP_NARROW_WIDTH"] = IntVal($arParams["MAP_NARROW_WIDTH"]);
if ($arParams["MAP_NARROW_WIDTH"] < 0)
{
    $arParams["MAP_NARROW_WIDTH"] = 0;
}

$arResult["PARAMS"]["ICONS"] = $obDataMixer->GetIconsSettings("desktop", array(
    "icon_" . $arParams["DATA_TYPE"],
    "icon_direction",
    "cluster"
), true);
?>