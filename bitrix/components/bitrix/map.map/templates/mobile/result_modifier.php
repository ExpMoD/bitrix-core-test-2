<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$obDataMixer = new \Bitrix\InteractiveMap\DataMixer();

$arParams["BAR_HEIGHT"] = IntVal($arParams["BAR_HEIGHT"]);
if (empty($arParams["BAR_HEIGHT"]))
{
    $arParams["BAR_HEIGHT"] = $obDataMixer->GetDefaultBarHeight();
}
if ($arParams["BAR_HEIGHT"] < 0)
{
    $arParams["BAR_HEIGHT"] = 0;
}

$arParams["PLATE_HEIGHT"] = IntVal($arParams["PLATE_HEIGHT"]);
if (empty($arParams["PLATE_HEIGHT"]))
{
    $arParams["PLATE_HEIGHT"] = $obDataMixer->GetDefaultPlateHeight();
}
if ($arParams["PLATE_HEIGHT"] < 0)
{
    $arParams["PLATE_HEIGHT"] = 0;
}

$arParams["DIRECTION_LINK"] = CUtil::JSEscape($arParams["DIRECTION_LINK"]);

$arResult["PARAMS"]["ICONS"] = $obDataMixer->GetIconsSettings("mobile", array(
    "icon_" . $arParams["DATA_TYPE"],
    "cluster"
), true);
?>