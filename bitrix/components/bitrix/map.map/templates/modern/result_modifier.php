<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$obDataMixer = new \Bitrix\InteractiveMap\DataMixer($arParams);

$arParams["MAP_HEIGHT"] = IntVal($arParams["MAP_HEIGHT"]);

if (empty($arParams["MAP_HEIGHT"]))
{
    $arParams["MAP_HEIGHT"] = $obDataMixer->GetDefaultMapHeight();
}

if ($arParams["MAP_HEIGHT"] < 270)
{
    $arParams["MAP_HEIGHT"] = 270;
}
if ($arParams["MAP_HEIGHT"] > 2000)
{
    $arParams["MAP_HEIGHT"] = 2000;
}

$arParams["MAP_NARROW_WIDTH"] = IntVal($arParams["MAP_NARROW_WIDTH"]);
if ($arParams["MAP_NARROW_WIDTH"] < 0)
{
    $arParams["MAP_NARROW_WIDTH"] = 0;
}

$arResult["PARAMS"]["ICONS"] = $obDataMixer->GetIconsSettings("desktop", array(
    "icon_" . $arParams["DATA_TYPE"],
    "icon_direction",
    "cluster"
), true);

if (!empty($arParams["AJAX_PATH"]))
{
    $query[] = "desktop=Y";
    $query[] = "version=3";

    $uri = parse_url($arParams["AJAX_PATH"]);
    $arParams["AJAX_PATH"] = $arParams["AJAX_PATH"] . (empty($uri["query"]) ? "?" : "&") . implode("&", $query);

}

if (empty($arParams["OVERLAY_TYPE"]))
{
    if ($arParams["FULLSCREEN_SHOW"] == "Y")
    {
        $arParams["OVERLAY_TYPE"][] = "show";
    }
    if ($arParams["FULLSCREEN_SLIDE"] == "Y" && $arParams["NO_CATS"] != "Y")
    {
        $arParams["OVERLAY_TYPE"][] = "slide";
    }

    if (count($arParams["OVERLAY_TYPE"]) > 0)
    {
        $arParams["OVERLAY_TYPE"] = implode(" ", $arParams["OVERLAY_TYPE"]);
    }
    else
    {
        $arParams["OVERLAY_TYPE"] = '';
    }
}

$arParams["OVERLAY_TYPE"] = Bitrix\Main\Web\Json::encode(trim($arParams["OVERLAY_TYPE"]));

$arResult["JSON_FIELDS"] = !empty($arResult["JSON_FIELDS"]) ? Bitrix\Main\Web\Json::encode($arResult["JSON_FIELDS"], JSON_FORCE_OBJECT) : '';
