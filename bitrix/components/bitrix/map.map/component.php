<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
global $DB;
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
if(strlen($arParams["IBLOCK_TYPE"])<=0)
    $arParams["IBLOCK_TYPE"] = "news";
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);

if ($arParams["MAP_TYPE"] != "yandex")
{
    $arParams["MAP_TYPE"] = "google";
}
if ($arParams["DATA_TYPE"] != "events" && $arParams["DATA_TYPE"] != "routes" && $arParams["DATA_TYPE"] != "direction")
{
    $arParams["DATA_TYPE"] = "objects";
}
if (substr($arParams["PARENT_SECTION"], 0, 1) == "s" && $arParams["DATA_TYPE"] != "routes")
{
    $arParams["PARENT_SECTION"] = substr($arParams["PARENT_SECTION"], 1);
}
$arParams["PARENT_SECTION"] = intval($arParams["PARENT_SECTION"]);

$arParams["SORT_BY1"] = trim($arParams["SORT_BY1"]);
if(strlen($arParams["SORT_BY1"])<=0)
    $arParams["SORT_BY1"] = "ACTIVE_FROM";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"]))
    $arParams["SORT_ORDER1"]="DESC";

if(strlen($arParams["SORT_BY2"])<=0)
    $arParams["SORT_BY2"] = "SORT";
if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
    $arParams["SORT_ORDER2"]="ASC";

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

$arParams["CHECK_DATES"] = $arParams["CHECK_DATES"]!="N";

$arParams["ELEMENTS_COUNT"] = intval($arParams["ELEMENTS_COUNT"]);
$arNavParams = false;


$arParams["SHOW_ELEMENTS_COUNT"] = $arParams["SHOW_ELEMENTS_COUNT"]!="N";

$arParams["NO_CATS"] = $arParams["NO_CATS"] == "Y" && $arParams["DATA_TYPE"] != "routes" ? "Y" : "N";

$arParams["LOAD_ITEMS"] = empty($arParams["LOAD_ITEMS"]) || $arParams["LOAD_ITEMS"] == "Y" || $arParams["NO_CATS"] == "Y";

if(!is_array($arParams["FIELD_CODE"]))
    $arParams["FIELD_CODE"] = array();
foreach($arParams["FIELD_CODE"] as $key=>$val)
    if(!$val)
        unset($arParams["FIELD_CODE"][$key]);

if(!is_array($arParams["PROPERTY_CODE"]))
    $arParams["PROPERTY_CODE"] = array();
foreach($arParams["PROPERTY_CODE"] as $key=>$val)
    if($val==="")
        unset($arParams["PROPERTY_CODE"][$key]);

$arParams["DETAIL_URL"]=trim($arParams["DETAIL_URL"]);


$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
    $arParams["CACHE_TIME"] = 0;

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["ADD_SECTIONS_CHAIN"] = $arParams["ADD_SECTIONS_CHAIN"]!="N"; //Turn on by default
$arParams["INCLUDE_IBLOCK_INTO_CHAIN"] = $arParams["INCLUDE_IBLOCK_INTO_CHAIN"]!="N";
$arParams["ACTIVE_DATE_FORMAT"] = trim($arParams["ACTIVE_DATE_FORMAT"]);
if(strlen($arParams["ACTIVE_DATE_FORMAT"])<=0)
    $arParams["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));
$arParams["PREVIEW_TRUNCATE_LEN"] = intval($arParams["PREVIEW_TRUNCATE_LEN"]);
$arParams["HIDE_LINK_WHEN_NO_DETAIL"] = $arParams["HIDE_LINK_WHEN_NO_DETAIL"]=="Y";

$arParams["USE_PERMISSIONS"] = $arParams["USE_PERMISSIONS"]=="Y";
if(!is_array($arParams["GROUP_PERMISSIONS"]))
    $arParams["GROUP_PERMISSIONS"] = array(1);


$arAddQuery = array();
if (!empty($arParams["QUERY_SECTION"])) {
    $arCat = explode(',', $arParams["QUERY_SECTION"]);
    $arCatsId = array();
    foreach ($arCat as $id) {
        $arCatsId[] = "s" . $id;
    }
    $arAddQuery[] = "cat=" . implode(",", $arCatsId);
}
if (!empty($arParams["QUERY_OBJECTS"])) {
    $arAddQuery[] = "item=e" . $arParams["QUERY_OBJECTS"];
}
$arParams["QUERY"] = count($arAddQuery) > 0 ? implode("&", $arAddQuery) : '';
$arParams["CUSTOM_VIEW"] = !empty($arParams["CUSTOM_VIEW"]) && $arParams["CUSTOM_VIEW"] == "Y" && $arParams["FULLSCREEN_SLIDE"] == "Y";
$arParams["OLD_DATA_MODE"] = $arParams["OLD_DATA_MODE"] == "Y";

$arParams["QID"] = md5(serialize($arParams));
if (!empty($arParams["AJAX_PATH"])) {
    $uri = parse_url($arParams["AJAX_PATH"]);
    $arParams["AJAX_PATH"] = $arParams["AJAX_PATH"] . (empty($uri["query"]) ? "?" : "&") . bitrix_sessid_get() . "&QID=".$arParams["QID"];//$arParams["IBLOCK_ID"];
}

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
    $arUserGroupArray = $USER->GetUserGroupArray();
    foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
    {
        if(in_array($PERM, $arUserGroupArray))
        {
            $bUSER_HAVE_ACCESS = true;
            break;
        }
    }
}

$arSessionParams = array();


if($this->StartResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arrFilter)))
{
    if(!\Bitrix\Main\Loader::includeModule("bitrix.map"))
    {
        $this->AbortResultCache();
        ShowError(Loc::getMessage("MAP_MODULE_NOT_INSTALLED"));
        return;
    }
    elseif(!\Bitrix\Main\Loader::includeModule("iblock"))
    {
        $this->AbortResultCache();
        ShowError(Loc::getMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }
    if(is_numeric($arParams["IBLOCK_ID"]))
    {
        $rsIBlock = CIBlock::GetList(array(), array(
                                                   "ACTIVE" => "Y",
                                                   "ID" => $arParams["IBLOCK_ID"],
                                              ));
    }
    else
    {
        $rsIBlock = CIBlock::GetList(array(), array(
                                                   "ACTIVE" => "Y",
                                                   "CODE" => $arParams["IBLOCK_ID"],
                                                   "SITE_ID" => SITE_ID,
                                              ));
    }
    if($arResult = $rsIBlock->GetNext())
    {
        $arInitParams = array(
            "IBLOCK_ID"=>$arResult["ID"],
            "NO_CATS"=>$arParams["NO_CATS"],
            "DATA_TYPE"=>$arParams["DATA_TYPE"],
            "SHOW_ELEMENTS_COUNT"=>$arParams["SHOW_ELEMENTS_COUNT"],
            "PARENT_SECTION"=>$arParams["PARENT_SECTION"],
            "ICONPOS_PROP_CODE"=>$arParams["ICONPOS_PROP_CODE"],
            "ROUTETYPE_PROP_CODE"=>$arParams["ROUTETYPE_PROP_CODE"],
            "CLOSED_PROP_CODE"=>$arParams["CLOSED_PROP_CODE"],
            "LOAD_ITEMS"=>$arParams["LOAD_ITEMS"],
            "OLD_DATA_MODE"=>$arParams["OLD_DATA_MODE"],
            "PREVIEW_TRUNCATE_LEN"=>$arParams["PREVIEW_TRUNCATE_LEN"],
            "ACTIVE_DATE_FORMAT"=>$arParams["ACTIVE_DATE_FORMAT"]
        );
        $obDataMixer = new \Bitrix\InteractiveMap\DataMixer($arInitParams);
        $arSessionParams["INIT_PARAMS"] = $arInitParams;
        $arSessionParams["DETAIL_URL"] = $arParams["DETAIL_URL"];
        $arSessionParams["ELEMENTS_NAV_PARAMS"] = $arNavParams;

        $arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;
        //SELECT
        $arSelect = array_merge($arParams["FIELD_CODE"], array(
                                                              "ID",
                                                              "IBLOCK_ID",
                                                              "IBLOCK_SECTION_ID",
                                                              "NAME",
                                                              "DETAIL_PAGE_URL",
                                                              "PREVIEW_TEXT",
                                                              "PREVIEW_TEXT_TYPE",
                                                              "PREVIEW_PICTURE",
                                                              "ACTIVE_FROM",
                                                              "ACTIVE_TO"
                                                         ));

        //WHERE
        $arFilter = array (
            "IBLOCK_ID" => $arResult["ID"],
            "IBLOCK_LID" => SITE_ID,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "Y",
        );
        $arSectionFilter = $arFilter;

        if($arParams["CHECK_DATES"])
            $arFilter["ACTIVE_DATE"] = "Y";

        $arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
                                                      $arParams["PARENT_SECTION"],
                                                          $arParams["PARENT_SECTION_CODE"],
                                                          array(
                                                               "GLOBAL_ACTIVE" => "Y",
                                                               "IBLOCK_ID" => $arResult["ID"],
                                                          )
        );

        if($arParams["PARENT_SECTION"]>0)
        {
            $arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
            $arFilter["INCLUDE_SUBSECTIONS"] = "Y";
//            $arSectionFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];

            $arResult["SECTION"]= array("PATH" => array());
            $rsPath = CIBlockSection::GetNavChain($arResult["ID"], $arParams["PARENT_SECTION"]);
            $rsPath->SetUrlTemplates("", $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
            while($arPath=$rsPath->GetNext())
            {
                $arResult["SECTION"]["PATH"][] = $arPath;
            }
        }
        else
        {
            $arResult["SECTION"]= false;
        }
        //ORDER BY
        $arSort = array(
            $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
            $arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
        );
        $arSectionsSort = array(
            "depth_level"=>"asc",
            $arParams["SORT_SECTIONS_BY1"]=>$arParams["SORT_SECTIONS_ORDER1"],
            $arParams["SORT_SECTIONS_BY2"]=>$arParams["SORT_SECTIONS_ORDER2"],
        );

        if (!empty($arParams["PARENT_PROP_CODE"]))
            $arSort["PROPERTY_" . $arParams["PARENT_PROP_CODE"]] = "ASC";
        if(!array_key_exists("ID", $arSort))
            $arSort["ID"] = "DESC";
        if(!array_key_exists("ID", $arSectionsSort))
            $arSectionsSort["ID"] = "DESC";

        $arSessionParams["SECTION_SORT"] = $arSectionsSort;
        $arSessionParams["SORT"] = $arSort;

        $arSectionSelect = array();
        if(array_key_exists("SECTION_FIELDS", $arParams) && !empty($arParams["SECTION_FIELDS"]) && is_array($arParams["SECTION_FIELDS"]))
        {
            foreach($arParams["SECTION_FIELDS"] as &$field)
            {
                if (!empty($field) && is_string($field))
                    $arSectionSelect[] = $field;
            }
            if (isset($field))
                unset($field);
        }

        if(!empty($arSectionSelect))
        {
            $arSectionSelect[] = "ID";
            $arSectionSelect[] = "NAME";
            $arSectionSelect[] = "LEFT_MARGIN";
            $arSectionSelect[] = "RIGHT_MARGIN";
            $arSectionSelect[] = "DEPTH_LEVEL";
            $arSectionSelect[] = "IBLOCK_ID";
            $arSectionSelect[] = "IBLOCK_SECTION_ID";
            $arSectionSelect[] = "LIST_PAGE_URL";
            $arSectionSelect[] = "SECTION_PAGE_URL";
        }

        $arSectionSelect[] = "UF_*";

        $arResult["ITEMS"] = array();
        $arResult["ELEMENTS"] = array();
        $arResult["SECTIONS"] = array();

        $arSessionParams["SECTION_FILTER"] = array_merge($arSectionFilter, $arrFilter);
        unset($arSectionFilter);
        $arSessionParams["SECTION_SELECT"] = $arSectionSelect;
        unset($arSectionSelect);

        $arResult["SECTIONS"] = $obDataMixer->getSections($arSessionParams["SECTION_SORT"], $arSessionParams["SECTION_FILTER"], $arSessionParams["SECTION_SELECT"]);
        
        //$arFilter['!PROPERTY_'.$arParams['LATITUDE_PROP_CODE']] = false;
        //$arFilter['!PROPERTY_'.$arParams['LONGITUDE_PROP_CODE']] = false;
        $arFilter[] = array(
            "LOGIC" => "OR",
            array("SECTION_ACTIVE" => "Y"),
            array("SECTION_ID" => false),
        );

        $elementsFilter = array_merge($arFilter, $arrFilter);
        $arSessionParams["ELEMENTS_FILTER"] = $elementsFilter;
        unset($elementsFilter);
        $arSessionParams["ELEMENTS_SELECT"] = $arSelect;
        unset($arSelect);

        if ($arParams["LOAD_ITEMS"])
        {
            $arResult["ELEMENTS"] = $obDataMixer->getElements(array(
                "sort"   => $arSessionParams["SORT"],
                "filter" => $arSessionParams["ELEMENTS_FILTER"],
                "nav"    => $arSessionParams["ELEMENTS_NAV_PARAMS"],
                "select" => $arSessionParams["ELEMENTS_SELECT"],
                "params" => array(
                    "DETAIL_URL" => $arSessionParams["DETAIL_URL"],
                    "IBLOCK_URL" => $arSessionParams["IBLOCK_URL"],
                ),
            ));
        }
        else
        {
            $arResult["PARAMS"]["BOUNDS"] = $obDataMixer->getMapBounds($arParams);
        }
        $arSessionParams["PROP_CODES"] = $obDataMixer->getSettingsPropCodes($arParams);
        $obDataMixer->setSystemFields($arSessionParams["PROP_CODES"]);
        $arResult["JSON_FIELDS"] = $obDataMixer->getJsonFields($arParams["PROPERTY_CODE"]);
        $obDataMixer->PrepareJsonData($arResult);
        
        $arResult["INTERFACE"] = $obDataMixer->GetInterfaceStrings(true);

        $arResult["PARAMS"]["LOAD_TIME"] = IntVal(\Bitrix\Main\Config\Option::get('bitrix.map', 'load_time'));
        $arResult["PARAMS"]["RESPONSE_TIME"] = IntVal(\Bitrix\Main\Config\Option::get('bitrix.map', 'response_time'));

        $arResult["PARAMS"]["VERTICAL_TIME"] = IntVal(\Bitrix\Main\Config\Option::get('bitrix.map', 'item_time'));
        $arResult["PARAMS"]["HORIZONTAL_TIME"] = IntVal(\Bitrix\Main\Config\Option::get('bitrix.map', 'list_time'));
        $arResult["PARAMS"]["ANIMATION_TIME"] = IntVal(\Bitrix\Main\Config\Option::get('bitrix.map', 'animation_time'));

        $arResult["PARAMS"]["ROUTE_TYPES"] = $obDataMixer->GetRouteTypes($arParams["MAP_TYPE"], true);
        $arResult["PARAMS"]["DEFAULT_PATHS"] = $obDataMixer->GetDefaultPaths(true);

        
        
        $arResult["SESSION_PARAMS"] = array_merge($arParams, $arSessionParams);
        $this->SetResultCacheKeys(array(
            "ID",
            "IBLOCK_TYPE_ID",
            "LIST_PAGE_URL",
            "NAV_CACHED_DATA",
            "NAME",
            "SECTION",
            "ELEMENTS",
            "SESSION_PARAMS",
            "JSON_FIELDS"
        ));
        $this->IncludeComponentTemplate();
    }
    else
    {
        $this->AbortResultCache();
        ShowError(GetMessage("T_NEWS_NEWS_NA"));
        @define("ERROR_404", "Y");
        if($arParams["SET_STATUS_404"]==="Y")
            CHTTP::SetStatus("404 Not Found");
    }
}

$_SESSION["MAP_SESSION_PARAMS"][$arParams["QID"]] = isset($arResult["SESSION_PARAMS"]) && is_array($arResult["SESSION_PARAMS"]) ? $arResult["SESSION_PARAMS"] : array();
if(isset($arResult["ID"]))
{
    $arTitleOptions = null;
    if($USER->IsAuthorized())
    {
        if(
            $APPLICATION->GetShowIncludeAreas()
            || (is_object($GLOBALS["INTRANET_TOOLBAR"]) && $arParams["INTRANET_TOOLBAR"]!=="N")
            || $arParams["SET_TITLE"]
        )
        {
            if(CModule::IncludeModule("iblock"))
            {
                $arButtons = CIBlock::GetPanelButtons(
                                    $arResult["ID"],
                                        0,
                                        $arParams["PARENT_SECTION"],
                                        array("SECTION_BUTTONS"=>false)
                );

                if($APPLICATION->GetShowIncludeAreas())
                    $this->AddIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));

                if(
                    is_array($arButtons["intranet"])
                    && is_object($INTRANET_TOOLBAR)
                    && $arParams["INTRANET_TOOLBAR"]!=="N"
                )
                {
                    $APPLICATION->AddHeadScript('/bitrix/js/main/utils.js');
                    foreach($arButtons["intranet"] as $arButton)
                        $INTRANET_TOOLBAR->AddButton($arButton);
                }

                if($arParams["SET_TITLE"])
                {
                    $arTitleOptions = array(
                        'ADMIN_EDIT_LINK' => $arButtons["submenu"]["edit_iblock"]["ACTION"],
                        'PUBLIC_EDIT_LINK' => "",
                        'COMPONENT_NAME' => $this->GetName(),
                    );
                }
            }
        }
    }

    $this->SetTemplateCachedData($arResult["NAV_CACHED_DATA"]);

    if($arParams["SET_TITLE"])
    {
        $APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
    }

    if($arParams["INCLUDE_IBLOCK_INTO_CHAIN"] && isset($arResult["NAME"]))
    {
        if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
            $APPLICATION->AddChainItem(
                        $arResult["NAME"]
                            ,strlen($arParams["IBLOCK_URL"]) > 0? $arParams["IBLOCK_URL"]: $arResult["LIST_PAGE_URL"]
            );
        else
            $APPLICATION->AddChainItem($arResult["NAME"]);
    }

    if($arParams["ADD_SECTIONS_CHAIN"] && is_array($arResult["SECTION"]))
    {
        foreach($arResult["SECTION"]["PATH"] as $arPath)
        {
            $APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
        }
    }

    return $arResult["ELEMENTS"];
}

?>