<?
define("NO_KEEP_STATISTIC", true);
define("BX_STATISTIC_BUFFER_USED", false);
define("NO_LANG_FILES", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_PUBLIC_TOOLS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!\Bitrix\Main\Loader::includeModule("bitrix.map") || !\Bitrix\Main\Loader::includeModule("iblock")) {
    return;
}

if (check_bitrix_sessid()) {
    $version = !empty($_GET["version"]) ? floatval($_GET["version"]) : 1;
    $QID  = !empty($_GET["QID"]) ? htmlspecialcharsbx($_GET["QID"]) : null;
    if (!$QID || empty($_SESSION["MAP_SESSION_PARAMS"][$QID]) || !is_array($_SESSION["MAP_SESSION_PARAMS"][$QID])) {
        die();
    }
    $arSessionParams = $_SESSION["MAP_SESSION_PARAMS"][$QID];
    $obDataMixer = new \Bitrix\InteractiveMap\DataMixer($arSessionParams["INIT_PARAMS"]);
    $obDataMixer->setSystemFields($arSessionParams["PROP_CODES"]);
    if (!empty($_GET["item"]))
    {
        $result = array("items"=>array());
        $arItems = explode(',', $_GET["item"]);
        foreach ($arItems as $item) {
            $arItemsId[] = (int)substr($item, -(strlen($item) - 1));
        }
        $arFilter = array("ID" => $arItemsId);

        $elementsFilter = array_merge($arSessionParams["ELEMENTS_FILTER"], $arFilter);
        $arElements = $obDataMixer->getElements(array(
            "sort"   => $arSessionParams["SORT"],
            "filter" => $elementsFilter,
            "nav"    => $arSessionParams["ELEMENTS_NAV_PARAMS"],
            "select" => $arSessionParams["ELEMENTS_SELECT"],
            "params" => array(
                "DETAIL_URL" => $arSessionParams["DETAIL_URL"],
                "IBLOCK_URL" => $arSessionParams["IBLOCK_URL"],
            ),
        ));

        switch ($_GET["type"])
        {
            case "routes":
                $result["items"] = $obDataMixer->getElementsToRoutes($arSections, $arElements);
                break;
            case "events":
                $result["items"] = $obDataMixer->getElementsToEvents($arElements);
                break;
            case "objects":
                $result["items"] = $obDataMixer->getElementsToObjects($arElements);
        }
        if (count($result["items"]) > 0) {
            header('Content-Type: application/json');
            if ($version < 3) {
                $result = !empty($result["items"]["description"]) ? $result["items"]["description"] : "";
            }
            echo Bitrix\Main\Web\Json::encode($result);
        }
    }
    else if (!empty($_GET["cat"]))
    {
        $result = array("cats"=>array(), "items"=>array());
        $arCat = explode(',', $_GET["cat"]);
        $emptyCats = !empty($_GET["empty"]) ? true : false;

        switch ($_GET["type"]) {
            case "routes":
                $arCatsId = $obDataMixer->getSectionRequest($arCat);
                if (count($arCatsId) > 0) {
                    $arSectionFilter = array_merge($arSessionParams["SECTION_FILTER"], array("UF_ROUTE_TYPE"=>$arCatsId));
                    $arSections = $obDataMixer->getSections($arSessionParams["SECTION_SORT"], $arSectionFilter, $arSessionParams["SECTION_SELECT"]);
                    $result["cats"] = $obDataMixer->getSectionsToRoutes();
                    if (!$emptyCats && count($arSections) > 0) {
                        $arFilter = array("SECTION_ID" => array_keys($arSections));
                        $elementsFilter = array_merge($arSessionParams["ELEMENTS_FILTER"], $arFilter);
                        $arElements = $obDataMixer->getElements(array(
                            "sort"   => $arSessionParams["SORT"],
                            "filter" => $elementsFilter,
                            "nav"    => $arSessionParams["ELEMENTS_NAV_PARAMS"],
                            "select" => $arSessionParams["ELEMENTS_SELECT"],
                            "params" => array(
                                "DETAIL_URL" => $arSessionParams["DETAIL_URL"],
                                "IBLOCK_URL" => $arSessionParams["IBLOCK_URL"],
                            )
                        ));
                        foreach ($arElements as $el) {
                            $str .= $el["ID"] ." ".$el["NAME"]."\n";
                        }
                        $result["items"] = $obDataMixer->getElementsToRoutes($arSections, $arElements);
                    }
                }
                break;
            case "events":
                $arSections = $obDataMixer->getSectionsToEvents();
                $arCatsId = array();
                if (count($arCat) > 0) {
                    foreach ($arCat as $cat) {
                        if (isset($arSections[$cat])) {
                            $result["cats"][$cat] = $arSections[$cat];
                        }
                    }
                    if (!$emptyCats && count($result["cats"]) > 0) {
                        $arElements = $obDataMixer->getElements(array(
                            "sort"   => $arSessionParams["SORT"],
                            "filter" => $arSessionParams["ELEMENTS_FILTER"],
                            "nav"    => $arSessionParams["ELEMENTS_NAV_PARAMS"],
                            "select" => $arSessionParams["ELEMENTS_SELECT"],
                            "params" => array(
                                "DETAIL_URL" => $arSessionParams["DETAIL_URL"],
                                "IBLOCK_URL" => $arSessionParams["IBLOCK_URL"],
                            ),
                        ));
                        $result["items"] = $obDataMixer->getElementsToEvents($arElements);
                    }
                }
                break;
            case "objects":
                $arCatsId = $obDataMixer->getSectionRequest($arCat);
                if (count($arCatsId) > 0) {
                    $arSections = $obDataMixer->getSections($arSessionParams["SECTION_SORT"], array("ID"=>$arCatsId, "ACTIVE"=>"Y"), $arSessionParams["SECTION_SELECT"]);
                    $result["cats"] = $obDataMixer->getSectionsToObjects($arSections);

                    if (!$emptyCats && count($arSections) > 0) {
                        
                        $arFilter = array("SECTION_ID" => array_keys($arSections));
                        $elementsFilter = array_merge($arSessionParams["ELEMENTS_FILTER"], $arFilter);
                        $arElements = $obDataMixer->getElements(array(
                            "sort"   => $arSessionParams["SORT"],
                            "filter" => $elementsFilter,
                            "nav"    => $arSessionParams["ELEMENTS_NAV_PARAMS"],
                            "select" => $arSessionParams["ELEMENTS_SELECT"],
                            "params" => array(
                                "DETAIL_URL" => $arSessionParams["DETAIL_URL"],
                                "IBLOCK_URL" => $arSessionParams["IBLOCK_URL"],
                            ),
                        ));
                        $result["items"] = $obDataMixer->getElementsToObjects($arElements);
                    }
                }
        }
         if (!$emptyCats) {
            $obDataMixer->setElementsCount($result["cats"], $result["items"]);
            $obDataMixer->delEmptySections($result["cats"]);
        }
        
        if (count($result["items"]) == 0) {
            //workaround: frontend ������ ������� ������
            $result["items"] = new stdClass();
        }
        if (count($result["cats"]) > 0) {
            header('Content-Type: application/json');
            if ($version < 3) {
                $result = $result["items"];
            }
            echo Bitrix\Main\Web\Json::encode($result);
        }
    }
}
?>
