<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (Bitrix\Main\Loader::includeModule("bitrix.map"))
{
    $APPLICATION->AddHeadString("<script src=\"/bitrix/js/bitrix.map/common_v3.js\" charset=\"utf-8\"></script>");
    $APPLICATION->SetAdditionalCss('/bitrix/components/bitrix/map.map/templates/mobile_modern/style.css');
}
?>