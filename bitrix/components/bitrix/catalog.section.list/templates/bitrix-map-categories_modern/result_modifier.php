<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (Bitrix\Main\Loader::includeModule("bitrix.map"))
{
    if ($arParams["DATA_TYPE"] == "routes" || $arParams["DATA_TYPE"] == "events") {
        $arParams["LOAD_ITEMS"] = true;
        $obDataMixer = new \Bitrix\InteractiveMap\DataMixer($arParams);
        $obDataMixer->setSystemFields($obDataMixer->getSettingsPropCodes($arParams));
        $arResult["JSON_FIELDS"] = $obDataMixer->getJsonFields($arParams["PROPERTY_CODE"]);
        $arResult["JSON_FIELDS"] = !empty($arResult["JSON_FIELDS"]) ? Bitrix\Main\Web\Json::encode($arResult["JSON_FIELDS"], JSON_FORCE_OBJECT) : '';
        $arSort = array(
            $arParams["SORT_BY1"]=>$arParams["SORT_ORDER1"],
            $arParams["SORT_BY2"]=>$arParams["SORT_ORDER2"],
        );
        $arResult["ELEMENTS"] = $obDataMixer->getElements(array(
            "sort" => $arSort,
            "filter" => array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ACTIVE"    => "Y"
            ),
            "select" => $arSelect,
            "params" => array(
                "DETAIL_URL" => $arParams["DETAIL_URL"],
                "IBLOCK_URL" => $arParams["IBLOCK_URL"],
            ),
        ));
        $obDataMixer->prepareJsonData($arResult);
    } else {
        $arParams["LOAD_ITEMS"] = false;
        $obDataMixer = new \Bitrix\InteractiveMap\DataMixer($arParams);
        $obDataMixer->PrepareSectionJsonData($arResult, $arParams);
    }

    $arParams["BAR_HEIGHT"] = IntVal($arParams["BAR_HEIGHT"]);
    if (empty($arParams["BAR_HEIGHT"]))
    {
        $arParams["BAR_HEIGHT"] = $obDataMixer->GetDefaultBarHeight();
    }
    if ($arParams["BAR_HEIGHT"] < 0)
    {
        $arParams["BAR_HEIGHT"] = 0;
    }

    $arResult["PARAMS"]["ICONS"] = $obDataMixer->GetIconsSettings("mobile", array(
        "icon_category",
    ), true);
}
else
{
    ShowError(GetMessage("MAP_MODULE_NOT_INSTALLED"));
    return;
}
?>