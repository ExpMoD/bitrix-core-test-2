<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (Bitrix\Main\Loader::includeModule("bitrix.map"))
{

    $obDataMixer = new \Bitrix\InteractiveMap\DataMixer();
    $obDataMixer->PrepareSectionJsonData($arResult, $arParams);

    $arParams["BAR_HEIGHT"] = IntVal($arParams["BAR_HEIGHT"]);
    if (empty($arParams["BAR_HEIGHT"]))
    {
        $arParams["BAR_HEIGHT"] = $obDataMixer->GetDefaultBarHeight();
    }
    if ($arParams["BAR_HEIGHT"] < 0)
    {
        $arParams["BAR_HEIGHT"] = 0;
    }

    $arResult["PARAMS"]["ICONS"] = $obDataMixer->GetIconsSettings("mobile", array(
        "icon_category",
    ), true);
}
else
{
    ShowError(GetMessage("MAP_MODULE_NOT_INSTALLED"));
    return;
}
?>