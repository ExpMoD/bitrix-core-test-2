<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
    $GeoMapp.init({
        defaultPath: {
            libs: '/bitrix/js/bitrix.map/',
            images: '/bitrix/components/bitrix/map.map/templates/mobile/images/'
        },
        pageType: 'category',
        barHeight: <?=(int)$arParams["BAR_HEIGHT"]?>,
        catLink: '<?=CUtil::JSEscape($arParams["SECTION_URL"])?>',
        <? if ($arResult["PARAMS"]["ICONS"]) { ?>
            icon: <?=$arResult["PARAMS"]["ICONS"]["icon"]?>,
        <? } ?>
        cats: <?=$arResult["JSON_SECTIONS"]?>
    });
</script>

<div id="bxMapContainer" class="map-wrapper"></div>