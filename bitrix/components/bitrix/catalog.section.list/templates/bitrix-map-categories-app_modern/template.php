<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
    $GeoMapp.init({
        device: {
            mobile: 'mobile_map_v3.js'
        },
        defaultPath: {
            libs: '/bitrix/js/bitrix.map/',
            images: '/bitrix/components/bitrix/map.map/templates/mobile/images/'
        },
        barHeight: <?=(int)$arParams["BAR_HEIGHT"]?>,
        catLink: '<?=CUtil::JSEscape($arParams["SECTION_URL"])?>',
        <?// if(!empty($arParams["DIRECTION_LINK"])) {?>
        directionLink: '<?=$arParams["DIRECTION_LINK"]?>',
        <?// } ?>
        <? if(!empty($arResult["PARAMS"]["BOUNDS"])) {?>
        mapBounds: <?=$arResult["PARAMS"]["BOUNDS"]?>,
        <? } ?>
        ajax: '<?=$arParams["AJAX_PATH"]?>',
        <? if ($arResult["PARAMS"]["ICONS"]) { ?>
            icon: <?=$arResult["PARAMS"]["ICONS"]["icon"]?>,
        <? } ?>
        <? if(!empty($arResult["JSON_FIELDS"])): ?>
        fields: <?=$arResult["JSON_FIELDS"]?>,
        <? endif; ?>
        cats: <?=$arResult["JSON_SECTIONS"]?>,
        <? if ($arParams["LOAD_ITEMS"]) { ?>
        items: <?=$arResult["JSON_ELEMENTS"]?>,
        <? } ?>
        pageType: '<?=(!empty($arParams["DATA_TYPE"]) && $arParams["DATA_TYPE"] != 'objects' ? $arParams["DATA_TYPE"] : 'category')?>'
    });
</script>

<div id="bxMapContainer" class="bxmap-wrapper"></div>