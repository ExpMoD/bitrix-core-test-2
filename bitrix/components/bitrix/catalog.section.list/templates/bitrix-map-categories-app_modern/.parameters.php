<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

global $USER_FIELD_MANAGER;

$arProperty_UF = array();
$arUserFields = $USER_FIELD_MANAGER->GetUserFields("IBLOCK_".$arCurrentValues["IBLOCK_ID"]."_SECTION");
foreach($arUserFields as $FIELD_NAME=>$arUserField)
{
    $arProperty_UF[$FIELD_NAME] = $arUserField["LIST_COLUMN_LABEL"]? $arUserField["LIST_COLUMN_LABEL"]: $FIELD_NAME;
}

$arTemplateParameters["ICONPOS_PROP_CODE"] = array(
    "NAME" => GetMessage("T_MAP_DESC_ICONPOS_PROP_CODE"),
    "TYPE" => "LIST",
    "DEFAULT" => "UF_ICON_POS",
    "VALUES" => $arProperty_UF,
    "ADDITIONAL_VALUES" => "Y",
);
$arTemplateParameters["BAR_HEIGHT"] = array(
    "NAME" => GetMessage("T_MAP_DESC_BAR_HEIGHT"),
    "TYPE" => "STRING",
    "DEFAULT" => "44",
);
?>
