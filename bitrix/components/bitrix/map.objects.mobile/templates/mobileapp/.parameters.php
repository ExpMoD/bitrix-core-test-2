<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "AJAX_PATH" => array(
        "NAME" => GetMessage("T_MAP_DESC_AJAX_PATH"),
        "TYPE" => "STRING",
        "DEFAULT" => "/bitrix/components/bitrix/map.map/ajax.php",
    ),
    "PLATE_HEIGHT" => array(
        "NAME" => GetMessage("T_MAP_DESC_PLATE_HEIGHT"),
        "TYPE" => "STRING",
        "DEFAULT" => "44",
    ),
    "FEW_OBJECTS_HEIGHT" => array(
        "NAME" => GetMessage("T_MAP_DESC_FEW_OBJECTS_HEIGHT"),
        "TYPE" => "STRING",
        "DEFAULT" => COption::GetOptionInt("bitrix.map", "def_few_objects_height"),
    ),
    "GEOLOC_LATITUDE" => array(
        "NAME" => GetMessage("T_MAP_DESC_GEOLOC_LATITUDE"),
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
    "GEOLOC_LONGITUDE" => array(
        "NAME" => GetMessage("T_MAP_DESC_GEOLOC_LONGITUDE"),
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
);
?>