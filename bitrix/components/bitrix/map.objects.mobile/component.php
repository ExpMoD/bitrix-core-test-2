<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arParams["DATA_TYPE"] != "events" && $arParams["DATA_TYPE"] != "routes" && $arParams["DATA_TYPE"] != "direction")
{
    $arParams["DATA_TYPE"] = "objects";
}

$arDefaultUrlTemplates404 = array(
    "sections" => "",
    "section" => "#SECTION_ID#/",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array(
    "SECTION_ID" => "cat",
    "DIRECTION" => "DIRECTION"
);

$arComponentVariables = array(
    "SECTION_ID",
    "DIRECTION",
);

$arVariables = array();

$arVariableAliases = $arDefaultVariableAliases;//CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams["VARIABLE_ALIASES"]);
CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);

$componentPage = "objects";

if(isset($arVariables["DIRECTION"]) && $arVariables["DIRECTION"] == "Y")
{
    $componentPage = "route";
}
elseif(!isset($arVariables["SECTION_ID"]))
{
    $componentPage = "categories";
}

$arResult = array(
    "FOLDER" => "",
    "URL_TEMPLATES" => Array(
        "category" => htmlspecialcharsbx($APPLICATION->GetCurPage()),
        "direction" => htmlspecialcharsbx($APPLICATION->GetCurPage())."?".$arVariableAliases["DIRECTION"]."=Y",
    ),
    "VARIABLES" => $arVariables,
    "ALIASES" => $arVariableAliases
);


$this->IncludeComponentTemplate($componentPage);
?>